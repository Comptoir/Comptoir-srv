# Operations on Comptoir du Libre

## Backup

### 1. Dump database

```sh
pg_dump -U comptoir -W -f SAVE_comptoir.sql
```

### 2. Copy uploaded files

From the "-srv" project, backup the `webroot/files` directory.

## Export DB Scructure only

View file `bin/COMPTOIR_export_DB_structure_only.sh`

## Export DB data only

View file `bin/COMPTOIR_export_DB_data_AND_images.sh`

## Restore

Follow help from file `bin/COMPTOIR_import_DB_data_AND_images.sh`
