# How to upgrade from v2.0.2 to v2.3.1

## Save `config/comptoir.php`

```shell
cd /home/comptoir/Comptoir-srv && \
    cp config/comptoir.php config/comptoir.php-$(date +%Y-%m-%d_%kh%M)
```

## Grab source code

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv && \
    git fetch -p && \
    git checkout "v2.3.1" --force
```

**NOTE**: `--force` is mandatory in this upgrade because of issues related to switching to Git-LFS

## Update `config/comptoir.php`

1. `cd /home/comptoir/Comptoir-srv/config && cp comptoir.default.php comptoir.php`
1. Re-add customized values from the old `comptoir.php` into the new one (like the Piwik stanza)

## Update DB content

```
UPDATE public.user_types SET name = 'Association', created = '2016-06-10 15:58:07.000000', modified = '2016-06-10 15:58:07.000000', cd = 'Association' WHERE id = 6;
```

## Remove temporary i18n files

```shell
sudo rm -f /home/comptoir/Comptoir-srv/tmp/cache/persistent/*
```