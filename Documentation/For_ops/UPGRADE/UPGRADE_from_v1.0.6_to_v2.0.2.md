# How to upgrade from v1.0.6 to v2.0.2

## Install Git-LFS locally

As `root` user, do:

```shell
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
apt-get install git-lfs
```

See https://packagecloud.io/github/git-lfs/install

## Grab source code

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv && \
    git checkout "v2.0.2-rc.1"
```

## Update Composer

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv && \
    composer update --no-dev
```

## Create `config/comptoir.php`

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/config/
cp comptoir.default.php comptoir.php
```

And:

* update the Piwik stanza

## Move "files/" directory

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/webroot/
mv files img/
```

## Update Apache vhost

... so that the canonical (and now unique) vhost points to `/home/comptoir/Comptoir-srv/webroot`

## Remove temporary i18n files

```shell
sudo rm -f \
    /home/comptoir/Comptoir-srv/tmp/cache/persistent/* \
    /home/comptoir/Comptoir-web/tmp/cache/persistent/*
```