# How to upgrade from v2.4.10 to v2.5.0


## 1) Backup - Export data

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh
```

## 2) Backup - Save `config/comptoir.php`

```shell
cd /home/comptoir/Comptoir-srv && \
    cp config/comptoir.php config/comptoir.php-$(date +%Y-%m-%d_%kh%M)
```

## 3) Grab source code

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv && \
    git fetch -p && \
    git checkout "v2.5.0"
```


## 4) Update vendor/
cd /home/comptoir/Comptoir-srv && \
rm -rvf vendor/ && \
composer install


## 6) Remove temporary i18n files

```shell
sudo rm -f /home/comptoir/Comptoir-srv/tmp/cache/persistent/*
```
