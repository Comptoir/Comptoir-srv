# How to upgrade from v1.0.0 to v1.1.0

## 0. Pre-requisites: Have the suitable database

   * a database `comptoir`
   * a user `comptoir`, owner of the database `comptoir`

   Verify it with:

   ```shell
   psql -U comptoir -W comptoir
   ```

## 1. Execute migration's field

* Create 2 new tables : Tags and Softwares_Tags

```shell
cd /home/comptoir/Comptoir-srv

bin/cake migrations migrate -t 20170113155603
bin/cake migrations migrate -t 20170113162044
```

## 2. Execute migration's field

* Alter tables with Foreign Key 'software_id' to add constraint : on delete cascade

```shell
cd /home/comptoir/Comptoir-srv

bin/cake migrations migrate -t 20170124170516
```
