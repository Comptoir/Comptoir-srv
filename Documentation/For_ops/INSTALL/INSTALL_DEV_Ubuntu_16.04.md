# COMPTOIR-SRV install

<!-- =============================================================================================================== -->
<!-- ===== Prerequisites =========================================================================================== -->
<!-- =============================================================================================================== -->

## PREREQUISITES:

All this part, you need to be as user `root`.

### Ubuntu packages

```shell
apt-get install \
    git \
    apache2 \
    php \
    php-intl \
    libapache2-mod-php \
    postgresql \
    php-pgsql \
    php7.0-sqlite3 \
    php7.0-xml \
    php7.0-mbstring

```

### PHP > timezone + max_upload


```shell
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
done
```

### Install Composer


```shell
php -r "readfile('https://getcomposer.org/installer');" \
    | php -- --install-dir=/usr/local/bin --filename=composer
```

## Prerequisites: configure locale en_US and fr_FR

As `root` user, do:

```shell
dpkg-reconfigure locales
```

...and select the following locales:

* `fr_FR.UTF8`
* `en_US.UTF8`

To avoid error messages about local variable: 

```shell
sudo vi /etc/environment
```

Add a new line:
```
LC_ALL="en_US.UTF-8"
```

### Postgres authentication

In `/etc/postgresql/9.5/main/pg_hba.conf`, replace

```
       local   all             all                                     peer
```

by

```
       local   all             all                                     password
```

and restart Postgres

```shell
sudo service postgresql restart
```

### Create system user


```shell
useradd -d /home/comptoir/ -m -s /bin/bash comptoir
```

<!-- =============================================================================================================== -->
<!-- ===== Installation ============================================================================================ -->
<!-- =============================================================================================================== -->

## Installation Comptoir

Let say:

* we are user `superdupont`,
* we want to have our Comptoir project files into `/home/superdupont/Project/Comptoir-srv/`,
* we can do `git` command as authenticated user (i.e. ssh key is placed on the Gitlab server)

As user `superdupont`, do :

```shell
cd /home/superdupont/Project/
git clone git@gitlab.adullact.net:Comptoir/Comptoir-srv.git
```

Now configure code for `comptoir` user:

```shell
sudo su - comptoir
cd /home/comptoir
ln -s /home/superdupont/Project/Comptoir-srv Comptoir-srv
exit
```

## POSTGRESQL Create user

As user `root` do :

```shell
sudo -u postgres psql
```

Adjust password to your needs:

```postgresql
CREATE USER comptoir WITH PASSWORD 'comptoir';
\q
```

## POSTGRESQL Set .pgpass file

Create the `.pgpass` file for user `comptoir`. As user `comptoir`, do:

```shell
PGPASSFILE=/home/comptoir/.pgpass
touch "${PGPASSFILE}"
chmod 0600 "${PGPASSFILE}"
cat >"${PGPASSFILE}" <<EOF
# hostname:port:database:username:password
localhost:5432:comptoir:comptoir:my-sql-password-for-user-comptoir-TO-BE-CHANGED
EOF
```

Don't forget to adjust password (used for the SQL user `comptoir`).

## POSTGRESQL Create DB + DB for tests and set ownership

As user `root` do :

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_database_and_set_ownership.sh -d /home/comptoir/Comptoir-srv -t
```

Test it :

```shell
psql -U comptoir -W comptoir
psql -U comptoir -W comptoir_test
```

## POSTGRESQL Create tables and procedures

As user `comptoir`, do :

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_tables_and_procedures.sh -d /home/comptoir/Comptoir-srv
```


<!-- =============================================================================================================== -->
<!-- ===== Configuration============================================================================================ -->
<!-- =============================================================================================================== -->


## BINARY FILES adjust permissions

Let's assume the current user is "superdupont". We create a "comptoir" group, and add "superdupont" to it.

```shell
sudo groupadd comptoir
sudo usermod -aG comptoir comptoir
sudo usermod -aG comptoir superdupont
```
Test it :

```shell
    grep -E "^comptoir:" /etc/group
```
Comptoir's group must contain 2 users : "comptoir, superdupont".



## Composer install Comptoir

As any user, do :

```shell
cd /home/comptoir/Comptoir-srv/
/usr/local/bin/composer install
```

## APP.PHP Debug mode

As a developer, you should have debug enabled to leverage all kinds of tests.

As `superdupont` user, modify `/home/comptoir/Comptoir-srv/config/app.php`:

```
    'debug' => true,
```

## APP.PHP Define Email Transport

In `/home/comptoir/Comptoir-srv/config/app.php`, section `EmailTransport`, edit the "default" stanza :

```
'EmailTransport' => [
           'default' => [
               'className' => 'Smtp',
               // The following keys are used in SMTP transports
               'host' => 'smtp.adullact.org',
               'port' => 25,
               'timeout' => 30,
               'username' => 'app.comptoirdulibre@adullact.org',
               'password' => 'my-password',
               'client' => null,
               'tls' => true
           ],
       ],
```

Replace `my-password` with suitable value.

## APP.PHP Configure database credentials

Adjust Postgres credentials in `config/app.php`, search for 'Datasources'. Adjust **both** 'default' and 'test' datasources:

```
'Datasources' => [

    'default' => [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Postgres',
                'persistent' => false,
                'host' => 'localhost',
                //'port' => 'non_standard_port_number',
                'username' => 'comptoir',
                'password' => 'comptoir',
                'database' => 'comptoir',
                'encoding' => 'utf8',
                'timezone' => 'Europe/Paris',
                'cacheMetadata' => true,
                'log' => false,
```

and

```
        'test' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Postgres',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'nonstandard_port_number',
            'username' => 'comptoir',
            'password' => 'comptoir',
            'database' => 'comptoir_test',
            'encoding' => 'utf8',
            'timezone' => 'Europe/Paris',
            'cacheMetadata' => true,
            'quoteIdentifiers' => false,
            'log' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        ],
```

See <https://book.cakephp.org/3.0/en/development/testing.html>

## COMPTOIR.PHP Configure

As any user, do :

Copy default parameters of `comptoir.default.php` into `comptoir.php`

```shell
cd /home/comptoir/Comptoir-srv
cp config/comptoir.default.php config/comptoir.php
```

In `comptoir.php`, the array `Categories => PickOfTheMonth` contain the four ids of softwares you want see in the section `Pick of the month` on the home page.
Default ids are

```
[   27, // Authentik
            49, // Maarch Courrier
            9,  // Asqatasun
            23  // OpenADS ]
```

## Set UNIX permissions

As user `root` do :

```shell
cd /home/comptoir
chmod -R o-w Comptoir-srv/*
for i in webroot logs tmp; do
    chown -R www-data.comptoir "Comptoir-srv/${i}"
    find "Comptoir-srv/$i" -type d -exec sudo chmod 775 {} \;
    find "Comptoir-srv/$i" -type f -exec sudo chmod 664 {} \;
done
```

<!-- =============================================================================================================== -->
<!-- ===== Virtual hosts =========================================================================================== -->
<!-- =============================================================================================================== -->

## APACHE Vhost preparation

As user `root`, do :

```
echo "127.0.0.1	comptoir-srv.local" >> /etc/hosts
```

## APACHE Vhost creation "comptoir-srv"

As user `root`, do :

Create the file `/etc/apache2/sites-available/comptoir-srv.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName comptoir-srv.local
    ServerAdmin webmaster@localhost
    DocumentRoot /home/comptoir/Comptoir-srv/webroot/

    ErrorLog ${APACHE_LOG_DIR}/comptoir-srv.local.log
    CustomLog ${APACHE_LOG_DIR}/comptoir-srv.local.log combined

    <Directory "/home/comptoir/Comptoir-srv/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## APACHE enable mod_rewrite and new vhosts

As `root` user, do:

```shell
a2enmod rewrite
a2ensite comptoir-srv.conf
service apache2 reload
```

## TEST

* Comptoir-SRV with: [http://comptoir-srv.local/api/v1/softwares](http://comptoir-srv.local/api/v1/softwares)

## FUNCTIONAL TESTING

* Cf: [Functional tests](../../For_developers/Functional_Tests/README.md)


<!-- =============================================================================================================== -->
<!-- ===== OPTIONAL Content ======================================================================================== -->
<!-- =============================================================================================================== -->

## OPTIONAL Content

### OPTIONAL : POSTGRESQL Insert content + binary files

TODO : add a data set.

Let say the archived content is stored in the two following files:

* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Data_only.sql.bz2`
* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Dir_Files.tar.bz2`

To import content, run as `root` user:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_import_DB_data_AND_images.sh \
    -d /home/comptoir/Comptoir-srv \
    -t 2017-01-10-18h05m44
```

/!\ Caution:

* Files **must** be in `/tmp' folder.
* The `-t` parameter is the **exact** timestamp copied/pasted from the filenames.


### POSTGRESQL set-up for debug-kit

as user `root` postgresql, do :

```postgresql
CREATE DATABASE debug_kit WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';
ALTER DATABASE debug_kit OWNER TO comptoir;
```

### APP.PHP Configure debug-kit database credentials // OPTIONAL si t'a installer la bdd debugkit

In `/home/comptoir/Comptoir-srv/config/app.php`, section 'Datasources' and 'test' : Comment it.  
Then add datasource for debug-kit.

```
        /**
         * Configure logging to debug-kit  
         */
    'debug_kit' => [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Postgres',
                'persistent' => false,
                'host' => 'localhost',
                'username' => 'comptoir',
                'password' => 'comptoir',
                'database' => 'debug_kit',
                'encoding' => 'utf8',
                'timezone' => 'Europe/Paris',
                'cacheMetadata' => true,
                'quoteIdentifiers' => false,
            ],
```

### OPTIONAL : Adjust permissions

We set permissions.

```
sudo chown -R www-data:comptoir /home/comptoir/Comptoir-srv/webroot/img/files
```


### Load Debug kit for Comptoir // Useless? <https://book.cakephp.org/3.0/en/debug-kit.html>

As user `superdupont`, do:

```shell
bin/cake plugin load DebugKit
```
