# Memento PHPCS (Code Sniffer)

## Install it

just do:

```shell
composer install
```

## Comptoir configuration

Our configuration is stored in `.phpcs.xml`.

## Use it: 1) global view

Regarding our configuration, for each sniff: lists number of files failing a given sniff

```shell
# Check files based on .phpcs.xml config
vendor/bin/phpcs
vendor/bin/phpcs --report-full     # each filename with each sniff (error and warning)
vendor/bin/phpcs --report-summary  # each filename with number of errors + warnings
vendor/bin/phpcs --report-source   # each sniff with numbers of errors + warnings
vendor/bin/phpcs --report-code     # shows a code snippet for each error and warning

# Check files and shows all reports
vendor/bin/phpcs --report-code --report-full --report-source --report-summary
```

Output

```
----------------------------------------------------------------------
    SOURCE                                                       COUNT
----------------------------------------------------------------------
[ ] Generic.Files.LineLength.TooLong                             93
[ ] PSR2.Classes.PropertyDeclaration.Underscore                  39
[x] PSR2.Methods.FunctionCallSignature.Indent                    18
[ ] PSR2.Methods.MethodDeclaration.Underscore                    10
[ ] Squiz.Scope.MethodScope.Missing                              2
[x] Generic.Formatting.DisallowMultipleStatements.SameLine       1
[ ] PEAR.Functions.ValidDefaultValue.NotAtEnd                    1
[ ] Squiz.Classes.ValidClassName.NotCamelCaps                    1
----------------------------------------------------------------------
A TOTAL OF 165 SNIFF VIOLATIONS WERE FOUND IN 8 SOURCES
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SOURCES AUTOMATICALLY (19 VIOLATIONS IN TOTAL)
----------------------------------------------------------------------
```

## Use it: 2) study one given sniff

Now you want to study the last sniff (Generic.Files.LineLength.TooLong). This can be done this way:

```shell
./vendor/bin/phpcs --sniffs=Generic.Files.LineLength --report=summary
```

**Note**:

* the sniff name passed as a parameter has been cut, its last token must be removed.
* `Generic.Files.LineLength.TooLong` --> `Generic.Files.LineLength`

Output:

```
------------------------------------------------------------------------------------
FILE                                                                ERRORS  WARNINGS
------------------------------------------------------------------------------------
src/Controller/AppController.php                                    0       1
src/Controller/Api/V1/PagesController.php                           0       2
src/Controller/Api/V1/RelationshipsSoftwaresController.php          0       2
src/Controller/Api/V1/RelationshipsSoftwaresUsersController.php     0       6
src/Controller/Api/V1/ReviewsController.php                         0       2
src/Controller/Api/V1/ScreenshotsController.php                     0       1
src/Controller/Api/V1/SoftwaresController.php                       0       10
src/Controller/Api/V1/UsersController.php                           0       5
src/Controller/Api/V1/UserTypesController.php                       0       1
src/Mailer/CrudMailer.php                                           0       3
src/Model/Table/ReviewsTable.php                                    0       2
src/Model/Table/ScreenshotsTable.php                                0       3
src/Model/Table/SoftwaresTable.php                                  0       12
src/Model/Table/StatisticsAveragesTable.php                         0       1
src/Model/Table/TagsTable.php                                       0       2
src/Model/Table/UsersTable.php                                      0       8
src/View/Helper/ActionHelper.php                                    0       4
src/View/Helper/ListsHelper.php                                     0       6
src/View/Helper/RatingHelper.php                                    0       3
src/View/Helper/ReviewHelper.php                                    0       4
src/View/Helper/ScoreHelper.php                                     0       1
src/View/Helper/ScreenshotHelper.php                                0       2
src/View/Helper/SoftwareHelper.php                                  0       4
src/View/Helper/TagHelper.php                                       0       3
src/View/Helper/UserHelper.php                                      0       5
------------------------------------------------------------------------------------
A TOTAL OF 0 ERRORS AND 93 WARNINGS WERE FOUND IN 25 FILES
------------------------------------------------------------------------------------
```

## Use it: 3) study a given file

Now you want to where are the failing lines in the first file `src/Controller/AppController.php`:

```shell
./vendor/bin/phpcs --sniffs=Generic.Files.LineLength --report=code src/Controller/AppController.php
```

Output:

```
----------------------------------------------------------------------------------------------------------------------------------------------
LINE 177: WARNING Line exceeds 120 characters; contains 131 characters (Generic.Files.LineLength.TooLong)
----------------------------------------------------------------------------------------------------------------------------------------------
   175:  ····{
   176:  ········$selectedLanguage·=·I18n::locale();
>> 177:  ········$lang·=·$this->request->param('language')·?·$this->request->param('language')·:·preg_replace('/_\w*/',·"",·I18n::locale());
   178:
   179:  ········if·($lang·&&·isset($this->availableLanguages[$lang]))·{
----------------------------------------------------------------------------------------------------------------------------------------------

```

## Automatically fix errors (that can be)

PHPCBF can fixe automatically some of errors

```shell
./vendor/bin/phpcbf my-file
```

## CI

This command line is executed in the CI to report a summary of errors found in the files commited.

```shell
${COMPTOIR_SRV_DIR}/vendor/bin/phpcs -n -s --report=summary ${COMMIT_FILES}
```



## More documentation

[PHPCS documentation](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Reporting)


