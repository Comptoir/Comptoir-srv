# Memento Composer

## Composer files (.json / .lock)

- The composer.json and composer.lock files **must** be committed at the same time.
- A pre-commit hook (`GrumPHP`) and the CI checks it too.

Manually you can [check it](https://getcomposer.org/doc/03-cli.md#validate) by running the following command line:

```bash
composer validate
    # ---> check if your composer.json is valid
    # ---> check if composer.lock exists and is up to date.
```

## Download packages in parallel

- [hirak/prestissimo](https://packagist.org/packages/hirak/prestissimo), a composer plugin that downloads packages in parallel
- `hirak/prestissimo` must be installed globaled for the user who launch `composer install` command.

```bash
composer global require hirak/prestissimo

    # hirak/prestissimo requires ext-curl
    php -v
    sudo apt -y install php<Major.Minor>-curl
```
