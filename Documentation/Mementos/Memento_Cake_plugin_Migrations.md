# Upgrade SQL Schema with Migrations

Migrations is a CakePHP 3 plugin: http://book.cakephp.org/3.0/en/migrations.html

## Getting status of a database before a migration

```sh
bin/cake migrations status
```

## Applying changes with Migrations

```sh
bin/cake migrations migrate
```

## Old notes -----------------------------------------------------------

To apply all changes from `config/Migrations/MetricsAddColumns`, do:

```sh
bin/cake migrations migrate --source Migrations/MetricsAddColumns
```

To apply all changes up to the ones from 06/06/2016 12:00, do:

```sh
bin/cake migrations migrate --target 20160606120000
```

## Force one migration to be "marked as migrated"

To mark as migrated all changes up to 01/01/1980 12:00, do:

```sh
bin/cake migrations mark_migrated --target 19800101120000
```
