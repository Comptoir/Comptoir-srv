# Functional testing

## Prerequisites: Comptoir-srv

* cf [Install Dev Ubuntu 16 04](../../For_ops/INSTALL/INSTALL_DEV_Ubuntu_16.04.md)
* Clean the database
* Use the following database dump [databaseData](../../../config/SQL/COMPTOIR_DB_For_Tests.sql)
* Use the following files [files.tar.gz](../../../tests/TestFiles/FunctionalsTests/files.tar.gz)
* Have a Comptoir running on http://localhost:8080/

##  Actual functional testing

```
php vendor/bin/codecept run
```
