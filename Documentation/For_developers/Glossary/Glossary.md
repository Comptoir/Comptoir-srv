# Glossary

## Metrics

### Raw metric

A *raw metric* is a metric extracted from the VCS (version control system)

Example for project XWiki on 04/05/2016:

* metric `last_commit_age`: 0
* metric `contributor_code_pcent`: 23%
* metric `nb_contributor`: 92

### Computed metric points

*Computed metric points* is the number of points attributed to a given metric.

To give the points, an average for the metric is computed over all the projects. Then depending on the distance between the average and the *raw metric* value, the points are attributed.

Points are usually comprised between -2 and +2.

