# Internationalisation (i18n) with CakePHP

## Workflow to be followed

1. re-create template files (.pot)
2. update the translation files (.po)

## Pre-requisites

```
apt install gettext
```

## Task: re-create template (.pot) files

From the root directory of the project, to generate all the .pot files (templates for translation), do:

```
bin/cake i18n extract --output ./src/Locale/ --paths ./src/ --extract-core no --exclude Test,vendor --merge no
```

## Task: update translation file (.po) from template (.pot) file

```
cd ./src/Locale/

MYFILE="Taxonomy"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Breadcrumbs"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="default"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="ElementNavigation"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Email"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Error"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Forms"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Home"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Layout"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Pages"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Screenshots"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Softwares"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Tags"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

MYFILE="Users"
msgmerge --update --verbose fr/${MYFILE}.po ${MYFILE}.pot --force-po
msgmerge --update --verbose en/${MYFILE}.po ${MYFILE}.pot --force-po

