<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//define('_ENV_TYPE', 'dev');
define('_ENV_TYPE', 'prod');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# the "SILL" tags must exist beforehand in comptoir app
$tagId = 578;  // Tag "SILL" --> https://comptoir-du-libre.org/fr/tags/578/software

# data source
$urlSill = "https://code.gouv.fr/sill/api/sill.json";
$urlComptoir = "https://comptoir-du-libre.org/public/export/comptoir-du-libre_export_v1.json";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (_ENV_TYPE === 'dev') {
    $tagId = 27;  // Tag "SILL-2018" --> http://localhost:8282/fr/tags/27/software
    $urlComptoir = "http://localhost:8282/public/export/comptoir-du-libre_export_v1.json";
}

// Get remote data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // get SILL file
        file_put_contents(basename($urlSill), file_get_contents($urlSill));

        // get Comptoir file
        file_put_contents(basename($urlComptoir), file_get_contents($urlComptoir));

// Extract SILL data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$SilljsonData = json_decode(file_get_contents(basename($urlSill)));
$sillData = [];
$sillDereferencing = [];
foreach ($SilljsonData as $key => $sillSofware) {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Object   (
//     ["categories"]                   => array( [0] =>  "Miscellaneous")
//                                         https://code.gouv.fr/sill/list?sort=referent_count&category=Miscellaneous
//     ["comptoirDuLibreSoftware"]      => object
//     ["description"]                  => "Compression: Création de .zip, .rar, .tar.gz etc."
//     ["doRespectRgaa"]                => boolean
//     ["id"]                           => integer
//     ["isFromFrenchPublicService"]    => boolean
//     ["isPresentInSupportContract"]   => boolean
//     ["isStillInObservation"]         => boolean
//     ["keywords"]                     => array("compression","décompression", "fichier", "archive", ...)
//     ["license"]                      => "LGPL-2.0-only"
//     ["logoUrl"]                      => "https://logodix.com/logo/1966390.png"
//     ["name"]                         => "7zipg"
//     ["referencedSinceTime"]          => integer  <-- timestamp
//     ["softwareType"]                 => object  ( ["type"] => "desktop/mobile"
//                                                   ["os"] => object  ( ["windows"] => boolean
//                                                                       ["android"] => boolean
//     ["testUrls"]                     => array()
//     ["updateTime"]                   => integer  <-- timestamp
//     ["versionMin"]                   => "19"
//     ["workshopUrls"]                 => array()
//     ["wikidataSoftware"]             => object  ( ["wikidataId"] => "Q215051"
//                                                   ["label"]      => "7-Zip"
//                                                   ["description"] => object  ( ["fr"] => "logiciel d'archivage... "
//                                                                                ["en"] => "open-source file archiver"
//                                                   ["logoUrl"]      => "//upload.wikimedia.org/..."
//                                                   ["framaLibreId"] => "7-zip"
//                                                   ["sourceUrl"]    => "https://sourceforge.net/..."
//                                                   ["websiteUrl"]   => "https://7-zip.org"
//                                                   ["license"]      => "LGPL"
//                                                   ["isLibreSoftware"] => boolean
//                                                   ["developers"] =>  array( [0] =>  object ( ["name"] => "Igor"
//                                                                                              ["id"]   => "Q531341"
//     ["similarWikidataSoftwares"]      => array( [0] =>  object ( ["wikidataId"]      => "Q688317"
//                                                                  ["label"]           => "WinZip"
//                                                                  ["description"]     => object  ( ["en"] => "..."
//                                                                  ["isLibreSoftware"] => boolean
//     ["hasExpertReferent"]             => boolean
//     ["userAndReferentCountByOrganization"] => object ( ["CNRS"] => object ( "referentCount" => integer
//                                                                             "userCount"     => integer
//                                                        ["IGN"]  => object ( "referentCount" => integer
//                                                                             "userCount"     => integer
//     ["instances"]                    => array()

                //////////////////////////////////////////////////////////
                /////// WikiData LABEL 1
                //            "wikidataSoftware": {
                //                "wikidataId": "Q11354",
                //              "label": "Apache HTTP Server",
                //              "description": {
                //                    "fr": "serveur web sous licence libre",
                //                "en": "open-source web server software"
                //              },
                //
                /////// WikiData LABEL 2
                //            "wikidataSoftware": {
                //                    "wikidataId": "Q616885",
                //              "label": {
                //                      "fr": "JMeter",
                //                      "en": "Apache JMeter"
                //              },
                //              "description": {
                //                        "fr": "Logiciel libre de tests de performance",
                //                       "en": "free performance testing software"
                //              },
                //////// dereferencing  ---> 23 items
                //            "id": 153,
                //            "name": "Vault",
                //            "dereferencing": {
                //                "time": 1695623225524,
                //              "reason": "Changement de license vers la BSL en août 2023",
                //              "lastRecommendedVersion": "N/A"
                //            },
                //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $sillId = (int) trim($sillSofware->id);
    $sillName = trim($sillSofware->name);
    $isStillInObservation = $sillSofware->isStillInObservation; // false for all software ? ---> TODO ask question
    $isFromFrenchPublicService = $sillSofware->isFromFrenchPublicService;
    $isDereferencing = (isset($sillSofware->dereferencing)) ? true : false;
    $comptoirId = (isset($sillSofware->comptoirDuLibreSoftware)) ? (int) trim($sillSofware->comptoirDuLibreSoftware->id) : '';
    $comptoirName = (isset($sillSofware->comptoirDuLibreSoftware)) ? trim($sillSofware->comptoirDuLibreSoftware->name) : '';
    $wikidataId = (isset($sillSofware->wikidataSoftware)) ? trim($sillSofware->wikidataSoftware->wikidataId) : '';
    $wikidataName = '';
    if (isset($sillSofware->wikidataSoftware)) {
        if (is_string($sillSofware->wikidataSoftware->label)) {
            $wikidataName = trim($sillSofware->wikidataSoftware->label);
        }
        else if (isset($sillSofware->wikidataSoftware->label->fr)) {
            $wikidataName = trim($sillSofware->wikidataSoftware->label->fr);
        }
        else if (isset($sillSofware->wikidataSoftware->label->en)) {
            $wikidataName = trim($sillSofware->wikidataSoftware->label->en);
        }
        else if (isset($sillSofware->wikidataSoftware->description->fr)) {
            $wikidataName = trim($sillSofware->wikidataSoftware->description->fr);
        }
        else if (isset($sillSofware->wikidataSoftware->description->en)) {
            $wikidataName = trim($sillSofware->wikidataSoftware->description->en);
        }
    }


    if ($isDereferencing === true) {
        echo "source SILL ---> Dereferencing  SILL $sillId - $sillName / $wikidataId | $wikidataName / Comptoir $comptoirId | $comptoirName --> [$isStillInObservation][$isFromFrenchPublicService]\n";
        $sillDereferencing["sillId_$sillId"] = [
            'sillId' => $sillId,
            'wikidataId' => "$wikidataId",
            'comptoirId' => $comptoirId,
            'sillName' => "$sillName",
            'wikidataName' => "$wikidataName",
            'comptoirName' =>"$comptoirName",
            'isDereferencing' => $isDereferencing,
        ];
    }
    else {
        echo "source SILL - SILL $sillId - $sillName / $wikidataId | $wikidataName / Comptoir $comptoirId | $comptoirName --> [$isStillInObservation][$isFromFrenchPublicService]\n";
        $sillData["sillId_$sillId"] = [
            'sillId' => $sillId,
            'wikidataId' => "$wikidataId",
            'comptoirId' => $comptoirId,
            'sillName' => "$sillName",
            'wikidataName' => "$wikidataName",
            'comptoirName' =>"$comptoirName",
            'isDereferencing' => $isDereferencing,
        ];
    }
}
ksort($sillData,  SORT_NATURAL);
ksort($sillDereferencing,  SORT_NATURAL);

$sillCsvHead  = '';
$sillCsvHead .= "\"sillId\";";
$sillCsvHead .= "\"wikidataId\";";
$sillCsvHead .= "\"comptoirId\";";
$sillCsvHead .= "\"sillName\";";
$sillCsvHead .= "\"wikidataName\";";
$sillCsvHead .= "\"comptoirName\";";
$sillCsvHead .= "\n";
$sillCsv = '';
foreach ($sillData as $row) {
    $sillCsv .= "\"". $row['sillId']        ."\";";
    $sillCsv .= "\"". $row['wikidataId']    ."\";";
    $sillCsv .= "\"". $row['comptoirId']    ."\";";
    $sillCsv .= "\"". $row['sillName']      ."\";";
    $sillCsv .= "\"". $row['wikidataName']  ."\";";
    $sillCsv .= "\"". $row['comptoirName']  ."\";";
    $sillCsv .= "\n";
}
file_put_contents(pathinfo($urlSill, PATHINFO_FILENAME) . '.csv', "$sillCsvHead" . "$sillCsv");

$sillDereferencingCsv = '';
foreach ($sillDereferencing as $row) {
    $sillDereferencingCsv .= "\"". $row['sillId']        ."\";";
    $sillDereferencingCsv .= "\"". $row['wikidataId']    ."\";";
    $sillDereferencingCsv .= "\"". $row['comptoirId']    ."\";";
    $sillDereferencingCsv .= "\"". $row['sillName']      ."\";";
    $sillDereferencingCsv .= "\"". $row['wikidataName']  ."\";";
    $sillDereferencingCsv .= "\"". $row['comptoirName']  ."\";";
    $sillDereferencingCsv .= "\n";
}
$sillDereferencingCsvHead  = $sillCsvHead;
file_put_contents(pathinfo($urlSill, PATHINFO_FILENAME) . '_dereferencing.csv', "$sillDereferencingCsvHead" . "$sillDereferencingCsv");

//print_r($sillData);
//print_r($sillCsv);


// Extract Comtpoir data
////////////////////////////////////////////////////////////////////////////////
$jsonData = json_decode(file_get_contents(basename($urlComptoir)));
$comptoir = $jsonData->softwares;
$comptoirData = [];
foreach ($comptoir as $key => $comptoirSofware) {
    $comptoirId = (int) trim($comptoirSofware->id);
    $comptoirName = trim($comptoirSofware->name);
    $wikidataId = '';
    if (isset($comptoirSofware->external_resources->wikidata->id)) {
        $wikidataId = trim($comptoirSofware->external_resources->wikidata->id);
    }
    $sillId = '';
    if (isset($comptoirSofware->external_resources->sill->id)) {
        $sillId = trim($comptoirSofware->external_resources->sill->id);
    }
    echo "Comptoir $comptoirId | $comptoirName | WIKIDATA $wikidataId | SILL $sillId\n";
    $comptoirData["comptoirId_$comptoirId"] = [
        'sillId' => $sillId,
        'wikidataId' => "$wikidataId",
        'comptoirId' => $comptoirId,
        'comptoirName' =>"$comptoirName",
    ];
}
ksort($comptoirData,  SORT_NATURAL);

$comptoirCsv = '';
$compareCsv = '';
$csvLineNumber = 1;
foreach ($comptoirData as $row) {
    $csvLineNumber++;
    $comptoirCsv .= "\"". $row['comptoirId']    ."\";";
    $comptoirCsv .= "\"". $row['comptoirName']  ."\";";
    $comptoirCsv .= "\"". $row['sillId']        ."\";";
    $comptoirCsv .= "\"". $row['wikidataId']    ."\";";
    $comptoirCsv .= "\n";

    // source: https://comptoir-du-libre.org
    $compareCsv .= "\"". $row['comptoirId']    ."\",";
    $compareCsv .= "\"". $row['comptoirName']  ."\",";
    $compareCsv .= "\"". $row['sillId']        ."\",";
    $compareCsv .= "\"". $row['wikidataId']    ."\",";

    // source: https://code.gouv.fr/sill
    $sillId = '';
    $sillName = '';
    $sillComptoirId = '';
    $sillWikidataId = '';
    $sillWikidataName = '';
    $comptoirSrcSillId = $row['sillId'];
    if (isset($sillData["sillId_$comptoirSrcSillId"])) {
        $sillId = $sillData["sillId_$comptoirSrcSillId"]['sillId'];
        $sillName = $sillData["sillId_$comptoirSrcSillId"]['sillName'];
        $sillComptoirId = $sillData["sillId_$comptoirSrcSillId"]['comptoirId'];
        $sillWikidataId = $sillData["sillId_$comptoirSrcSillId"]['wikidataId'];
        $sillWikidataName = $sillData["sillId_$comptoirSrcSillId"]['wikidataName'];
    }
    $compareCsv .= "\"". $sillId        ."\",";
    $compareCsv .= "\"". $sillName        ."\",";
    $compareCsv .= "\"". $sillComptoirId    ."\",";
    $compareCsv .= "\"". $sillWikidataId    ."\",";
    $compareCsv .= "\"". $sillWikidataName    ."\",";
    $compareCsv .= "=SI(C$csvLineNumber=\"\";\"\";A$csvLineNumber=G$csvLineNumber),";
    $compareCsv .= "=SI(C$csvLineNumber=\"\";\"\";D$csvLineNumber=H$csvLineNumber),";
    $compareCsv .= "=SI(C$csvLineNumber=\"\";\"\";B$csvLineNumber=F$csvLineNumber),";
    $compareCsv .= "\n";
}
$comptoirCsvHead  = '';
$comptoirCsvHead .= "\"comptoir_ID\";";
$comptoirCsvHead .= "\"comptoir_NAME\";";
$comptoirCsvHead .= "\"comptoir_SILL_ID\";";
$comptoirCsvHead .= "\"comptoir_WIKIDATA_ID\";";
$comptoirCsvHead .= "\n";
file_put_contents(pathinfo($urlComptoir, PATHINFO_FILENAME) . '.csv', "$comptoirCsvHead" . "$comptoirCsv");


$compareCsvHead  = '';
$compareCsvHead .= "\"comptoir_ID\",";
$compareCsvHead .= "\"comptoir_NAME\",";
$compareCsvHead .= "\"comptoir_SILL_ID\",";
$compareCsvHead .= "\"comptoir_WIKIDATA_ID\",";
$compareCsvHead .= "\"sill_ID\",";
$compareCsvHead .= "\"sill_NAME\",";
$compareCsvHead .= "\"sill_COMPTOIR_ID\",";
$compareCsvHead .= "\"sill_WIKIDATA_ID\",";
$compareCsvHead .= "\"sill_WIKIDATA_NAME\",";
$compareCsvHead .= "\"compare_COMPTOIR_ID\",";
$compareCsvHead .= "\"compare_WIKIDATA_ID\",";
$compareCsvHead .= "\"compare_NAME\",";
$compareCsvHead .= "\n";
file_put_contents('compare.csv', "$compareCsvHead" . "$compareCsv");
//print_r($comptoirData);
//print_r($comptoirCsv);

// Compare  Comtpoir and SILL data + generate SQL files
////////////////////////////////////////////////////////////////////////////////
$sqlMajTag = [];
$sqlMajTagNotFound = [];
$sqlMajSoftware = [];
$sqlMajSoftwareNotFound = [];
foreach ($sillData as $row) {
    // print_r($row);
    //        Array (     [sillId] => 248
    //                    [wikidataId] => Q15252222
    //                    [comptoirId] => 470
    //                    [sillName] => Composer
    //                    [wikidataName] => Composer
    //                    [comptoirName] => Composer
    extract($row, EXTR_OVERWRITE);
    echo "SILL data ---> SILL $sillId - $sillName / $wikidataId | $wikidataName / Comptoir $comptoirId | $comptoirName \n";
    if (empty($wikidataId)) {
        $sqlSoftware = "UPDATE softwares SET sill = $sillId, wikidata = NULL          WHERE id = $comptoirId;";
    }
    else {
        $sqlSoftware = "UPDATE softwares SET sill = $sillId, wikidata = '$wikidataId' WHERE id = $comptoirId;";
    }
    $sqlTag = "INSERT INTO softwares_tags (software_id, tag_id) VALUES ('$comptoirId', '$tagId');";
    if (!empty($comptoirId)) {
        if(isset($comptoirData["comptoirId_$comptoirId"])) {
            $sqlMajTag["comptoirId_$comptoirId"] = "$sqlTag -- Comtpoir: $comptoirName / SILL: $sillName ($sillId)";
            $sqlMajSoftware["comptoirId_$comptoirId"] = "$sqlSoftware -- Comtpoir: $comptoirName / SILL: $sillName ($sillId)";
        }
        else if (_ENV_TYPE === 'prod') {
            throw new Exception("Invalid $comptoirId ");
        }
    }
    else {
        $sqlMajTagNotFound["sillId_$sillId"] = "$sqlTag -- Comtpoir: $comptoirName / SILL: $sillName ($sillId)";
        $sqlMajSoftwareNotFound["sillId_$sillId"] = "$sqlSoftware -- Comtpoir: $comptoirName / SILL: $sillName ($sillId)";
    }
}
ksort($sqlMajTag,  SORT_NATURAL);
array_unshift($sqlMajTag, "DELETE FROM softwares_tags WHERE tag_id = '$tagId';");
file_put_contents("SQL_maj_tagSILL.sql", implode("\n", $sqlMajTag));
file_put_contents("SQL_maj_tagSILL_notFound.sql", implode("\n", $sqlMajTagNotFound));

ksort($sqlMajSoftware,  SORT_NATURAL);
array_unshift($sqlMajSoftware, "UPDATE softwares SET sill = NULL, wikidata = NULL;");
file_put_contents("SQL_maj_softwareSILL.sql", implode("\n", $sqlMajSoftware));
file_put_contents("SQL_maj_softwareSILL_notFound.sql", implode("\n", $sqlMajSoftwareNotFound));

// TODO
echo "\n\n";
echo "\n------> ". (count($comptoirData)). " Comptoir software";
echo "\n------> ". (count($sillData))        . " SILL software";
echo "\n------> ". (count($sqlMajTag) - 1)   . " SILL software with Comptoir ID";
echo "\n------> ". (count($sqlMajTagNotFound)). " SILL software without Comptoir ID";
echo "\n------> ".  (count($sqlMajTag) - 1 + count($sqlMajTagNotFound))
                 . " verification : égal à ". (count($sillData))        . " ? ---> fichier SILL json à jour ?" ;
echo "\n\n";

