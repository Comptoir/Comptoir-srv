<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('_DATA_DIR', './data_wikidata');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
# data source
$urlComptoir = "https://comptoir-du-libre.org/public/export/comptoir-du-libre_export_v1.json";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Get remote data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (!is_dir(_DATA_DIR) && !mkdir(_DATA_DIR, 0750, true)) {
    throw new \RuntimeException(sprintf('Directory "%s" was not created', _DATA_DIR));
}

        // get Comptoir file
        $comptoirPath = _DATA_DIR . '/src_'. basename($urlComptoir);
        file_put_contents("$comptoirPath", file_get_contents($urlComptoir));

// Extract Comtpoir data
////////////////////////////////////////////////////////////////////////////////
$jsonData = json_decode(file_get_contents($comptoirPath));
$comptoir = $jsonData->softwares;
$sqlRequests = [];
foreach ($comptoir as $key => $comptoirSofware) {
    $comptoirId = (int) trim($comptoirSofware->id);
    $comptoirName = trim($comptoirSofware->name);
    $wikidataId = '';
    if (isset($comptoirSofware->external_resources->wikidata->id)) {
        $wikidataId = trim($comptoirSofware->external_resources->wikidata->id);
        $jsonData = json_decode(file_get_contents("https://www.wikidata.org/wiki/Special:EntityData/$wikidataId.json"));
        $data = $jsonData->entities->$wikidataId;

        $labelFr = '';
        if (isset($data->labels->fr)) {
            $labelFr = trim($data->labels->fr->value);
        }
        $labelEn = '';
        if (isset($data->labels->en)) {
            $labelEn = trim($data->labels->en->value);
        }

        $wikipediaFrSlug = '';
        if (isset($data->sitelinks->frwiki->url)) {
            $wikipediaFrSlug = trim(str_replace("https://fr.wikipedia.org/wiki/", "", $data->sitelinks->frwiki->url));
        }
        $wikipediaEnSlug = '';
        if (isset($data->sitelinks->enwiki->url)) {
            $wikipediaEnSlug = trim(str_replace("https://en.wikipedia.org/wiki/", "", $data->sitelinks->enwiki->url));
        }
        $framalibreSlug = '';
        if (isset($data->claims->P4107[0]->mainsnak->datavalue->value)) {
            $framalibreSlug = trim($data->claims->P4107[0]->mainsnak->datavalue->value);
        }
        echo "\n--------------\n$comptoirId - $comptoirName - $wikidataId ---> FR $labelFr | EN $labelEn\n";
        $sqlUpdate = '';
        if (!empty($wikipediaFrSlug) | !empty($wikipediaEnSlug) | !empty($framalibreSlug)) {
            $sqlUpdate .= "-- Comtpoir: $comptoirId -> $comptoirName / ";
            $sqlUpdate .= " WidiData: $wikidataId ---> FR $labelFr | EN $labelEn\n";
            if (!empty($wikipediaFrSlug)) {
                $sqlUpdate .= "UPDATE softwares SET wikipedia_fr = '$wikipediaFrSlug' WHERE id = $comptoirId;\n";
            }
            if (!empty($wikipediaEnSlug)) {
                $sqlUpdate .= "UPDATE softwares SET wikipedia_en = '$wikipediaEnSlug' WHERE id = $comptoirId;\n";
            }
            if (!empty($framalibreSlug)) {
                $sqlUpdate .= "UPDATE softwares SET framalibre = '$framalibreSlug' WHERE id = $comptoirId;\n";
            }
            $sqlRequests["comptoirId_$comptoirId"] = "$sqlUpdate";
            echo "Wikipedia FR | $wikipediaFrSlug | https://fr.wikipedia.org/wiki/$wikipediaFrSlug\n";
            echo "Wikipedia EN | $wikipediaEnSlug | https://en.wikipedia.org/wiki/$wikipediaEnSlug\n";
            echo "FramaLibre | $framalibreSlug | https://framalibre.org/content/$framalibreSlug\n";
            echo "$sqlUpdate\n";
        }
    }
}
ksort($sqlRequests, SORT_NATURAL);
array_unshift($sqlRequests, "UPDATE softwares SET framalibre = NULL, wikipedia_en = NULL, wikipedia_fr = NULL;");
file_put_contents(_DATA_DIR . "/output_SQL_maj_WIKIDATA_data.sql", implode("\n", $sqlRequests));
