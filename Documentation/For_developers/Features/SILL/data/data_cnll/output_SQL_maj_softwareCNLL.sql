UPDATE softwares SET cnll = NULL;


-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      11 ---> XWiki
-- CNLL:          3 --->  XWiki
-- SILL:          214
UPDATE softwares SET cnll = '3' WHERE id = 11;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      22 ---> XiVO
-- CNLL:          576 --->  XiVO
-- SILL:          276
UPDATE softwares SET cnll = '576' WHERE id = 22;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      25 ---> BlueMind
-- CNLL:          100 --->  Bluemind
-- SILL:          
UPDATE softwares SET cnll = '100' WHERE id = 25;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      29 ---> Alfresco
-- CNLL:          523 --->  Alfresco
-- SILL:          
UPDATE softwares SET cnll = '523' WHERE id = 29;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      33 ---> LibreOffice
-- CNLL:          284 --->  LibreOffice
-- SILL:          78
UPDATE softwares SET cnll = '284' WHERE id = 33;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      36 ---> SPIP
-- CNLL:          502 --->  SPIP
-- SILL:          211
UPDATE softwares SET cnll = '502' WHERE id = 36;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      38 ---> WordPress
-- CNLL:          572 --->  WordPress
-- SILL:          162
UPDATE softwares SET cnll = '572' WHERE id = 38;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      60 ---> QGIS
-- CNLL:          157 --->  QGIS
-- SILL:          120
UPDATE softwares SET cnll = '157' WHERE id = 60;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      67 ---> GIMP
-- CNLL:          431 --->  GIMP
-- SILL:          51
UPDATE softwares SET cnll = '431' WHERE id = 67;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      76 ---> Scribus
-- CNLL:          4 --->  Scribus
-- SILL:          128
UPDATE softwares SET cnll = '4' WHERE id = 76;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      98 ---> OPENLDAP
-- CNLL:          366 --->  OpenLDAP
-- SILL:          105
UPDATE softwares SET cnll = '366' WHERE id = 98;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      110 ---> Zimbra
-- CNLL:          521 --->  Zimbra
-- SILL:          202
UPDATE softwares SET cnll = '521' WHERE id = 110;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      116 ---> OpenBSD
-- CNLL:          471 --->  OpenBSD
-- SILL:          
UPDATE softwares SET cnll = '471' WHERE id = 116;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      117 ---> NextCloud
-- CNLL:          466 --->  Nextcloud
-- SILL:          93
UPDATE softwares SET cnll = '466' WHERE id = 117;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      122 ---> MediaWiki
-- CNLL:          177 --->  MediaWiki
-- SILL:          86
UPDATE softwares SET cnll = '177' WHERE id = 122;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      123 ---> PostgreSQL
-- CNLL:          155 --->  PostgreSQL
-- SILL:          118
UPDATE softwares SET cnll = '155' WHERE id = 123;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      132 ---> Blender
-- CNLL:          15 --->  Blender
-- SILL:          258
UPDATE softwares SET cnll = '15' WHERE id = 132;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      140 ---> PeerTube
-- CNLL:          482 --->  PeerTube
-- SILL:          197
UPDATE softwares SET cnll = '482' WHERE id = 140;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      142 ---> samba
-- CNLL:          506 --->  Samba
-- SILL:          127
UPDATE softwares SET cnll = '506' WHERE id = 142;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      164 ---> Odoo
-- CNLL:          61 --->  odoo
-- SILL:          
UPDATE softwares SET cnll = '61' WHERE id = 164;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      170 ---> Eclipse
-- CNLL:          96 --->  Eclipse
-- SILL:          
UPDATE softwares SET cnll = '96' WHERE id = 170;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      175 ---> Git
-- CNLL:          56 --->  Git
-- SILL:          52
UPDATE softwares SET cnll = '56' WHERE id = 175;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      176 ---> Maven
-- CNLL:          395 --->  Apache Maven
-- SILL:          8
UPDATE softwares SET cnll = '395' WHERE id = 176;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      193 ---> Chromium
-- CNLL:          414 --->  Chromium
-- SILL:          22
UPDATE softwares SET cnll = '414' WHERE id = 193;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      197 ---> Redmine
-- CNLL:          381 --->  Redmine
-- SILL:          122
UPDATE softwares SET cnll = '381' WHERE id = 197;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      204 ---> Apache SolR
-- CNLL:          62 --->  Apache Solr
-- SILL:          9
UPDATE softwares SET cnll = '62' WHERE id = 204;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      205 ---> Tomcat
-- CNLL:          397 --->  Apache Tomcat
-- SILL:          10
UPDATE softwares SET cnll = '397' WHERE id = 205;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      206 ---> MariaDB
-- CNLL:          80 --->  MariaDB
-- SILL:          84
UPDATE softwares SET cnll = '80' WHERE id = 206;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      207 ---> PostGIS
-- CNLL:          560 --->  PostGIS
-- SILL:          117
UPDATE softwares SET cnll = '560' WHERE id = 207;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      208 ---> MongoDB
-- CNLL:          459 --->  MongoDB
-- SILL:          88
UPDATE softwares SET cnll = '459' WHERE id = 208;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      212 ---> Apache
-- CNLL:          141 --->  Apache HTTP Server
-- SILL:          6
UPDATE softwares SET cnll = '141' WHERE id = 212;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      213 ---> Nginx
-- CNLL:          467 --->  Nginx
-- SILL:          95
UPDATE softwares SET cnll = '467' WHERE id = 213;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      216 ---> PostFix
-- CNLL:          486 --->  Postfix
-- SILL:          116
UPDATE softwares SET cnll = '486' WHERE id = 216;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      218 ---> Docker
-- CNLL:          420 --->  Docker
-- SILL:          34
UPDATE softwares SET cnll = '420' WHERE id = 218;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      219 ---> Kubernetes
-- CNLL:          450 --->  Kubernetes
-- SILL:          75
UPDATE softwares SET cnll = '450' WHERE id = 219;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      221 ---> OpenStack
-- CNLL:          553 --->  OpenStack
-- SILL:          108
UPDATE softwares SET cnll = '553' WHERE id = 221;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      227 ---> Rudder
-- CNLL:          163 --->  Rudder
-- SILL:          164
UPDATE softwares SET cnll = '163' WHERE id = 227;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      239 ---> CentOS
-- CNLL:          79 --->  CentOS
-- SILL:          19
UPDATE softwares SET cnll = '79' WHERE id = 239;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      241 ---> Debian
-- CNLL:          322 --->  Debian
-- SILL:          32
UPDATE softwares SET cnll = '322' WHERE id = 241;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      250 ---> Moodle
-- CNLL:          462 --->  Moodle
-- SILL:          208
UPDATE softwares SET cnll = '462' WHERE id = 250;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      259 ---> Joomla!
-- CNLL:          344 --->  Joomla!
-- SILL:          
UPDATE softwares SET cnll = '344' WHERE id = 259;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      264 ---> Drupal
-- CNLL:          394 --->  Drupal
-- SILL:          36
UPDATE softwares SET cnll = '394' WHERE id = 264;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      279 ---> Symfony
-- CNLL:          193 --->  Symfony
-- SILL:          247
UPDATE softwares SET cnll = '193' WHERE id = 279;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      452 ---> ASTERISK
-- CNLL:          527 --->  Asterisk
-- SILL:          
UPDATE softwares SET cnll = '527' WHERE id = 452;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      482 ---> Apache Subversion
-- CNLL:          314 --->  Apache Subversion
-- SILL:          
UPDATE softwares SET cnll = '314' WHERE id = 482;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      489 ---> Python
-- CNLL:          2 --->  Python
-- SILL:          294
UPDATE softwares SET cnll = '2' WHERE id = 489;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      517 ---> Vim
-- CNLL:          374 --->  Vim
-- SILL:          283
UPDATE softwares SET cnll = '374' WHERE id = 517;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      526 ---> Angular
-- CNLL:          391 --->  Angular
-- SILL:          274
UPDATE softwares SET cnll = '391' WHERE id = 526;

-- Matching type: CNLL-json_SILL-ID 
-- Comtpoir:      530 ---> Flask
-- CNLL:          113 --->  Flask
-- SILL:          272
UPDATE softwares SET cnll = '113' WHERE id = 530;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      549 ---> OCaml
-- CNLL:          210 --->  OCaml
-- SILL:          
UPDATE softwares SET cnll = '210' WHERE id = 549;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      597 ---> Fedora
-- CNLL:          428 --->  Fedora
-- SILL:          
UPDATE softwares SET cnll = '428' WHERE id = 597;

-- Matching type: CNLL-html_SOFTWARE-NAME 
-- Comtpoir:      617 ---> GNOME
-- CNLL:          433 --->  GNOME
-- SILL:          
UPDATE softwares SET cnll = '433' WHERE id = 617;
