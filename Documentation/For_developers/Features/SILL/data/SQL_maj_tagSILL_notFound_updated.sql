-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: Survey (questionnaire) (55)
--
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: Mobile (87)
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: PortraitOrLandscape (115)
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: Qwant (extension de recherche) (121)
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: Swift (OpenStack) (141)
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: WebAccess (159)
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: GNU Emacs (174)
-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: Correcteur terminologique fr (FranceTerme) (190)


INSERT INTO softwares_tags (software_id, tag_id) VALUES ('416', '578'); -- Comtpoir:  / SILL: XCP-ng (235)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('495', '578'); -- Comtpoir:  / SILL: Publicodes (242)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('496', '578'); -- Comtpoir:  / SILL: Zotero (243)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('497', '578'); -- Comtpoir:  / SILL: Dovecot (244)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('498', '578'); -- Comtpoir:  / SILL: udata (245)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('499', '578'); -- Comtpoir:  / SILL: JabRef (246)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('500', '578'); -- Comtpoir:  / SILL: Beekeeper Studio (250)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('501', '578'); -- Comtpoir:  / SILL: ILIAS (251)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('539', '578'); -- Comtpoir:  / SILL: opendcim (252)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('502', '578'); -- Comtpoir:  / SILL: XSane (253)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('503', '578'); -- Comtpoir:  / SILL: MkDocs (254)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('504', '578'); -- Comtpoir:  / SILL: GNU Data Language (255)

INSERT INTO softwares_tags (software_id, tag_id) VALUES ('521', '578'); -- Comtpoir:  / SILL: dokku (256)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('523', '578'); -- Comtpoir:  / SILL: SuiteCRM (257)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('473', '578'); -- Comtpoir:  / SILL: KiCad (259)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('525', '578'); -- Comtpoir:  / SILL: GanttProject (260)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('538', '578'); -- Comtpoir:  / SILL: sdkman (261)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('527', '578'); -- Comtpoir:  / SILL: Apache Atlas (266)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('537', '578'); -- Comtpoir:  / SILL: Bonita (267)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('528', '578'); -- Comtpoir:  / SILL: podman (268)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('536', '578'); -- Comtpoir:  / SILL: ART (270)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('531', '578'); -- Comtpoir:  / SILL: G'MIC (271)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('530', '578'); -- Comtpoir:  / SILL: Flask (272)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('529', '578'); -- Comtpoir:  / SILL: Cherrytree (273)

INSERT INTO softwares_tags (software_id, tag_id) VALUES ('524', '578'); -- Comtpoir:  / SILL: Minio (275)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('522', '578'); -- Comtpoir:  / SILL: PlantUML (277)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('535', '578'); -- Comtpoir:  / SILL: KiTTY (279)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('519', '578'); -- Comtpoir:  / SILL: Deno (280)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('518', '578'); -- Comtpoir:  / SILL: TDP (281)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('540', '578'); -- Comtpoir:  / SILL: Taiga (282)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('517', '578'); -- Comtpoir:  / SILL: Vim (283)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('516', '578'); -- Comtpoir:  / SILL: GitLanding (285)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('515', '578'); -- Comtpoir:  / SILL: AtoM - Access to Memory (293)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('489', '578'); -- Comtpoir:  / SILL: Python (294)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('514', '578'); -- Comtpoir:  / SILL: Clojure (295)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('513', '578'); -- Comtpoir:  / SILL: Homebrew (297)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('512', '578'); -- Comtpoir:  / SILL: Bitwarden (298)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('511', '578'); -- Comtpoir:  / SILL: VisiData (299)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('534', '578'); -- Comtpoir:  / SILL: Raspberry Pi OS (300)

-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: BrowserSelector (301)

INSERT INTO softwares_tags (software_id, tag_id) VALUES ('510', '578'); -- Comtpoir:  / SILL: OpenTestFactory (303)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('509', '578'); -- Comtpoir:  / SILL: Kate (304)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('506', '578'); -- Comtpoir:  / SILL: Spectacle (307)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('507', '578'); -- Comtpoir:  / SILL: KDE Plasma 5 (308)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('505', '578'); -- Comtpoir:  / SILL: Groff (309)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('533', '578'); -- Comtpoir:  / SILL: Kleopatra (310)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('541', '578'); -- Comtpoir:  / SILL: Chocolatey (311)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('546', '578'); -- Comtpoir:  / SILL: ImageJ (313)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('542', '578'); -- Comtpoir:  / SILL: LaTeX (314)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('544', '578'); -- Comtpoir:  / SILL: Jupyter Notebook (316)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('545', '578'); -- Comtpoir:  / SILL: JupyterLab (317)

-- INSERT INTO softwares_tags (software_id, tag_id) VALUES ('', '578'); -- Comtpoir:  / SILL: INSECA (318)

INSERT INTO softwares_tags (software_id, tag_id) VALUES ('526', '578'); -- Comtpoir:  / SILL: Angular (320)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('547', '578'); -- Comtpoir:  / SILL: GNU Octave (321)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('548', '578'); -- Comtpoir:  / SILL: GNU TeXmacs (322)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('549', '578'); -- Comtpoir:  / SILL: OCaml (323)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('550', '578'); -- Comtpoir:  / SILL: FreeFem++ (324)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('199', '578'); -- Comtpoir:  / SILL: PHPBB3 (325)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('552', '578'); -- Comtpoir:  / SILL: Arduino IDE (326)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('553', '578'); -- Comtpoir:  / SILL: SchredOS (327)

INSERT INTO softwares_tags (software_id, tag_id) VALUES ('557', '578'); -- Comtpoir:  / SILL: Trivy (329)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('558', '578'); -- Comtpoir:  / SILL: Sugarizer (332)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('4', '578'); -- Comtpoir:  / SILL: Iparapheur (333)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('561', '578'); -- Comtpoir:  / SILL: cal.com (335)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('565', '578'); -- Comtpoir:  / SILL: Osuny (336)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('564', '578'); -- Comtpoir:  / SILL: Duplicati (338)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('563', '578'); -- Comtpoir:  / SILL: Prodige (343)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('570', '578'); -- Comtpoir:  / SILL: DashLord (344)
INSERT INTO softwares_tags (software_id, tag_id) VALUES ('575', '578'); -- Comtpoir:  / SILL: PacketFence (345)
