# SILL (Socle Interministériel de Logiciels Libres)

* Website: https://code.gouv.fr/sill
* Full data (Json): https://code.gouv.fr/sill/api/sill.json
* [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/socle-interministeriel-de-logiciels-libres/)
* Partial data
  * PDF -  https://code.gouv.fr/data/sill.pdf
  * Mardwon - https://code.gouv.fr/data/sill.md
  * Text / Org - https://code.gouv.fr/data/sill.org
  * Text / Tabulare - https://code.gouv.fr/data/sill.tsv
* List of last added:
  * https://code.gouv.fr/sill/list?sort=added_time (sort by date added)
  * RSS: https://code.gouv.fr/data/latest-sill.xml
* code repositories
  * https://github.com/codegouvfr/sill-api
  * https://github.com/codegouvfr/sill-web

------------------------

- Tag `SILL` ---> [ID 578](https://comptoir-du-libre.org/fr/tags/578/software)


## Update `SILL`  tag and external IDs (SILL, Wikidata, ...) of Software

**TODO**: improve documentation

The `SILL`  tag must exist beforehand in comptoir app.
```php
cd data
php 00_prepare_SQL.php
       # - load Comptoir data
       # - load SILL data ---> https://code.gouv.fr/sill/api/sill.json
       # - generate a SQL file to update "SILL" tag
       # - generate a SQL file to update external IDs (SILL, Wikidata, ...)
       # - generate a file of missing software on Comptoir
```


```sql
psql <base>

    -- Update SILL tag ---> /!\ the "SILL" tag must exist beforehand in comptoir app
   DELETE FROM softwares_tags WHERE tag_id = '578';
   INSERT INTO softwares_tags (software_id, tag_id) VALUES ('9', '578'); -- Comtpoir: Asqatasun / SILL: Asqatasun (12)
   INSERT INTO softwares_tags (software_id, tag_id) VALUES ('11', '578'); -- Comtpoir: XWiki / SILL: XWiki (214)

   -- Update  external IDs (SILL, Wikidata, ...) of Software
   UPDATE softwares SET sill = NULL, wikidata = NULL;
   UPDATE softwares SET sill = 12, wikidata = 'Q24026504' WHERE id = 9; -- Comtpoir: Asqatasun / SILL: Asqatasun (12)
   UPDATE softwares SET sill = 214, wikidata = 'Q526699' WHERE id = 11; -- Comtpoir: XWiki / SILL: XWiki (214)

   -- exit
    \q

rm -v .psql_history
```
