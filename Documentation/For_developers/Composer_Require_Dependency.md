# Composer dependencies

## Composer files (.json / .lock)

- The composer.json and composer.lock files **must** be committed at the same time.
- A pre-commit hook (`GrumPHP`) and the CI checks it too.

Manually you can check it by running the following command line:
```bash
composer validate
    # ---> check if your composer.json is valid
    # ---> check if composer.lock exists and is up to date.
```

## Dev dependencies

### GrumPHP

[phpro/grumphp](https://phpqa.io/projects/grumphp.html)
will register some git hooks in your package repository.
When somebody commits changes, GrumPHP will run some tests on the committed code.

Usage on the project:

- [ ] IDE
- [X] manual check
- [X] pre-commit check
- [ ] CI check

```bash
# install
composer require --dev phpro/grumphp

# config file
grumphp.yml

# Files pre-commit hook
# ---> run tasks only on files in the git "stage" (index) : added by "git add <file>"
php vendor/bin/grumphp git:pre-commmit

# Message pre-commit hook
vendor/bin/grumphp git:commit-msg

# run all tasks on the full codebase
vendor/bin/grumphp run

#  run only a subset of the configured tasks on the full codebase
vendor/bin/grumphp run --tasks=<task1>,<task2>
vendor/bin/grumphp run --tasks=composer,phplint

# run testSuite tasks on the full codebase
vendor/bin/grumphp run --testsuite <testSuiteName>
vendor/bin/grumphp run --testsuite php_testSuite
```

```bash
phpro/grumphp suggests installing
    behat/behat (Lets GrumPHP validate your project features.)
    brianium/paratest (Lets GrumPHP run PHPUnit in parallel.)
    designsecurity/progpilot (Lets GrumPHP be sure that there are no vulnerabilities in your code.)
    doctrine/orm (Lets GrumPHP validate your Doctrine mapping files.)
    phan/phan (Lets GrumPHP unleash a static analyzer on your code)
    friendsofphp/php-cs-fixer (Lets GrumPHP automatically fix your codestyle.)
    infection/infection (Lets GrumPHP evaluate the quality your unit tests)
    localheinz/composer-normalize (Lets GrumPHP tidy and normalize your composer.json file.)
    maglnet/composer-require-checker (Lets GrumPHP analyze composer dependencies.)
    povils/phpmnd (Lets GrumPHP help you detect magic numbers in PHP code.)
    phpspec/phpspec (Lets GrumPHP spec your code.)
    phpstan/phpstan (Lets GrumPHP discover bugs in your code without running it.)
    sebastian/phpcpd (Lets GrumPHP find duplicated code.)
    symfony/phpunit-bridge (Lets GrumPHP run your unit tests with the phpunit-bridge of Symfony.)
    vimeo/psalm (Lets GrumPHP discover errors in your code without running it.)
    symplify/easycodingstandard (Lets GrumPHP check coding standard.)
    allocine/twigcs (Lets GrumPHP check Twig coding standard.)
    ....
```

### PHP_CodeSniffer (phpcs)

[squizlabs/php_codesniffer](https://phpqa.io/projects/phpcs.html)
detects violations of a defined set of coding standards.

Usage on the project:
- [X] IDE
- [X] manual check
- [X] pre-commit check
- [X] CI check

```bash
# install
composer require --dev "squizlabs/php_codesniffer"

# Show rules and coding standards
vendor/bin/phpcs -i  #  list of installed coding standards
vendor/bin/phpcs -e  #  list of rules used by .phpcs.xml config

# Check files based on .phpcs.xml config
vendor/bin/phpcs
vendor/bin/phpcs --report-full     # each filename with each sniff (error and warning)
vendor/bin/phpcs --report-summary  # each filename with number of errors + warnings
vendor/bin/phpcs --report-source   # each sniff with numbers of errors + warnings
vendor/bin/phpcs --report-code     # shows a code snippet for each error and warning

# Check files and shows all reports
vendor/bin/phpcs --report-code --report-full --report-source --report-summary

# Check specific files and directories
vendor/bin/phpcs <dir>/<file>
vendor/bin/phpcs <dir>/*

# Check files with specific coding standard
vendor/bin/phpcs --standard=PSR12 src/*

# Automatically fix errors (that can be)
vendor/bin/phpcbf
```

### PHP Compatibility (adds rules to phpcs)

[phpcompatibility/php-compatibility](https://phpqa.io/projects/php-compatibility.html)
is a set of sniffs for PHP_CodeSniffer that checks for PHP version compatibility.
It will allow you to analyse your code for compatibility with higher and lower versions of PHP.

Usage on the project:
- [ ] IDE
- [X] manual check
- [X] pre-commit check
- [X] CI check

```bash
# install
composer require --dev "phpcompatibility/php-compatibility"
vendor/bin/phpcs --config-set installed_paths vendor/phpcompatibility/php-compatibility

# Show rules and coding standards
vendor/bin/phpcs --standard=PHPCompatibility <dir>/<file>

vendor/bin/phpcs src/ -p --standard=PHPCompatibility --runtime-set testVersion 7.2-7.4
vendor/bin/phpcs src/ -p --standard=PHPCompatibility --runtime-set testVersion 7.0-
```

### PHP Copy/Paste Detector (PHCPD)

[sebastian/phpcpd ](https://phpqa.io/projects/phpcpd.html)
is a Copy/Paste Detector for PHP code.

Usage on the project:
- [ ] IDE
- [X] manual check
- [ ] pre-commit check
- [ ] CI check

```bash
# install
composer require --dev sebastian/phpcpd

# usage
vendor/bin/phpcpd --exclude=vendor .
vendor/bin/phpcpd --exclude=vendor . -v
```


### PHP Mess Detector (PHPMD)

[phpmd/phpmd](https://phpqa.io/projects/phpmd.html)
scans PHP source code and looks for potential problems such as possible bugs,
dead code, suboptimal code, and overcomplicated expressions.

Usage on the project:
- [X] IDE
- [X] manual check
- [ ] pre-commit check
- [ ] CI check

```bash
# install
composer require --dev phpmd/phpmd

# Scan PHP source code
vendor/bin/phpmd <file> <format> <ruleset>[,ruleset]
vendor/bin/phpmd <dir>  text     <ruleset>[,ruleset]
vendor/bin/phpmd src/ html cleancode     > phpmdReport_cleancode.html
vendor/bin/phpmd src/ html codesize      > phpmdReport_codesize.html
vendor/bin/phpmd src/ html controversial > phpmdReport_controversial.html
vendor/bin/phpmd src/ html design        > phpmdReport_design.html
vendor/bin/phpmd src/ html naming        > phpmdReport_naming.html
vendor/bin/phpmd src/ html unusedcode    > phpmdReport_unusedcode.html
```


### PHP Parallel Lint

[jakub-onderka/php-parallel-lint](https://phpqa.io/projects/php-parallel-lint.html)
checks the syntax of PHP files,  because "Parse error: syntax error" is not allowed

Usage on the project:
- [ ] IDE
- [X] manual check
- [X] pre-commit check
- [X] CI check

```bash
# install
composer require --dev jakub-onderka/php-parallel-lint

# usage
vendor/bin/parallel-lint ./src/
vendor/bin/parallel-lint ./tests/
vendor/bin/parallel-lint --exclude "vendor" .
```

### SensioLabs Security Checker

[sensiolabs/security-checker](https://phpqa.io/projects/security-checker.html) is
command line tool that checks if your application uses dependencies with known security vulnerabilities.

Usage on the project:
- [ ] IDE
- [X] manual check
- [ ] pre-commit check
- [ ] CI check

```bash
# install
composer require --dev sensiolabs/security-checker

# usage
vendor/bin/security-checker security:check
```


## On 2019-05-03

### Necessary for unit tests:

- "josegonzalez/cakephp-upload": "~3.8"
- "cakephp/migrations": "~1.7"
- "alt3/cakephp-swagger": "1.0.*"

### Necessary for CRUD

- "friendsofcake/crud": "~5.2.2"

### Necessary for using helper (view)

- "gourmet/social-meta": "^1.0"


