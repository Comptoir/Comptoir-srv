# Memento Unit tests : PhpUnit

## You need a db test

as user `superdupont`, do: 

```shell
sudo -u postgres psql
CREATE DATABASE tests WITH TEMPLATE = template0;
ALTER DATABASE tests OWNER TO comptoir;
\q
```
As any user do:

```shell
sudo -u postgres psql -d tests -f COMPTOIR_DB_For_Tests.sql
```


## If you don't have phpUnit installed 

(It should already be installed on Le comptoir.)

As user `root`, do: 

```shell
php composer.phar require --dev phpunit/phpunit:"^5.7"
```
See more on : https://book.cakephp.org/3.0/fr/development/testing.html

## install Xdebug

As any user, do:

```shell
sudo apt-get install -y php-xdebug
```

## Set test database in config/app.php

By default in the `Datasources` section, `test` is commented. Be sure to decommented that part.
If it's commented you can run as any user: 

```
sudo -u comptoir sed -i -e "s/\/\/COMPTOIR-DEBUG//" "${COMPTOIR_DIR}/config/app.php"
```

## Run Unit tests

As any user, at the root of the project do:

To run all tests:

```shell
vendor/bin/phpunit --configuration phpunit.xml --coverage-text
```
To run one test:

```shell
vendor/bin/phpunit --filter test_function_name path/to/file.php 
```

To run a file of tests:
```shell
vendor/bin/phpunit my/tests/filename.php
```

## Read the results

```
 OK, but incomplete or skipped tests!
 Tests: 1, Assertions: 1, Incomplete: 1.
```

```
FAILURES!
Tests: 90, Assertions: 352, Failures: 1, Incomplete: 6.
```

* An incomplete test is denoted by an I in the output of the PHPUnit command-line test runner.
* A test that has been skipped is denoted by an S in the output of the PHPUnit command-line test runner.
* When a test failed a red F is denoted in the outpour of the PHPUnit command-line test runner.
