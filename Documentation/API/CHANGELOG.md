# API CHANGELOG

All notable changes to API will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

--------------------------------

## v1.2.1 - 2023-10-10

### Fixed

- [#980](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/issues/980) Update SILL URL : use `code.gouv.fr/sill/` instead of `sill.etalab.gouv.fr`
- [#974](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/974) use production URLs instead of development URLs for the following properties:
  - `api_documentation -> changelog`
  - `api_documentation -> documentation`


--------------------------------

## v1.2.0 - 2022-12-26

### Added

- add `software -> external_resources -> cnll` (software ID and URL)
- add `software -> license` (will replace  `software -> licence` in next major version)
- add `data_date` (will replace  `date_of_export` in next major version)
- add `data_statistics`
- add `data_documentation -> license`
- add `api_documentation`
  - `api_documentation -> version`
  - `api_documentation -> changelog`
  - `api_documentation -> documentation`
  - `data_documentation -> deprecated`

### Deprecated

- `date_of_export` will be replaced by [ `data_date` ] in next major version
- `number_of_software` will be replaced by [ `data_statistics -> software` ] in next major version
- `software -> licence` will be replaced by [ `software -> license` ] in next major version


--------------------------------

## v1.1.0, 2022.09.10

### Added

- add  `software -> external_resources`
  - **Wikipedia** (software i18n slugs and URLs)
  - **WikiData** (software ID and URL)
  - **FramaLibre** (software slug and URL)
  - **SILL** (software ID and i18n URLs)


--------------------------------

## v1.0.0, 2021.08.10

First release


--------------------------------

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

### Deprecated

### Fixed

### Security

--------------------------------

```
