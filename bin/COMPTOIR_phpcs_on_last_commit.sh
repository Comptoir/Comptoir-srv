#!/usr/bin/env bash

set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=`getopt -o d:c:q -- "$@"`

if [[ $? != 0 ]] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo 'Run PHP_CodeSniffer on files modified in the last commit'
    echo ''
    echo 'usage: ./COMPTOIR_phpcs_on_last_commit.sh [OPTIONS]...'
    echo ''
    echo '  -d <git-dir>     (MANDATORY) Absolute path to git directory, *without* trailing slash, eg "/home/<user>/...'
    echo '  -c <commit-sha>  (MANDATORY) SHA identifier of a commit'
    echo '  -q                           Quickly fail: script stopped at the 1st file that do not pass the tests'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare COMPTOIR_SRV_DIR=
declare COMMIT_SHA=
declare QUICKLY_FAIL=false
while true; do
    case "$1" in
        -d ) COMPTOIR_SRV_DIR="$2"; shift 2 ;;
        -c ) COMMIT_SHA="$2"; shift 2 ;;
        -q ) QUICKLY_FAIL=true; shift ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [[ "${COMPTOIR_SRV_DIR}" = "" || "${COMMIT_SHA}" = "" ]]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Variables
# #############################################################################

COMMIT_FILES=$(git diff-tree --no-commit-id --name-only -r "${COMMIT_SHA}" | tr '\n' ' ')

# #############################################################################
# Actual job
# #############################################################################
if ${QUICKLY_FAIL}  ; then
    # Quickly fail: script stopped at the 1st file that do not pass the tests
    for FILE in ${COMMIT_FILES}
    do
        if [ -f ${FILE} ] ; then
            echo "run ---> vendor/bin/phpcs --report=full ${FILE}"
            ${COMPTOIR_SRV_DIR}/vendor/bin/phpcs --report=full ${FILE}
        else
            echo "no run ---> deleted file: ${FILE}"
        fi
    done
else
    # Longer processing time: all files are tested
    TESTED_FILES=''
    for FILE in ${COMMIT_FILES}
    do
        if [ -f ${FILE} ]
        then
            echo "run ---> vendor/bin/phpcs --report=full ${FILE}"
            TESTED_FILES="${TESTED_FILES} ${FILE}"
        else
            echo "no run ---> deleted file: ${FILE}"
        fi
    done
    ${COMPTOIR_SRV_DIR}/vendor/bin/phpcs --report=full ${TESTED_FILES}
fi


# #############################################################################
RETVAL=$?
if [[ ! "${RETVAL}" ]]
then
    # /!\ I think the following code is never executed because of 'set -o errexit'
    echo -e "To know how to fix, please see Documentation/Mementos/Memento_PHPCS.md \n"
    exit -1
fi
