#!/usr/bin/env bash

TIMESTAMP=$(date +%Y-%m-%d-%Hh%Mm%S)
DUMP_PATH=/home/comptoir/Comptoir-EXPORT
SRV_PATH=/home/comptoir/Comptoir-srv

SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Data_only.sql"
LINK_LAST_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_LAST_Data_only.sql.bz2"
SAVE_WEBROOT_FILES_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Dir_Files.tar.bz2"
LINK_LAST_WEBROOT_FILES_FILE="${DUMP_PATH}/SAVE_COMPTOIR_LAST_Dir_Files.tar.bz2"

if [[ ! -d "${DUMP_PATH}" ]]; then
    mkdir -p "${DUMP_PATH}"
fi

echo "============================================================================="
echo "Exporting SQL data"
pg_dump -U comptoir --no-password --data-only -f "${SAVE_SQL_FILE}"

echo "============================================================================="
echo "Exporting Binary files"
bzip2 ${SAVE_SQL_FILE}
cd ${SRV_PATH}/webroot/img/
tar cvfj ${SAVE_WEBROOT_FILES_FILE} files/

echo "============================================================================="
echo "Update LAST symbolic links "
if [[ -L "${LINK_LAST_SQL_FILE}" ]]
then
    unlink "${LINK_LAST_SQL_FILE}"
fi
ln  -sv  "${SAVE_SQL_FILE}.bz2"  ${LINK_LAST_SQL_FILE}

if [[ -L "${LINK_LAST_WEBROOT_FILES_FILE}" ]]
then
    unlink "${LINK_LAST_WEBROOT_FILES_FILE}"
fi
ln  -sv  "${SAVE_WEBROOT_FILES_FILE}"  ${LINK_LAST_WEBROOT_FILES_FILE}

