#!/usr/bin/env bash

TIMESTAMP=$(date +%Y-%m-%d-%Hh%Mm%S)
DUMP_PATH=/home/comptoir/Comptoir-EXPORT

SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Data_only.sql"
LINK_LAST_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_LAST_Data_only.sql.bz2"

if [ ! -d "${DUMP_PATH}" ]; then
    mkdir -p "${DUMP_PATH}"
fi

echo "============================================================================="
echo "Exporting SQL data"
pg_dump -U comptoir --no-password --data-only -f ${SAVE_SQL_FILE}
bzip2 ${SAVE_SQL_FILE}


echo "============================================================================="
echo "Update LAST symbolic links "
if [[ -L "${LINK_LAST_SQL_FILE}" ]]
then
    unlink "${LINK_LAST_SQL_FILE}"
fi
ln  -sv  "${SAVE_SQL_FILE}.bz2"  ${LINK_LAST_SQL_FILE}
