#!/usr/bin/env bash

set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=$(getopt -o d:t:h:p: --long no-sudo -- "$@")

if [[ $? != 0 ]] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo 'Erase the actual content of Comptoir DB, then import SQL data and binary files.'
    echo ''
    echo 'usage: ./COMPTOIR_import_DB_data_AND_images.sh [OPTIONS]...'
    echo ''
    echo '  -d <comptoir-srv-dir>   (MANDATORY) Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"'
    echo '  -t <timestamp>          (MANDATORY) Timestamp of the files to import, eg "2016-06-28-15h06m42"'
    echo '  -h <database-host>      hostname of the Postgres server (default value: localhost)'
    echo '  -p <dump-path>          Path to Comptoir dump files to import, *without* trailing slash (default value: /home/comptoir/Comptoir-EXPORT). Write access is needed.'
    echo '  --no-sudo               Do not use SUDO (useful for Docker or Vagrant)'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare COMPTOIR_SRV_DIR=
declare TIMESTAMP=
declare DB_HOST=localhost
declare DUMP_PATH=/home/comptoir/Comptoir-EXPORT
declare SUDO=sudo

while true; do
    case "$1" in
        -d )            COMPTOIR_SRV_DIR="$2";  shift 2 ;;
        -t )            TIMESTAMP="$2";         shift 2 ;;
        -h )            DB_HOST="$2";           shift 2 ;;
        -p )            DUMP_PATH="$2";         shift 2 ;;
        --no-sudo )     SUDO="" ;               shift ;;
        -- )                                    shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [[ "${COMPTOIR_SRV_DIR}" = "" || "${TIMESTAMP}" = "" ]]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Variables
# #############################################################################

SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Data_only.sql"
SAVE_BINARY_DATA="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Dir_Files.tar.bz2"
DIR_WEBROOT_FILES_PARENT="${COMPTOIR_SRV_DIR}/webroot/img"
DIR_WEBROOT_FILES="${DIR_WEBROOT_FILES_PARENT}/files"
DIR_WEBROOT_FILES_TMP="${DIR_WEBROOT_FILES_PARENT}/files-tmp"

# #############################################################################
# Actual import
# #############################################################################

# #############################################################################
# Part 1: importing SQL data

echo "Importing SQL data..."
psql \
    -U comptoir \
    --host="${DB_HOST}" \
    -f "${COMPTOIR_SRV_DIR}/config/SQL/COMPTOIR_DB_truncate_tables.sql"

echo "DEBUG ======="
ls "${DUMP_PATH}"

if [[ -e  "${SAVE_SQL_FILE}.bz2" && ! -e "${SAVE_SQL_FILE}" ]]; then
    bunzip2 "${SAVE_SQL_FILE}.bz2"
fi

echo "DEBUG ======="
ls "${DUMP_PATH}"

if [[ ! -e  "${SAVE_SQL_FILE}" ]]; then
    echo "Error: SQL file missing" >&2
    echo "File: ${SAVE_SQL_FILE}" >&2
    exit 1
fi

psql \
    -U comptoir \
    --host="${DB_HOST}" \
    -f "${SAVE_SQL_FILE}"

# #############################################################################
# Part 2: importing binary files

echo "Importing files..."

if [[ ! -e "${SAVE_BINARY_DATA}" ]]; then
    echo "Error: SQL file missing" >&2
    echo "File: ${SAVE_BINARY_DATA}" >&2
    exit 1
fi

if [[ -e "${DIR_WEBROOT_FILES_TMP}" ]]; then
    echo "Removing old backup"
    ${SUDO} rm -rf "${DIR_WEBROOT_FILES_TMP}"
fi
if [[ -e "${DIR_WEBROOT_FILES}" ]]; then
    echo "Archiving previous existing files"
    ${SUDO} mv "${DIR_WEBROOT_FILES}" "${DIR_WEBROOT_FILES_TMP}"
fi

echo "Extracting files"
cd "${DIR_WEBROOT_FILES_PARENT}"
tar xvfj "${SAVE_BINARY_DATA}"


echo ""
echo "Please, now do the following steps:"
echo "1. Run COMPTOIR_import_set_unix_permissions.sh"
echo "2. Verify in the application everything is OK"
echo "3. Then do: sudo rm -rf \"${DIR_WEBROOT_FILES_TMP}/\""
echo ""
