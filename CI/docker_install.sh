#!/usr/bin/env bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# hirak/prestissimo: composer plugin that downloads packages in parallel
# /usr/local/bin/composer global require hirak/prestissimo

# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# All previous instructions up to here could be placed in a custom Docker image to speed up Job execution
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

# Comptoir Installation (debug mode)
/usr/local/bin/composer install --no-progress \
    && cp config/app.default.php config/app.php \
    && sed -i -e "s/\/\/COMPTOIR-DEBUG//" config/app.php \
    && sed -i -e "s/__SALT__/somerandomsalt/" config/app.php \
    && sed -i -e "s/'php',/env('SESSION_DEFAULTS', 'php'),/" config/app.php \
    && cp config/comptoir.default.php config/comptoir.php
