<?php
use Migrations\AbstractMigration;

/**
 * Alter Tags table
 * -----------------------------------
 * Add "external Ids" fields (sill, Wikidata)
 */

class AlterSoftwaresAddExternalIdsField extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('softwares');
        // --------------------------------------
        $table->addColumn('sill', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('wikidata', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('framalibre', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('wikipedia_en', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('wikipedia_fr', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        // --------------------------------------
        $table->update();
    }
}
