<?php
use Migrations\AbstractMigration;

/**
 * Alter TaxonomysSoftwares table
 * -----------------------------------
 * Add an common index on foreign keys (taxonomy_id, software_id, user_id)
 * with the constraint "unique":
 *      ----> to forbid the addition of several lines
 *            with the same triplet of IDs (taxonomy_id, software_id, user_id) .
 */
class AlterTaxonomysSoftwaresAddConstraintOnForeignKeys extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('taxonomys_softwares');
        // --------------------------------------
        $table->addIndex(
            [
                'taxonomy_id',
                'software_id',
                'user_id'
            ],
            ['unique' => true]
        );
        // --------------------------------------
        $table->update();
    }
}
