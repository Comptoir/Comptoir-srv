<?php
use Migrations\AbstractMigration;

class CreateTaxonomysSoftwares extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {

        $table = $this->table('taxonomys_softwares');
        // --------------------------------------
        $table->addColumn('taxonomy_id', 'integer', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('software_id', 'integer', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        // --------------------------------------
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        // --------------------------------------
        // Add a foreign key
        $table->addForeignKey('taxonomy_id', 'taxonomys', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
         ]);
        $table->addForeignKey('user_id', 'users', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        // --------------------------------------
        $table->create();
    }
}
