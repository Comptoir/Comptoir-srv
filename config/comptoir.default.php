<?php

# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
#
# CAUTION: This file is DEPRECATED since 2019-03-19
#
# Please use file template/comptoir.php.epp in repos https://gitlab.adullact.net/Comptoir/puppet-comptoir
#
# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\

use Cake\Core\Configure;

return [

    'OpenGraph' => [
        'url' => Configure::read('App.fullBaseUrl'),
        'type' => 'website',
        'title' => 'Comptoir du libre',
        'description' => __d("Layout", "opengraph.description"),
        'image' => Configure::read('App.fullBaseUrl') . DS . 'img/logos/Logo-CDL_kranken.io-lossless.png',
        'card' => 'summary_large_image'
    ],
    'Mail'=> [
        'Contact' => [
            'UserToUser' => [
                'Prefix' => '[Comptoir du Libre] : '
            ],
        ]
    ],

# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
#
# CAUTION: This file is DEPRECATED since 2019-03-19
#
# Please use file template/comptoir.php.epp in repos https://gitlab.adullact.net/Comptoir/puppet-comptoir
#
# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\

//    Only use for the index page
    'Categories'=>[
        'PickOfTheMonth'=>[
            9,  // Asqatasun
            115, // Départements et Notaires
            10, // Lutece
            162 // Thingsboard
        ]
    ],
//    Some kind of ACL for views
    'ACL' => [
        'TaxonomysSoftwares' => [
            'mappingForm' => [
                'Administration' => true,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
        ],
        'Users' => [
            'add' => [
                'Administration' => false,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => false,
            ],
            'contact' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => false,
                'Unknown' => true,
            ],
        ],
        'Softwares' => [
            'edit' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'add' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'usersSoftware' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => false,
                'Unknown' => true,
            ],
            'addReview' => [
                'Administration' => true,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
            'servicesProviders' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'deleteUsersSoftware' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'deleteServicesProviders' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'workswellSoftwares' => [
                'Administration' => false,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
            'alternativeTo' => [
                'Administration' => false,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
        ]
    ],
    "Piwik" => <<<EOF

EOF

# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\
#
# CAUTION: This file is DEPRECATED since 2019-03-19
#
# Please use file template/comptoir.php.epp in repos https://gitlab.adullact.net/Comptoir/puppet-comptoir
#
# /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\


];
