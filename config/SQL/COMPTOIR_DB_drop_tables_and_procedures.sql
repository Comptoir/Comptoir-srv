DROP TABLE IF EXISTS
    licence_types,
    licenses,
    panels,
    phinxlog,
    raw_metrics_softwares,
    relationship_types,
    relationships,
    relationships_softwares,
    relationships_softwares_users,
    relationships_users,
    requests,
    reviews,
    screenshots,
    softwares,
    softwares_statistics,
    softwares_tags,
    statistics_averages,
    tags,
    user_types,
    users
CASCADE ;

DROP FUNCTION IF EXISTS calculate_score_for_softwares_statistics() CASCADE;
DROP FUNCTION IF EXISTS calculated_points_from_manivelle() CASCADE;
DROP FUNCTION IF EXISTS calculated_points_from_server() CASCADE;
DROP FUNCTION IF EXISTS points_for_reviews(integer) CASCADE;
DROP FUNCTION IF EXISTS points_for_screenshots(integer) CASCADE;
DROP FUNCTION IF EXISTS points_for_users_of(integer) CASCADE;
DROP FUNCTION IF EXISTS raw_metrics_from_manivelle() CASCADE;
DROP FUNCTION IF EXISTS score_age(integer, integer, integer) CASCADE;
DROP FUNCTION IF EXISTS score_delta_commit_12m(real, integer) CASCADE;
DROP FUNCTION IF EXISTS score_delta_commit_1m(real, integer) CASCADE;
DROP FUNCTION IF EXISTS score_hight_commiter_percent(real, integer) CASCADE;
DROP FUNCTION IF EXISTS score_number_of_contributors(real, integer) CASCADE;

