#!/usr/bin/env bash

# stop at first error
set -e

VAGRANT_HOME="/home/vagrant"

# enable debug mode
bash "${VAGRANT_HOME}/00_bin/01_switch_debug_TRUE.sh"

# launch unit tests
cd /home/comptoir/Comptoir-srv/
sudo -u comptoir vendor/bin/phpunit ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10};

