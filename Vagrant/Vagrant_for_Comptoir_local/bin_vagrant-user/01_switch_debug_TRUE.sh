#!/usr/bin/env bash

# stop at first error
set -e

#  use /home/comptoir/Comptoir-srv/bin/cake
FILE="/home/comptoir/Comptoir-srv/config/app.php"
sudo -u comptoir sed -i -e "s/'debug' => false,/'debug' => true,/" "${FILE}"
echo "---> Debug mode is now enabled"
