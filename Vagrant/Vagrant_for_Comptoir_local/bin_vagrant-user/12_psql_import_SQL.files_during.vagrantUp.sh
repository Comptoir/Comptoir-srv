#!/usr/bin/env bash

# stop at first error
set -e

# Import SQL files in database
cd "/home/vagrant/00_bin/import_SQL.files_during.vagrantUp"
FILES=$(ls)
for FILE in ${FILES}
do
    if [ -f ${FILE} ] && [[ ${FILE#*.} = 'sql' ]] ; then
        echo "--- Import ${FILE#*.} file: '${FILE}'"
        echo "-------------> sudo -u comptoir psql < '${FILE}'"
        sudo -u comptoir psql < "${FILE}"
    fi
done



