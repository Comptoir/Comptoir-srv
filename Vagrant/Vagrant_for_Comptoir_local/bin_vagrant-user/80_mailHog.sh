#!/usr/bin/env bash
############################################################################
# Using  MailHog  ---> email testing tool for developers
############################################################################
#   - Without any options: installs and launches MailHog inside the VM.
#   - With the --server option: uses your own MailHog server (outside the VM).
#   - In both cases, the webapp configuration is modified to use MaiHog.
#   - The --help option: display documentation
#   - The --quit option: stops MailHog server if necessary
#                        and disables SMTP in the webapp configuration
############################################################################
# stop at first error
set -e

#### CONFIG ################################################################
COMTPOIR_CONFIG_FILE="/home/comptoir/Comptoir-srv/config/app.php"
VAGRANT_HOME="/home/vagrant"
CURRENT_CONFIG_FILE="${VAGRANT_HOME}/.mailHog.config"
BIN_PATH="${VAGRANT_HOME}/go/bin/"
MAILHOG_PATH="${BIN_PATH}/MailHog"
MAILHOG_HOST="localhost"
SMTP_DEFAULT_PORT=1025     # MailHog SMTP default port
GUI_DEFAULT_PORT=8025      # MailHog GUI default port

#### USEFUL VARIABLES ######################################################
SCRIPT=`basename ${BASH_SOURCE[0]}`

#Set fonts for Help
    BOLD=$(tput bold)
  # STOT=$(tput smso)
    UNDR=$(tput smul)
    REV=$(tput rev)
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
  # YELLOW=$(tput setaf 3)
  # MAGENTA=$(tput setaf 5)
  # WHITE=$(tput setaf 7)
    NORM=$(tput sgr0)

####### FUNCTIONS #########################################################
function header () {
    echo ''
    echo " Using ${BOLD}${REV} MailHog ${NORM} ---> email testing tool for developers"
    echo " --------------------------------------------------------------"
}

function usage () {
    echo " ${BOLD}Help documentation${NORM}"
    echo " - Without any options: installs and launches MailHog inside the VM."
    echo " - With the ${BOLD}--server${NORM} option: uses your own MailHog server (outside the VM)."
    echo " - In both cases, the webapp configuration is modified to use MaiHog."
    echo " - The ${BOLD}${RED}--quit${NORM} option: stops MailHog server if necessary "
    echo "                      and disables SMTP in the webapp configuration"
    echo " "
    echo " ${BOLD}${SCRIPT}${NORM} [OPTIONS]"
    echo " --------------------------------------------------------------"
    echo "   ${BOLD}-s${NORM} | --server  <server>   MailHog host (outside the VM)"
    echo "   ${BOLD}-p${NORM} | --port    <smptPort> MailHog SMTP port, see (1) - Default is '1025'"
    echo "   ${BOLD}-g${NORM} | --gui     <guiPort>  MailHog GUI port,  see (1) - Default is '8025'"
    echo "   ${BOLD}-h${NORM} | --help              ${REV} Show this help                     ${NORM}"
    echo "   ${BOLD}-i${NORM} | --interactive        Run interactive mode"
    echo "   ${BOLD}${RED}-q${NORM} | --quit               Stop using MailHog"
    echo " "
    echo "                  (1) to be used only with option ${BOLD}-s${NORM} | --server "
    echo " --------------------------------------------------------------"
    exit 2
}

function fail() {
    local ERROR_MSG=$*
    echo ""
    echo " ${RED}-----------------------------------------------------------${NORM}"
    echo " ${BOLD}FAILURE${NORM}:"
    echo " ${RED}${ERROR_MSG}${NORM}"
    echo " ${RED}-----------------------------------------------------------${NORM}"
    usage
    exit 0
}

# Check URL (return HTTP code)
function check_URL() {
    local URL="$1"
    set +e
    local RESULT=$(curl -o /dev/null --silent --write-out '%{http_code}\n' "${URL}")
    set -e
    echo "${RESULT}"
}

function readCurrentConfig() {
    if [[ ! -f ${CURRENT_CONFIG_FILE} ]] ; then
        fail "File ${CURRENT_CONFIG_FILE} does not exist."
    fi
    CURRENT_CONFIG=$(cat "${CURRENT_CONFIG_FILE}")
    CURRENT_CONFIG_HOST=$(echo "${CURRENT_CONFIG}" | cut -f1 -d:)
    CURRENT_CONFIG_SMTP_PORT=$(echo "${CURRENT_CONFIG}" | cut -f2 -d:)
    CURRENT_CONFIG_GUI_PORT=$(echo "${CURRENT_CONFIG}" | cut -f3 -d:)
}

# Kill mailhog (VM process)
function killMailHog() {
    set +e
    ps | grep MailHog 1>/dev/null 2>&1
    local RETVAL=$?
    set -e
    if [[ "${RETVAL}" -ne 1 ]]
    then
        echo " ----> Kill mailhog (VM process)"
        killall ${MAILHOG_PATH}
    fi
}

# Install MailHog (Option 1) ----> Slow
# from source code (master branch) using Go
function installMailHogFromSourceCode() {
    # Install Go (prerequisite for MailHog)
    # ---> documentation: https://github.com/golang/go/wiki/Ubuntu
    local GO_PATH="/usr/bin/go"
    if [ ! -f ${GO_PATH} ] ; then
        echo ''; echo ' ----> Install Go (prerequisite for MailHog)'
        sudo -i -u add-apt-repository -y ppa:longsleep/golang-backports
        sudo -i -u apt update
        sudo -i -u root apt-get install -yqq golang-go
    else
        echo " ----> Go is already installed : ${GO_PATH}"
    fi

    ## Install MailHog
    # ---> documentation: https://github.com/mailhog/MailHog
    if [ ! -f ${MAILHOG_PATH} ] ; then
        echo ''; echo ' ----> Install MailHog'
        cd
        go get -v github.com/mailhog/MailHog
    else
        echo " ----> MailHog is already installed : ${MAILHOG_PATH}"
    fi
}

# Install MailHog (Option 2) ----> Fast
# from the binary of the last realease
function installMailHogFromBinaryRelease() {
    if [ ! -f ${MAILHOG_PATH} ] ; then
        echo ''; echo ' ----> Install MailHog'
        local BIN_RELEASE_URL="https://github.com/mailhog/MailHog/releases/download/v1.0.0/MailHog_linux_amd64"
        mkdir -p "${BIN_PATH}"
        set +e
        /usr/bin/wget ${BIN_RELEASE_URL} -O "${MAILHOG_PATH}"
        local RETVAL=$?
        set -e
        if [[ "${RETVAL}" -ne 0 ]]
        then
            rm -vrf "${BIN_PATH}"
            fail "----> Download failure"
        else
            chmod +x "${MAILHOG_PATH}"
            echo " ----> MailHog is now installed"
        fi
    else
        echo " ----> MailHog is already installed : ${MAILHOG_PATH}"
    fi
}

# Install MailHog if necessary
# and launch it if it is not started
function launchMailHogInTheVM() {

    # Option 1 ----> Slow
    # install MailHog from source code (master branch) using go
    # see ---> installMailHogFromSourceCode()

    # Option 2 ---> Fast
    # install MailHog from the binary of the last realease
    installMailHogFromBinaryRelease

    ## Run MailHog
    local URL="http://127.0.0.1:${GUI_DEFAULT_PORT}"
    local RESULT=$(check_URL ${URL})
    if [[ "${RESULT}" != "200" ]]; then
        echo " ----> Start MailHog"
        ${MAILHOG_PATH} > "${VAGRANT_HOME}/mailHog.log" 2> "${VAGRANT_HOME}/mailHog_error.log"  &
    else
        echo " ----> MailHog is already running"
    fi
    SMTP_PORT="${SMTP_DEFAULT_PORT}"
    GUI_PORT=${GUI_DEFAULT_PORT}""
}

function runNoInteractiveMode () {
    if [[ "${PARAM_MAILHOG_HOST}" != "" ]] ;then
        echo " ----> Use another MailHog instance"
        MAILHOG_HOST="${PARAM_MAILHOG_HOST}"
        GUI_PORT="${PARAM_GUI_PORT}"
        SMTP_PORT="${PARAM_SMTP_PORT}"
        killMailHog
    else
        echo " ----> DEFAULT : start MailHog in the VM"
        launchMailHogInTheVM;
    fi
}

function runInteractiveMode () {
    echo " ----> Interactive mode"
    echo " By default we start MailHog in the VM."
    echo -n " Do you want to use your own MailHog instance?   (y/n) "
    read answer
    if [ "$answer" != "${answer#[Yy]}" ] ;then
        interactiveConfigForAnotherMailHog;
    else
        echo " ----> DEFAULT : start MailHog in the VM"
        launchMailHogInTheVM;
    fi
}

# Interactive configuration for another instance of MailHog
function interactiveConfigForAnotherMailHog() {
    echo -n " - What is your MailHog host? "
    read MAILHOG_HOST
    echo -n " - What is the SMTP port of Mailhog? "
    read SMTP_PORT
    echo -n " - What is the GUI port of Mailhog? "
    read GUI_PORT
    killMailHog
}

# Check if MailHog responds correctly
function checkMailHogResponds() {
    # Check if MailHog GUI responds correctly
    local URL="http://${MAILHOG_HOST}:${GUI_PORT}"
    local RESULT=$(check_URL ${URL})
    if [[ "${RESULT}" != "200" ]]; then
        fail "----> MailHog GUI - Error: '${URL}' does not respond"
    fi

    # Check if MailHog SMTP responds correctly
    set +e
    nc -z ${MAILHOG_HOST} ${SMTP_PORT}
    if [[ $? -ne 0 ]]
    then
        fail "----> MailHog SMTP - Error: '${MAILHOG_HOST} ${SMTP_PORT}' does not respond"
    fi
    set -e
}

# Configure "Comptoir" webapp
function configureWebApp() {
    echo " ----> Configure Comptoir webapp"
    sudo -u comptoir sed -i -e "s/'className' => 'Debug'/'className' => 'Smtp'/"            "${COMTPOIR_CONFIG_FILE}"
    sudo -u comptoir sed -i -e "s/'host' => 'localhost',/'host' => '${MAILHOG_HOST}',/"     "${COMTPOIR_CONFIG_FILE}"
    sudo -u comptoir sed -i -e "s/'port' => 25,/'port' => ${SMTP_PORT},/"                   "${COMTPOIR_CONFIG_FILE}"
    sudo -u comptoir sed -i -e "s/'port' => ,/'port' => ${SMTP_PORT},/"                     "${COMTPOIR_CONFIG_FILE}"
    sudo -u comptoir sed -i -e "s/'port' => ${SMTP_DEFAULT_PORT},/'port' => ${SMTP_PORT},/" "${COMTPOIR_CONFIG_FILE}"
    if [[ -f ${CURRENT_CONFIG_FILE} ]] ; then  # OLD config
        readCurrentConfig
        sudo -u comptoir sed -i -e "s/'host' => '${CURRENT_CONFIG_HOST}',/'host' => '${MAILHOG_HOST}',/" "${COMTPOIR_CONFIG_FILE}"
        sudo -u comptoir sed -i -e "s/'port' => ${CURRENT_CONFIG_SMTP_PORT},/'port' => ${SMTP_PORT},/"   "${COMTPOIR_CONFIG_FILE}"
    fi

    # Backup config
    echo "${MAILHOG_HOST}:${SMTP_PORT}:${GUI_PORT}" > "${CURRENT_CONFIG_FILE}"
}

# Stop using MailHog
function stop() {
    echo " --> Stop using MailHog"
    killMailHog
    echo " ----> Re-configure Comptoir webapp"
    sudo -u comptoir sed -i -e "s/'className' => 'Smtp'/'className' => 'Debug'/" "${COMTPOIR_CONFIG_FILE}"
    if [[ -f ${CURRENT_CONFIG_FILE} ]] ; then  # OLD config
        readCurrentConfig
        sudo -u comptoir sed -i -e "s/'host' => '${CURRENT_CONFIG_HOST}',/'host' => 'localhost',/" "${COMTPOIR_CONFIG_FILE}"
        sudo -u comptoir sed -i -e "s/'port' => ${CURRENT_CONFIG_SMTP_PORT},/'port' => 25,/"   "${COMTPOIR_CONFIG_FILE}"
    fi
    echo ''
    exit 2
}

function displayCurrentConfig () {
    readCurrentConfig
    local INFO_PORT=''
    if [[ ${CURRENT_CONFIG_HOST} = 'localhost' ]] ; then
        local INFO_PORT=" --> Use 'vagrant port' to see port mapping"
    fi
    echo " -------------------------------------------------------"
    echo " MailHog Host ........ ${CURRENT_CONFIG_HOST}"
    echo " MailHog SMTP port ... ${CURRENT_CONFIG_SMTP_PORT}"
    echo " MailHog GUI  port ... ${CURRENT_CONFIG_GUI_PORT}${INFO_PORT}"
    echo " MailHog Webmail ..... ${GREEN}http://${CURRENT_CONFIG_HOST}:${CURRENT_CONFIG_GUI_PORT}${NORM}"
    echo " -------------------------------------------------------"
}
###########################################################################
###########################################################################
header

# CLI option
TEMP=`getopt -o s:p:g:iqh --long server:,port:,gui:,help,quit,interactive -- "$@"`
if [[ $? != 0 ]] ; then
    echo "Terminating..." >&2 ;
    exit 1 ;
fi
# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare INTERACTIVE_MODE=false
declare PARAM_MAILHOG_HOST=''
declare PARAM_SMTP_PORT="${SMTP_DEFAULT_PORT}"
declare PARAM_GUI_PORT="${SMTP_DEFAULT_PORT}"
while true; do
    case "$1" in
        -h | --help         ) usage;                    shift ;;
        -q | --quit         ) stop;                     shift ;;
        -i | --interactive  ) INTERACTIVE_MODE=true;    shift ;;
        -s | --server       ) PARAM_MAILHOG_HOST="$2";  shift 2 ;;
        -p | --port         ) PARAM_SMTP_PORT="$2";     shift 2 ;;
        -g | --gui          ) PARAM_GUI_PORT="$2";      shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# Interactive mode ?
if ${INTERACTIVE_MODE}  ; then
    runInteractiveMode
else
    runNoInteractiveMode
fi
checkMailHogResponds  # FAIL if MailHog not works
configureWebApp       # Configure "Comptoir" webapp
displayCurrentConfig
###########################################################################
###########################################################################
