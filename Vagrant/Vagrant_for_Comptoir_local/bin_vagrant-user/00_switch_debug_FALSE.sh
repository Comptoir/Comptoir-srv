#!/usr/bin/env bash

# stop at first error
set -e

# disable debug mode
FILE="/home/comptoir/Comptoir-srv/config/app.php"
sudo -u comptoir sed -i -e "s/'debug' => true,/'debug' => false,/" "${FILE}"
echo "---> Debug mode is now disabled"
