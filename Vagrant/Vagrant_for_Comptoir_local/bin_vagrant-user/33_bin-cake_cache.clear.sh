#!/usr/bin/env bash

# stop at first error
set -e

#  use /home/comptoir/Comptoir-srv/bin/cake
cd /home/comptoir/Comptoir-srv/
sudo -u comptoir bin/cake cache clear_all;
