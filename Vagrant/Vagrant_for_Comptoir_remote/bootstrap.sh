#!/usr/bin/env bash

# stop at first error
set -e

# Variables
declare DEBIAN_FRONTEND noninteractive
###     COMPTOIR_HOME ==> Absolute directory containing the Comptoir-srv, *without* trailing slash, eg "/home/comptoir"
declare COMPTOIR_HOME="/home/comptoir"
###     COMPTOIR_DIR ==> Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"
declare COMPTOIR_DIR="${COMPTOIR_HOME}/Comptoir-srv"
###     COMPTOIR_DIR_DATASET ==> Absolute directory to copy the needed files to insert a dataset, *without* trailing slash, eg "/home/comptoir/Dataset"
declare COMPTOIR_DIR_DATASET="${COMPTOIR_HOME}/Dataset"
###     COMPTOIR_REVISION ==> The revision number of a particular commit or tag, eg "b444c25b232824c9edf94bfc733d39ddc7eaabb2" or "tags/<tag_name>"
declare COMPTOIR_REVISION=""
###     DATASET_TIMESTAMP ==> Timestamp of the files of data to import, eg "2016-06-28-15h06m42"
declare DATASET_TIMESTAMP="2018-09-12-16h13m32"

# PREREQUISITES
apt-get update

## Ubuntu packages
echo "===== Ubuntu packages"
apt-get install -y \
    apache2 \
    git \
    libapache2-mod-php \
    libicu55 \
    php \
    php-curl \
    php-intl \
    php-mbstring \
    php-pgsql \
    php-xml \
    postgresql \
    unzip \
    zlib1g

## PHP > timezone + max_upload
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
done

## Install Composer
echo "===== Install Composer"
php -r "readfile('https://getcomposer.org/installer');" \
    | sudo php -- --install-dir=/usr/local/bin --filename=composer

## Configure locale
sudo locale-gen fr_FR.UTF-8

## Postgres authentication
echo "===== Postgres authentication"
sudo sed -i 's/local   all             all                                     peer/local   all             all                                     password/' /etc/postgresql/9.5/main/pg_hba.conf

sudo service postgresql restart

## Create system user
echo "===== Create system user"
useradd -d "${COMPTOIR_HOME}" -m -s /bin/bash comptoir

# Installation Comptoir
echo "===== Installation Comptoir"
cd "${COMPTOIR_HOME}"
sudo -u comptoir git clone https://gitlab.adullact.net/Comptoir/Comptoir-srv.git

## Isolate datasets from git repository
echo "===== Isolate datasets from git repository"
sudo -u comptoir cp -r "${COMPTOIR_DIR}/tests/Datasets/" "${COMPTOIR_DIR_DATASET}/"
sudo -u comptoir cp "${COMPTOIR_DIR}/bin/COMPTOIR_import_DB_data_AND_images.sh" "${COMPTOIR_DIR_DATASET}/"
sudo -u comptoir mkdir -p "${COMPTOIR_DIR_DATASET}/config/SQL/" && cp "${COMPTOIR_DIR}/config/SQL/COMPTOIR_DB_truncate_tables.sql" "${COMPTOIR_DIR_DATASET}/config/SQL/"

## OPTIONAL Installation Comptoir: set the COMPTOIR_VERSION to install an older version
echo "===== OPTIONAL Installation Comptoir"
if [ ! -z "${COMPTOIR_REVISION}" ]
    then
    cd "${COMPTOIR_DIR}"
    sudo -u comptoir git checkout "${COMPTOIR_REVISION}"
fi

## POSTGRESQL Create user
echo "===== POSTGRESQL Create user"
sudo -u postgres psql -c "CREATE USER comptoir WITH PASSWORD 'comptoir'"

## POSTGRESQL Set .pgpass file
echo "===== POSTGRESQL Set .pgpass file"
PGPASSFILE="${COMPTOIR_HOME}/.pgpass"
sudo -u comptoir touch "${PGPASSFILE}"
sudo -u comptoir chmod 0600 "${PGPASSFILE}"
sudo -u comptoir cat >"${PGPASSFILE}" <<EOF
# hostname:port:database:username:password
localhost:5432:comptoir:comptoir:comptoir
EOF

## POSTGRESQL Create DB
echo "===== POSTGRESQL Create DB"
sudo "${COMPTOIR_DIR}/bin/COMPTOIR_create_DB_database_and_set_ownership.sh" -d "${COMPTOIR_DIR}"

## POSTGRESQL Create tables and procedures
echo "===== POSTGRESQL Create tables and procedures"
sudo -u comptoir "${COMPTOIR_DIR}/bin/COMPTOIR_create_DB_tables_and_procedures.sh" -d "${COMPTOIR_DIR}"

## Composer install Comptoir
echo "===== Composer install Comptoir"
cd "${COMPTOIR_DIR}"
sudo -u comptoir /usr/local/bin/composer --no-progress install

## APP.PHP Define Email Transport
### Something TODO for smtp

## APP.PHP Configure database credentials
echo "===== APP.PHP Configure database credentials"
sudo -u comptoir sed -i -e "s/\/\/COMPTOIR-DEBUG//" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/__SALT__/somerandomsalt/" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/'php',/env('SESSION_DEFAULTS', 'php'),/" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/'host' => 'localhost',/'host' => 'smtp',/" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/'from' => 'barman@comptoir-du-libre.org',/'from' => 'barman-DEV@comptoir-du-libre.org',/" "${COMPTOIR_DIR}/config/app.php"

## COMPTOIR.PHP Configure
echo "===== COMPTOIR.PHP Configure"
sudo -u comptoir cp "${COMPTOIR_DIR}/config/comptoir.default.php" "${COMPTOIR_DIR}/config/comptoir.php"

cd /home/comptoir
chmod -R o-w Comptoir-srv/*
for i in webroot logs tmp; do
    chown -R www-data.comptoir "Comptoir-srv/${i}"
    find "Comptoir-srv/$i" -type d -exec sudo chmod 775 {} \;
    find "Comptoir-srv/$i" -type f -exec sudo chmod 664 {} \;
done

## APACHE Vhost preparation
#echo "127.0.0.1	comptoir.example.com" >> /etc/hosts

## APACHE Vhost creation "comptoir.example"
echo "===== APACHE Vhost creation comptoir.example"
cat > /etc/apache2/sites-available/comptoir-srv.conf << EOF
<VirtualHost *:80>
    ServerName comptoir.example.com
    ServerAdmin webmaster@localhost
    DocumentRoot ${COMPTOIR_DIR}/webroot/

EOF

echo '    ErrorLog ${APACHE_LOG_DIR}/comptoir.example.com.log' >> /etc/apache2/sites-available/comptoir-srv.conf
echo '    CustomLog ${APACHE_LOG_DIR}/comptoir.example.com.log combined' >> /etc/apache2/sites-available/comptoir-srv.conf

cat >> /etc/apache2/sites-available/comptoir-srv.conf << EOF

    <Directory "${COMPTOIR_DIR}/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
EOF

## APACHE enable mod_rewrite and new vhosts
echo "===== APACHE enable mod_rewrite and new vhosts"
sudo a2enmod rewrite
sudo a2ensite comptoir-srv.conf
sudo service apache2 reload
sudo a2dissite 000-default.conf
sudo service apache2 reload

## Puts the data files back in place
echo "===== DATA FILES back in place"
if [ ! -z "${COMPTOIR_REVISION}" ]
    then
        sudo -u comptoir mkdir -p "${COMPTOIR_DIR_DATASET}/tests/Datasets" && cp -r "${COMPTOIR_DIR_DATASET}/Dataset02" "${COMPTOIR_DIR}/tests/Datasets/"
        sudo -u comptoir mkdir -p "${COMPTOIR_DIR}/bin/" && cp "${COMPTOIR_DIR_DATASET}/COMPTOIR_import_DB_data_AND_images.sh" "${COMPTOIR_DIR}/bin/"
        sudo -u comptoir mkdir -p "${COMPTOIR_DIR}/config/SQL/" && cp "${COMPTOIR_DIR_DATASET}/config/SQL/COMPTOIR_DB_truncate_tables.sql" "${COMPTOIR_DIR}/config/SQL/"
fi

## INSERT demo data
echo "===== COMPTOIR: Import data"

sudo -u comptoir "${COMPTOIR_DIR}/bin/COMPTOIR_import_DB_data_AND_images.sh" \
    -d "${COMPTOIR_DIR}" \
    -t "${DATASET_TIMESTAMP}" \
    -h localhost \
    -p "${COMPTOIR_DIR}/tests/Datasets/Dataset02" \

##  Backup softwares versions
INFO_FILE=/90_softwares-version.txt
echo "--- OS -------------"    >> ${INFO_FILE}             && \
cat /etc/os-release            >> ${INFO_FILE}  && echo '' && \
echo "--- PostgreSQL ------"   >> ${INFO_FILE}             && \
psql --version                 >> ${INFO_FILE}  && echo '' && \
echo "---  Apache -----------" >> ${INFO_FILE}             && \
apache2 -v                     >> ${INFO_FILE}             && \
echo "--- PHP ---------"       >> ${INFO_FILE}  && echo '' && \
php  --version                 >> ${INFO_FILE}
