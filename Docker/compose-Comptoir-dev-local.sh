#!/usr/bin/env bash

# Shell script to build and run Comptoir with local files in a Docker container

# /!\ must be run from this very directory

# ====== Step 0 =====
# Remove previous containers and images
docker-compose -f ./compose-Comptoir-dev-local.yml rm -s -v

# ====== Step 1 =====
# Copy source code files into Docker scope, so they can be included within the Docker image
rsync -av --delete --exclude-from=./Comptoir-dev-local/rsync_exclude.txt ../ ./Comptoir-dev-local/Comptoir-source-code/

# ====== Step 2 =====
# Build
docker-compose -f ./compose-Comptoir-dev-local.yml build

# ====== Step 3 =====
# Run
docker-compose -f ./compose-Comptoir-dev-local.yml up -d
echo -en "\n"
echo -en "Go to http://localhost:8080/ \n"
echo -en "Thanks for being here :) \n"


