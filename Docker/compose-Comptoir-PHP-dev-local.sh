#!/usr/bin/env bash

# Shell script to build and run Comptoir with local files in a Docker container
# /!\ must be run from this very directory

set -e

function clean() {
    # ====== Step 0 =====
    # Remove previous containers and images
    docker-compose -f ./compose-Comptoir-PHP-dev-local.yml rm -s -v
}

function copy-source() {
    # ====== Step 1 =====
    # Copy source code files into Docker scope, so they can be included within the Docker image
    rsync \
        -av --delete --exclude-from=./Comptoir-PHP-dev-local/rsync_exclude.txt \
        ../ \
        ./Comptoir-PHP-dev-local/Comptoir-source-code/
}

function container-build() {
    # ====== Step 2 =====
    # Build
    docker-compose -f ./compose-Comptoir-PHP-dev-local.yml build
}

function container-run() {
    # ====== Step 3 =====
    # Run
    docker-compose -f ./compose-Comptoir-PHP-dev-local.yml up -d \
    && echo -en "\n" \
    && echo -en "Go to http://localhost:8080/ \n" \
    && echo -en "Thanks for being here :) \n"
}

clean \
    && copy-source \
    && container-build \
    && container-run
