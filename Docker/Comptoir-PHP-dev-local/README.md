# Docker image: Comptoir-PHP-dev-local

This image is used when developing Comptoir.

It uses the source code place on the developer's host.

By now the source code is *copied* into the container. This means you need to recreate the container when source code
have been modified. One day, it'll be improved by using a Docker Volume instead od a COPY command.
