#!/usr/bin/env bash

set -e

declare DATASET_REL_PATH="tests/Datasets/Dataset02"
declare DATASET_TIMESTAMP="2018-09-12-16h13m32"
declare COMPTOIR_ROOT="/var/www/html"

# Create tables and procedures
echo "===== COMPTOIR: Create tables and procedures"
${COMPTOIR_ROOT}/bin/COMPTOIR_create_DB_tables_and_procedures.sh -h postgres -d ${COMPTOIR_ROOT}

# Import data
echo "===== COMPTOIR: Import data"
${COMPTOIR_ROOT}/bin/COMPTOIR_import_DB_data_AND_images.sh \
    -d "${COMPTOIR_ROOT}" \
    -t "${DATASET_TIMESTAMP}" \
    -h postgres \
    -p "${COMPTOIR_ROOT}/${DATASET_REL_PATH}" \
    --no-sudo

# Set unix permissions
echo "===== COMPTOIR: Set unix permissions"
${COMPTOIR_ROOT}/bin/COMPTOIR_import_set_unix_permissions.sh \
    -d "${COMPTOIR_ROOT}" \
    --no-sudo

# Finally, run Apache
echo "===== Finally, run Apache"
apache2-foreground
