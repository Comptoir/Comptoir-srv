# Docker image: Comptoir-Ubuntu-base

This image is used to save time when building the actual image for Comptoir app.

## How to upload image to Gitlab Docker Registry


```
docker login gitlab.adullact.net:4567
docker build -t gitlab.adullact.net:4567/comptoir/comptoir-srv/comptoir-ubuntu-base:v1.0.0-rc.2 .
docker images
```

check that the image has been built.

```
docker push gitlab.adullact.net:4567/comptoir/comptoir-srv/comptoir-ubuntu-base:v1.0.0-rc.2
```

Be careful to the tag of the version. Set up the right version you want to use.
If no tag specified, by default it will be tag `latest`.
One time the build of the image will be done directly on the gitlab.
