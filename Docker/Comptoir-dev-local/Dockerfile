FROM gitlab.adullact.net:4567/comptoir/comptoir-srv/comptoir-ubuntu-base:v1.0.0-rc.2

ENV DEBIAN_FRONTEND noninteractive

####################################################
# Comptoir specifics                               #
####################################################
WORKDIR /var/www/html
RUN rm -rf /var/www/html/*
COPY Comptoir-source-code/ /var/www/html/
RUN composer install --no-progress \
    && cp config/app.default.php config/app.php \
    && sed -i -e "s/\/\/COMPTOIR-DEBUG//" config/app.php \
    # Inject some non random salt for this example
    && sed -i -e "s/__SALT__/somerandomsalt/" config/app.php \
    # Make sessionhandler based on env file
    && sed -i -e "s/'php',/env('SESSION_DEFAULTS', 'php'),/" config/app.php \
    && cp config/comptoir.default.php config/comptoir.php \
    && tar zxvf tests/TestFiles/FunctionalsTests/files.tar.gz -C webroot/img/ \
    && groupadd comptoir \
    && usermod -aG comptoir root \
    && usermod -aG comptoir www-data \
    && chgrp -R comptoir logs tmp webroot \
    && chmod -R g+rw logs tmp webroot \
    && chmod g+s tmp/cache/persistent/

####################################################
# Apache: adjust conf and run                      #
####################################################
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]
