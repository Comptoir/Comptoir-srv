# Comptoir-du-libre with Docker

## Pre-requisites

1. [Install docker](https://docs.docker.com/engine/installation/linux/ubuntu/#install-using-the-repository)
2. Clone the [`Comptoir-srv` repository](https://gitlab.adullact.net/Comptoir/Comptoir-srv.git) in your project directory

## Usage

1. Get into the `Docker` directory of the project you just cloned
2. run `./compose-Comptoir-PHP-dev-local.sh`
3. Now go to [http://localhost:8080](http://localhost:8080)

Enjoy ! :)

## View logs of the container

```
docker logs -f docker_comptoir_php_dev_local_1
```

## Enter into the container

```
docker exec -it docker_comptoir_php_dev_local_1 /bin/bash
```

