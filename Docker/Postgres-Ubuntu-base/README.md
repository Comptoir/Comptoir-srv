# Docker image: POSTGRES-Ubuntu-base

This image is used to save time when building the actual image for Comptoir app.

## How to upload image to Gitlab Docker Registry

```
docker login gitlab.adullact.net:4567
docker build -t gitlab.adullact.net:4567/comptoir/comptoir-srv/postgres-ubuntu-base:v1.0.0 .
docker push gitlab.adullact.net:4567/comptoir/comptoir-srv/postgres-ubuntu-base:v1.0.0
```
