--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.13
-- Dumped by pg_dump version 9.5.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: calculate_score_for_softwares_statistics(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.calculate_score_for_softwares_statistics() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE calculated_score REAL;

    DECLARE  stats_line       softwares_statistics%ROWTYPE;
    DECLARE  stats            softwares_statistics.software_id%TYPE;

    DECLARE  total_softwares  INTEGER;

BEGIN

    SELECT count(id)
    INTO total_softwares
    FROM softwares_statistics;

    FOR stats_line IN SELECT *
                      FROM softwares_statistics LOOP

        /* Get points */
        calculated_score :=
        (stats_line.last_commit_age +
         stats_line.project_age +
         stats_line.delta_commit_one_month +
         stats_line.delta_commit_one_year +
         stats_line.nb_contributors +
         stats_line.contributors_code_percent +
         stats_line.declared_users +
         stats_line.average_review_score +
         stats_line.screenshots) / 29 * 100;

        /* Update the statistics table */
        UPDATE softwares_statistics
        SET score = calculated_score
        WHERE software_id = stats_line.software_id;

    END LOOP;
END;
$$;


ALTER FUNCTION public.calculate_score_for_softwares_statistics() OWNER TO comptoir;

--
-- Name: calculated_points_from_manivelle(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.calculated_points_from_manivelle() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE     point_reviews      INTEGER;
    DECLARE points_screenshots INTEGER;
    DECLARE point_users_of     INTEGER;

    DECLARE stats_line         softwares_statistics%ROWTYPE;
    DECLARE stats              softwares_statistics.software_id%TYPE;


BEGIN
    FOR stats_line IN SELECT *
                      FROM softwares_statistics LOOP

        /*RAISE NOTICE  'test %' ,stats_line.software_id;*/
        /* Get points */
        point_reviews := points_for_reviews(stats_line.software_id);
        points_screenshots := points_for_screenshots(stats_line.software_id);
        point_users_of := points_for_users_of(stats_line.software_id);

        /* Update the statistics table */
        UPDATE softwares_statistics
        SET average_review_score = point_reviews,
            screenshots          = points_screenshots,
            declared_users       = point_users_of
        WHERE software_id = stats_line.software_id;

    END LOOP;
END
$$;


ALTER FUNCTION public.calculated_points_from_manivelle() OWNER TO comptoir;

--
-- Name: calculated_points_from_server(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.calculated_points_from_server() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE total_softwares                  REAL;
    DECLARE  raw_line                         raw_metrics_softwares%ROWTYPE;
    DECLARE  avg_delta_commit_1m              REAL;
    DECLARE  avg_delta_commit_12m             REAL;
    DECLARE  avg_hight_commiter_percent       REAL;
    DECLARE  avg_number_of_contributors       REAL;
    DECLARE  final_avg_delta_commit_1m        REAL;
    DECLARE  final_avg_delta_commit_12m       REAL;
    DECLARE  final_avg_hight_commiter_percent REAL;
    DECLARE  final_avg_number_of_contributors REAL;

BEGIN
    SELECT COUNT(*)
    INTO total_softwares
    FROM (SELECT DISTINCT software_id
          FROM raw_metrics_softwares) AS count;

    avg_delta_commit_1m := 0;
    avg_delta_commit_12m := 0;
    avg_hight_commiter_percent := 0;
    avg_number_of_contributors := 0;

    FOR raw_line IN (WITH grouped_softwares AS (SELECT
                                                    *,
                                                    ROW_NUMBER()
                                                    OVER (PARTITION BY software_id
                                                        ORDER BY id DESC) AS rk
                                                FROM raw_metrics_softwares) SELECT g.*
                                                                            FROM grouped_softwares g
                                                                            WHERE g.rk = 1) LOOP

        /* Get sum */
        avg_delta_commit_1m := avg_delta_commit_1m + (raw_line.delta_commit_one_month);
        avg_delta_commit_12m := avg_delta_commit_12m + (raw_line.delta_commit_twelve_month);
        avg_hight_commiter_percent := avg_hight_commiter_percent + (raw_line.high_committer_percent);
        avg_number_of_contributors := avg_number_of_contributors + (raw_line.number_of_contributors);

    END LOOP;

    /* Get average */
    avg_delta_commit_1m         := avg_delta_commit_1m / total_softwares;
    avg_delta_commit_12m        := avg_delta_commit_12m / total_softwares;
    avg_hight_commiter_percent  := avg_hight_commiter_percent / total_softwares;
    avg_number_of_contributors  := avg_number_of_contributors / total_softwares;


    FOR raw_line IN (WITH grouped_softwares AS (SELECT
                                                    *,
                                                    ROW_NUMBER()
                                                    OVER (PARTITION BY software_id
                                                        ORDER BY id DESC) AS rk
                                                FROM raw_metrics_softwares) SELECT g.*
                                                                            FROM grouped_softwares g
                                                                            WHERE g.rk = 1) LOOP

        final_avg_delta_commit_1m := (raw_line.delta_commit_one_month - avg_delta_commit_1m) / avg_delta_commit_1m *
                                     100;
        final_avg_delta_commit_12m := (raw_line.delta_commit_twelve_month - avg_delta_commit_12m) / avg_delta_commit_12m
                                      * 100;
        final_avg_hight_commiter_percent := raw_line.high_committer_percent;
        /*(raw_line.high_committer_percent-avg_hight_commiter_percent)/avg_hight_commiter_percent*100;*/
        final_avg_number_of_contributors :=
        (raw_line.number_of_contributors - avg_number_of_contributors) / avg_number_of_contributors * 100;

        PERFORM score_age(raw_line.project_age, raw_line.last_commit_age, raw_line.software_id);
        PERFORM score_delta_commit_1m(final_avg_delta_commit_1m, raw_line.software_id);
        PERFORM score_delta_commit_12m(final_avg_delta_commit_12m, raw_line.software_id);
        PERFORM score_hight_commiter_percent(final_avg_hight_commiter_percent, raw_line.software_id);
        PERFORM score_number_of_contributors(final_avg_number_of_contributors, raw_line.software_id);


    END LOOP;

END;$$;


ALTER FUNCTION public.calculated_points_from_server() OWNER TO comptoir;

--
-- Name: points_for_reviews(integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.points_for_reviews(stats_id integer) RETURNS real
    LANGUAGE plpgsql
    AS $$DECLARE total_reviews REAL;
    DECLARE  points        REAL;
BEGIN
    points :=0;
    SELECT avg(reviews.evaluation)
    INTO total_reviews
    FROM reviews
    WHERE reviews.software_id = stats_id;

    IF total_reviews > 2.5
    THEN points := 2;
    ELSIF total_reviews <= 2.5
        THEN points := 1;
    ELSIF total_reviews = 0
        THEN points := 0;
    END IF;
    RETURN points;
END;
$$;


ALTER FUNCTION public.points_for_reviews(stats_id integer) OWNER TO comptoir;

--
-- Name: points_for_screenshots(integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.points_for_screenshots(stats_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$

DECLARE     total_screenshots INTEGER;
    DECLARE points            INTEGER;

BEGIN
    SELECT count(screenshots.id)
    INTO total_screenshots
    FROM screenshots
    WHERE screenshots.software_id = stats_id;

    IF total_screenshots > 2
    THEN points := 2;
    ELSIF total_screenshots BETWEEN 0 AND 2
        THEN points = 1;
    ELSIF total_screenshots = 0
        THEN points := 0;
    END IF;
    RETURN points;
END;
$$;


ALTER FUNCTION public.points_for_screenshots(stats_id integer) OWNER TO comptoir;

--
-- Name: points_for_users_of(integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.points_for_users_of(stats_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE total_users_of INTEGER;
    DECLARE  points         INTEGER;
BEGIN
    SELECT count(relationships_softwares_users.id)
    INTO total_users_of
    FROM relationships_softwares_users
    WHERE relationships_softwares_users.software_id = stats_id;

    IF total_users_of > 5
    THEN points := 2;
    ELSIF total_users_of BETWEEN 0 AND 5
        THEN points = 1;
    ELSIF total_users_of = 0
        THEN points := 0;
    END IF;
    RETURN points;
END;
$$;


ALTER FUNCTION public.points_for_users_of(stats_id integer) OWNER TO comptoir;

--
-- Name: raw_metrics_from_manivelle(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.raw_metrics_from_manivelle() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE avg_reviews       DECIMAL;
    DECLARE  total_screenshots INTEGER;
    DECLARE  total_users_of    INTEGER;

    DECLARE  stats_line        raw_metrics_softwares%ROWTYPE;
    DECLARE  id_software       raw_metrics_softwares.software_id%TYPE;


BEGIN
    FOR stats_line IN SELECT *
                      FROM raw_metrics_softwares LOOP

        /*RAISE NOTICE  'test %' ,stats_line.software_id;*/
        /* Get points */
        SELECT avg(reviews.evaluation)
        INTO avg_reviews
        FROM reviews
        WHERE reviews.software_id = stats_line.software_id;

        SELECT count(screenshots.id)
        INTO total_screenshots
        FROM screenshots
        WHERE screenshots.software_id = stats_line.software_id;

        SELECT count(relationships_softwares_users.id)
        INTO total_users_of
        FROM relationships_softwares_users
        WHERE relationships_softwares_users.software_id = stats_line.software_id;

        /* Update the statistics table */
        UPDATE raw_metrics_softwares
        SET average_review_score = avg_reviews,
            screenshots          = total_screenshots,
            declared_users       = total_users_of
        WHERE software_id = stats_line.software_id;

    END LOOP;
END

$$;


ALTER FUNCTION public.raw_metrics_from_manivelle() OWNER TO comptoir;

--
-- Name: score_age(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_age(p_project_age integer, p_commit_age integer, p_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE     d_score_project INTEGER;
    DECLARE d_score_commit  INTEGER;

BEGIN

    IF p_project_age > 365 * 5
    THEN d_score_project := 2;
    ELSIF p_project_age > 365 * 3
        THEN d_score_project := 1;
    ELSE d_score_project :=0;
    END IF;

    IF p_commit_age > 365 * 3
    THEN d_score_commit := 0;
    ELSIF p_commit_age > 365 * 2
        THEN d_score_commit := 1;
    ELSIF p_commit_age > 90
        THEN d_score_commit := 2;
    ELSIF p_commit_age > 30
        THEN d_score_commit := 3;
    ELSE d_score_commit :=4;
    END IF;

    IF EXISTS(SELECT id
              FROM softwares_statistics
              WHERE software_id = p_id)
    THEN UPDATE softwares_statistics
    SET project_age = d_score_project, last_commit_age = d_score_commit
    WHERE software_id = p_id;
    ELSE INSERT INTO softwares_statistics (software_id, project_age, last_commit_age)
    VALUES (p_id, d_score_project, d_score_commit);
    END IF;

END;$$;


ALTER FUNCTION public.score_age(p_project_age integer, p_commit_age integer, p_id integer) OWNER TO comptoir;

--
-- Name: FUNCTION score_age(p_project_age integer, p_commit_age integer, p_id integer); Type: COMMENT; Schema: public; Owner: comptoir
--

COMMENT ON FUNCTION public.score_age(p_project_age integer, p_commit_age integer, p_id integer) IS 'updates the scores based on age

NOTE: this needs to be the first function called to avoid updating a row that does not exist!';


--
-- Name: score_delta_commit_12m(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_delta_commit_12m(p_pourcentage_delta_12m real, p_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE     d_score        INTEGER;
    DECLARE d_score_commit INTEGER;

BEGIN

    IF p_pourcentage_delta_12m > 50
    THEN d_score := 4;
    ELSIF p_pourcentage_delta_12m > 25
        THEN d_score := 3;
    ELSIF p_pourcentage_delta_12m > -25
        THEN d_score := 1;
    ELSE d_score :=0;
    END IF;

    UPDATE softwares_statistics
    SET delta_commit_one_year = d_score
    WHERE software_id = p_id;

END;$$;


ALTER FUNCTION public.score_delta_commit_12m(p_pourcentage_delta_12m real, p_id integer) OWNER TO comptoir;

--
-- Name: score_delta_commit_1m(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_delta_commit_1m(p_pourcentage_delta_1m real, p_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE d_score INTEGER;

BEGIN

    IF p_pourcentage_delta_1m > 50
    THEN d_score := 4;
    ELSIF p_pourcentage_delta_1m > 25
        THEN d_score := 3;
    ELSIF p_pourcentage_delta_1m > -25
        THEN d_score := 1;
    ELSE d_score :=0;
    END IF;

    UPDATE softwares_statistics
    SET delta_commit_one_month = d_score
    WHERE software_id = p_id;

END;$$;


ALTER FUNCTION public.score_delta_commit_1m(p_pourcentage_delta_1m real, p_id integer) OWNER TO comptoir;

--
-- Name: score_hight_commiter_percent(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_hight_commiter_percent(p_pourcentage_high real, p_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE d_score INTEGER;

BEGIN

    IF p_pourcentage_high > 90
    THEN d_score := 0;
    ELSIF p_pourcentage_high > 50
        THEN d_score := 2;
    ELSIF p_pourcentage_high > 10
        THEN d_score := 3;
    ELSE d_score :=4;
    END IF;

    UPDATE softwares_statistics
    SET contributors_code_percent = d_score
    WHERE software_id = p_id;

END;$$;


ALTER FUNCTION public.score_hight_commiter_percent(p_pourcentage_high real, p_id integer) OWNER TO comptoir;

--
-- Name: score_number_of_contributors(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_number_of_contributors(pourcentage_nb_contributors real, p_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE d_score INTEGER;

BEGIN

    IF pourcentage_nb_contributors > 50
    THEN d_score := 4;
    ELSIF pourcentage_nb_contributors > 25
        THEN d_score := 3;
    ELSIF pourcentage_nb_contributors > 0
        THEN d_score := 1;
    ELSE d_score :=0;
    END IF;

    UPDATE softwares_statistics
    SET nb_contributors = d_score
    WHERE software_id = p_id;

END;
$$;


ALTER FUNCTION public.score_number_of_contributors(pourcentage_nb_contributors real, p_id integer) OWNER TO comptoir;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: licence_types; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.licence_types (
    id bigint NOT NULL,
    name text,
    created timestamp with time zone,
    modified timestamp with time zone,
    cd text
);


ALTER TABLE public.licence_types OWNER TO comptoir;

--
-- Name: licence_types_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.licence_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.licence_types_id_seq OWNER TO comptoir;

--
-- Name: licence_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.licence_types_id_seq OWNED BY public.licence_types.id;


--
-- Name: licenses; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.licenses (
    id bigint NOT NULL,
    name text NOT NULL,
    license_type_id bigint NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone
);


ALTER TABLE public.licenses OWNER TO comptoir;

--
-- Name: licenses_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.licenses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.licenses_id_seq OWNER TO comptoir;

--
-- Name: licenses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.licenses_id_seq OWNED BY public.licenses.id;


--
-- Name: panels; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.panels (
    id uuid NOT NULL,
    request_id uuid NOT NULL,
    panel character varying,
    title character varying,
    element character varying,
    summary character varying,
    content bytea
);


ALTER TABLE public.panels OWNER TO comptoir;

--
-- Name: phinxlog; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.phinxlog (
    version bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    migration_name character varying(100),
    breakpoint boolean DEFAULT false NOT NULL
);


ALTER TABLE public.phinxlog OWNER TO comptoir;

--
-- Name: raw_metrics_softwares; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.raw_metrics_softwares (
    id integer NOT NULL,
    software_id integer,
    name character varying(255),
    created timestamp without time zone,
    modified timestamp without time zone,
    last_commit_age integer NOT NULL,
    high_committer_percent real NOT NULL,
    number_of_contributors integer NOT NULL,
    declared_users integer,
    average_review_score numeric,
    screenshots integer,
    code_gouv_label integer,
    metrics_date timestamp without time zone,
    project_age integer,
    delta_commit_one_month integer,
    delta_commit_twelve_month integer,
    commit_activity_one_month real,
    commit_activity_twelve_month real
);


ALTER TABLE public.raw_metrics_softwares OWNER TO comptoir;

--
-- Name: raw_metrics_softwares_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.raw_metrics_softwares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raw_metrics_softwares_id_seq OWNER TO comptoir;

--
-- Name: raw_metrics_softwares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.raw_metrics_softwares_id_seq OWNED BY public.raw_metrics_softwares.id;


--
-- Name: relationship_types; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationship_types (
    id bigint NOT NULL,
    name text,
    created timestamp with time zone,
    modified timestamp with time zone,
    cd text
);


ALTER TABLE public.relationship_types OWNER TO comptoir;

--
-- Name: relationship_types_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationship_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationship_types_id_seq OWNER TO comptoir;

--
-- Name: relationship_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationship_types_id_seq OWNED BY public.relationship_types.id;


--
-- Name: relationships; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships (
    id bigint NOT NULL,
    name text NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone,
    cd text,
    relationship_type_id bigint
);


ALTER TABLE public.relationships OWNER TO comptoir;

--
-- Name: relationships_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_id_seq OWNER TO comptoir;

--
-- Name: relationships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_id_seq OWNED BY public.relationships.id;


--
-- Name: relationships_softwares; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships_softwares (
    id bigint NOT NULL,
    software_id bigint NOT NULL,
    relationship_id bigint NOT NULL,
    recipient_id bigint NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone
);


ALTER TABLE public.relationships_softwares OWNER TO comptoir;

--
-- Name: relationships_softwares_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_softwares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_softwares_id_seq OWNER TO comptoir;

--
-- Name: relationships_softwares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_softwares_id_seq OWNED BY public.relationships_softwares.id;


--
-- Name: relationships_softwares_users; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships_softwares_users (
    id bigint NOT NULL,
    software_id bigint NOT NULL,
    user_id bigint NOT NULL,
    relationship_id bigint NOT NULL,
    created timestamp with time zone,
    modified timestamp with time zone
);


ALTER TABLE public.relationships_softwares_users OWNER TO comptoir;

--
-- Name: relationships_softwares_users_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_softwares_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_softwares_users_id_seq OWNER TO comptoir;

--
-- Name: relationships_softwares_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_softwares_users_id_seq OWNED BY public.relationships_softwares_users.id;


--
-- Name: relationships_users; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships_users (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    recipient_id bigint NOT NULL,
    relationship_id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone
);


ALTER TABLE public.relationships_users OWNER TO comptoir;

--
-- Name: relationships_users_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_users_id_seq OWNER TO comptoir;

--
-- Name: relationships_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_users_id_seq OWNED BY public.relationships_users.id;


--
-- Name: requests; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.requests (
    id uuid NOT NULL,
    url character varying NOT NULL,
    content_type character varying,
    status_code integer,
    method character varying,
    requested_at timestamp without time zone NOT NULL
);


ALTER TABLE public.requests OWNER TO comptoir;

--
-- Name: reviews; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.reviews (
    id bigint NOT NULL,
    comment text NOT NULL,
    title text NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id bigint,
    software_id bigint,
    evaluation bigint,
    modified timestamp with time zone
);


ALTER TABLE public.reviews OWNER TO comptoir;

--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq OWNER TO comptoir;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.reviews_id_seq OWNED BY public.reviews.id;


--
-- Name: screenshots; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.screenshots (
    id bigint NOT NULL,
    software_id bigint,
    url_directory text,
    created timestamp with time zone,
    modified timestamp with time zone,
    name text,
    photo text
);


ALTER TABLE public.screenshots OWNER TO comptoir;

--
-- Name: screenshots_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.screenshots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.screenshots_id_seq OWNER TO comptoir;

--
-- Name: screenshots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.screenshots_id_seq OWNED BY public.screenshots.id;


--
-- Name: softwares; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.softwares (
    id bigint NOT NULL,
    softwarename text,
    url_repository text NOT NULL,
    description text,
    licence_id bigint,
    created timestamp with time zone,
    modified timestamp with time zone,
    logo_directory text,
    photo text,
    url_website character varying(255),
    web_site_url character varying(255)
);


ALTER TABLE public.softwares OWNER TO comptoir;

--
-- Name: softwares_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.softwares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.softwares_id_seq OWNER TO comptoir;

--
-- Name: softwares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.softwares_id_seq OWNED BY public.softwares.id;


--
-- Name: softwares_statistics; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.softwares_statistics (
    id integer NOT NULL,
    software_id integer,
    last_commit_age integer,
    project_age integer,
    delta_commit_one_month integer,
    delta_commit_twelve_month integer,
    number_of_contributors integer,
    high_committer_percent numeric,
    declared_users integer,
    average_review_score numeric,
    screenshots integer,
    code_gouv_label integer,
    score numeric
);


ALTER TABLE public.softwares_statistics OWNER TO comptoir;

--
-- Name: softwares_tags; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.softwares_tags (
    id integer NOT NULL,
    software_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.softwares_tags OWNER TO comptoir;

--
-- Name: softwares_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.softwares_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.softwares_tags_id_seq OWNER TO comptoir;

--
-- Name: softwares_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.softwares_tags_id_seq OWNED BY public.softwares_tags.id;


--
-- Name: statistics_averages; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.statistics_averages (
    id integer NOT NULL,
    delta_commit_one_month real NOT NULL,
    delta_commit_twelve_month real NOT NULL,
    number_of_contributors real NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


ALTER TABLE public.statistics_averages OWNER TO comptoir;

--
-- Name: statistics_averages_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.statistics_averages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.statistics_averages_id_seq OWNER TO comptoir;

--
-- Name: statistics_averages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.statistics_averages_id_seq OWNED BY public.statistics_averages.id;


--
-- Name: statistics_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.statistics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.statistics_id_seq OWNER TO comptoir;

--
-- Name: statistics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.statistics_id_seq OWNED BY public.softwares_statistics.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


ALTER TABLE public.tags OWNER TO comptoir;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO comptoir;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;


--
-- Name: user_types; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.user_types (
    id bigint NOT NULL,
    name text,
    created timestamp with time zone,
    modified timestamp with time zone,
    cd text
);


ALTER TABLE public.user_types OWNER TO comptoir;

--
-- Name: user_types_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.user_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_types_id_seq OWNER TO comptoir;

--
-- Name: user_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.user_types_id_seq OWNED BY public.user_types.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    username text,
    created timestamp with time zone NOT NULL,
    logo_directory text,
    url text,
    user_type_id bigint NOT NULL,
    description text,
    modified timestamp with time zone NOT NULL,
    role text NOT NULL,
    password text NOT NULL,
    email text,
    photo text,
    active boolean DEFAULT false,
    digest_hash text,
    token uuid
);


ALTER TABLE public.users OWNER TO comptoir;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO comptoir;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licence_types ALTER COLUMN id SET DEFAULT nextval('public.licence_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licenses ALTER COLUMN id SET DEFAULT nextval('public.licenses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.raw_metrics_softwares ALTER COLUMN id SET DEFAULT nextval('public.raw_metrics_softwares_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationship_types ALTER COLUMN id SET DEFAULT nextval('public.relationship_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships ALTER COLUMN id SET DEFAULT nextval('public.relationships_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares ALTER COLUMN id SET DEFAULT nextval('public.relationships_softwares_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users ALTER COLUMN id SET DEFAULT nextval('public.relationships_softwares_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users ALTER COLUMN id SET DEFAULT nextval('public.relationships_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.screenshots ALTER COLUMN id SET DEFAULT nextval('public.screenshots_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares ALTER COLUMN id SET DEFAULT nextval('public.softwares_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_statistics ALTER COLUMN id SET DEFAULT nextval('public.statistics_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags ALTER COLUMN id SET DEFAULT nextval('public.softwares_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.statistics_averages ALTER COLUMN id SET DEFAULT nextval('public.statistics_averages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.user_types ALTER COLUMN id SET DEFAULT nextval('public.user_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: licence_types; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.licence_types (id, name, created, modified, cd) FROM stdin;
2	Libre	2016-04-08 17:58:01+02	2016-04-08 17:58:01+02	Free
3	Propriètaire	2016-04-11 11:59:28+02	2016-04-13 11:22:02+02	NotFree
\.


--
-- Name: licence_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.licence_types_id_seq', 3, true);


--
-- Data for Name: licenses; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.licenses (id, name, license_type_id, created, modified) FROM stdin;
5	CeCILL V2	2	2016-04-13 11:22:24+02	2016-04-13 11:22:24+02
6	MIT	2	2016-04-13 11:22:38+02	2016-04-13 11:22:38+02
3	Propriètaire	3	2016-04-11 11:59:53+02	2016-04-13 12:01:14+02
9	BSD	2	2016-04-28 11:58:25+02	2016-04-28 11:58:25+02
10	GNU GPLv3	2	2016-06-10 17:25:19+02	2016-06-10 17:25:19+02
7	GNU GPL	2	2016-04-13 11:22:43+02	2016-06-10 18:40:48+02
8	GNU aGPL v3	2	2016-04-13 11:50:51+02	2016-06-10 18:41:20+02
12	CeCILL-B	2	2016-06-13 15:56:03+02	2016-06-13 15:56:03+02
13	GNU LGPL	2	2016-06-13 17:40:22+02	2016-06-13 17:40:22+02
14	LGPLv3	2	2016-06-14 10:17:18+02	2016-06-14 10:17:18+02
15	GNU GPLv2	2	2016-07-21 16:57:01+02	2016-07-21 16:57:01+02
16	Apache License 2.0	2	2017-02-07 11:11:57+01	2017-02-07 11:11:57+01
17	Mozilla Public License, version 2.0	2	2017-02-07 13:25:17+01	2017-02-07 13:25:17+01
18	Common Public Attribution License (CPAL)	2	2017-02-07 13:36:19+01	2017-02-07 13:36:19+01
19	Creative Commons Attribution-ShareAlike 3.0 licence (CC BY-SA)	2	2017-02-07 13:43:00+01	2017-02-07 13:43:00+01
\.


--
-- Name: licenses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.licenses_id_seq', 19, true);


--
-- Data for Name: panels; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.panels (id, request_id, panel, title, element, summary, content) FROM stdin;
\.


--
-- Data for Name: phinxlog; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.phinxlog (version, start_time, end_time, migration_name, breakpoint) FROM stdin;
\.


--
-- Data for Name: raw_metrics_softwares; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.raw_metrics_softwares (id, software_id, name, created, modified, last_commit_age, high_committer_percent, number_of_contributors, declared_users, average_review_score, screenshots, code_gouv_label, metrics_date, project_age, delta_commit_one_month, delta_commit_twelve_month, commit_activity_one_month, commit_activity_twelve_month) FROM stdin;
48	3	Asalae	2016-04-11 10:25:00	2016-06-21 16:22:17.461802	175	62.9000015	7	5	\N	15	\N	2016-06-21 16:22:17.461802	2482	0	485	\N	\N
7	3	\N	\N	\N	126	62.9000015	7	5	\N	15	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
32	3	Asalae	2016-04-11 10:25:00	2016-06-21 09:32:46.429381	175	62.9000015	7	5	\N	15	\N	2016-06-21 09:32:46.429381	2482	0	485	\N	\N
17	16	\N	\N	\N	1	22.6000004	11	1	\N	12	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
16	15	\N	\N	\N	490	78.8000031	6	6	\N	11	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
19	18	\N	\N	\N	145	50	4	3	\N	4	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
20	19	\N	\N	\N	4	77	15	3	\N	1	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
18	17	\N	\N	\N	82	60.4000015	7	4	\N	8	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
15	14	\N	\N	\N	32	98.5	3	4	\N	1	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
49	11	Xwiki	2016-04-28 10:01:49	2016-06-21 16:22:17.461802	0	18	0	0	\N	0	\N	2016-06-21 16:22:17.461802	3539	30	2029	\N	\N
6	11	\N	\N	\N	0	23	92	0	\N	0	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
26	11	Xwiki	2016-04-28 12:01:49	2016-06-16 14:16:22.76073	0	18	94	0	\N	0	\N	2016-06-16 14:16:22.76073	3533	30	2052	\N	\N
29	11	Xwiki	2016-04-28 12:01:49	2016-06-17 09:56:25.856372	1	18	94	0	\N	0	\N	2016-06-17 09:56:25.856372	3534	30	2044	\N	\N
33	11	Xwiki	2016-04-28 10:01:49	2016-06-21 09:32:46.429381	0	18	0	0	\N	0	\N	2016-06-21 09:32:46.429381	3538	30	2023	\N	\N
50	12	Xemelios	2016-04-28 10:17:30	2016-06-21 16:22:17.461802	70	50	0	0	\N	4	\N	2016-06-21 16:22:17.461802	1765	0	2	\N	\N
9	12	\N	\N	\N	20	44	5	0	\N	4	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
34	12	Xemelios	2016-04-28 10:17:30	2016-06-21 09:32:46.429381	69	50	0	0	\N	4	\N	2016-06-21 09:32:46.429381	1764	0	2	\N	\N
51	10	Lutèce	2016-04-28 09:57:55	2016-06-21 16:22:17.461802	75	24	0	0	5.0000000000000000	4	\N	2016-06-21 16:22:17.461802	3196	0	193	\N	\N
4	10	\N	\N	\N	25	21	44	0	5.0000000000000000	4	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
35	10	Lutèce	2016-04-28 09:57:55	2016-06-21 09:32:46.429381	74	24	0	0	5.0000000000000000	4	\N	2016-06-21 09:32:46.429381	3195	0	193	\N	\N
53	21	Trustedbird Client	2016-04-28 12:45:08	2016-06-21 16:22:17.461802	193	35	0	0	\N	0	\N	2016-06-21 16:22:17.461802	3351	0	3	\N	\N
22	21	\N	\N	\N	143	35	13	0	\N	0	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
37	21	Trustedbird Client	2016-04-28 12:45:08	2016-06-21 09:32:46.429381	192	35	0	0	\N	0	\N	2016-06-21 09:32:46.429381	3350	0	3	\N	\N
54	13	Chouette2	2016-04-28 12:27:09	2016-06-21 16:22:17.461802	68	27	0	1	\N	0	\N	2016-06-21 16:22:17.461802	1627	0	281	\N	\N
14	13	\N	\N	\N	18	31	10	1	\N	0	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
38	13	Chouette2	2016-04-28 12:27:09	2016-06-21 09:32:46.429381	68	27	0	1	\N	0	\N	2016-06-21 09:32:46.429381	1627	0	281	\N	\N
55	20	Exoplatform	2016-04-28 12:43:35	2016-06-21 16:22:17.461802	5	10	0	0	\N	2	\N	2016-06-21 16:22:17.461802	3384	22	267	\N	\N
21	20	\N	\N	\N	0	10	212	0	\N	2	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
39	20	Exoplatform	2016-04-28 12:43:35	2016-06-21 09:32:46.429381	5	10	0	0	\N	2	\N	2016-06-21 09:32:46.429381	3383	22	267	\N	\N
57	9	Asqatasun	2016-04-28 09:48:23	2016-06-21 16:22:17.461802	3	73	0	0	\N	3	\N	2016-06-21 16:22:17.461802	2322	30	1173	\N	\N
3	9	\N	\N	\N	46	59	38	0	\N	3	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
41	9	Asqatasun	2016-04-28 09:48:23	2016-06-21 09:32:46.429381	3	73	0	0	\N	3	\N	2016-06-21 09:32:46.429381	2322	30	1173	\N	\N
58	30	LinID OpenPaaS	2016-06-13 15:48:56	2016-06-21 16:22:17.461802	0	15	0	2	\N	1	\N	2016-06-21 16:22:17.461802	911	30	3746	\N	\N
42	30	LinID OpenPaaS	2016-06-13 15:48:56	2016-06-21 09:32:46.429381	0	15	0	2	\N	1	\N	2016-06-21 09:32:46.429381	910	30	3736	\N	\N
60	5	S²LOW	2016-04-13 09:44:52	2016-06-21 16:22:17.461802	7	86.0999985	4	13	\N	6	\N	2016-06-21 16:22:17.461802	2240	41	292	\N	\N
8	5	\N	\N	\N	5	85.9000015	4	13	\N	6	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
44	5	S²LOW	2016-04-13 09:44:52	2016-06-21 09:32:46.429381	7	86.0999985	4	13	\N	6	\N	2016-06-21 09:32:46.429381	2240	41	292	\N	\N
61	25	BlueMind	2016-06-13 08:55:59	2016-06-21 16:22:17.461802	8	31	0	4	\N	7	\N	2016-06-21 16:22:17.461802	1554	23	1370	\N	\N
45	25	BlueMind	2016-06-13 08:55:59	2016-06-21 09:32:46.429381	7	31	0	4	\N	7	\N	2016-06-21 09:32:46.429381	1553	23	1370	\N	\N
62	3	Asalae	2016-04-11 10:25:00	2016-11-15 14:28:45.849571	322	62.9000015	7	5	\N	15	\N	2016-11-15 14:28:45.849571	2629	0	11	0	5
52	2	Pastell	2016-04-08 15:55:11	2016-06-21 16:22:17.461802	0	95.9000015	8	15	\N	15	\N	2016-06-21 16:22:17.461802	2193	9	211	\N	\N
11	2	\N	\N	\N	11	95.9000015	8	15	\N	15	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
36	2	Pastell	2016-04-08 15:55:11	2016-06-21 09:32:46.429381	0	95.9000015	8	15	\N	15	\N	2016-06-21 09:32:46.429381	2193	9	212	\N	\N
63	2	Pastell	2016-04-08 15:55:11	2016-11-15 14:28:45.849571	13	96.1999969	8	15	\N	15	\N	2016-11-15 14:28:45.849571	2340	7	164	7	82
56	4	i-Parapheur - serveur	2016-04-13 09:41:07	2016-06-21 16:22:17.461802	0	14.6000004	13	15	4.5000000000000000	16	\N	2016-06-21 16:22:17.461802	2986	13	178	\N	\N
5	4	\N	\N	\N	7	14.6999998	13	15	4.5000000000000000	16	\N	2016-05-03 09:32:44	\N	\N	\N	\N	\N
40	4	i-Parapheur - serveur	2016-04-13 09:41:07	2016-06-21 09:32:46.429381	0	14.6000004	13	15	4.5000000000000000	16	\N	2016-06-21 09:32:46.429381	2986	13	178	\N	\N
64	4	i-Parapheur - serveur	2016-04-13 09:41:07	2016-11-15 14:28:45.849571	20	30.3999996	13	15	4.5000000000000000	16	\N	2016-11-15 14:28:45.849571	3133	2	149	2	74
65	16	WebRsa	2016-04-28 12:34:41	2016-11-15 14:55:32.401555	0	41.5999985	11	1	\N	12	\N	2016-11-15 14:55:32.401555	2736	103	743	51	371
66	15	i-delibRE	2016-04-28 12:33:12	2016-11-15 14:55:32.401555	686	78.8000031	6	6	\N	11	\N	2016-11-15 14:55:32.401555	1371	0	0	0	0
67	18	openScrutin	2016-04-28 12:40:06	2016-11-15 14:55:32.401555	341	50	4	3	\N	4	\N	2016-11-15 14:55:32.401555	1898	0	2	0	2
68	24	Web-delib	2016-06-13 07:58:42	2016-11-15 14:55:32.401555	61	62.7999992	16	8	4.0000000000000000	1	\N	2016-11-15 14:55:32.401555	3382	0	529	0	176
69	19	openMairie	2016-04-28 12:41:00	2016-11-15 14:55:32.401555	5	76.5	15	3	\N	1	\N	2016-11-15 14:55:32.401555	2325	18	283	6	35
70	17	openCimetière	2016-04-28 12:39:02	2016-11-15 15:02:21.581383	33	61.0999985	8	4	\N	8	\N	2016-11-15 15:02:21.581383	1898	0	74	0	24
71	14	OpenRecensement	2016-04-28 12:29:11	2016-11-15 15:02:21.581383	228	98.5	3	4	\N	1	\N	2016-11-15 15:02:21.581383	2237	0	11	0	11
72	23	OpenADS	2016-06-10 16:38:49	2016-11-15 15:02:21.581383	0	45.2000008	6	5	\N	3	\N	2016-11-15 15:02:21.581383	242	5	372	2	62
73	40	OpenRésultat	2016-11-03 08:06:17	2016-11-15 15:02:21.581383	349	84.4000015	6	1	\N	0	\N	2016-11-15 15:02:21.581383	1792	0	2	0	2
75	32	WebGFC	2016-06-14 07:55:41	2016-11-15 15:17:14.52263	5	64.8000031	6	2	4.0000000000000000	0	\N	2016-11-15 15:17:14.52263	2020	62	505	62	252
\.


--
-- Name: raw_metrics_softwares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.raw_metrics_softwares_id_seq', 75, true);


--
-- Data for Name: relationship_types; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.relationship_types (id, name, created, modified, cd) FROM stdin;
4	UserToUser	2016-04-08 18:11:23+02	2016-04-08 18:11:23+02	UserToUser
5	UserToSoftware	2016-04-11 10:07:29+02	2016-04-11 10:07:29+02	UserToSoftware
6	SoftwareToSoftware	2016-04-12 17:45:49+02	2016-04-12 17:45:49+02	SoftwareToSoftware
\.


--
-- Name: relationship_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.relationship_types_id_seq', 6, true);


--
-- Data for Name: relationships; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.relationships (id, name, created, modified, cd, relationship_type_id) FROM stdin;
2	BelongsTo	2016-04-11 11:43:22+02	2016-04-11 11:43:22+02	BelongsTo	4
3	CreatorOf	2016-04-11 11:50:07+02	2016-04-11 11:50:07+02	CreatorOf	5
4	ServicesProvider	2016-04-11 11:51:07+02	2016-04-11 11:51:07+02	ServicesProvider	5
5	AlternativeTo	2016-04-12 17:46:13+02	2016-04-12 17:46:13+02	AlternativeTo	6
6	Works well with	2016-04-13 11:57:17+02	2016-04-13 11:57:17+02	WorksWellWith	6
7	UserOf	2016-04-15 11:15:41+02	2016-04-15 11:15:41+02	UserOf	5
8	BackerOf	2016-04-18 16:36:10+02	2016-04-18 16:36:10+02	BackerOf	5
9	ContributorOf	2016-04-18 16:36:24+02	2016-04-18 16:36:24+02	ContributorOf	5
10	ProviderFor	2016-04-18 18:02:04+02	2016-04-18 18:02:04+02	ProviderFor	5
\.


--
-- Name: relationships_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.relationships_id_seq', 10, true);


--
-- Data for Name: relationships_softwares; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.relationships_softwares (id, software_id, relationship_id, recipient_id, created, modified) FROM stdin;
4	2	6	3	2016-04-13 11:58:03+02	2016-04-13 11:58:03+02
5	2	6	5	2016-04-13 14:54:39+02	2016-04-13 14:54:39+02
6	2	6	4	2016-04-13 14:54:47+02	2016-04-13 14:54:47+02
9	5	6	4	2016-06-14 11:02:58+02	2016-06-14 11:02:58+02
10	5	6	3	2016-06-14 11:03:21+02	2016-06-14 11:03:21+02
11	5	6	2	2016-06-14 11:03:37+02	2016-06-14 11:03:37+02
12	4	6	3	2016-06-14 11:05:24+02	2016-06-14 11:05:24+02
13	4	6	2	2016-06-14 11:05:31+02	2016-06-14 11:05:31+02
14	4	6	5	2016-06-14 11:05:47+02	2016-06-14 11:05:47+02
15	3	6	4	2016-06-14 11:06:37+02	2016-06-14 11:06:37+02
16	3	6	2	2016-06-14 11:06:44+02	2016-06-14 11:06:44+02
17	3	6	5	2016-06-14 11:06:54+02	2016-06-14 11:06:54+02
18	22	6	25	2016-06-24 15:15:59+02	2016-06-24 15:15:59+02
19	25	6	22	2016-06-24 15:16:57+02	2016-06-24 15:16:57+02
20	33	6	79	2017-02-13 08:42:51+01	2017-02-13 08:42:51+01
21	79	6	33	2017-02-13 08:43:34+01	2017-02-13 08:43:34+01
22	77	6	33	2017-02-13 08:44:34+01	2017-02-13 08:44:34+01
23	33	6	78	2017-02-28 15:55:56+01	2017-02-28 15:55:56+01
24	33	6	77	2017-02-28 15:56:30+01	2017-02-28 15:56:30+01
25	83	6	89	2017-03-01 13:54:08+01	2017-03-01 13:54:08+01
26	88	6	89	2017-03-01 13:54:56+01	2017-03-01 13:54:56+01
27	88	6	90	2017-03-01 14:04:03+01	2017-03-01 14:04:03+01
28	35	6	84	2017-03-01 14:15:59+01	2017-03-01 14:15:59+01
29	35	6	91	2017-03-01 14:16:11+01	2017-03-01 14:16:11+01
30	35	6	92	2017-03-01 14:21:04+01	2017-03-01 14:21:04+01
31	35	6	93	2017-03-01 14:38:19+01	2017-03-01 14:38:19+01
32	35	6	94	2017-03-01 14:38:39+01	2017-03-01 14:38:39+01
33	35	6	95	2017-03-01 14:38:52+01	2017-03-01 14:38:52+01
\.


--
-- Name: relationships_softwares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.relationships_softwares_id_seq', 33, true);


--
-- Data for Name: relationships_softwares_users; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.relationships_softwares_users (id, software_id, user_id, relationship_id, created, modified) FROM stdin;
4	2	4	7	2016-04-15 11:22:21+02	2016-04-15 11:22:21+02
5	2	5	7	2016-04-15 11:22:32+02	2016-04-15 11:22:32+02
9	4	6	3	2016-05-30 17:02:22+02	2016-05-30 17:02:22+02
10	15	6	3	2016-05-30 17:02:33+02	2016-05-30 17:02:33+02
12	24	21	7	2016-06-13 10:15:01+02	2016-06-13 10:15:01+02
13	23	21	7	2016-06-13 10:16:40+02	2016-06-13 10:16:40+02
14	22	10	3	2016-06-13 10:50:38+02	2016-06-13 10:50:38+02
15	22	10	4	2016-06-13 10:51:11+02	2016-06-13 10:51:11+02
17	25	9	3	2016-06-13 11:04:46+02	2016-06-13 11:04:46+02
18	25	9	4	2016-06-13 11:05:13+02	2016-06-13 11:05:13+02
19	25	34	7	2016-06-13 11:13:15+02	2016-06-13 11:13:15+02
20	25	35	7	2016-06-13 11:46:44+02	2016-06-13 11:46:44+02
21	26	36	7	2016-06-13 12:03:31+02	2016-06-13 12:03:31+02
22	14	8	3	2016-06-13 12:05:07+02	2016-06-13 12:05:07+02
23	17	8	3	2016-06-13 12:05:19+02	2016-06-13 12:05:19+02
24	18	8	3	2016-06-13 12:05:31+02	2016-06-13 12:05:31+02
25	19	8	3	2016-06-13 12:05:44+02	2016-06-13 12:05:44+02
26	23	8	3	2016-06-13 12:06:07+02	2016-06-13 12:06:07+02
27	14	8	4	2016-06-13 12:06:51+02	2016-06-13 12:06:51+02
28	17	8	4	2016-06-13 12:07:02+02	2016-06-13 12:07:02+02
29	18	8	4	2016-06-13 12:07:12+02	2016-06-13 12:07:12+02
30	19	8	4	2016-06-13 12:07:24+02	2016-06-13 12:07:24+02
31	23	8	4	2016-06-13 12:07:41+02	2016-06-13 12:07:41+02
33	23	21	9	2016-06-13 12:13:24+02	2016-06-13 12:13:24+02
34	26	7	3	2016-06-13 12:14:43+02	2016-06-13 12:14:43+02
35	2	6	3	2016-06-13 12:17:48+02	2016-06-13 12:17:48+02
37	5	6	3	2016-06-13 12:19:56+02	2016-06-13 12:19:56+02
38	3	6	3	2016-06-13 12:20:22+02	2016-06-13 12:20:22+02
39	24	6	3	2016-06-13 12:20:45+02	2016-06-13 12:20:45+02
40	16	6	3	2016-06-13 12:21:07+02	2016-06-13 12:21:07+02
44	4	16	7	2016-06-13 12:34:26+02	2016-06-13 12:34:26+02
45	5	16	7	2016-06-13 12:34:36+02	2016-06-13 12:34:36+02
46	24	16	7	2016-06-13 12:35:00+02	2016-06-13 12:35:00+02
47	2	4	9	2016-06-13 14:38:23+02	2016-06-13 14:38:23+02
48	2	20	7	2016-06-13 14:41:22+02	2016-06-13 14:41:22+02
49	5	4	7	2016-06-13 14:45:53+02	2016-06-13 14:45:53+02
50	4	4	7	2016-06-13 14:46:07+02	2016-06-13 14:46:07+02
51	14	25	7	2016-06-13 14:50:36+02	2016-06-13 14:50:36+02
52	17	25	7	2016-06-13 14:51:00+02	2016-06-13 14:51:00+02
53	18	25	7	2016-06-13 14:51:18+02	2016-06-13 14:51:18+02
54	19	25	7	2016-06-13 14:51:37+02	2016-06-13 14:51:37+02
56	24	18	7	2016-06-13 14:56:16+02	2016-06-13 14:56:16+02
57	4	18	7	2016-06-13 14:56:33+02	2016-06-13 14:56:33+02
58	3	18	7	2016-06-13 14:56:51+02	2016-06-13 14:56:51+02
59	5	18	7	2016-06-13 14:57:06+02	2016-06-13 14:57:06+02
60	4	23	7	2016-06-13 14:58:14+02	2016-06-13 14:58:14+02
61	5	23	7	2016-06-13 14:58:30+02	2016-06-13 14:58:30+02
62	5	32	7	2016-06-13 14:59:09+02	2016-06-13 14:59:09+02
63	27	7	3	2016-06-13 16:16:45+02	2016-06-13 16:16:45+02
64	2	12	9	2016-06-13 16:17:35+02	2016-06-13 16:17:35+02
65	4	12	9	2016-06-13 16:17:52+02	2016-06-13 16:17:52+02
66	27	15	7	2016-06-13 16:24:31+02	2016-06-13 16:24:31+02
67	27	37	7	2016-06-13 16:32:32+02	2016-06-13 16:32:32+02
68	27	18	7	2016-06-13 16:32:59+02	2016-06-13 16:32:59+02
70	3	40	7	2016-06-13 16:56:41+02	2016-06-13 16:56:41+02
71	15	39	7	2016-06-13 16:58:22+02	2016-06-13 16:58:22+02
72	2	37	7	2016-06-13 16:59:14+02	2016-06-13 16:59:14+02
73	2	41	7	2016-06-13 17:07:12+02	2016-06-13 17:07:12+02
74	2	41	9	2016-06-13 17:07:46+02	2016-06-13 17:07:46+02
75	24	42	7	2016-06-13 17:11:15+02	2016-06-13 17:11:15+02
76	4	42	7	2016-06-13 17:11:30+02	2016-06-13 17:11:30+02
77	2	42	7	2016-06-13 17:11:47+02	2016-06-13 17:11:47+02
78	4	43	7	2016-06-13 17:14:28+02	2016-06-13 17:14:28+02
79	15	44	7	2016-06-13 17:18:14+02	2016-06-13 17:18:14+02
80	28	11	3	2016-06-13 17:32:03+02	2016-06-13 17:32:03+02
81	28	11	4	2016-06-13 17:32:16+02	2016-06-13 17:32:16+02
82	29	14	4	2016-06-13 17:42:27+02	2016-06-13 17:42:27+02
83	29	13	4	2016-06-13 17:42:59+02	2016-06-13 17:42:59+02
84	30	11	3	2016-06-13 17:49:15+02	2016-06-13 17:49:15+02
85	30	11	4	2016-06-13 17:49:38+02	2016-06-13 17:49:38+02
86	31	11	3	2016-06-14 09:51:55+02	2016-06-14 09:51:55+02
87	32	6	3	2016-06-14 09:56:13+02	2016-06-14 09:56:13+02
88	32	30	7	2016-06-14 09:56:50+02	2016-06-14 09:56:50+02
89	4	45	7	2016-06-14 09:59:21+02	2016-06-14 09:59:21+02
90	2	45	7	2016-06-14 09:59:45+02	2016-06-14 09:59:45+02
91	5	45	7	2016-06-14 10:00:01+02	2016-06-14 10:00:01+02
92	24	45	7	2016-06-14 10:00:24+02	2016-06-14 10:00:24+02
93	15	45	7	2016-06-14 10:00:39+02	2016-06-14 10:00:39+02
94	24	46	7	2016-06-14 10:03:18+02	2016-06-14 10:03:18+02
95	4	47	7	2016-06-14 10:11:45+02	2016-06-14 10:11:45+02
96	5	5	7	2016-06-14 10:11:56+02	2016-06-14 10:11:56+02
97	33	14	4	2016-06-14 10:20:16+02	2016-06-14 10:20:16+02
98	2	48	7	2016-06-14 10:29:14+02	2016-06-14 10:29:14+02
99	22	44	7	2016-06-14 10:40:10+02	2016-06-14 10:40:10+02
100	22	49	7	2016-06-14 10:43:18+02	2016-06-14 10:43:18+02
101	22	50	7	2016-06-14 10:46:48+02	2016-06-14 10:46:48+02
102	27	51	7	2016-06-14 10:53:40+02	2016-06-14 10:53:40+02
105	23	20	7	2016-06-22 11:42:24+02	2016-06-22 11:42:24+02
106	33	54	4	2016-07-12 10:54:00+02	2016-07-12 10:54:00+02
107	33	57	4	2016-07-12 11:59:03+02	2016-07-12 12:08:21+02
108	33	56	4	2016-07-12 12:09:00+02	2016-07-12 12:09:00+02
109	33	55	4	2016-07-12 12:09:18+02	2016-07-12 12:09:18+02
110	33	16	7	2016-07-12 12:13:35+02	2016-07-12 12:13:35+02
111	33	58	7	2016-07-13 11:33:20+02	2016-07-13 11:33:20+02
112	33	59	7	2016-07-13 11:36:02+02	2016-07-13 11:36:02+02
113	33	60	7	2016-07-13 11:36:14+02	2016-07-13 11:36:14+02
114	33	61	4	2016-07-13 11:49:27+02	2016-07-13 11:49:27+02
115	33	62	4	2016-07-13 12:04:09+02	2016-07-13 12:04:09+02
116	33	63	4	2016-07-13 12:04:22+02	2016-07-13 12:04:22+02
117	33	64	4	2016-07-13 14:01:55+02	2016-07-13 14:01:55+02
118	35	66	3	2016-07-21 17:27:38+02	2016-07-21 17:27:38+02
120	36	67	9	2016-07-26 15:50:39+02	2016-07-26 15:50:39+02
122	36	67	4	2016-07-26 16:19:50+02	2016-07-26 16:19:50+02
124	5	70	7	2016-10-31 12:09:29+01	2016-10-31 12:09:29+01
125	15	70	7	2016-10-31 12:10:02+01	2016-10-31 12:10:02+01
126	4	70	7	2016-10-31 12:10:27+01	2016-10-31 12:10:27+01
127	15	72	7	2016-11-02 17:39:48+01	2016-11-02 17:39:48+01
128	24	72	7	2016-11-02 17:51:49+01	2016-11-02 17:51:49+01
129	4	72	7	2016-11-02 17:52:15+01	2016-11-02 17:52:15+01
130	33	72	7	2016-11-02 17:52:51+01	2016-11-02 17:52:51+01
131	36	72	7	2016-11-02 17:53:11+01	2016-11-02 17:53:11+01
132	35	72	7	2016-11-02 17:53:37+01	2016-11-02 17:53:37+01
133	5	72	7	2016-11-02 17:59:53+01	2016-11-02 17:59:53+01
135	4	73	7	2016-11-03 09:09:32+01	2016-11-03 09:09:32+01
136	33	73	7	2016-11-03 09:09:49+01	2016-11-03 09:09:49+01
137	40	72	7	2016-11-03 09:09:54+01	2016-11-03 09:09:54+01
138	17	73	7	2016-11-03 09:10:09+01	2016-11-03 09:10:09+01
139	2	73	7	2016-11-03 09:10:25+01	2016-11-03 09:10:25+01
140	5	73	7	2016-11-03 09:10:35+01	2016-11-03 09:10:35+01
141	36	73	7	2016-11-03 09:10:43+01	2016-11-03 09:10:43+01
142	14	72	7	2016-11-03 09:16:17+01	2016-11-03 09:16:17+01
143	41	72	7	2016-11-03 10:14:56+01	2016-11-03 10:14:56+01
144	42	72	7	2016-11-03 11:24:35+01	2016-11-03 11:24:35+01
145	43	72	7	2016-11-03 11:35:47+01	2016-11-03 11:35:47+01
146	44	72	7	2016-11-03 11:50:54+01	2016-11-03 11:50:54+01
147	45	72	7	2016-11-03 11:53:40+01	2016-11-03 11:53:40+01
148	4	74	7	2016-11-04 10:27:46+01	2016-11-04 10:27:46+01
149	5	74	7	2016-11-04 10:27:59+01	2016-11-04 10:27:59+01
150	3	74	7	2016-11-04 10:28:24+01	2016-11-04 10:28:24+01
151	2	74	7	2016-11-04 10:29:16+01	2016-11-04 10:29:16+01
152	13	75	10	2016-11-04 17:22:55+01	2016-11-04 17:22:55+01
154	42	73	7	2016-11-17 13:44:31+01	2016-11-17 13:44:31+01
155	43	73	7	2016-11-17 13:44:47+01	2016-11-17 13:44:47+01
156	46	76	10	2016-11-22 19:48:09+01	2016-11-22 19:48:09+01
157	47	68	10	2016-11-22 20:33:10+01	2016-11-22 20:33:10+01
158	29	30	7	2016-12-14 09:03:19+01	2016-12-14 09:03:19+01
159	15	30	7	2016-12-14 09:04:30+01	2016-12-14 09:04:30+01
160	4	30	7	2016-12-14 09:04:43+01	2016-12-14 09:04:43+01
161	2	30	7	2016-12-14 09:05:03+01	2016-12-14 09:05:03+01
162	22	30	7	2016-12-14 09:05:24+01	2016-12-14 09:05:24+01
163	38	30	7	2016-12-14 09:05:35+01	2016-12-14 09:05:35+01
164	5	30	7	2016-12-14 09:05:50+01	2016-12-14 09:05:50+01
165	12	30	7	2016-12-14 09:06:15+01	2016-12-14 09:06:15+01
167	35	70	7	2016-12-14 11:27:28+01	2016-12-14 11:27:28+01
168	42	70	7	2016-12-14 11:27:59+01	2016-12-14 11:27:59+01
169	15	30	10	2016-12-14 17:27:46+01	2016-12-14 17:27:46+01
170	3	30	10	2016-12-14 17:28:28+01	2016-12-14 17:28:28+01
171	4	30	10	2016-12-14 17:29:17+01	2016-12-14 17:29:17+01
172	29	30	10	2016-12-14 17:29:30+01	2016-12-14 17:29:30+01
173	2	30	10	2016-12-14 17:29:49+01	2016-12-14 17:29:49+01
174	32	30	10	2016-12-14 17:30:07+01	2016-12-14 17:30:07+01
175	38	30	10	2016-12-14 17:30:30+01	2016-12-14 17:30:30+01
176	48	82	10	2017-01-02 14:32:29+01	2017-01-02 14:32:29+01
177	49	85	10	2017-02-04 18:22:24+01	2017-02-04 18:22:24+01
134	49	72	7	2016-11-02 18:10:19+01	2017-02-09 14:14:36+01
179	75	87	10	2017-02-09 14:36:14+01	2017-02-09 14:36:14+01
180	15	37	7	2017-02-15 06:47:49+01	2017-02-15 06:47:49+01
181	80	37	7	2017-02-15 06:51:01+01	2017-02-15 06:51:01+01
182	85	37	7	2017-02-15 07:01:05+01	2017-02-15 07:01:05+01
183	86	37	7	2017-02-15 07:12:09+01	2017-02-15 07:12:09+01
185	4	37	7	2017-02-15 07:16:48+01	2017-02-15 07:16:48+01
186	87	37	7	2017-02-15 07:24:26+01	2017-02-15 07:24:26+01
187	97	76	10	2017-03-10 15:02:34+01	2017-03-10 15:02:34+01
188	98	76	10	2017-03-10 15:10:46+01	2017-03-10 15:10:46+01
189	99	76	10	2017-03-10 15:21:54+01	2017-03-10 15:21:54+01
190	100	76	10	2017-03-10 15:49:59+01	2017-03-10 15:49:59+01
191	101	76	10	2017-03-10 15:56:01+01	2017-03-10 15:56:01+01
192	101	127	10	2017-03-10 16:38:33+01	2017-03-10 16:38:33+01
193	100	127	10	2017-03-10 16:38:58+01	2017-03-10 16:38:58+01
194	98	127	10	2017-03-10 16:40:04+01	2017-03-10 16:40:04+01
195	29	127	10	2017-03-10 16:40:19+01	2017-03-10 16:40:19+01
196	46	127	10	2017-03-10 16:40:36+01	2017-03-10 16:40:36+01
197	102	127	10	2017-03-10 16:43:44+01	2017-03-10 16:43:44+01
198	103	127	10	2017-03-10 16:46:03+01	2017-03-10 16:46:03+01
200	86	127	10	2017-03-10 17:17:18+01	2017-03-10 17:17:18+01
201	104	127	10	2017-03-10 17:52:27+01	2017-03-10 17:52:27+01
202	105	127	10	2017-03-10 18:11:33+01	2017-03-10 18:11:33+01
204	24	6	10	2017-03-14 10:07:05+01	2017-03-14 10:07:05+01
205	32	6	10	2017-03-14 10:11:19+01	2017-03-14 10:11:19+01
206	16	6	10	2017-03-14 10:12:10+01	2017-03-14 10:12:10+01
209	106	6	10	2017-03-14 13:27:45+01	2017-03-14 13:27:45+01
6	2	6	10	2016-04-18 11:16:34+02	2016-04-18 11:16:34+02
41	4	6	10	2016-06-13 12:21:29+02	2016-06-13 12:21:29+02
43	5	6	10	2016-06-13 12:23:28+02	2016-06-13 12:23:28+02
210	15	6	10	2017-03-14 13:34:57+01	2017-03-14 13:34:57+01
211	3	6	10	2017-03-14 13:35:52+01	2017-03-14 13:35:52+01
213	25	132	10	2017-03-20 17:58:45+01	2017-03-20 17:58:45+01
214	102	132	10	2017-03-20 17:59:50+01	2017-03-20 17:59:50+01
215	98	132	10	2017-03-20 18:00:28+01	2017-03-20 18:00:28+01
216	11	132	10	2017-03-20 18:01:19+01	2017-03-20 18:01:19+01
217	108	132	10	2017-03-20 22:25:15+01	2017-03-20 22:25:15+01
218	109	132	10	2017-03-21 14:22:16+01	2017-03-21 14:22:16+01
219	110	132	10	2017-03-21 15:36:26+01	2017-03-21 15:36:26+01
220	9	134	10	2017-03-28 19:51:08+02	2017-03-28 19:51:08+02
221	107	134	10	2017-03-28 19:51:50+02	2017-03-28 19:51:50+02
\.


--
-- Name: relationships_softwares_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.relationships_softwares_users_id_seq', 242, true);


--
-- Data for Name: relationships_users; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.relationships_users (id, user_id, recipient_id, relationship_id, created, modified) FROM stdin;
\.


--
-- Name: relationships_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.relationships_users_id_seq', 2, true);


--
-- Data for Name: requests; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.requests (id, url, content_type, status_code, method, requested_at) FROM stdin;
9e50f962-0b46-4602-8fe1-c277b60e2f74	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:14:15
8bd95d4a-25a9-4644-866c-eb86d8b56009	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:18:06
e704e954-baa1-4fa4-afb4-c84ac599b9f4	/manivelle/api/v1/request/3	application/json	200	GET	2016-05-10 13:20:01
a9614a9c-e5d3-4944-ba69-4c92d3a74ada	/manivelle/api/v1/request/3	application/json	200	GET	2016-05-10 13:33:21
b701628b-89f2-46f7-a6f7-cbd4203b8f24	/manivelle/api/v1/relationships-softwares-users/getServicesProviders.json?Relationships=ServicesProvider	application/json	200	GET	2016-05-10 13:33:27
9c43a89a-8631-4733-9a5d-9e3b3ab89c77	/manivelle/api/v1/request/5	application/json	200	GET	2016-05-10 13:33:30
6f61217e-e9d2-40c4-aab4-4a33311e1d48	/manivelle/api/v1/request/7	application/json	200	GET	2016-05-10 13:33:31
641286bd-ea94-4626-8d87-9c69364bf5a3	/manivelle/api/v1/request/15	application/json	200	GET	2016-05-10 13:38:25
93964f6e-9f1b-4bbc-b5b4-6557c5e0b618	/manivelle/api/v1/request?q=	application/json	200	GET	2016-05-10 13:42:37
2d6cbf55-da79-4cf3-a924-125f0078a29d	/	application/json	500	GET	2016-05-10 12:59:28
5d1eb6fa-a0e7-43e5-994f-a909f7b15579	/	application/json	500	GET	2016-05-10 13:07:52
f7942e31-0588-4146-8176-f6a58ea3b60d	/	application/json	500	GET	2016-05-10 13:14:45
def985e8-5679-4d98-ba4e-043078079b72	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:18:06
5b66c291-2a15-41d2-8a9b-3e2f4565d1c1	/manivelle/api/v1/request/5	application/json	200	GET	2016-05-10 13:20:01
ed563e43-2772-4fcb-89e9-15e3ec83bbd2	/manivelle/api/v1/request/5	application/json	200	GET	2016-05-10 13:33:21
9910a233-a263-425e-8cc8-eb1d0c9663f3	/manivelle/api/v1/users/view/6	application/json	200	GET	2016-05-10 13:33:27
b25dc125-d0ca-4aed-8db6-11ff87d7a64b	/manivelle/api/v1/request/4	application/json	200	GET	2016-05-10 13:33:30
c0052343-fbc7-4999-b971-d439d55edbf7	/manivelle/api/v1/request/11	application/json	200	GET	2016-05-10 13:33:31
baa5e46d-f75d-4602-a162-e7937c757c4f	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/15	application/json	200	GET	2016-05-10 13:38:55
c205ab8d-d259-4e21-9ac9-069ad2c492a2	/	application/json	500	GET	2016-05-10 13:42:37
80c5267a-9f2b-4de1-8ad2-c4bf51aa07a3	/	application/json	500	GET	2016-05-10 13:00:38
3d7d496b-e802-4870-9bc7-02803997da8c	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:08:23
7af114bb-0347-41b1-a9a2-ddd0f1982a93	/	application/json	500	GET	2016-05-10 13:14:56
7a40548d-3166-4080-bb12-da523d27c9e9	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:18:42
b6ad8e4c-587f-41a5-ba95-dc2319996d7d	/manivelle/api/v1/request/4	application/json	200	GET	2016-05-10 13:20:01
316b8e18-a49c-40ea-b0cc-cfd8d5e1c270	/manivelle/api/v1/request/4	application/json	200	GET	2016-05-10 13:33:21
2a863f43-6426-4a5d-b776-3fedf6c31e1b	/manivelle/api/v1/users/	application/json	200	GET	2016-05-10 13:33:28
a5e08b30-5cf0-4d79-8f32-bb452ce67ce4	/manivelle/api/v1/request/21	application/json	200	GET	2016-05-10 13:33:30
b757af90-835d-44b9-81a4-f91ee44f9586	/manivelle/api/v1/request/8	application/json	200	GET	2016-05-10 13:33:31
20a683ad-4f93-4d3e-bd70-3f170b47999a	/manivelle/api/v1/request/15	application/json	200	GET	2016-05-10 13:38:56
536070bb-264a-405c-8c79-171c8707b3d6	/	application/json	404	GET	2016-05-10 13:42:38
456b3f3f-989a-427f-86ae-e7afd4014320	/	application/json	500	GET	2016-05-10 13:01:32
655340c1-5b93-4183-a9f8-9bff51e881cf	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:12:51
1efc849c-6dfe-495e-9797-23a4cd2cd134	/	application/json	500	GET	2016-05-10 13:14:57
ae68331e-4473-4f5e-bdbd-5c566f36455a	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:18:42
3fadd602-9b08-4c43-bab7-5fae838c4fdd	/manivelle/api/v1/request/5	application/json	200	GET	2016-05-10 13:20:07
dbe608e3-3783-4de1-b95d-f97f0a3c2279	/manivelle/api/v1/users/	application/json	200	GET	2016-05-10 13:33:24
61597865-9231-45ae-b720-b5fa7040611a	/manivelle/api/v1/users/view/3	application/json	200	GET	2016-05-10 13:33:28
5301e909-779b-4ad6-b2c8-11f9e62edcab	/manivelle/api/v1/request/19	application/json	200	GET	2016-05-10 13:33:30
e7b90143-ac2b-4cc1-b145-7ec8dfb989f0	/manivelle/api/v1/request/20	application/json	200	GET	2016-05-10 13:33:31
eee916f7-ba1d-4e5b-a293-34f105ca4c23	/manivelle/api/v1/request/15	application/json	200	GET	2016-05-10 13:38:56
e7c14595-8a42-459f-8de8-dccc19690c5a	/	application/json	500	GET	2016-05-10 13:01:32
a84b500e-d812-428c-9993-3c1503ccc843	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:13:20
c1000565-6c39-4b31-ac78-89ab1ba30dfa	/	application/json	500	GET	2016-05-10 13:14:58
4dd63ef2-daa8-4f5a-bd99-bc48c05c544a	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:18:42
6fbbb625-1bb4-408d-8725-f051797586f1	/manivelle/api/v1/request/6	application/json	200	GET	2016-05-10 13:20:08
f2874062-a165-46d2-aca8-f67cfa43704b	/manivelle/api/v1/users/view/3	application/json	200	GET	2016-05-10 13:33:24
976b3a74-c4cc-4ef3-982f-4db4ba7135a1	/manivelle/api/v1/users/view/2	application/json	200	GET	2016-05-10 13:33:28
2b5eff80-566b-417e-ade3-bae922c20f20	/manivelle/api/v1/request/18	application/json	200	GET	2016-05-10 13:33:30
73b238ac-0b4e-4e02-920d-955444a09668	/manivelle/api/v1/request/13	application/json	200	GET	2016-05-10 13:33:32
f7d8c95e-4e0d-470a-a4d7-526a70e7e490	/manivelle/api/v1/users/	application/json	200	GET	2016-05-10 13:42:34
d2f8d84e-df00-4ca9-9c95-d4e9cd502a19	/	application/json	500	GET	2016-05-10 13:01:59
d93dcbcf-56fa-4616-8f6e-fb4acc32046c	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:14:03
abd6cb98-e6cc-4852-9eee-026c601e88a5	/	application/json	500	GET	2016-05-10 13:14:58
6e86b101-09de-42c4-8a99-c48229d94ef5	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:18:42
a95815e0-a19d-45e2-a88b-bc0ab1fc42e5	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:32:48
b424ca68-42ad-4e33-abc0-f2dde8817f01	/manivelle/api/v1/users/view/2	application/json	200	GET	2016-05-10 13:33:25
260cda43-aa29-4428-bdb6-31123c5298bb	/manivelle/api/v1/users/view/4	application/json	200	GET	2016-05-10 13:33:28
0af1c687-a775-4e20-bd56-194000a14bdd	/manivelle/api/v1/request/17	application/json	200	GET	2016-05-10 13:33:30
7da0e18f-5b16-45e9-82f7-88d5bee3eeea	/	application/json	404	GET	2016-05-10 13:33:32
42d5324d-e0a6-4f77-864d-2eecc0d0f07f	/manivelle/api/v1/users/view/3	application/json	200	GET	2016-05-10 13:42:34
690c6b91-aae1-4115-8d5e-2dd33927e7f7	/manivelle/api/v1/relationships-softwares-users/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:02:15
c61f7445-05f9-4a87-ab9a-ff49ddb95de0	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:15:27
0e4ed0a0-086e-4ae5-8d60-d024e5ffabca	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:18:42
5f1dad14-713e-47d0-aa3d-7cec0be47b9e	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:32:49
ea10c865-8998-4e01-99ac-734e6f50ad5a	/manivelle/api/v1/users/view/4	application/json	200	GET	2016-05-10 13:33:25
6b7cc407-9ede-4409-97a8-aef48e4978e4	/manivelle/api/v1/users/view/5	application/json	200	GET	2016-05-10 13:33:28
4d6cee75-c177-4bca-a9e4-9f2f587631dc	/manivelle/api/v1/request/14	application/json	200	GET	2016-05-10 13:33:30
3e8464c3-119e-41bc-998a-44587e97d303	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:33:47
b63c17fb-4a62-45e3-88a0-8e7fed8e0190	/manivelle/api/v1/users/view/2	application/json	200	GET	2016-05-10 13:42:34
7c3bc6c7-ac67-42b8-bd05-705c8c17c051	/	application/json	500	GET	2016-05-10 13:05:19
a9ccb7d3-bff6-48da-8caa-d4a8ddce12a7	/manivelle/api/v1/relationships-softwares/getWorkingWellRelationshipsBySoftwareId/2.json	application/json	200	GET	2016-05-10 13:16:47
06f69927-fb09-4fc8-8343-8124b209839c	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:19:21
b45942fb-bf96-4904-a026-d22a6eb91613	/manivelle/api/v1/request/3	application/json	200	GET	2016-05-10 13:32:49
ce1eafbb-45fe-4e81-9ed7-274e5f493815	/manivelle/api/v1/users/view/5	application/json	200	GET	2016-05-10 13:33:25
74fa5079-7ba9-438d-ad62-8a6f1641b176	/manivelle/api/v1/users/view/6	application/json	200	GET	2016-05-10 13:33:28
6f96e3fa-6cda-478a-9348-951b4f2d72b8	/manivelle/api/v1/request/12	application/json	200	GET	2016-05-10 13:33:30
4f82e408-e2a3-4452-a9d1-5f11a831b808	/manivelle/api/v1/request/15	application/json	200	GET	2016-05-10 13:34:11
337e2e7a-3d3c-4029-87a7-47a69faa0d0c	/manivelle/api/v1/users/view/4	application/json	200	GET	2016-05-10 13:42:34
86c87b62-5709-4869-bbd9-bdff15798dbd	/manivelle-web/	text/html	200	GET	2016-05-25 15:44:18
e7e4762a-bf9c-4aa8-b43e-c93094c87a3c	/manivelle/api/v1/relationships-softwares-users/getWorkingWellRelationshipsBySoftwareId/5.json	application/json	200	GET	2016-05-10 12:55:44
43429569-945e-4e80-abe6-e0add30efd47	/	application/json	500	GET	2016-05-10 13:05:45
69155d3b-e44d-4259-8ad0-51db81dc2939	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:16:49
d85f1bac-ce16-489c-826f-5f668b7b1705	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:19:40
049410b6-f229-4d91-a3e3-b981e76f9dc0	/manivelle/api/v1/request/5	application/json	200	GET	2016-05-10 13:32:49
b24875e9-fd34-4f0c-b1da-2fc1b139c760	/manivelle/api/v1/users/view/6	application/json	200	GET	2016-05-10 13:33:25
d78f37c3-e4c2-45d0-94fd-172ce0e02280	/manivelle/api/v1/softwares/	application/json	200	GET	2016-05-10 13:33:29
40a73384-9b78-4c0c-9129-61dc0b3ee109	/manivelle/api/v1/request/3	application/json	200	GET	2016-05-10 13:33:31
b71703e9-8028-4cb1-8e75-def238d183f6	/	application/json	404	GET	2016-05-10 13:34:12
118a2f01-ab2c-4064-a1c2-006130fc8ced	/manivelle/api/v1/users/view/5	application/json	200	GET	2016-05-10 13:42:35
d7d5f66f-69a3-463e-b7d3-debab799c13d	/manivelle-web/	text/html	200	GET	2016-05-25 15:44:19
4ba315be-73c2-4195-abcb-b4fa25993a94	/manivelle/api/v1/relationships-softwares-users/getWorkingWellRelationshipsBySoftwareId/4.json	application/json	200	GET	2016-05-10 12:55:46
576f55a3-f312-4fb4-b6a3-66e36663ff04	/	application/json	500	GET	2016-05-10 13:05:46
a0cc4f66-c45c-4d69-8b8c-fbf18460d6c4	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:17:05
3c9e5b35-1fdc-404d-accd-c6ff63eae478	/	application/json	404	GET	2016-05-10 13:19:52
7c2f616e-e5fa-4b9e-bc4b-c214cd7652df	/manivelle/api/v1/request/4	application/json	200	GET	2016-05-10 13:32:49
f13388c8-d89b-4542-adef-633ac3ca1a3c	/manivelle/api/v1/request?q=	application/json	200	GET	2016-05-10 13:33:26
36b4ae1d-73cb-46bf-87e7-7b40c07676c1	/manivelle/api/v1/request/16	application/json	200	GET	2016-05-10 13:33:29
e9c768e4-8aa3-4ff1-9ed9-19308ce2faa8	/manivelle/api/v1/request/9	application/json	200	GET	2016-05-10 13:33:31
51e92539-17d0-4d74-a2f0-1c2a7ab4f0fb	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/15	application/json	200	GET	2016-05-10 13:34:14
23c79ed8-63df-4c50-a446-6a048bd63d92	/manivelle/api/v1/users/view/6	application/json	200	GET	2016-05-10 13:42:35
6c535deb-bfb2-4249-b118-fc0a2fa83375	/	application/json	500	GET	2016-05-10 12:56:11
13d3edd6-45fe-4eb0-9c3c-c306f33c92c6	/	application/json	500	GET	2016-05-10 13:05:46
b85ad212-6aa0-4530-ae4f-199d8eaeb799	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:17:41
16890b88-6842-4f16-953e-6cc33e47a7dc	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:20:01
7ad340ec-e868-4e8e-89c2-ad91705040b0	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:33:21
09a99dea-d1cc-444e-8d5b-c6e5e9e5ad55	/	application/json	500	GET	2016-05-10 13:33:26
16900c1b-d413-441f-baec-b1fbdc7c8691	/manivelle/api/v1/request/15	application/json	200	GET	2016-05-10 13:33:29
9db57d6e-47c2-44a5-857e-15629146fbb6	/manivelle/api/v1/request/6	application/json	200	GET	2016-05-10 13:33:31
1a0d51ee-8592-4282-9b19-d32c279ff17d	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/15	application/json	200	GET	2016-05-10 13:38:25
7d242562-b164-46b7-92ff-763748fed24f	/manivelle/api/v1/relationships-softwares-users/getServicesProviders.json?Relationships=ServicesProvider	application/json	200	GET	2016-05-10 13:42:36
19174de7-bdae-469d-9ef0-a67bf51e9f20	/	application/json	500	GET	2016-05-10 12:56:35
4fb038d9-7656-4465-ad3d-a632935964b4	/	application/json	500	GET	2016-05-10 13:05:46
e98fd9ee-b7c7-4e35-8bf6-b1a465b1707a	/manivelle/api/v1/RelationshipsSoftwares/getWorkingWellRelationshipsBySoftwareId/2	application/json	200	GET	2016-05-10 13:17:59
0a994734-6f4b-461b-a9ea-8b8d481ca53a	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:20:01
43467f09-c60d-48fd-bd0f-4483fc95fc35	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:33:21
d11dc63a-5455-43c7-b8f7-5cbe903424bf	/	application/json	404	GET	2016-05-10 13:33:26
929210cf-bd10-4e42-a5e5-3ceaacb5095d	/manivelle/api/v1/request/2	application/json	200	GET	2016-05-10 13:33:29
aa32ddd1-0ac2-4433-a44e-8c034ec8bfb0	/manivelle/api/v1/request/10	application/json	200	GET	2016-05-10 13:33:31
845be07e-2cae-40ff-8d82-f1a565abe44f	/manivelle/api/v1/request/15	application/json	200	GET	2016-05-10 13:38:25
dcf4a9fc-fcc7-4441-a8d6-979566d1baf7	/manivelle/api/v1/users/view/6	application/json	200	GET	2016-05-10 13:42:36
578da07a-21bf-48f8-a5d9-fd9b20ba3eca	/	application/json	500	GET	2016-05-10 12:56:59
5c71e606-7e39-4681-bc01-ee4755e26e49	/	application/json	500	GET	2016-05-10 13:07:31
1a92be87-1940-4b3f-a498-e30129f36ae8	/manivelle-web/	text/html	200	GET	2016-06-09 16:34:59
4caddf59-32b8-4392-aca9-634007737f0d	/manivelle-web/	text/html	200	GET	2016-06-09 16:40:34
320d3135-c79d-467c-bdd4-5debf50b7649	/manivelle-web/	text/html	200	GET	2016-06-09 16:51:15
\.


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.reviews (id, comment, title, created, user_id, software_id, evaluation, modified) FROM stdin;
10	Libre office me suffit, je l'utilisé de façon intuitive surtout writter et impress. J'ai rencontrée des difficultés pour passer d'un logiciel à l' autre, en tant qu'agent on ne passe pas de l'un à l'autre sans une formation.  	Libre office	2016-06-23 15:50:38+02	29	33	3	2016-06-23 15:50:38+02
6	Produit complet mais un manque de support purement technique. 	Manque support technique	2016-06-23 12:30:26+02	45	33	3	2016-06-23 12:30:26+02
7	Logiciel nous servant à éditer les notes pour composer les délibérations et décisions de façon dématérialisé\r\nGrosse pluvalue sur la rédaction, mise en page et la synthèse des notes\r\nN'est pas encore utilisé dans notre structure pour l'envoi direct par Tdt ou pour la gestion des instances' mais ça viendra.\r\n\r\n	Étape d'édition des notes pour le moment, et création des délibérations. 	2016-06-23 12:43:41+02	42	24	3	2016-06-23 12:43:41+02
8	Nous recherchions une solution de parapheur souple, ergonomique et évolutive. Après une étude comparative, nous avons choisis le parapheur de l'Adullact. Nos premières mises en pratiques sont concluantes avec l'élaboration des pièces marchés en pilote à la DSI. Les prochaines seront la signature du PES et les processus RH.\r\nAvantages: ergonomie, souplesse, connexions avec les logiciels métier\r\nInconvénients : un peu technique pour la mise en place pour des non informaticiens 	Le parapheur souple et ergonomique	2016-06-23 14:07:08+02	52	4	3	2016-06-23 14:07:08+02
9	Alfesco est notre GED transverse pour le stockage de l'ensemble des documents. Elle sert aujourd'hui pour le stockage des pièces justificatives du PES V2 via le connecteur GED de Grand Angle et pour l'affichage du dossier bénéficiaire pour la MDPH via une visionneuse développée spécifiquement.\r\nAvantages : évolutivité, robustesse\r\nInconvénients: nécessité de développer des surcouches Web car share est trop compliqué pour nos agents	Note GED transverse au département de Seine Maritime	2016-06-23 14:13:24+02	52	29	3	2016-06-23 14:13:24+02
12	Un bon produit qui évolue rapidement à l'écoute de ses utilisateurs et de leurs besoins.	De mieux en mieux...	2016-06-24 14:21:54+02	30	32	3	2016-06-24 14:21:54+02
4	Développement des @services de la métropole et pour l ensemble des communes membres. Lancement du site mobile le 4 juillet 2016.	Gestion des relations citoyens 	2016-06-23 09:42:47+02	36	26	4	2016-06-23 09:42:47+02
11	LUTECE propose une plate forme très modulaire pour bâtir des services numériques de différents : téléservices, prises de rendez-vous, suivi des demandes administratives et relation usager, ... .  Le socle offre un portail qui peut être facilement étendu par des fonctions métiers. Il offre toutes les garanties d'interoperabilité, de robustesse et de sécurité. \r\n 	Lutece,  plate forme de développement de services numériques 	2016-06-23 17:31:22+02	15	10	4	2016-06-23 17:31:22+02
\.


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.reviews_id_seq', 39, true);


--
-- Data for Name: screenshots; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.screenshots (id, software_id, url_directory, created, modified, name, photo) FROM stdin;
306	23	\N	2016-06-21 19:39:34+02	2016-06-21 19:39:34+02		\N
307	31	\N	2016-06-21 19:46:06+02	2016-06-21 19:46:06+02		\N
271	22	files/Screenshots	2016-06-15 15:07:33+02	2016-06-15 15:07:33+02	Screenshot_xivo_login.png	1465996053_Screenshot_xivo_login.png
115	10	files/Screenshots	2016-06-10 17:27:45+02	2016-06-10 17:27:45+02	Screenshot_lutece_lalla.png	1465572465_Screenshot_lutece_lalla.png
118	2	files/Screenshots	2016-06-10 17:29:47+02	2016-06-10 17:29:47+02	Screenshot_pastell_connecteurglobaux.jpg	1465572587_Screenshot_pastell_connecteurglobaux.jpg
127	5	files/Screenshots	2016-06-13 12:38:39+02	2016-06-13 12:38:39+02	Screenshot_s2low_action.png	1465814319_Screenshot_s2low_action.png
161	2	files/Screenshots	2016-06-14 11:31:40+02	2016-06-14 11:31:40+02	Screenshot_pastell_detaildoc.jpg	1465896700_Screenshot_pastell_detaildoc.jpg
163	2	files/Screenshots	2016-06-14 11:34:09+02	2016-06-14 11:34:09+02	Screenshot_pastell_connection.jpg	1465896849_Screenshot_pastell_connection.jpg
166	2	files/Screenshots	2016-06-14 11:37:05+02	2016-06-14 11:37:05+02	Screenshot_pastell_accueil.jpg	1465897025_Screenshot_pastell_accueil.jpg
167	2	files/Screenshots	2016-06-14 11:41:08+02	2016-06-14 11:41:08+02	Screenshot_pastell_associationfluxconnecteur.jpg	1465897268_Screenshot_pastell_associationfluxconnecteur.jpg
168	2	files/Screenshots	2016-06-14 11:42:04+02	2016-06-14 11:42:04+02	Screenshot_pastell_carnetadresse.jpg	1465897324_Screenshot_pastell_carnetadresse.jpg
169	2	files/Screenshots	2016-06-14 11:42:42+02	2016-06-14 11:42:42+02	Screenshot_pastell_configconnecteur.jpg	1465897362_Screenshot_pastell_configconnecteur.jpg
171	2	files/Screenshots	2016-06-14 11:46:30+02	2016-06-14 11:46:30+02	Screenshot_pastell_entitée.jpg	1465897590_Screenshot_pastell_entitée.jpg
172	2	files/Screenshots	2016-06-14 11:50:56+02	2016-06-14 11:50:56+02	Screenshot_pastell_menuuser.jpg	1465897856_Screenshot_pastell_menuuser.jpg
173	2	files/Screenshots	2016-06-14 11:53:16+02	2016-06-14 11:53:16+02	Screenshot_pastell_doc.jpg	1465897996_Screenshot_pastell_doc.jpg
174	2	files/Screenshots	2016-06-14 11:54:17+02	2016-06-14 11:54:17+02	Screenshot_pastell_etatdoc.jpg	1465898057_Screenshot_pastell_etatdoc.jpg
175	2	files/Screenshots	2016-06-14 11:55:12+02	2016-06-14 11:55:13+02	Screenshot_pastell_flux.jpg	1465898112_Screenshot_pastell_flux.jpg
176	2	files/Screenshots	2016-06-14 11:56:13+02	2016-06-14 11:56:13+02	Screenshot_pastell_journalevent.jpg	1465898173_Screenshot_pastell_journalevent.jpg
177	2	files/Screenshots	2016-06-14 11:57:37+02	2016-06-14 11:57:37+02	Screenshot_pastell_listedoc.jpg	1465898257_Screenshot_pastell_listedoc.jpg
178	2	files/Screenshots	2016-06-14 11:58:52+02	2016-06-14 11:58:52+02	Screenshot_pastell_rechercheavancée.jpg	1465898332_Screenshot_pastell_rechercheavancée.jpg
180	3	files/Screenshots	2016-06-14 12:01:33+02	2016-06-14 12:01:33+02	Screenshot_as@lae_accueil.jpg	1465898493_Screenshot_as@lae_accueil.jpg
181	3	files/Screenshots	2016-06-14 12:01:45+02	2016-06-14 12:01:45+02	Screenshot_as@lae__ajouttransfert.jpg	1465898505_Screenshot_as@lae__ajouttransfert.jpg
182	3	files/Screenshots	2016-06-14 12:04:02+02	2016-06-14 12:04:02+02	Screenshot_as@lae_bleurestitutuion.jpg	1465898642_Screenshot_as@lae_bleurestitutuion.jpg
183	3	files/Screenshots	2016-06-14 12:04:58+02	2016-06-14 12:04:58+02	Screenshot_as@lae_contenu.jpg	1465898698_Screenshot_as@lae_contenu.jpg
184	3	files/Screenshots	2016-06-14 12:05:51+02	2016-06-14 12:05:51+02	Screenshot_as@lae_controle.jpg	1465898751_Screenshot_as@lae_controle.jpg
185	3	files/Screenshots	2016-06-14 12:06:40+02	2016-06-14 12:06:40+02	Screenshot_as@lae_editiontransfert.jpg	1465898800_Screenshot_as@lae_editiontransfert.jpg
186	3	files/Screenshots	2016-06-14 12:07:19+02	2016-06-14 12:07:19+02	Screenshot_as@lae_listeentrée.jpg	1465898839_Screenshot_as@lae_listeentrée.jpg
187	3	files/Screenshots	2016-06-14 12:07:46+02	2016-06-14 12:07:46+02	Screenshot_as@lae_listetransferts.jpg	1465898866_Screenshot_as@lae_listetransferts.jpg
188	3	files/Screenshots	2016-06-14 12:08:02+02	2016-06-14 12:08:02+02	Screenshot_as@lae_menu.jpg	1465898882_Screenshot_as@lae_menu.jpg
189	3	files/Screenshots	2016-06-14 12:08:18+02	2016-06-14 12:08:18+02	Screenshot_as@lae_nonconforme.jpg	1465898898_Screenshot_as@lae_nonconforme.jpg
190	3	files/Screenshots	2016-06-14 12:08:31+02	2016-06-14 12:08:31+02	Screenshot_as@lae_orangeélimination.jpg	1465898911_Screenshot_as@lae_orangeélimination.jpg
191	3	files/Screenshots	2016-06-14 12:08:42+02	2016-06-14 12:08:42+02	Screenshot_as@lae_piecesjointes.jpg	1465898922_Screenshot_as@lae_piecesjointes.jpg
192	3	files/Screenshots	2016-06-14 12:08:57+02	2016-06-14 12:08:57+02	Screenshot_as@lae_transfertarchivage.jpg	1465898937_Screenshot_as@lae_transfertarchivage.jpg
194	3	files/Screenshots	2016-06-14 12:10:29+02	2016-06-14 12:10:29+02	Screenshot_as@lae_validitétransfert.jpg	1465899029_Screenshot_as@lae_validitétransfert.jpg
195	3	files/Screenshots	2016-06-14 12:11:22+02	2016-06-14 12:11:22+02	Screenshot_as@lae_versement.jpg	1465899082_Screenshot_as@lae_versement.jpg
196	9	files/Screenshots	2016-06-14 12:15:07+02	2016-06-14 12:15:07+02	Screenshot_Asqatasun_Login.png	1465899307_Screenshot_Asqatasun_Login.png
197	9	files/Screenshots	2016-06-14 12:15:17+02	2016-06-14 12:15:17+02	Screenshot_Asqatasun_acceuil.png	1465899317_Screenshot_Asqatasun_acceuil.png
198	9	files/Screenshots	2016-06-14 12:15:26+02	2016-06-14 12:15:26+02	Screenshot_Asqatasun_submit.png	1465899326_Screenshot_Asqatasun_submit.png
204	15	files/Screenshots	2016-06-14 15:39:04+02	2016-06-14 15:39:04+02	Screenshot_idelibre_index.jpg	1465911544_Screenshot_idelibre_index.jpg
205	15	files/Screenshots	2016-06-14 15:40:33+02	2016-06-14 15:40:33+02	Screenshot_idelibre_editer.jpg	1465911633_Screenshot_idelibre_editer.jpg
208	15	files/Screenshots	2016-06-14 15:41:09+02	2016-06-14 15:41:09+02	Screenshot_idelibre_convocation.jpg	1465911669_Screenshot_idelibre_convocation.jpg
209	15	files/Screenshots	2016-06-14 15:41:24+02	2016-06-14 15:41:24+02	Screenshot_idelibre_accreception.jpg	1465911684_Screenshot_idelibre_accreception.jpg
210	15	files/Screenshots	2016-06-14 15:41:36+02	2016-06-14 15:41:36+02	Screenshot_idelibre_autrebloc.jpg	1465911696_Screenshot_idelibre_autrebloc.jpg
211	15	files/Screenshots	2016-06-14 15:41:51+02	2016-06-14 15:41:51+02	Screenshot_idelibre_bloc.jpg	1465911711_Screenshot_idelibre_bloc.jpg
212	15	files/Screenshots	2016-06-14 15:42:03+02	2016-06-14 15:42:03+02	Screenshot_idelibre_accueil.jpg	1465911723_Screenshot_idelibre_accueil.jpg
213	15	files/Screenshots	2016-06-14 15:42:13+02	2016-06-14 15:42:13+02	Screenshot_idelibre_connection.jpg	1465911733_Screenshot_idelibre_connection.jpg
214	15	files/Screenshots	2016-06-14 15:43:51+02	2016-06-14 15:43:51+02	Screenshot_idelibre_ordredujour.jpg	1465911831_Screenshot_idelibre_ordredujour.jpg
215	15	files/Screenshots	2016-06-14 15:44:37+02	2016-06-14 15:44:37+02	Screenshot_idelibre_votreavis.jpg	1465911877_Screenshot_idelibre_votreavis.jpg
216	5	files/Screenshots	2016-06-14 16:15:25+02	2016-06-14 16:15:25+02	Screenshot_s2low_nouvelletransaction.png	1465913725_Screenshot_s2low_nouvelletransaction.png
217	5	files/Screenshots	2016-06-14 16:17:15+02	2016-06-14 16:17:15+02	Screenshot_s2low_dematerialisation.png	1465913835_Screenshot_s2low_dematerialisation.png
218	5	files/Screenshots	2016-06-14 16:17:29+02	2016-06-14 16:17:29+02	Screenshot_s2low_liste.png	1465913849_Screenshot_s2low_liste.png
219	5	files/Screenshots	2016-06-14 16:17:42+02	2016-06-14 16:17:42+02	Screenshot_s2low_enveloppe.png	1465913862_Screenshot_s2low_enveloppe.png
220	5	files/Screenshots	2016-06-14 16:17:57+02	2016-06-14 16:17:57+02	Screenshot_s2low_bienvenue.png	1465913877_Screenshot_s2low_bienvenue.png
221	12	files/Screenshots	2016-06-14 16:25:58+02	2016-06-14 16:25:58+02	Screenshot_xemelios_flux.png	1465914358_Screenshot_xemelios_flux.png
223	12	files/Screenshots	2016-06-14 16:26:44+02	2016-06-14 16:26:44+02	Screenshot_xemelios_plus.png	1465914404_Screenshot_xemelios_plus.png
224	12	files/Screenshots	2016-06-14 16:26:54+02	2016-06-14 16:26:54+02	Screenshot_xemelios_base.png	1465914414_Screenshot_xemelios_base.png
225	12	files/Screenshots	2016-06-14 16:28:00+02	2016-06-14 16:28:00+02	Screenshot_xemelios_export.png	1465914480_Screenshot_xemelios_export.png
227	20	files/Screenshots	2016-06-15 14:49:51+02	2016-06-15 14:49:51+02	Screenshot from 2016-06-01 11:20:43.png	1465994991_Screenshot from 2016-06-01 11:20:43.png
228	20	files/Screenshots	2016-06-15 14:50:27+02	2016-06-15 14:50:27+02	Screenshot from 2016-06-01 11:19:43.png	1465995027_Screenshot from 2016-06-01 11:19:43.png
229	4	files/Screenshots	2016-06-15 14:51:19+02	2016-06-15 14:51:19+02	Screenshot_iparapheur_archive.jpg	1465995079_Screenshot_iparapheur_archive.jpg
230	4	files/Screenshots	2016-06-15 14:51:30+02	2016-06-15 14:51:30+02	Screenshot_iparapheur_mailsecur.jpg	1465995090_Screenshot_iparapheur_mailsecur.jpg
231	4	files/Screenshots	2016-06-15 14:51:40+02	2016-06-15 14:51:40+02	Screenshot_iparapheur_confirmationsignature.jpg	1465995100_Screenshot_iparapheur_confirmationsignature.jpg
232	4	files/Screenshots	2016-06-15 14:51:53+02	2016-06-15 14:51:53+02	Screenshot_iparapheur_traiterdossier.jpg	1465995113_Screenshot_iparapheur_traiterdossier.jpg
233	4	files/Screenshots	2016-06-15 14:52:06+02	2016-06-15 14:52:06+02	Screenshot_iparapheur_bordereau.jpg	1465995126_Screenshot_iparapheur_bordereau.jpg
234	4	files/Screenshots	2016-06-15 14:52:20+02	2016-06-15 14:52:20+02	Screenshot_iparapheur_aperçudossier.jpg	1465995140_Screenshot_iparapheur_aperçudossier.jpg
235	4	files/Screenshots	2016-06-15 14:52:32+02	2016-06-15 14:52:32+02	Screenshot_iparapheur_creationdossier.jpg	1465995152_Screenshot_iparapheur_creationdossier.jpg
236	4	files/Screenshots	2016-06-15 14:52:49+02	2016-06-15 14:52:49+02	Screenshot_iparapheur_preferences.jpg	1465995169_Screenshot_iparapheur_preferences.jpg
237	4	files/Screenshots	2016-06-15 14:53:52+02	2016-06-15 14:53:52+02	Screenshoy_iparapheur.png	1465995232_Screenshoy_iparapheur.png
238	4	files/Screenshots	2016-06-15 14:54:34+02	2016-06-15 14:54:34+02	Screenshot_iparapheur_document.png	1465995274_Screenshot_iparapheur_document.png
239	4	files/Screenshots	2016-06-15 14:54:47+02	2016-06-15 14:54:47+02	Screenshot_iparapheur_option.png	1465995287_Screenshot_iparapheur_option.png
240	4	files/Screenshots	2016-06-15 14:54:57+02	2016-06-15 14:54:57+02	Screenshot_iparapheur_connexion.jpg	1465995297_Screenshot_iparapheur_connexion.jpg
241	4	files/Screenshots	2016-06-15 14:55:08+02	2016-06-15 14:55:08+02	Screenshot_iparapheur_deco.jpg	1465995308_Screenshot_iparapheur_deco.jpg
242	4	files/Screenshots	2016-06-15 14:55:19+02	2016-06-15 14:55:19+02	Screenshot_iparapheur_navigationbureau.jpg	1465995319_Screenshot_iparapheur_navigationbureau.jpg
243	4	files/Screenshots	2016-06-15 14:55:31+02	2016-06-15 14:55:31+02	Screenshot_iparapheur_tableaudebord.jpg	1465995331_Screenshot_iparapheur_tableaudebord.jpg
244	4	files/Screenshots	2016-06-15 14:56:44+02	2016-06-15 14:56:44+02	Screenshot_iparapheur_aperçudossier.jpg	1465995404_Screenshot_iparapheur_aperçudossier.jpg
245	10	files/Screenshots	2016-06-15 14:57:47+02	2016-06-15 14:57:47+02	Screenshot_lutece_calendar.png	1465995467_Screenshot_lutece_calendar.png
246	10	files/Screenshots	2016-06-15 14:58:05+02	2016-06-15 14:58:05+02	Screenshot_lutece_workflow.png	1465995485_Screenshot_lutece_workflow.png
247	10	files/Screenshots	2016-06-15 14:58:17+02	2016-06-15 14:58:17+02	Screenshot_Software_lutece_acceuil.png	1465995497_Screenshot_Software_lutece_acceuil.png
248	17	files/Screenshots	2016-06-15 14:59:05+02	2016-06-15 14:59:05+02	Screenshot_opencimetiere_aperçu.png	1465995545_Screenshot_opencimetiere_aperçu.png
249	17	files/Screenshots	2016-06-15 14:59:36+02	2016-06-15 14:59:36+02	Screenshot_opencimetiere_archive.png	1465995576_Screenshot_opencimetiere_archive.png
250	17	files/Screenshots	2016-06-15 14:59:47+02	2016-06-15 14:59:47+02	Screenshot_opencimetiere_entreprise.png	1465995587_Screenshot_opencimetiere_entreprise.png
251	17	files/Screenshots	2016-06-15 14:59:56+02	2016-06-15 14:59:56+02	screenshot_opencimetiere_recherche.png	1465995596_screenshot_opencimetiere_recherche.png
252	17	files/Screenshots	2016-06-15 15:00:19+02	2016-06-15 15:00:19+02	Screenshot_opencimetiere_accueil.png	1465995619_Screenshot_opencimetiere_accueil.png
253	17	files/Screenshots	2016-06-15 15:00:28+02	2016-06-15 15:00:28+02	Screenshot_opencimetiere_inhumation.png	1465995628_Screenshot_opencimetiere_inhumation.png
254	17	files/Screenshots	2016-06-15 15:00:37+02	2016-06-15 15:00:37+02	screenshot-opencimetiere.png	1465995637_screenshot-opencimetiere.png
255	18	files/Screenshots	2016-06-15 15:02:04+02	2016-06-15 15:02:04+02	Screenshot_openscrutin_parametreadministration.png	1465995724_Screenshot_openscrutin_parametreadministration.png
256	18	files/Screenshots	2016-06-15 15:02:16+02	2016-06-15 15:02:16+02	Screenshot_openscrutin_elu.png	1465995736_Screenshot_openscrutin_elu.png
257	18	files/Screenshots	2016-06-15 15:02:30+02	2016-06-15 15:02:30+02	Screenshot_openscrutin_accueil.png	1465995750_Screenshot_openscrutin_accueil.png
258	16	files/Screenshots	2016-06-15 15:03:20+02	2016-06-15 15:03:20+02	Screenshot_webrsa_participant.png	1465995800_Screenshot_webrsa_participant.png
259	16	files/Screenshots	2016-06-15 15:03:35+02	2016-06-15 15:03:35+02	Screenshot_webrsa_commissionep.png	1465995815_Screenshot_webrsa_commissionep.png
260	16	files/Screenshots	2016-06-15 15:03:45+02	2016-06-15 15:03:45+02	Screenshot_webrsa_commission.png	1465995825_Screenshot_webrsa_commission.png
261	16	files/Screenshots	2016-06-15 15:03:57+02	2016-06-15 15:03:57+02	Screenshot_webrsa_recherchelancée.png	1465995837_Screenshot_webrsa_recherchelancée.png
262	16	files/Screenshots	2016-06-15 15:04:13+02	2016-06-15 15:04:13+02	Screenshot_webrsa_recherche.png	1465995853_Screenshot_webrsa_recherche.png
264	16	files/Screenshots	2016-06-15 15:04:42+02	2016-06-15 15:04:42+02	Screenshot_webrsa_tableauCER.png	1465995882_Screenshot_webrsa_tableauCER.png
265	16	files/Screenshots	2016-06-15 15:04:57+02	2016-06-15 15:04:57+02	Screenshot_webrsa_ajoutréorientation.png	1465995897_Screenshot_webrsa_ajoutréorientation.png
266	16	files/Screenshots	2016-06-15 15:05:12+02	2016-06-15 15:05:12+02	Screenshot_webrsa_réorientation.png	1465995912_Screenshot_webrsa_réorientation.png
267	16	files/Screenshots	2016-06-15 15:05:29+02	2016-06-15 15:05:29+02	Screenshot_webrsa_relancé.png	1465995929_Screenshot_webrsa_relancé.png
268	16	files/Screenshots	2016-06-15 15:05:46+02	2016-06-15 15:05:47+02	Screenshot_webrsa_formulaire.png	1465995947_Screenshot_webrsa_formulaire.png
269	16	files/Screenshots	2016-06-15 15:05:56+02	2016-06-15 15:05:56+02	Screenshot_webrsa_relancesep.png	1465995956_Screenshot_webrsa_relancesep.png
270	22	files/Screenshots	2016-06-15 15:07:03+02	2016-06-15 15:07:04+02	Screenshot_xivo_interfaceAdmin.png	1465996024_Screenshot_xivo_interfaceAdmin.png
272	22	files/Screenshots	2016-06-15 15:07:48+02	2016-06-15 15:07:48+02	Screenshot_software_xivo.jpg	1465996068_Screenshot_software_xivo.jpg
273	25	files/Screenshots	2016-06-15 15:08:32+02	2016-06-15 15:08:32+02	Screenshot_bluemind_chat.png	1465996112_Screenshot_bluemind_chat.png
274	25	files/Screenshots	2016-06-15 15:08:52+02	2016-06-15 15:08:52+02	Screenshot_bluemind_taches.png	1465996132_Screenshot_bluemind_taches.png
275	25	files/Screenshots	2016-06-15 15:09:05+02	2016-06-15 15:09:05+02	Screenshot_bluemind_clendar.png	1465996145_Screenshot_bluemind_clendar.png
276	25	files/Screenshots	2016-06-15 15:09:14+02	2016-06-15 15:09:14+02	Screenshot_bluemind_email.png	1465996154_Screenshot_bluemind_email.png
277	25	files/Screenshots	2016-06-15 15:09:21+02	2016-06-15 15:09:21+02	Screenshot_bluemind_mail.png	1465996161_Screenshot_bluemind_mail.png
279	31	files/Screenshots	2016-06-15 15:12:39+02	2016-06-15 15:12:39+02	Screenshot_obm_synchroAgenda-2.png	1465996359_Screenshot_obm_synchroAgenda-2.png
280	31	files/Screenshots	2016-06-15 15:13:05+02	2016-06-15 15:13:05+02	Screenshot_obm_ligthningAgenda-3.png	1465996385_Screenshot_obm_ligthningAgenda-3.png
281	31	files/Screenshots	2016-06-15 15:13:16+02	2016-06-15 15:13:16+02	Screenshot_obm_outlookAgenda-3.png	1465996396_Screenshot_obm_outlookAgenda-3.png
282	31	files/Screenshots	2016-06-15 15:13:45+02	2016-06-15 15:13:45+02	Screenshot_obm_fiche_document.png	1465996425_Screenshot_obm_fiche_document.png
283	31	files/Screenshots	2016-06-15 15:14:01+02	2016-06-15 15:14:01+02	Screenshot_obm_resultat_recherche_doc.png	1465996441_Screenshot_obm_resultat_recherche_doc.png
284	31	files/Screenshots	2016-06-15 15:14:15+02	2016-06-15 15:14:15+02	Screenshot_obm_documents.png	1465996455_Screenshot_obm_documents.png
285	31	files/Screenshots	2016-06-15 15:14:30+02	2016-06-15 15:14:31+02	Screenshot_obm_production_fiche_projet.png	1465996471_Screenshot_obm_production_fiche_projet.png
286	31	files/Screenshots	2016-06-15 15:15:18+02	2016-06-15 15:15:18+02	Screenshot_obm_project_planning.png	1465996518_Screenshot_obm_project_planning.png
287	31	files/Screenshots	2016-06-15 15:15:33+02	2016-06-15 15:15:33+02	Screenshot_obm_production_gestion_des_temps.png	1465996533_Screenshot_obm_production_gestion_des_temps.png
288	31	files/Screenshots	2016-06-15 15:15:53+02	2016-06-15 15:15:53+02	Screenshot_obm_ContactModifier.png	1465996553_Screenshot_obm_ContactModifier.png
289	31	files/Screenshots	2016-06-15 15:16:40+02	2016-06-15 15:16:40+02	Screenshot_obm_ContactRecherche.png	1465996600_Screenshot_obm_ContactRecherche.png
290	31	files/Screenshots	2016-06-15 15:16:50+02	2016-06-15 15:16:50+02	Screenshot_obm_connecteurs.png	1465996610_Screenshot_obm_connecteurs.png
291	31	files/Screenshots	2016-06-15 15:16:59+02	2016-06-15 15:16:59+02	Screenshot_obm_calendar.png	1465996619_Screenshot_obm_calendar.png
292	31	files/Screenshots	2016-06-15 15:17:11+02	2016-06-15 15:17:11+02	Screenshot_obm_recherche_creneaux_libres.png	1465996631_Screenshot_obm_recherche_creneaux_libres.png
293	31	files/Screenshots	2016-06-15 15:17:23+02	2016-06-15 15:17:23+02	Screenshot_obm_AgendaRDVRessource.png	1465996643_Screenshot_obm_AgendaRDVRessource.png
294	31	files/Screenshots	2016-06-15 15:17:33+02	2016-06-15 15:17:33+02	Screenshot_obm_GrpRessourceCreer.png	1465996653_Screenshot_obm_GrpRessourceCreer.png
295	33	files/Screenshots	2016-06-15 15:18:43+02	2016-06-15 15:18:43+02	Screenshot_office_writerwindows.png	1465996723_Screenshot_office_writerwindows.png
296	33	files/Screenshots	2016-06-15 15:18:55+02	2016-06-15 15:18:55+02	Screenshot_office_writer.png	1465996735_Screenshot_office_writer.png
297	33	files/Screenshots	2016-06-15 15:19:19+02	2016-06-15 15:19:19+02	Screenshot_office_calc.png	1465996759_Screenshot_office_calc.png
298	33	files/Screenshots	2016-06-15 15:19:30+02	2016-06-15 15:19:30+02	Screenshot_office_calcgraph.png	1465996770_Screenshot_office_calcgraph.png
299	33	files/Screenshots	2016-06-15 15:19:43+02	2016-06-15 15:19:43+02	Screenshot_office_draw.png	1465996783_Screenshot_office_draw.png
300	33	files/Screenshots	2016-06-15 15:19:56+02	2016-06-15 15:19:56+02	Screenshot_office_drawpictures.png	1465996796_Screenshot_office_drawpictures.png
301	33	files/Screenshots	2016-06-15 15:21:24+02	2016-06-15 15:21:24+02	Screenshot_office_math.png	1465996884_Screenshot_office_math.png
302	33	files/Screenshots	2016-06-15 15:21:36+02	2016-06-15 15:21:36+02	Screenshot_office_impress.png	1465996896_Screenshot_office_impress.png
303	33	files/Screenshots	2016-06-15 15:21:45+02	2016-06-15 15:21:45+02	Screenshot_office_impresspresentation.png	1465996905_Screenshot_office_impresspresentation.png
305	25	\N	2016-06-21 10:58:34+02	2016-06-21 10:58:34+02		\N
308	27	\N	2016-06-21 19:48:48+02	2016-06-21 19:48:48+02		\N
309	28	\N	2016-06-21 19:50:02+02	2016-06-21 19:50:02+02		\N
310	17	\N	2016-06-21 20:04:48+02	2016-06-21 20:04:48+02		\N
311	16	\N	2016-06-21 20:05:24+02	2016-06-21 20:05:24+02		\N
312	15	\N	2016-06-21 20:06:42+02	2016-06-21 20:06:42+02		\N
313	22	\N	2016-06-21 20:14:56+02	2016-06-21 20:14:56+02		\N
314	18	\N	2016-06-21 20:15:37+02	2016-06-21 20:15:37+02		\N
315	14	\N	2016-06-21 20:18:28+02	2016-06-21 20:18:28+02		\N
316	33	\N	2016-06-21 20:20:56+02	2016-06-21 20:20:56+02		\N
317	25	\N	2016-06-22 11:48:42+02	2016-06-22 11:48:42+02		\N
318	23	\N	2016-06-22 15:02:33+02	2016-06-22 15:02:33+02		\N
319	23	\N	2016-06-22 15:25:40+02	2016-06-22 15:25:40+02		\N
320	33	\N	2016-06-22 17:59:12+02	2016-06-22 17:59:12+02		\N
321	24	\N	2016-07-12 14:20:05+02	2016-07-12 14:20:05+02		\N
322	19	\N	2016-07-12 14:24:21+02	2016-07-12 14:24:21+02		\N
323	35	\N	2016-07-21 17:05:10+02	2016-07-21 17:05:10+02		\N
324	35	files/Screenshots	2016-07-21 17:18:42+02	2016-07-21 17:18:42+02	Screenshot_glpi_central.png	1469114322_Screenshot_glpi_central.png
325	35	files/Screenshots	2016-07-21 17:21:04+02	2016-07-21 17:21:04+02	Screenshot_glpi_ordinateur1.png	1469114464_Screenshot_glpi_ordinateur1.png
326	35	files/Screenshots	2016-07-21 17:21:17+02	2016-07-21 17:21:17+02	Screenshot_glpi_ordinateur2.png	1469114477_Screenshot_glpi_ordinateur2.png
327	35	files/Screenshots	2016-07-21 17:21:29+02	2016-07-21 17:21:29+02	Screenshot_glpi_suivi.png	1469114489_Screenshot_glpi_suivi.png
328	35	files/Screenshots	2016-07-21 17:21:44+02	2016-07-21 17:21:44+02	Screenshot_glpi_intervention.png	1469114504_Screenshot_glpi_intervention.png
329	35	files/Screenshots	2016-07-21 17:21:56+02	2016-07-21 17:21:56+02	Screenshot_glpi_base_connaissance.png	1469114516_Screenshot_glpi_base_connaissance.png
330	36	files/Screenshots	2016-07-26 15:36:41+02	2016-07-26 15:36:41+02	Screenshot_spip_suivit.png	1469540201_Screenshot_spip_suivit.png
331	36	files/Screenshots	2016-07-26 15:36:54+02	2016-07-26 15:36:54+02	Screenshot_spip_article.png	1469540214_Screenshot_spip_article.png
332	36	files/Screenshots	2016-07-26 15:37:23+02	2016-07-26 15:37:23+02	Screenshot_spip_versionarticle.png	1469540243_Screenshot_spip_versionarticle.png
333	36	files/Screenshots	2016-07-26 15:37:46+02	2016-07-26 15:37:46+02	Screenshot_spip_moteurrecherche.png	1469540266_Screenshot_spip_moteurrecherche.png
334	36	files/Screenshots	2016-07-26 15:45:24+02	2016-07-26 15:45:24+02	Screenshot_spip_navigation.png	1469540724_Screenshot_spip_navigation.png
335	36	files/Screenshots	2016-07-26 15:45:37+02	2016-07-26 15:45:37+02	Screenshot_spip_forum.png	1469540737_Screenshot_spip_forum.png
336	36	files/Screenshots	2016-07-26 15:46:03+02	2016-07-26 15:46:03+02	Screenshot_spip_stat.png	1469540763_Screenshot_spip_stat.png
337	36	files/Screenshots	2016-07-26 15:46:13+02	2016-07-26 15:46:13+02	Screenshot_spip_textmode.png	1469540773_Screenshot_spip_textmode.png
338	30	\N	2016-07-27 11:53:28+02	2016-07-27 11:53:28+02		\N
339	10	files/Screenshots	2017-04-12 15:26:04+02	2017-04-12 15:26:04+02	\N	correctSoftwareLogo.jpg
340	13	files/Screenshots	2017-04-12 15:27:14+02	2017-04-12 15:27:14+02	\N	correctSoftwareLogo.jpg
341	20	files/Screenshots	2017-04-12 15:49:11+02	2017-04-12 15:49:11+02	\N	correctSoftwareLogo.jpg
\.


--
-- Name: screenshots_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.screenshots_id_seq', 341, true);


--
-- Data for Name: softwares; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.softwares (id, softwarename, url_repository, description, licence_id, created, modified, logo_directory, photo, url_website, web_site_url) FROM stdin;
22	XiVO	https://gitlab.com/groups/xivo.solutions	XiVO is an IP-PBX, a system that connects phones inside an organization with public and mobile telephone networks. An IP-PBX is a PBX that provides audio, video, and instant messaging services through the TCP/IP protocol. 	10	2016-06-10 17:24:47+02	2017-03-19 21:02:20+01	files/Softwares/22/photo/avatar	Software_Logo_xivo.png	https://www.xivo.solutions/	\N
20	Exoplatform	https://github.com/exoplatform/platform.git	eXo Platform is an open source social collaboration software designed for enterprises. It is full featured, based on standards, extensible and has an amazing design.	\N	2016-04-28 14:43:35+02	2016-06-15 14:50:27+02	files/Softwares/20/photo/avatar	Software_Logo_exoplateform.png	https://www.exoplatform.com/	\N
11	Xwiki	https://github.com/xwiki/xwiki-platform.git	XWiki Platform is a generic wiki platform offering runtime services for applications built on top of it.\r\nXWiki Platform, XWiki Enterprise, XWiki Commons and XWiki Rendering are part of the XWiki.org software forge. They are released together and share the same version.	\N	2016-04-28 12:01:49+02	2016-06-13 15:31:13+02	files/Softwares/11/photo/avatar	Software_Logo_xwiki.png	www.xwiki.com	\N
12	Xemelios	https://scm.adullact.net/anonscm/git/xemelios/xemelios.git	XEMELIOS - Outil de recherche et de consultation de documents XML	7	2016-04-28 12:17:30+02	2016-06-14 16:28:00+02	files/Softwares/12/photo/avatar	Software_logo_xemelios.png	http://xemelios.org/	\N
13	Chouette2	https://github.com/afimb/chouette2.git	Chouette2 is a Ruby on Rails web app for editing, reading, validating and exchanging Public Transport datas.	12	2016-04-28 14:27:09+02	2016-06-13 16:01:26+02	files/Softwares/13/photo/avatar	Software_Logo_chouette.jpg	http://www.chouette.mobi/	\N
10	Lutèce	https://github.com/lutece-platform/lutece-core.git	Content Management Framework libre and puissant en Java	9	2016-04-28 11:57:55+02	2016-06-15 14:58:17+02	files/Softwares/10/photo/avatar	logo-lutece-fd-noir.svg	http://fr.lutece.paris.fr/fr/	\N
16	WebRsa	https://scm.adullact.net/anonscm/svn/webrsa/trunk	Gestion du RSA (Revenu de Solidarité Active) destinée aux CG et à leurs partenaires. Suivi des bénéficiaires du RSA dans leurs démarches d'insertion. Inter-opérable avec le @rSa de la CNAF via des flux XML formalisés.	5	2016-04-28 14:34:41+02	2016-06-21 20:05:24+02	files/Softwares/16/photo/avatar	Software_logo_webrsa.jpg	https://www.libriciel.fr/web-rsa	\N
15	i-delibRE	https://scm.adullact.net/anonscm/svn/idelibre/trunk	Le projet i-delibRE est le porte-document nomade des élus pour le suivi des séances délibérantes de la collectivité.	5	2016-04-28 14:33:12+02	2017-03-14 10:47:17+01	files/Softwares/15/photo/avatar	logo_i_delibRE_transparent_350x250_CL.png	https://www.libriciel.fr/i-delibre	\N
4	i-Parapheur	https://scm.adullact.net/anonscm/svn/paraphelec/trunk	i-Parapheur est un outil de modélisation de procédures de gestion et de validation documentaire. Le i-Parapheur permet la circulation de documents regroupés en « dossiers », au travers d’un circuit de validation comportant des phases successives de visa(s), de signature(s) apposés sur un ou plusieurs documents ou sur des flux métiers. \r\nLe logiciel est basé sur un outil de GED libre ALFRESCO (http://www.alfresco.com/fr/).	5	2016-04-13 11:41:07+02	2017-03-14 13:21:30+01	files/Softwares/4/photo/avatar	i-parapheur_350x250.png	https://www.libriciel.fr/i-parapheur	\N
19	openMairie	https://scm.adullact.net/anonscm/svn/openmairie/	openMairie est un framework de développement PHP orienté objet, modulaire, et qui permet de créer rapidement tous types d'applications avec le SGBD mysql ou pgsql. Depuis la version 3.00, openMairie propose un generateur... [http://www.openmairie.org/]	7	2016-04-28 14:41:00+02	2016-07-12 14:24:21+02	files/Softwares/19/photo/avatar	logo_open-mairie.png	http://www.openmairie.org	\N
28	LinShare	https://github.com/linagora/linshare.git	LinShare vous permettra :    \r\n\r\n    De déposer ou partager des fichiers dans ou à partir de votre espace personnel.          \r\n    De gérer (créer ou supprimer) des groupes ou listes de diffusion.          \r\n    D'avoir une traçabilité des partages ou des téléchargements grâce à des notifications par mail lorsque ceux-ci sont réalisés, mais aussi grâce à votre  historique.    	8	2016-06-13 17:27:50+02	2016-06-21 19:50:02+02	files/Softwares/28/photo/avatar	Software_Logo_linshare.png	http://www.linshare.org	\N
31	OBM	http://git.obm.org/obm	OBM est un logiciel Open Source de travail collaboratif, intégrant les meilleurs composants libres pour gérer et partager les informations au sein d'une organisation.	7	2016-06-14 09:51:10+02	2016-06-21 19:46:06+02	files/Softwares/31/photo/avatar	Software_logo_obm.png	http://obm.org	\N
18	openScrutin	https://scm.adullact.net/anonscm/svn/openscrutin/trunk	openScrutin permet la gestion de la composition des bureaux de vote lors d'une élection politique : secrétaire, planton, président, vice président, assesseur, délégué, ... [http://www.openmairie.org/]	7	2016-04-28 14:40:06+02	2016-06-21 20:15:37+02	files/Softwares/18/photo/avatar	Software_Logo_openscrutin.png	http://www.openmairie.org/catalogue/openscrutin	\N
27	Authentik	http://repos.entrouvert.org/authentic.git	Authentik est une boite à outils permettant de gérer les identités numériques que ce soit en interne (agents) ou en externe (gestion relation usagers).	\N	2016-06-13 16:13:33+02	2016-06-21 19:48:48+02	files/Softwares/27/photo/avatar	Software_Logo_authentik.png	https://dev.entrouvert.org//projects/authentic	\N
21	Trustedbird Client	https://scm.adullact.net/anonscm/git/milimail/milimail.git	The purpose of Trustedbird project is to develop trusted messaging system based on Mozilla Thunderbird, PostFix and MDA compliant with SIEVE.\r\nLe but du projet Trustedbird est de réaliser une messagerie de confiance sur la base de Mozilla Thunderbird, PostFix, les MDA supportant SIEVE, etc...	7	2016-04-28 14:45:08+02	2016-06-13 15:52:03+02	files/Softwares/21/photo/avatar	Software_Logo_Trustedbird.png	http://adullact.net/plugins/mediawiki/wiki/milimail/index.php/Trustedbird_Project	\N
3	As@lae	https://scm.adullact.net/anonscm/svn/asalae/trunk	Système d'archivage électronique à valeur probante, supportant le standard d’échange de données pour l'archivage (SEDA).	8	2016-04-11 12:25:00+02	2017-03-14 13:10:14+01	files/Softwares/3/photo/avatar	asalae_350x250_CL.png	https://www.libriciel.fr/aslae	\N
38	WordPress	git://core.git.wordpress.org/	WordPress constitue le nec plus ultra en matière de plates-formes sémantiques de publication personnelle, alliant esthétique, standards du Web et ergonomie. Gratuit, WordPress n’en est pas moins inestimable. Sous licence GPLv2+, WordPress est un logiciel libre et gratuit.	15	2016-11-02 15:26:37+01	2016-11-02 15:26:37+01	files/Softwares/38/photo/avatar	Logo-wordpress-logo-notext-rgb.png	https://fr.wordpress.com/	\N
106	web-CIL	https://scm.adullact.net/anonscm/svn/webcil/trunk	web-CIL : l’outil de gestion du Correspondant Informatique et Libertés.\r\nGestion du workflow réglementaire des déclarations CNIL.	5	2017-03-14 10:42:04+01	2017-03-14 10:42:59+01	files/Softwares/106/photo/avatar	logo_WebCil_350x250_CL.png	https://www.libriciel.fr/web-cil/	\N
36	SPIP	https://git.spip.net/diffusion/CSPIP/spip-core.git	SPIP est un Système de Publication pour l’Internet. Kesako ? Il s’agit d’un ensemble de fichiers, installés sur votre compte Web, qui vous permettent de bénéficier d’un certain nombre d’automatismes : gérer un site à plusieurs, mettre en page vos articles sans avoir à taper de HTML, modifier très facilement la structure de votre site... Avec le même logiciel qui sert à visiter un site (Chrome, Firefox, Edge, Safari...), SPIP permet de fabriquer et de tenir un site à jour, grâce à une interface très simple d’utilisation.	7	2016-07-26 15:06:12+02	2016-07-26 15:46:13+02	files/Softwares/36/photo/avatar	Logo-spip.png	http://www.spip.net/	\N
45	Jorani	https://github.com/bbalet/jorani.git	Logiciel libre de gestion des absences\r\nJorani est une application web gratuite de gestion des congés. 	8	2016-11-03 11:53:30+01	2016-11-03 11:53:30+01	files/Softwares/45/photo/avatar	Logo-Jorani-100.png	http://fr.jorani.org	\N
44	OpenBiblio	https://bitbucket.org/mstetson/obiblio	OpenBiblio is an easy to use, automated library system written in PHP containing OPAC, circulation, cataloging, and staff administration functionality. 	15	2016-11-03 11:50:45+01	2016-11-03 11:50:45+01	files/Softwares/44/photo/avatar	Logo-openbiblio.gif	http://obiblio.sourceforge.net/	\N
42	LimeSurvey	https://github.com/LimeSurvey/LimeSurvey.git	Le logiciel libre le plus populaire pour les sondages et enquêtes	15	2016-11-03 11:24:09+01	2016-11-03 11:24:09+01	files/Softwares/42/photo/avatar	Logo-Limesurvey.png	https://www.limesurvey.org	\N
41	OpenReglement	:pserver:anonymous@scm.adullact.net:/var/lib/gforge/chroot/scmrepos/cvs/openreglement	Le logiciel permet aux communes de gerer les demandes de cartes d identitée, passeport, attestation d accueil, attestation de sortie du territoire jusqu à leur attribution de ces documents.	7	2016-11-03 10:14:27+01	2016-11-03 10:14:27+01	\N	\N	http://www.openmairie.org/catalogue/openreglement	\N
40	OpenRésultat	https://scm.adullact.net/anonscm/svn/openresultat/trunk	openRésultat, logiciel de gestion des résultats électoraux, permet l'animation des soirées electorales par l'affichage des résultats ainsi que le transfert dématérialisé en préfecture et bien plus encore... [http://www.openmairie.org/]	7	2016-11-03 09:06:17+01	2016-11-03 09:06:17+01	files/Softwares/40/photo/avatar	Logo-openresultat.png	http://www.openmairie.org/catalogue/openresultat	\N
17	openCimetière	https://scm.adullact.net/anonscm/svn/opencimetiere/trunk	openCimetière a pour objectif de gérer les concessions de cimetières; les autorisations, la gestion de place et entretien. openCimetiere_facture est le module de facturation des concessions et fait les arretes... [http://www.openmairie.org/]	7	2016-04-28 14:39:02+02	2016-06-21 20:04:48+02	files/Softwares/17/photo/avatar	Software_Logo_opencimetiere.png	http://www.openmairie.org/catalogue/opencimetiere	\N
14	OpenRecensement	https://scm.adullact.net/anonscm/svn/openrecensement/trunk	Gerer le recensement des jeunes de 18 ans\r\n- recuperation des données Etat Civil (city)\r\n- validation, attestation, avis de recensement\r\n- envoi service national procédure PECOTO	7	2016-04-28 14:29:11+02	2016-06-21 20:18:28+02	files/Softwares/14/photo/avatar	Software_Logo_openrecensement.png	http://www.openmairie.org/front-page/catalogue/openrecensement	\N
25	BlueMind	https://www.bluemind.net/	BlueMind est une solution complète de messagerie collaborative d’entreprise et de communications unifiées, alternative crédible aux solutions Exchange, Domino ou Google.	10	2016-06-13 10:55:59+02	2016-06-22 11:48:42+02	files/Softwares/25/photo/avatar	Software_Logo-bluemind.png	https://www.bluemind.net/	\N
30	LinID	https://github.com/linagora/openpaas-esn.git	Dans OpenPaas, on retrouvera non seulement les fonctionnalités usuelles de courriel, contact, agenda et synchronisation, mais aussi un réseau social d'entreprise, des outils de rédaction collaboratifs et la recommandation contextuelle. Le projet qui s'étendra sur 4 ans coûtera 20 millions d'euros dont une partie est supportée par le gouvernement à hauteur de 10,7 millions d'euros.	9	2016-06-13 17:48:56+02	2016-07-27 11:53:28+02	files/Softwares/30/photo/avatar	Software_logo_linid.png	http://www.linid.org/welcome/index.html	\N
35	GLPI	https://github.com/glpi-project/glpi.git	Solution open-­source de gestion de parc informatique et de servicedesk, GLPI est une application Full Web pour gérer l’ensemble de vos problématiques de gestion de parc informatique : de la gestion de l’inventaire des composantes matérielles ou logicielles d’un parc informatique à la gestion de l’assistance aux utilisateurs.	15	2016-07-21 16:57:49+02	2016-07-21 17:21:56+02	files/Softwares/35/photo/avatar	Software_Logo_GLPI.jpg	http://glpi-project.org	\N
23	OpenADS	https://scm.adullact.net/anonscm/svn/openfoncier/trunk	openADS couvre les fonctions suivantes :\r\n\r\n    Gestion des permis de construire,\r\n    gestion des permis d'aménager,\r\n    gestion des permis de démolir,\r\n    déclaration préalable des travaux,\r\n    suivi et séquencement des étapes d'instruction,\r\n    éditions des documents,\r\n    lien possible avec le SIG.	7	2016-06-10 18:38:49+02	2016-06-22 15:25:40+02	files/Softwares/23/photo/avatar	Software_logo-openads.png	http://www.openmairie.org/catalogue/openads	\N
26	Publik	http://repos.entrouvert.org/publik-base-theme.git	Publik regroupe plusieurs briques logicielles, toutes libres.	8	2016-06-13 11:52:48+02	2016-06-13 15:33:17+02	files/Softwares/26/photo/avatar	Software_logo-publik.png	https://publik.entrouvert.com/	\N
43	GRR - Gestion Réservation de Ressources	:pserver:anonymous@scm.adullact.net:/var/lib/gforge/chroot/scmrepos/cvs/grr	GRR, système de Gestion et de Réservations de Ressources est une adaptation de MRBS.\r\nGRR permet une administration très fine des droits de gestion et d'utilisation des différents domaines et des ressources à l'intérieur des domaines.	7	2016-11-03 11:35:31+01	2016-11-03 11:35:31+01	\N	\N	https://grr.devome.com/fr/	\N
5	S²LOW	https://scm.adullact.net/anonscm/svn/s2low/	Service Sécurisé Libre et interOpérable de Validation et de Vérification (S²LOW).\r\nS²LOW est la solution libre et sécurisée pour traiter la télé-transmission des documents échangés régulièrement entre les collectivités, leurs partenaires et autorités de tutelle.	5	2016-04-13 11:44:52+02	2017-03-14 13:37:30+01	files/Softwares/5/photo/avatar	logo_S2LOW_350x250.png	https://www.libriciel.fr/s2low	\N
51	VeraCrypt	https://git01.codeplex.com/veracrypt	VeraCrypt is a free disk encryption software brought to you by IDRIX (https://www.idrix.fr) and that is based on TrueCrypt 7.1a.	16	2017-02-07 11:13:30+01	2017-02-07 11:13:30+01	files/Softwares/51/photo/avatar	VeraCrypt128x128.png	https://veracrypt.codeplex.com/	\N
54	Métamorphose	https://github.com/metamorphose/metamorphose3.git	A cross platform file and folder mass renamer, allows many different renaming operations in a GUI.	15	2017-02-07 12:56:23+01	2017-02-07 12:56:23+01	files/Softwares/54/photo/avatar	metamorphose_big.png	http://file-folder-ren.sourceforge.net/	\N
53	UltraCopier	 git://github.com/alphaonex86/Ultracopier.git	Ultracopier est un logiciel sous GPL3 pour remplacer la boite de dialogue de copie de fichier. Mais il fournit la reprise sur erreur, limitation de la vitesse, reprise/pause, gestion des erreurs/collisions,...	10	2017-02-07 12:15:58+01	2017-02-07 12:15:58+01	files/Softwares/53/photo/avatar	96px-Ultracopier_logo.png	http://ultracopier-fr.first-world.info/	\N
52	FreeFileSync	https://github.com/feinstaub/freefilesync-pak.git	FreeFileSync is a free Open Source software that helps you synchronize files and synchronize folders for Windows, Linux and Mac OS X. It is designed to save your time setting up and running backup jobs while having nice visual feedback along the way.	10	2017-02-07 11:20:23+01	2017-02-07 11:20:23+01	files/Softwares/52/photo/avatar	freefilesync-logo.png	http://www.freefilesync.org/	\N
47	Dolibarr ERP/CRM	http://www.dolibarr.fr/	Dolibarr ERP & CRM est un logiciel moderne de gestion de votre activité professionnelle ou associative (contacts, factures, commandes, stocks, agenda, etc...).\r\n\r\nC'est un logiciel libre et gratuit adapté pour les entreprises, auto-entrepreneurs ou association.\r\nVous pouvez l'installer et l'utiliser comme une application autonome, ou en ligne sur un serveur mutualisé ou dédié afin d'y accéder depuis n'importe où. Dolibarr est également disponible comme solution prête à l'emploi dans des services Cloud.	10	2016-11-22 20:32:43+01	2016-11-22 20:32:43+01	files/Softwares/47/photo/avatar	DolibarrLogo-600px.png	http://www.dolibarr.fr/	\N
49	Maarch Courrier	https://labs.maarch.org/maarch/MaarchCourrier	Maarch Courrier est une solution complète de la gestion des correspondance et de l'organisation de l'information administrative. \r\n\r\nSes évolutions sont directement piloté par les utilisateurs qui nous co-construisent le logiciel dont ils ont réellement besoin, ce qui permet d'affirmer que Maarch Courrier est la solution de Gestion Électronique des Correspondances, Libre et Open Source, la plus complète du marché !	10	2017-02-04 18:14:08+01	2017-02-04 18:14:08+01	files/Softwares/49/photo/avatar	LOGO CHOISI2.png	http://maarch.com/	\N
48	Rubedo	https://github.com/WebTales/rubedo	Rubedo est une plateforme digitale permettant de créer des plateformes web (CMS) et ecommerce.\r\nSes particularités sont :\r\n- Multisites : la gestion unifiée du multi-sites, des utilisateurs, des contenus et des médias,\r\n- Autonomie : création et modification des sites sans développement,\r\n- Prédictif : les sites peuvent se personnaliser aux centres d'intérêt des internautes connectés ou anonymes (dans le respect des données personnelles),\r\n- Big data : Rubedo est conçu sur MongoDB, ElasticSearch, PHP et AngularJS\r\n	10	2016-12-21 15:45:37+01	2016-12-21 15:45:37+01	files/Softwares/48/photo/avatar	logo_RUBEDO_petit.png	http://www.rubedo-project.org	\N
32	WebGFC	https://scm.adullact.net/anonscm/svn/webgfc/trunk	La gestion du flux prend une place à part dans les processus d'instruction et de gestion des collectivités territoriales.\r\n\r\nLa dématérialisation et les nouvelles exigences induites par les engagements de type « agenda 21 » et les contextes « zéro papier », lui donnent une toute nouvelle place, eu égard au nombre de correspondances produites quotidiennement par les collectivités comme du fait du très grand nombre de flux qui sont reçus chaque jour par les collectivités.	5	2016-06-14 09:55:41+02	2017-03-14 10:10:26+01	files/Softwares/32/photo/avatar	gfc_logo_transparent_350x250_CL.png	https://www.libriciel.fr/web-gfc	\N
61	Freeplane	https://github.com/freeplane/freeplane.git	Freeplane is a free and open source software application that supports thinking, sharing information and getting things done at work, in school and at home. The software can be used for mind mapping and analyzing the information contained in mind maps. Freeplane runs on any operating system that has a current version of Java installed. It can be run locally or portably from removable storage like a USB drive.	7	2017-02-07 15:54:27+01	2017-02-07 15:54:29+01	files/Softwares/61/photo/avatar	Software_logo_freeplane.jpg	https://www.freeplane.org/wiki/index.php/Main_Page	\N
62	VLC	http://git.videolan.org/git/vlc.git	VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols. 	19	2017-02-07 16:08:18+01	2017-02-07 16:08:19+01	files/Softwares/62/photo/avatar	largeVLC.png	https://www.videolan.org/vlc/	\N
63	NVDA	https://github.com/nvaccess/nvda.git	NVDA (NonVisual Desktop Access) is a free “screen reader” which enables blind and vision impaired people to use computers. It reads the text on the screen in a computerised voice. You can control what is read to you by moving the cursor to the relevant area of text with a mouse or the arrows on your keyboard.\r\n\r\nNVDA can also convert the text into braille if the computer user owns a device called a “braille display”.\r\n\r\nNVDA provides the key to education and employment for many blind people. It also provides access to social networking, online shopping, banking and news.\r\n\r\nNVDA works with Microsoft Windows. You can download it to your PC, or to a USB stick which you can use with any computer.\r\n\r\nNormally screen readers are expensive, making them unaffordable for many blind people. NVDA is free. It’s been downloaded 70,000+ times, in 43 languages.	15	2017-02-07 16:09:44+01	2017-02-07 16:09:45+01	files/Softwares/63/photo/avatar	Software_logo_NVDA.png	https://www.nvaccess.org/	\N
64	FileZilla	https://svn.filezilla-project.org/svn/FileZilla3/trunk filezilla	FileZilla is a cross-platform graphical FTP, SFTP, and FTPS file management tool for Windows, Linux, Mac OS X, and more. With tons of intuitive tools, FileZilla helps you quickly move files between your computer and Web server. If you plan to use FileZilla regularly, you might like the advanced features like manual configuration and speed limit monitoring. While FTP may seem outdated, it’s actually a very reliable way to transfer large files or groups of files to your Web server. And, with FileZilla, you can deploy multiple simultaneous connections to speed up file transfers. Overall, FileZilla has everything you need to support your FTP needs including a documentation wiki and a forum. 	15	2017-02-07 16:16:58+01	2017-02-07 16:17:00+01	files/Softwares/64/photo/avatar	Software_logo_FileZilla.png	https://filezilla-project.org/	\N
65	Jitsi	https://github.com/jitsi/jitsi.git	Jitsi is an audio/video Internet phone and instant messenger written in Java. It supports some of the most popular instant messaging and telephony protocols such as SIP, Jabber/XMPP (and hence Facebook and Google Talk), AIM, ICQ, MSN, Yahoo! Messenger.\r\nThe development of Jitsi started at the University of Strasbourg, France. Originally the project was known as SIP Communicator. Throughout the years our community has grown to include members and contributors from Brazil, Bulgaria, Cameroon, China, Estonia, France, Germany, India, Japan, Romania, Spain, Switzerland, UK, USA, and others.\r\nJitsi is based on the OSGi architecture using the Felix implementation from Apache. This makes it very extensible and particularly developer friendly.	16	2017-02-07 19:52:22+01	2017-02-07 19:52:23+01	files/Softwares/65/photo/avatar	logo3.png	https://jitsi.org/	\N
66	Dia Diagram Editor	https://git.code.sf.net/p/vsd2svg/code vsd2svg-code	Dia is a program to draw structured diagrams.	15	2017-02-07 21:02:26+01	2017-02-07 21:02:27+01	files/Softwares/66/photo/avatar	dia.png	http://dia-installer.de/	\N
60	QGIS	https://github.com/qgis/QGIS.git	 A Free and Open Source Geographic Information System. Create, edit, visualise, analyse and publish geospatial information on Windows, Mac, Linux, BSD (Android coming soon).	19	2017-02-07 13:45:15+01	2017-02-07 13:45:15+01	files/Softwares/60/photo/avatar	QGIS.jpg	https://www.qgis.org	\N
55	Notepad++	https://github.com/notepad-plus-plus/notepad-plus-plus.git	Notepad++ is a free (as in "free speech" and also as in "free beer") source code editor and Notepad replacement that supports several languages. Running in the MS Windows environment, its use is governed by GPL License.\r\n\r\nBased on the powerful editing component Scintilla, Notepad++ is written in C++ and uses pure Win32 API and STL which ensures a higher execution speed and smaller program size. By optimizing as many routines as possible without losing user friendliness, Notepad++ is trying to reduce the world carbon dioxide emissions. When using less CPU power, the PC can throttle down and reduce power consumption, resulting in a greener environment.	7	2017-02-07 13:05:26+01	2017-02-07 13:05:26+01	files/Softwares/55/photo/avatar	NotePad++.png	https://notepad-plus-plus.org/	\N
57	Sumatra PDF	https://github.com/sumatrapdfreader/sumatrapdf.git	Sumatra PDF est un lecteur gratuit pour documents PDF, eBook (ePub, Mobi), XPS, DjVu, CHM, bandes dessinées (CBZ et CBR) pour Windows.\r\n\r\nSumatra PDF est petit, portable et démarre très rapidement.\r\n\r\nLa simplicité de l'interface utilisateur est la première priorité.	10	2017-02-07 13:15:21+01	2017-02-07 13:15:21+01	files/Softwares/57/photo/avatar	Sumatra_PDF_logo.png	https://www.sumatrapdfreader.org/	\N
58	BlueGriffon	https://github.com/therealglazou/bluegriffon.git	The next-gen Web and EPUB Editor based on the rendering engine of Firefox®	17	2017-02-07 13:26:14+01	2017-02-07 13:26:14+01	files/Softwares/58/photo/avatar	bluegriffon.png	http://bluegriffon.org/	\N
67	GIMP	https://git.gnome.org/browse/gimp-web-devel	GIMP is a cross-platform image editor available for GNU/Linux, OS X, Windows and more operating systems. It is free software, you can change its source code and distribute your changes.\r\nWhether you are a graphic designer, photographer, illustrator, or scientist, GIMP provides you with sophisticated tools to get your job done. You can further enhance your productivity with GIMP thanks to many customization options and 3rd party plugins.	7	2017-02-07 21:17:01+01	2017-02-07 21:17:01+01	files/Softwares/67/photo/avatar	Software_logo_GIMP.png	https://www.gimp.org/	\N
68	Inkscape	https://github.com/inkscape/inkscape.git	Inkscape est un logiciel libre d'édition vectorielle similaire à Adobe Illustrator, Corel Draw, Freehand et Xara X. Il se distingue par l'utilisation du format W3C (SVG), un standard ouvert du W3C basé sur le XML, en tant que format natif.	7	2017-02-07 21:30:02+01	2017-02-07 21:30:03+01	files/Softwares/68/photo/avatar	Software_logo_inkscap.jpg	https://inkscape.org/fr/	\N
69	Audacity	https://github.com/audacity/audacity.git	Audacity® is free, open source, cross-platform audio software for multi-track recording and editing.	7	2017-02-07 21:35:56+01	2017-02-07 21:35:56+01	files/Softwares/69/photo/avatar	software_logo_audacity.png	http://www.audacityteam.org/	\N
70	Avidemux	https://github.com/mean00/avidemux2.git	Avidemux is a free video editor designed for simple cutting, filtering and encoding tasks. It supports many file types, including AVI, DVD compatible MPEG files, MP4 and ASF, using a variety of codecs. Tasks can be automated using projects, job queue and powerful scripting capabilities.	7	2017-02-07 22:35:07+01	2017-02-07 22:35:08+01	files/Softwares/70/photo/avatar	Software_logo_avidemux.png	http://fixounet.free.fr/avidemux/	\N
71	Ultradefrag	https://github.com/svn2github/UltraDefrag.git	UltraDefrag is an open source disk defragmenter for Windows. It can quickly defragment everything including files which are usually locked by Windows. For instance, UltraDefrag can re-join paging and hibernation files.	7	2017-02-07 22:42:00+01	2017-02-07 22:42:00+01	files/Softwares/71/photo/avatar	Software_UltraDefrag-7-logo.png	http://ultradefrag.sourceforge.net/en/index.html	\N
33	LibreOffice	https://github.com/LibreOffice/core.git	LibreOffice is a powerful office suite – its clean interface and feature-rich tools help you unleash your creativity and enhance your productivity. LibreOffice includes several applications that make it the most powerful Free and Open Source office suite on the market: Writer (word processing), Calc (spreadsheets), Impress (presentations), Draw (vector graphics and flowcharts), Base (databases), and Math (formula editing).	14	2016-06-14 10:16:56+02	2016-06-22 17:59:12+02	files/Softwares/33/photo/avatar	Software_Logo_libreoffice.png	https://fr.libreoffice.org/	\N
73	Clonezilla	https://github.com/stevenshiau/clonezilla.git	Clonezilla is a partition and disk imaging/cloning program similar to True Image® or Norton Ghost®. It helps you to do system deployment, bare metal backup and recovery. Two types of Clonezilla are available, Clonezilla live and Clonezilla SE (server edition). Clonezilla live is suitable for single machine backup and restore. While Clonezilla SE is for massive deployment, it can clone many (40 plus!) computers simultaneously. Clonezilla saves and restores only used blocks in the hard disk. This increases the clone efficiency. With some high-end hardware in a 42-node cluster, a multicast restoring at rate 8 GB/min was reported.	15	2017-02-08 10:00:31+01	2017-02-08 10:00:32+01	files/Softwares/73/photo/avatar	clonezilla_logo_transparent.png	http://clonezilla.org/	\N
74	DVDstyler	pserver:anonymous@dvdstyler.cvs.sourceforge.net:/cvsroot/dvdstyler 	DVDStyler is a cross-platform free DVD authoring application that makes possible for video enthusiasts to create professional-looking DVDs. DVDStyler provides over 20 DVD menu templates, allowing you to create your own menu designs and photo slideshows. After you select your DVD label name, video quality, video format, aspect ratio, and audio format, you can select a template to add video materials to. DVDStyler's interface supports drag-and-drop so you can add project buttons and movies around with ease. Some basic video editing operations such as trimming and cropping are also available.	15	2017-02-08 10:28:47+01	2017-02-08 10:28:48+01	files/Softwares/74/photo/avatar	dvdstyler_logo.png	http://www.dvdstyler.org	\N
75	FusionForge	https://scm.fusionforge.org/anonscm/git/fusionforge/fusionforge.git 	FusionForge est un système de gestion de développement collaboratif de logiciels. Il fournit une interface unifiée à une série de logiciels serveur et intègre plusieurs applications à code source ouvert. Il s'agit essentiellement du renommage de la version libre du logiciel GForge, intervenu après la décision du GForge Group (éditeur de GForge) de ne plus maintenir que la version GForge AS (propriétaire). C'est donc un descendant direct du logiciel SourceForge.\r\n\r\nFusionForge apporte les fonctionnalités suivantes : hébergement de projet, gestion de version de code source de logiciel (Bazaar, CVS, Darcs, Git, Mercurial et Subversion), suivi de bugs et de projets, forums et messagerie. Ces fonctionnalités peuvent être déployées afin de faire fonctionner une forge logicielle auto-hébergée.	7	2017-02-09 14:35:33+01	2017-02-09 14:35:34+01	files/Softwares/75/photo/avatar	icon.png	https://fusionforge.org	\N
107	Contrast-Finder	https://github.com/Asqatasun/Contrast-Finder	Contrast-Finder trouve les bons contrastes de couleurs pour l'accessibilité web.	8	2017-03-14 14:36:41+01	2017-03-15 16:10:14+01	files/Softwares/107/photo/avatar	android-chrome-256x256.png	https://contrast-finder.org/	\N
50	KeePass	https://svn.code.sf.net/p/keepass/code/trunk keepass-code	Today you need to remember many passwords. You need a password for the Windows network logon, your e-mail account, your website's FTP password, online passwords (like website member account), etc. etc. etc. The list is endless. Also, you should use different passwords for each account. Because if you use only one password everywhere and someone gets this password you have a problem... A serious problem. The thief would have access to your e-mail account, website, etc. Unimaginable.\r\n\r\nKeePass is a free open source password manager, which helps you to manage your passwords in a secure way. You can put all your passwords in one database, which is locked with one master key or a key file. So you only have to remember one single master password or select the key file to unlock the whole database. The databases are encrypted using the best and most secure encryption algorithms currently known (AES and Twofish).	10	2017-02-07 10:56:42+01	2017-02-07 10:56:42+01	files/Softwares/50/photo/avatar	keepass_256x256.png	http://keepass.info/	\N
2	Pastell	https://scm.adullact.net/anonscm/svn/pastell/trunk	Pastell est une plateforme d'échange de données sécurisée assurant des fonctions :\r\n- de routages de documents\r\n- de pilotage de flux\r\n- de création et de saisie de formulaires\r\n- connexion au TDT	5	2016-04-08 17:55:11+02	2017-03-14 13:30:24+01	files/Softwares/2/photo/avatar	Pastell_350x250_CL.png	https://www.libriciel.fr/pastell/	\N
59	ProjectLibre	git://git.code.sf.net/p/projectlibre/code projectlibre-code	The goal of ProjectLibre is to provide free and open source project management software around the world.  We are very pleased to have been adopted in over 200 countries and making a difference around the world.  Large Fortune 500 companies, governments, small business and non-profits around the world are benefiting.  ProjectLibre can open existing Microsoft Project files and has comprehensive project management features.  ProjectLibre has been translated into Arabic, Chinese (Simplified), Czech, Dutch, English,  French, Finnish, Galician, German, Hindi, Italian, Japanese, Korean, Persian, Portuguese, Slovak, Spanish, Swedish, Russian and Ukrainian.  We are looking for volunteers to continue translating both the product but also documentation. The documentation is a community document wiki that is a work in progress and can be accessed when you log into this website. 	18	2017-02-07 13:37:14+01	2017-02-07 13:37:14+01	files/Softwares/59/photo/avatar	ProjetLibre _logo.png	http://www.projectlibre.com/	\N
46	FusionDirectory	http://git.fusiondirectory.org	FusionDirectory est un gestionnaire d'infrastructure qui s'appuie sur un Annuaire OpenLDAP. \r\n\r\nLa complexité de gestion des annuaires quand on n'a pas d'application pour les gérer valablement nous amène souvent à la problématique suivante: “Comment puis-je gérer élégamment  mes annuaires et  surtout, comment vais-je pouvoir déléguer les tâches à faire quand j'ai une équipe et que je souhaite que d'autres personnes interagissent, quand je veux que les usagers puissent changer leur mot de passe, leur adresse e-mail ou leur n° de GSM?”\r\n\r\nSa philosophie c'est: “Une interface pour tout gérer”. Avec FusionDirectory, on est capable de gérer les utilisateurs, les groupes, les rôles, les systèmes et on va jusqu'au déploiement de machines. \r\n\r\nFusionDirectory fait en sorte que si une personne est capable de créer un utilisateur , elle sait aussi comment créer un groupe, une machine, comment gérer SUDO etc.\r\n\r\nFusionDirectory est une des rares logiciels à avoir une intégration complète de SupAnn, on peut gérer son annuaire avec la Norme SupAnn directement de manière graphique et élégante. \r\n\r\nUn système d'ACL évolue permet de définir qui peut faire quoi à travers l'application et permet aussi de faire des systèmes de self-service (par ex.: l'utilisateur peut changer lui-même son adresse personnelle, son mot de passe etc. …)	15	2016-11-22 19:45:56+01	2016-11-22 19:45:56+01	files/Softwares/46/photo/avatar	fd-logo-square.png	https://www.fusiondirectory.org/	\N
29	Alfresco	https://wiki.alfresco.com/wiki/Download_and_Install_Alfresco	Alfresco est un système de gestion de contenu (en anglais ECM pour Enterprise Content Management) créé par Alfresco Software en 2005 et distribué sous licence libre. Il se distingue des autres systèmes par sa forme. En effet, il peut se comporter sur un ordinateur comme un disque virtuel (se montant et se démontant), ce qui permet à l'utilisateur de partager des fichiers simplement en les déplaçant sur le disque dédié.	13	2016-06-13 17:39:41+02	2016-06-13 17:41:58+02	files/Softwares/29/photo/avatar	Softaware_Logo_alfresco.jpg	https://www.alfresco.com	\N
56	Greenshot	https://github.com/greenshot/greenshot.git	Greenshot is a light-weight screenshot software tool for Windows with the following key features:\r\n\r\n    Quickly create screenshots of a selected region, window or fullscreen; you can even capture complete (scrolling) web pages from Internet Explorer.\r\n    Easily annotate, highlight or obfuscate parts of the screenshot.\r\n    Export the screenshot in various ways: save to file, send to printer, copy to clipboard, attach to e-mail, send Office programs or upload to photo sites like Flickr or Picasa, and others. and a lot more options simplyfying creation of and work with screenshots every day.\r\n\r\nBeing easy to understand and configurable, Greenshot is an efficient tool for project managers, software developers, technical writers, testers and anyone else creating screenshots.	7	2017-02-07 13:10:11+01	2017-02-07 13:10:11+01	files/Softwares/56/photo/avatar	90_400x400.png	http://greenshot.fr	\N
72	7-zip	https://github.com/pornel/7z.git	7-Zip is a file archiver with a high compression ratio for ZIP and GZIP formats, which is between 2 to 10% better than its peers, depending on the exact data tested. And 7-Zip boosts its very own 7z archive format that also offers a significantly higher compression ratio than its peers—up to 40% higher! This is mainly because 7-Zip uses LZMA and LZMA2 compression, with strong compression settings and dictionary sizes, to slow but vastly improve density. If a zip tool gains its appeal from its ability to efficiently compress files, then 7-Zip proves it has a little bit o’ magic.	13	2017-02-07 22:46:57+01	2017-02-07 22:46:58+01	files/Softwares/72/photo/avatar	Software_logo_7zip.png	http://www.7-zip.org/	\N
76	Scribus	https://www.scribus.net/	A page layout program for Linux, FreeBSD, PC-BSD, NetBSD, OpenBSD, Solaris, OpenIndiana, Debian GNU/Hurd, Mac OS X, OS/2 Warp 4, eComStation, Haiku and Windows.	15	2017-02-09 16:22:11+01	2017-02-09 16:22:12+01	files/Softwares/76/photo/avatar	scribus_header.png	https://www.scribus.net/	\N
77	MultiDiff	http://www.starxpert.fr/openoffice-org-libreoffice/extensions/#multidiff	(LGPL) Il s’agit d’un outil de diffusion qui offre la possibilité d’exporter le document courant sous plusieurs formats simultanément. La diffusion se fait soit par enregistrement dans un répertoire accessible à d’autres utilisateurs, soit par envoi de mail.	14	2017-02-09 16:27:16+01	2017-02-09 16:27:16+01	\N	\N	http://www.starxpert.fr/openoffice-org-libreoffice/extensions/#multidiff	\N
78	Ooo.HG	http://ooo.hg.free.fr/	L'alternative gratuite et libre pour créer facilement tout document Histoire-Géographie.	19	2017-02-09 16:32:18+01	2017-02-09 16:32:20+01	files/Softwares/78/photo/avatar	logooohg-2008.jpg	http://ooo.hg.free.fr/	\N
79	Grammalecte	http://www.dicollecte.org/grammalecte/	Grammalecte est un correcteur grammatical open source dédié à la langue française, pour Writer (LibreOffice, OpenOffice) et Firefox. Il est dérivé de Lightproof, qui a été écrit pour le hongrois.\r\n\r\nGrammalecte essaie d’apporter une aide à l’écriture du français sans parasiter l’attention des utilisateurs avec de fausses alertes. Ce correcteur suit donc le principe suivant : le moins de « faux positifs » possible ; s’il n’est pas possible de déterminer avec de fortes chances qu’une suite de mots douteuse est erronée, le correcteur ne signalera rien.\r\n\r\nGrammalecte est en cours de développement.	10	2017-02-09 16:36:22+01	2017-02-09 16:36:22+01	files/Softwares/79/photo/avatar	grammalecte.png	http://www.dicollecte.org/grammalecte/	\N
80	Thunderbird	https://www.mozilla.org/fr/thunderbird/	Un logiciel pour rendre votre messagerie plus facile.\r\n\r\nThunderbird est une application de messagerie facile à configurer et à personnaliser — et elle comporte de nombreuses fonctionnalités très pratiques !	19	2017-02-09 16:40:47+01	2017-02-09 16:40:48+01	files/Softwares/80/photo/avatar	thunderbird.png	https://www.mozilla.org/fr/thunderbird/	\N
81	DBAN	http://dban.org/	Free Open-Source Data Deletion Software for Personal Use\r\n\r\nDelete information stored on hard disk drives (HDDs) in PC laptops, desktops or servers. Plus, remove viruses/spyware from Microsoft Windows installations.	15	2017-02-09 16:52:20+01	2017-02-09 16:52:21+01	files/Softwares/81/photo/avatar	dban-logo.gif	http://dban.org/	\N
82	Firefox ESR	https://www.mozilla.org/en-US/firefox/organizations/all/	Firefox ESR (Extended Support Release) est la version entreprise du navigateur Mozilla Firefox. Firefox ESR est destiné aux personnes qui déploient et maintiennent un environnement bureautique dans de grandes organisations comme les Universités ou les écoles, les mairies ou les entreprises.	19	2017-02-09 16:57:41+01	2017-02-09 16:57:41+01	files/Softwares/82/photo/avatar	firefox.png	https://www.mozilla.org/en-US/firefox/organizations/all/	\N
92	GLPI Plugin:Data Injection	https://github.com/pluginsGLPI/datainjection.git	Cette extension permet l'injection de données dans GLPI à l'aide de fichiers CSV\r\n\r\nElle permet de créer des modèles d'injection pour une réutilisation future, et a été créée afin de répondre aux besoins suivants :\r\n\r\n    reprise des données d'autres outils d'inventaires\r\n    injection de bons de livraisons électroniques\r\n\r\nLes données pouvant-être injectées sont :\r\n\r\n    données d'inventaires (sauf logiciels et licences),\r\n    données de gestion (contrat, contact, fournisseur),\r\n    données de configuration (utilisateur, groupe, entité).\r\n	15	2017-03-01 14:20:40+01	2017-03-01 14:20:40+01	files/Softwares/92/photo/avatar	datainjection.png	http://plugins.glpi-project.org/#/plugin/datainjection	\N
84	GPLI - plugin - FusionInventory	https://github.com/fusioninventory/fusioninventory-for-glpi	FusionInventory acts like a gateway and collect information sent by the agents. It will create or update the information in GLPI it minimal effort from the administrator.	19	2017-02-14 14:07:27+01	2017-02-14 14:07:27+01	files/Softwares/84/photo/avatar	fusioninventory_logo.png	http://fusioninventory.org	\N
85	eGroupware	https://github.com/EGroupware/egroupware	Logiciel de travail collaboratif : agenda, carnet d'adresse, gestion et partage de ressources, de tâches, fichiers, de projets...	15	2017-02-15 07:00:26+01	2017-02-15 07:00:27+01	files/Softwares/85/photo/avatar	csm_egw-signet-300_d74207e29a.png	https://www.egroupware.org/en/products/installation-on-your-own-server-free-version/	\N
86	Nuxeo	https://github.com/nuxeo	Moteur de gestion de contenus (ECM et DAM) ou de gestion électronique de documents (GED)	16	2017-02-15 07:11:42+01	2017-02-15 07:11:43+01	files/Softwares/86/photo/avatar	nuxeo460607.png	https://www.nuxeo.com/fr/	\N
87	Typo3	https://git.typo3.org/	Web CMS	15	2017-02-15 07:24:09+01	2017-02-15 07:24:09+01	files/Softwares/87/photo/avatar	typo3-logo.png	https://typo3.org/	\N
88	OCS Inventory NG Agent pour Windows	https://github.com/OCSInventory-NG/WindowsAgent.git	Open Computers and Software Inventory Next Generation est une solution de gestion technique de parc informatique.	15	2017-03-01 11:03:21+01	2017-03-01 11:03:21+01	\N	\N	http://www.ocsinventory-ng.org/fr	\N
89	MSofficeKey	http://wiki.ocsinventory-ng.org/index.php?title=Plugins:MSofficeKey	Récupérer les versions et clés Microsoft Office.	19	2017-03-01 13:52:36+01	2017-03-01 13:52:36+01	\N	\N	http://wiki.ocsinventory-ng.org/index.php?title=Plugins:MSofficeKey	\N
90	OCS Inventory NG Plugins:WinServices	http://wiki.ocsinventory-ng.org/index.php?title=Plugins:WinServices	 Récupérer la liste des Services.	7	2017-03-01 14:03:43+01	2017-03-01 14:03:43+01	\N	\N	http://wiki.ocsinventory-ng.org/index.php?title=Plugins:WinServices	\N
83	OCS Inventory NG	https://github.com/OCSInventory-NG/OCSInventory-Server.git	Open Computers and Software Inventory Next Generation  est une solution de gestion technique de parc informatique. Depuis 2001, OCS Inventory NG cherche à rendre l’inventaire matériel et logiciel des ordinateurs plus performant. OCS Inventory NG interroge ses agents pour connaître la composition soft et hard de chaque machine, chaque serveur. OCS Inventory interroge aussi le réseau pour y découvrir les éléments actifs ne pouvant recevoir d’agent. Depuis la version 2.0, OCS Inventory NG intègre la fonctionnalité de scans SNMP.	15	2017-02-14 13:52:59+01	2017-02-14 13:53:00+01	files/Softwares/83/photo/avatar	ocsinventoryng.png	http://www.ocsinventory-ng.org	\N
91	GLPI Plugin:OCS Inventory Ng	https://github.com/pluginsGLPI/ocsinventoryng.git	Ce plugin permet de synchroniser GLPI avec la solution d'inventaire OCS Inventory NG (http://www.ocsinventory-ng.org/fr/).\r\n\r\nIl remplace le mode OCS natif de GLPI (en version 0.84) et d'apporter les fonctionnalités du plugin massocsimport afin d'offrir une meilleure compatibilité et évolutivité avec OCS Inventory.\r\n\r\nAutomatisation de l'import OCSInventory-NG Il est composée d'un script (PHP ou Shell) permettant d'automatiser l'import et la mise à jour des machines. Une interface affiche la liste des scripts en cours ou terminés, ainsi que l'ensemble des données qui s'y rattachent.	15	2017-03-01 14:13:49+01	2017-03-01 14:13:50+01	files/Softwares/91/photo/avatar	ocsinventoryng.png	https://github.com/pluginsGLPI/ocsinventoryng	\N
94	GLPI Plugin:Uninstall	https://github.com/pluginsGLPI/uninstall.git	Ce plugin vous permet de classer ou supprimer facilement et proprement les objets en fin de vie de votre parc.	15	2017-03-01 14:28:36+01	2017-03-01 14:28:36+01	files/Softwares/94/photo/avatar	uninstall.png	http://plugins.glpi-project.org/#/plugin/uninstall	\N
95	GLPI Plugin:Behaviors	https://forge.glpi-project.org/projects/behaviors/repository	Cette extension permet d'ajouter des comportements optionnels à GLPI.\r\n	5	2017-03-01 14:30:45+01	2017-03-01 14:30:45+01	files/Softwares/95/photo/avatar	behaviors.png	http://plugins.glpi-project.org/#/search/Behaviors	\N
93	GLPI Plugin:AdditionalAlerts	https://github.com/InfotelGLPI/additionalalerts.git	\r\nCe plugin vous permet d'envoyer les alertes email supplémentaires\r\n\r\n    Nouvelles machines inventoriés depuis OCS-NG (Ayant le statut défini dan la configuration du module OCS-NG),\r\n    Machines non inventoriés depuis X jours (Choix des statuts pris en charge : pour ne pas prendre en compte les machines en stock),\r\n    Matériels ayant une date d'achat vide (Choix des types pris en charge : pour ne pas prendre en compte les matériels non achetés),\r\n    Niveaux des cartouches bas (information provenant de Fusion Inventory).\r\n	15	2017-03-01 14:24:12+01	2017-03-09 15:20:52+01	files/Softwares/93/photo/avatar	additionalalerts.png	http://plugins.glpi-project.org/#/plugin/additionalalerts	\N
96	GLPI Plugin:Timelineticket	https://github.com/pluginsGLPI/timelineticket.git	Ce plugin vous permet d'ajouter un onglet Chronologie sur les tickets\r\n\r\n        Historisation et statistiques sur les différents statuts du tickets\r\n        Historisation et statistiques sur les différents techniciens du tickets\r\n        Historisation et statistiques sur les différents groupes de techniciens du tickets\r\n        Rapport sur les temps passés sur les tickets clos	8	2017-03-01 14:32:16+01	2017-03-01 14:32:17+01	files/Softwares/96/photo/avatar	behaviors.png	http://plugins.glpi-project.org/#/plugin/timelineticket	\N
97	OPSI	https://github.com/opsi-org	Opsi est un système de gestion de client Windows et Linux open source basé sur un serveur Linux\r\n\r\nPrincipales caractéristiques:\r\n\r\n- Installation automatique du système d'exploitation (sans surveillance ou basée sur l'image)\r\n- Distribution automatique de logiciels et gestion des correctifs\r\n- Inventaire matériel et logiciel\r\n- Gestion des licences\r\n\r\nOpsi fonctionne sur Debian, Ubuntu, OpenSuse, SLES, CentOS, UCS et RHEL.	8	2017-03-10 15:02:13+01	2017-03-10 15:02:14+01	files/Softwares/97/photo/avatar	logo.png	http://www.opsi.org/en?set_language=en	\N
98	OPENLDAP	http://www.openldap.org/software/repo/openldap.git	OpenLDAP Software is an open source implementation of the Lightweight Directory Access Protocol.\r\nThe suite includes:\r\n\r\n- slapd - stand-alone LDAP daemon (server)\r\n- libraries implementing the LDAP protocol, and\r\n- utilities, tools, and sample clients. 	6	2017-03-10 15:10:23+01	2017-03-10 15:14:42+01	files/Softwares/98/photo/avatar	LDAPworm.png	http://www.openldap.org/	\N
99	Argonaut	https://git.fusiondirectory.org/	Argonaut est un système client / serveur modulaire basé sur JSON-RPCprotocol. Les deux côtés client et serveur peuvent charger des modules au démarrage. Argonaut a deux fonctions principales.\r\n\r\nRun a given operation on a system through a client\r\n\r\nExécuter une opération donnée sur un système via un client\r\n\r\nDeux fonctions de base: redémarrer le service et allumer / éteindre un système. Certains modules peuvent fournir quelques fonctionnalités supplémentaires:\r\n\r\n- Argonaut-ldap2zone: mettre à jour une zone dns, créer une vue, créer acls\r\n- Argonaut-quota: appliquer un quota\r\n- Argonaut-fai-mirror: créer des miroirs des depots de paquets\r\n- Argonaut-fai-monitor: suivre l'installation de FAI et signaler les états à FusionDirectory\r\n- Argonaut-dovecot: crée le quota de la boîte aux lettres et l'applique\r\n- Argonaut-user-reminder pour gérer le plugin de suivi d'expiration des comptes de FusionDirectory\r\n- Argonaut-clean-audit pour nettoyer la branche d'audit de FusionDirectory\r\n\r\nPermettre l'intégration avec les outils de déploiement FAI / OPSI / puppet	15	2017-03-10 15:21:37+01	2017-03-10 15:21:37+01	files/Softwares/99/photo/avatar	argonaut.png	https://www.argonaut-project.org/	\N
100	LDAP Synchronisation connector	https://github.com/lsc-project/lsc.git	LSC est un connecteur Open Source pour synchroniser les identités entre un annuaire LDAP et n'importe quelle source de données, y compris toute base de données avec un connecteur JDBC, un autre serveur LDAP, des fichiers plats, une API REST ...	9	2017-03-10 15:49:44+01	2017-03-10 15:49:45+01	files/Softwares/100/photo/avatar	lsc_logo.png	https://lsc-project.org/doku.php	\N
101	LemonLDAP NG	http://svn.forge.objectweb.org/svnroot/lemonldap	LemonLDAP :: NG est un logiciel open source de Web Single Sign On (WebSSO), gestion des accès et Identity Federation, écrit en Perl et Javascript.\r\n\r\nLemonldap :: NG est un logiciel libre, distribué sous licence GPL.\r\n\r\nLemonLDAP :: NG est le premier logiciel SSO déployé dans les administrations françaises. Il peut traiter plus de 350 000 utilisateurs. De nombreuses entreprises privées l'utilisent également. Consultez nos références!	15	2017-03-10 15:55:44+01	2017-03-10 15:56:33+01	files/Softwares/101/photo/avatar	logo_llng.png	https://www.lemonldap-ng.org/welcome/	\N
102	LDAP Tool Box Self Service Password	https://github.com/ltb-project/self-service-password	 Self Service Password is a PHP application that allows users to change their password in an LDAP directory.\r\n\r\nThe application can be used on standard LDAPv3 directories (OpenLDAP, OpenDS, ApacheDS, Sun Oracle DSEE, Novell, etc.) and also on Active Directory. 	10	2017-03-10 16:43:18+01	2017-03-10 16:43:18+01	files/Softwares/102/photo/avatar	ltb-logo.png	https://ltb-project.org/documentation/self-service-password	\N
103	LDAP Tool Box White Pages	https://github.com/ltb-project/white-pages	 White Pages is a PHP application that allows users to search and display data stored in an LDAP directory.\r\n\r\nThe application can be used on standard LDAPv3 directories and Active Directory, as all searched attributes can be set in configuration. 	10	2017-03-10 16:45:06+01	2017-03-10 16:45:07+01	files/Softwares/103/photo/avatar	ltb-logo.png	https://ltb-project.org/documentation/white-pages	\N
104	Magnolia CMS	https://git.magnolia-cms.com	Magnolia est un logiciel libre de gestion de contenu web.\r\n\r\nIl est 100% Java et utilise notamment les technologies suivantes: J2EE, Freemarker, Vaadin, Content repository API for Java.\r\n\r\nMagnolia CMS peut être facilement customisé grâce à son architecture modulable. De nombreux modules développés par la communauté permettent de l'intégrer avec d'autres outils.\r\n\r\nMagnolia CMS peut être découplé en plusieurs instances, un back-office et un ou plusieurs front(s)-office,  ce qui permet de répondre à des contraintes fortes de sécurité (limiter les risques d'intrusion et contrôler les accès via un SSO sur le BO) et de haute disponibilité (redonder ou "clusteriser" les serveurs fronts).	10	2017-03-10 17:51:38+01	2017-03-10 17:51:39+01	files/Softwares/104/photo/avatar	magnoliacms_logo.png	https://www.magnolia-cms.com/	\N
105	Pentaho	https://github.com/pentaho	Pentaho est une suite de produits permettant de regrouper, visualiser, analyser et exploiter ses données pour répondre à des problématiques d'intelligence décisionnelle (BI), de big data ou de data science.	15	2017-03-10 18:11:02+01	2017-03-10 18:11:02+01	files/Softwares/105/photo/avatar	Pentaho_new_logo_2013.png	http://www.pentaho.com/	\N
24	web-delib	https://scm.adullact.net/anonscm/svn/webdelib/trunk	Né des réflexions d’un groupe de travail constitué de collectivités territoriales, web-delib est un libriciel de gestion des délibérations développé en PHP / jQuery qui permet de gérer la vie des assemblées délibérantes, depuis la rédaction des projets jusqu’au contrôle de légalité.	5	2016-06-13 09:58:42+02	2017-03-14 10:21:02+01	files/Softwares/24/photo/avatar	logo-webdelib_350x250_CL_2.png	https://www.libriciel.fr/web-delib	\N
108	Zabbix	https://github.com/monitoringartist/zabbix-community-repos	Zabbix est un logiciel de supervision distribué qui permet une surveillance continue et en temps réel de vos serveurs et autres équipements réseaux. Zabbix génère à l’aide des données qu’il récolte des notifications sur des règles entièrement personnalisable.\r\nL’interface web de Zabbix fourni un moyen efficace de visualisation de tous les éléments supervisés sous différentes formes (graphes, tableaux, cartes…).	15	2017-03-20 22:24:37+01	2017-03-20 22:24:38+01	files/Softwares/108/photo/avatar	logo_zabbix_petit.gif	https://www.zabbix.org/wiki/Main_Page	\N
109	Apache JMeter™	http://jmeter.apache.org/download_jmeter.cgi	L'application Apache JMeter est un logiciel open source, une application conçue pour simuler le comportement fonctionnel de test et de mesurer les temps de réponses tout au long de la montée en charge. Il a été initialement conçu pour scénariser le test des applications Web , mais a été depuis étendu à d'autres fonctions de test.	16	2017-03-21 14:17:34+01	2017-03-21 14:19:14+01	files/Softwares/109/photo/avatar	logoApacheJmeter.svg	http://jmeter.apache.org/	\N
110	Zimbra	https://www.zimbra.com/downloads/	Zimbra est une solution de messagerie collaborative développée pour proposer une alternative libre à MS Exchange ou Lotus Notes. Son vif succès en fait aujourd’hui une plateforme collaborative majeure, comptant déjà plusieurs millions d’utilisateurs. Son interface Web 2.0 très ergonomique alliée à son support de nombreux terminaux mobiles sont des atouts majeurs pour chaque organisation.\r\n	18	2017-03-21 15:36:03+01	2017-03-21 15:36:04+01	files/Softwares/110/photo/avatar	zimbra_logo.png	https://www.zimbra.com/open-source-email-overview/	\N
9	Asqatasun	https://github.com/Asqatasun/Asqatasun	Asqatasun is an opensource web site analyzer, used for web accessibility (a11y) and Search Engine Optimization (SEO)	8	2016-04-28 11:48:23+02	2017-04-14 09:48:32+02	files/Softwares/9/photo/avatar	Software_Logo_Asqatasun.png	http://asqatasun.org/	\N
\.


--
-- Name: softwares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.softwares_id_seq', 126, true);


--
-- Data for Name: softwares_statistics; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.softwares_statistics (id, software_id, last_commit_age, project_age, delta_commit_one_month, delta_commit_twelve_month, number_of_contributors, high_committer_percent, declared_users, average_review_score, screenshots, code_gouv_label, score) FROM stdin;
695	2	3	3	0	2	1	0	3	0	3	0	55.55555555555556
691	3	1	3	0	0	1	1	3	0	3	0	44.44444444444444
689	4	3	3	0	1	1	2	3	2	3	0	66.66666666666666
692	5	3	3	0	0	0	1	3	0	3	0	48.148148148148145
687	9	3	3	0	0	0	1	0	0	2	0	33.33333333333333
688	10	2	3	0	0	0	3	0	3	2	0	48.148148148148145
690	11	3	3	0	0	0	3	0	0	0	0	33.33333333333333
693	12	2	2	0	0	0	2	0	0	2	0	29.629629629629626
698	13	2	2	0	0	0	3	1	0	0	0	29.629629629629626
699	14	1	3	0	1	0	0	2	0	1	0	29.629629629629626
700	15	0	2	0	0	1	1	3	0	3	0	37.03703703703704
701	16	3	3	1	3	1	2	1	0	3	0	62.96296296296296
702	17	2	3	0	1	1	1	2	0	3	0	48.148148148148145
703	18	1	3	0	0	0	2	2	0	2	0	37.03703703703704
704	19	3	3	0	1	2	1	2	0	1	0	48.148148148148145
705	20	3	3	0	0	0	3	0	0	1	0	37.03703703703704
706	21	1	3	0	0	0	2	0	0	0	0	22.22222222222222
710	23	3	0	0	1	1	2	3	0	2	0	44.44444444444444
711	24	2	3	0	3	2	1	3	2	1	0	62.96296296296296
707	25	3	2	0	0	0	2	2	0	3	0	44.44444444444444
708	30	3	1	0	0	0	3	2	0	1	0	37.03703703703704
712	32	3	3	1	3	1	1	2	2	0	0	59.25925925925925
714	40	1	2	0	0	1	1	1	0	0	0	22.22222222222222
\.


--
-- Data for Name: softwares_tags; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.softwares_tags (id, software_id, tag_id) FROM stdin;
\.


--
-- Name: softwares_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.softwares_tags_id_seq', 1, false);


--
-- Data for Name: statistics_averages; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.statistics_averages (id, delta_commit_one_month, delta_commit_twelve_month, number_of_contributors, created, modified) FROM stdin;
1	10.4210529	538.421082	4	2016-10-27 18:05:14.898355	2016-10-27 18:05:14.898355
2	10.4210529	538.421082	4	2016-10-27 18:16:45.481509	2016-10-27 18:16:45.481509
3	10.4210529	538.421082	4	2016-10-27 18:23:37.57663	2016-10-27 18:23:37.57663
4	10.4210529	538.421082	4	2016-10-27 18:23:37.617403	2016-10-27 18:23:37.617403
5	9.73684216	509.473694	4	2016-11-15 14:28:45.886188	2016-11-15 14:28:45.886188
6	15.3000002	561.849976	4	2016-11-15 14:55:32.429374	2016-11-15 14:55:32.429374
7	14.136364	531.636353	4	2016-11-15 15:02:21.604169	2016-11-15 15:02:21.604169
8	15.791667	517.958313	5	2016-11-15 15:17:14.57377	2016-11-15 15:17:14.57377
\.


--
-- Name: statistics_averages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.statistics_averages_id_seq', 8, true);


--
-- Name: statistics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.statistics_id_seq', 714, true);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.tags (id, name, created, modified) FROM stdin;
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.tags_id_seq', 1, false);


--
-- Data for Name: user_types; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.user_types (id, name, created, modified, cd) FROM stdin;
2	Administration	2016-04-08 18:03:58+02	2016-04-15 11:21:37+02	Administration
4	Person	2016-04-11 11:42:17+02	2016-04-15 11:21:48+02	Person
5	Company	2016-06-10 14:27:59+02	2016-06-10 14:27:59+02	Company
6	Association	2016-06-10 15:58:07+02	2016-06-10 15:58:07+02	Association
\.


--
-- Name: user_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.user_types_id_seq', 6, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: comptoir
--

COPY public.users (id, username, created, logo_directory, url, user_type_id, description, modified, role, password, email, photo, active, digest_hash, token) FROM stdin;
87	Roland Mas	2017-02-09 12:02:39+01	\N	https://www.gnurandal.com/	5	Prestataire de services en logiciel libre, avec spécialisation sur l'administration de systèmes d'exploitation libres (notamment Debian et dérivés) d'une part, et sur FusionForge d'autre part.	2017-02-09 12:02:40+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org87	\N	f	3324768c7168e1b1fe2d2f4bb924914d	\N
74	Groupement d'Intérêt Public E-Bourgogne	2016-11-04 10:10:06+01	files/Users/photo/74/avatar	\N	2		2016-11-04 10:10:06+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org74	Logo-ebourgogne-h350.png	f	546ddf72812ec032c88399f50e4f5649	\N
78	LIBERLOG	2016-11-29 10:29:07+01	files/Users/photo/78/avatar	\N	5	LIBERLOG, services et édition de livres, s’intéresse à gagner le maximum de temps dans la création de votre site web ou de votre logiciel. Avec LIBERLOG, vous pouvez aussi migrer vers une nouvelle version de votre outil RAD.	2016-11-29 10:31:11+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org78	2009-07_logo_liberlog_mini.png	f	a48ecb9b77d79fe432328fb521192db2	\N
82	WebTales	2016-12-21 15:30:27+01	files/Users/photo/82/avatar	http://www.rubedo-project.org	5	WebTales est l'éditeur de la solution open-source Rubedo. Rubedo est une plateforme digitale qui permet de créer des sites web et ecommerce.	2016-12-21 15:30:27+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org82	logo.jpg	f	16d91743fd6c578b62588293c383e678	\N
86	Julie Gauthier	2017-02-07 10:45:09+01	\N		4		2017-02-07 10:45:09+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org86	\N	f	553e762c05be86da2039cf145e60d74f	\N
75	AF83	2016-11-04 17:07:20+01	files/Users/photo/75/avatar	\N	5	Nous accompagnons nos clients dans la transformation numérique des services, produits et lieux.	2016-11-04 17:21:31+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org75	Logo-AF83-w255.jpg	f	d1d0254f128f08f02c2f8bfa40381175	\N
79	Atelier-111 - Web collectivités	2016-11-29 11:43:02+01	files/Users/photo/79/avatar	\N	5	Web collectivités est une solution web Open source dédiée aux acteurs publics. Elle repose sur le noyau technique WordPress.	2016-11-29 11:43:02+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org79	Logo_Atelier-111_webco_w350.jpg	f	fef1bbdd52665e788aeaa10444f60c67	\N
83	Intermezzo	2017-01-09 14:45:41+01	files/Users/photo/83/avatar	http://www.intermezzo-coop.eu	5	Parce que nous pensons qu'il n'y a pas d'intelligence territoriale sans croisement des approches, nous avons créé Intermezzo, espace de rencontre multi-thématiques : géographie, énergie, informatique, sciences sociales, mobilité, ingénierie. Nous réinterrogeons les savoirs et les usages afin de proposer des services simples, innovants et adaptés.\r\nIntermezzo est une structure indépendante, agissant en toute liberté.\r\n\r\nNous accompagnons les acteurs publics et privés dans l'élaboration et la conduite de leurs politiques et projets opérationnels traitant du développement durable, du changement et de l'adaptation des modes de vies.	2017-01-09 14:46:48+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org83	logo-intermezzo-p.png	f	b0a985a1181c153e6eb3f364273d4a59	\N
30	COGITIS	2016-06-10 16:51:55+02	files/Users/photo/30/avatar	http://cogitis.fr/	2	Syndicat Mixte, nous sommes les premiers utilisateurs des services que nous proposons dans le cadre du bouquet de services MutualiTIC, à destination des communes et EPCI. \r\nNous intervenons également en tant qu'intégrateur auprès de nos adhérents (départements) sur des projets notamment autour de l'archivage (Azalae, Pastell, Agape).	2016-12-14 17:33:57+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org30	User_Logo_cogitis.jpg	f	5cf925b866f22e99fff64b901d33067b	\N
11	Linagora (92)	2016-06-10 14:36:26+02	files/Users/photo/11/avatar	https://www.linagora.com	5	Nous croyons au modèle d'éditeur de logiciels 100% libre. \r\nNous vous aidons à construire votre indépendance technologique.\r\nChaque jour, nous agissons dans le respect des valeurs \r\nque nous défendons : partage, créativité, honnêteté.	2016-07-25 15:46:18+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org11	User_Logo_Linagora.png	f	\N	\N
68	Open-DSI	2016-10-31 11:21:20+01	files/Users/photo/68/avatar	\N	5	Notre objectif est de guider les associations, collectivités locales, TPE et PME dans la bonne direction en matière d’Informatique et Telecoms. \r\n\r\nNos spécificités : \r\n- La préconisation et la mise en place de logiciels libres ou open source chaque fois cela est possible \r\n- L’accompagnement dans les démarches d’externalisation par l’utilisation de solutions de cloud computing pour plus d’agilité grâce à nos solutions cloud prêtes à l’emploi.\r\n\r\nPrésent sur Lyon et sa région, nous faisons de la proximité un argument fort pour bâtir une relation durable et de confiance avec nos clients.	2016-11-22 20:49:56+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org68	LogoEtBaselinev5-327x105px.png	f	928fdce3a60fd9c6a5c7543f5725b10e	\N
10	Avencall	2016-06-10 14:34:00+02	files/Users/photo/10/avatar	http://www.avencall.com/	5	Avencall transforme l’industrie des télécommunications d’entreprise et son modèle économique en donnant un accès universel à des solutions de téléphonie qui assurent à ses clients liberté, évolutivité et créativité au service de leur métier. Une réussite rendue possible par la collaboration et le savoir-faire de ses équipes.	2016-06-10 15:07:00+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org10	User_Logo_Avencall.png	f	\N	\N
12	Berger Levrault	2016-06-10 14:45:07+02	files/Users/photo/12/avatar	http://www.berger-levrault.com/	5	Accélérer la dynamique des nouveaux usages publics\r\npour rapprocher les citoyens de leurs administrations,\r\nleurs collectivités et leurs établissements de soins.	2016-06-10 15:08:22+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org12	User_Logo_bergerLevrault.png	f	\N	\N
22	Association la Mouette	2016-06-10 15:54:07+02	files/Users/photo/22/avatar	http://www.lamouette.org/	6		2016-06-10 16:03:25+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org22	User_Logo_La-Mouette.png	f	\N	\N
7	Entr'ouvert	2016-06-02 10:22:12+02	files/Users/photo/7/avatar	http://www.entrouvert.com/en/	5		2016-06-14 14:33:54+02	cooperative company	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org7	User_Logo_entrouvert.png	f	\N	\N
9	BlueMind	2016-06-10 14:20:25+02	files/Users/photo/9/avatar	https://www.bluemind.net/	5	Blue Mind est une SAS française, créée en septembre 2010 et éditeur de la solution de messagerie collaborative Open Source Blue Mind.	2016-06-13 11:04:08+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org9	User_Logo_BlueMind.png	f	\N	\N
69	kuczynski	2016-10-31 11:27:55+01	\N	\N	4		2016-10-31 11:27:55+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org69	\N	f	f8482333a1b35aecd36c75c8f8f2acec	\N
70	SIVOM de la Communauté du Béthunois	2016-10-31 12:06:19+01	files/Users/photo/70/avatar	\N	2		2016-12-14 11:25:21+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org70	Logo-sivom.png	f	96e594f4b0a13c06b5f8e26c609e978e	\N
67	Pyrat.net	2016-07-26 14:54:18+02	files/Users/photo/67/avatar	http://www.pyrat.net/	5	Services de création de sites web bien référencés et accessibles à tous avec le CMS SPIP. Transfert de compétence, assistance à la demande, accompagnement sont les moyens que nous prenons  pour la liberté de nos clients.	2016-07-26 14:55:29+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org67	User_Logo_pyranet.jpg	f	21048c56099b9babe63f5d050f3f7257	\N
13	AtolCD	2016-06-10 15:02:32+02	files/Users/photo/13/avatar	https://www.atolcd.com/	5	Notre volonté : contribuer à la compétitivité et à la performance de nos clients avec des applications réalisées dans le cadre de démarches agiles centrées sur les besoins fonctionnels et l'expérience utilisateur.	2016-06-10 15:04:29+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org13	User_Logo_ATOL-CD.png	f	506b4d7a912d0ce3be6096f5fa2a6854	\N
14	StarXpert	2016-06-10 15:09:10+02	files/Users/photo/14/avatar	http://starxpert.fr/	5	StarXpert apporte une alternative open source à vos besoins de collaboration et de dématérialisation.	2016-06-10 15:11:20+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org14	User_Logo_StarXpert.png	f	0c70d21a4bcddc6c4e895f10e72c08b4	\N
84	François ELIE	2017-02-03 18:37:48+01	\N	https://elie.org	4	Président de l'Adullact\r\nAdjoint au maire d'Angoulême\r\nConseiller délégué à GrandAngoulême	2017-02-03 18:37:48+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org84	\N	f	cafff629150498a5bb14007dc8ef8811	\N
8	atReal	2016-06-02 14:10:21+02	files/Users/photo/8/avatar		5		2016-06-14 12:20:32+02	SSII	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org8	User_logo_atreal.png	f	5404aebd325bd6cf719891361a3b4200	\N
25	Ville d'Arles (13) 	2016-06-10 16:10:17+02	files/Users/photo/25/avatar	http://www.agglo-accm.fr/arles.html	2		2016-07-25 15:59:07+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org25	User_Logo_arlesaccm.jpg	f	\N	\N
20	Syndicat Mixte Megalis Bretagne -SMMB- (35) 	2016-06-10 15:48:43+02	files/Users/photo/20/avatar	https://www.megalisbretagne.org/jcms/j_6/accueil	2		2016-07-25 12:09:54+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org20	User_Logo_megalisBretagne.png	f	\N	\N
16	Ville Mions (69) 	2016-06-10 15:29:41+02	files/Users/photo/16/avatar	http://www.mions.fr/	2		2016-07-25 14:37:17+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org16	User_Logo_mions.jpg	f	\N	\N
17	Ville Montpellier (34) 	2016-06-10 15:31:47+02	files/Users/photo/17/avatar	https://www.montpellier.fr/	2		2016-07-25 14:38:15+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org17	User_Logo_mairieMontpellier.png	f	\N	\N
23	Conseil Départemental Lozère (48) 	2016-06-10 16:04:18+02	files/Users/photo/23/avatar	http://lozere.fr/	2		2016-07-25 15:37:38+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org23	User_Logo_lozere.png	f	\N	\N
21	Ville Marseille (13) 	2016-06-10 15:51:20+02	files/Users/photo/21/avatar	http://www.marseille.fr/	2		2016-07-25 14:42:08+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org21	User_logo_marseille.gif	f	\N	\N
55	Arawa	2016-07-12 11:40:11+02	files/Users/photo/55/avatar	http://arawa.fr/	5		2016-07-12 11:41:40+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org55	Logo_arawa.svg	f	\N	\N
24	Ville de Cergy (95) 	2016-06-10 16:06:25+02	files/Users/photo/24/avatar	http://www.ville-cergy.fr/accueil/	2		2016-07-25 16:00:15+02	user\r\n	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org24	User_Logo_cergy.png	f	\N	\N
15	Ville de Paris (75)	2016-06-10 15:18:01+02	files/Users/photo/15/avatar	http://www.paris.fr/	2		2016-07-25 16:01:36+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org15	User_Logo_villeParis.jpg	f	\N	\N
5	Centre De Gestion Isère (38) 	2016-04-15 11:20:14+02	files/Users/photo/5/avatar	http://www.cdg38.fr/	2		2016-07-25 15:40:10+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org5	User_Logo_CDG38.svg	f	\N	\N
19	MutuaLibre et Ligue de l'Enseignement, Fédération de l'Aisne	2016-06-10 15:43:21+02	files/Users/photo/19/avatar	http://www.laligue.org/federation-de-laisne-2/	6		2016-07-25 15:44:06+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org19	User_Logo_Mutualibre.jpg	f	\N	\N
27	Ville Vedène (84) 	2016-06-10 16:38:04+02	files/Users/photo/27/avatar	http://www.mairie-vedene.fr/	2		2016-07-25 14:49:20+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org27	User_Logo_vendene.png	f	\N	\N
32	Ville Pertuis (84) 	2016-06-10 16:59:02+02	files/Users/photo/32/avatar	http://www.ville-pertuis.fr/	2		2016-07-25 14:50:08+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org32	USer_Logo_pertuis.jpg	f	\N	\N
18	Communauté d'Agglomération Valence Romans - SUD Rhône-Alpes (26)	2016-06-10 15:39:24+02	files/Users/photo/18/avatar	http://www.valenceromansagglo.fr/	2		2016-07-25 15:36:40+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org18	User_Logo_AggloValenceRomans.jpg	f	\N	\N
29	Ville de Saint Martin D'Uriage (38) 	2016-06-10 16:47:01+02	files/Users/photo/29/avatar	http://www.saint-martin-uriage.com/1.aspx	2		2016-07-26 10:00:30+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org29	User_Logo_saintMartinUriage.jpg	f	\N	\N
26	Groupement d'Intérêt Public Region Centre Interactive -RECIA- (45) 	2016-06-10 16:32:56+02	files/Users/photo/26/avatar	https://www.recia.fr/	2		2016-07-25 15:48:56+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org26	User_Logo_recia.png	f	\N	\N
43	Conseil Départemental Côtes d'Armor (22)	2016-06-13 17:14:15+02	files/Users/photo/43/avatar	http://cotesdarmor.fr/	2		2016-07-25 15:45:11+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org43	USer_Logo_cd22.png	f	\N	\N
56	Osinet	2016-07-12 11:42:09+02	files/Users/photo/56/avatar	http://www.osinet.fr/	5		2016-07-12 11:43:19+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org56	logo_osinet.png	f	\N	\N
48	Syndicat Mixte Somme Numerique (80) 	2016-06-14 10:27:17+02	files/Users/photo/48/avatar	http://www.sommenumerique.fr/	2	Somme numérique est un syndicat mixte, c'est à dire une union de collectivités qui lui ont confié la compétence d'aménagement numérique du territoire. A ce titre Somme Numérique est le propriétaire du Réseau d'Initiative Publique du département de la Somme. Nous développons aussi des services en matière de e-éducation et de e-administration pour les collectivités locales samariennes.	2016-07-25 12:17:35+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org48	USer_Logo_sommenumerique.gif	f	\N	\N
72	Ville de Bezons	2016-11-02 17:37:38+01	files/Users/photo/72/avatar	\N	2		2016-11-02 17:37:38+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org72	logo_Bezons.jpg	f	3cae4c14140254dcee428b80110a3559	\N
73	Syndicat Mixte des Inforoutes	2016-11-03 09:06:02+01	files/Users/photo/73/avatar	\N	2	Depuis 1995, le Syndicat Mixte des Inforoutes (anciennement SIVU) aide les collectivités territoriales à maîtriser les technologies de l’information et de la communication. Il rassemble la très grande majorité des communes ardéchoises et quelques-unes du département limitrophe de la Drôme.	2016-11-03 09:15:41+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org73	350x185_simple.png	f	b08040fb0efff8a341c99fc0a0b4d9c7	\N
77	PAIN Didier	2016-11-28 09:27:59+01	\N	\N	2	responsable informatique du SDIS de la Haute Garonne	2016-11-28 09:27:59+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org77	\N	f	5f0265bbb4c4a1ab72b7074e7b672151	\N
57	2i2l	2016-07-12 11:43:48+02	files/Users/photo/57/avatar	http://www.2i2l.fr/	5	2i2L = Informatique Internet & Logiciel Libre\r\n2i2L est une société de services en informatique, organisme de formation, fondée en 2007, spécialisée dans la formation et l’accompagnement à la migration vers les logiciels libres.\r\nNous travaillons dans un objectif humain de transmission de savoirs et de mise à niveau des compétences, mais également dans une démarche d’adhésion aux intérêts forts que proposent, à tous, les logiciels libres.\r\nNous considérons l’informatique libre comme un moyen de formation indépendant et d’auto-formation, qui permet la construction de connaissances techniques nécessaires à la démocratisation et à la maîtrise des usages numériques.	2016-07-12 11:44:59+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org57	logo_2i2l.jpg	f	\N	\N
54	OpenGO	2016-07-11 18:35:33+02	files/Users/photo/54/avatar	http://opengo.fr/	5	Implanté en région Rhône-Alpes depuis 2006, nous vous accompagnons pour mener à bien vos projets.\r\nNotre métier, répondre à vos besoins spécifiques, parce que chacun a son histoire.\r\nNotre expertise, vous former.\r\nNos valeurs, être à vos côtés dans une démarche qualité, basée sur la relation confiance.	2016-07-12 10:50:14+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org54	logo_Opengo.png	f	\N	\N
60	Ville de Corbas (69)	2016-07-13 11:19:05+02	files/Users/photo/60/avatar	http://www.ville-corbas.fr/	2		2016-07-25 16:00:38+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org60	logo_ville_CORBAS.jpg	f	\N	\N
45	Centre Régional Réunion (974) 	2016-06-14 09:58:11+02	files/Users/photo/45/avatar	http://www.regionreunion.com/fr/spip/	2		2016-07-29 10:55:27+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org45	User_Logo_REGION_REUNION.jpg	f	\N	\N
61	Collabora	2016-07-13 11:48:33+02	files/Users/photo/61/avatar	https://www.collabora.com	5		2016-07-13 11:49:06+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org61	logo_collabora.png	f	\N	\N
62	Igalia	2016-07-13 11:55:42+02	files/Users/photo/62/avatar	https://www.igalia.com/	5		2016-07-13 11:59:44+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org62	logo_igalia.png	f	\N	\N
63	CIB	2016-07-13 12:02:32+02	files/Users/photo/63/avatar	https://libreoffice.cib.de/	5		2016-07-13 12:03:19+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org63	logo_CIB.png	f	\N	\N
36	Metro Montpellier Méditerranée Métropole (34) 	2016-06-13 11:59:28+02	files/Users/photo/36/avatar	http://www.montpellier3m.fr/	2		2016-07-25 11:49:39+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org36	User_Logo_Montpellier-Metropole.png	f	\N	\N
35	Ville Saint Ouen (93) 	2016-06-13 11:44:46+02	files/Users/photo/35/avatar	http://www.saint-ouen.fr/	2		2016-07-25 11:50:34+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org35	User_Logo_saint-ouen.jpg	f	\N	\N
44	Communauté d'Agglomération Clermont Communauté (63) 	2016-06-13 17:17:24+02	files/Users/photo/44/avatar	http://www.clermontcommunaute.fr/accueil/	2		2016-07-25 15:29:05+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org44	User_logo_clermont.png	f	\N	\N
42	Communauté d'Agglomération Sud Est Toulousain -SICOVAL - (31) 	2016-06-13 17:09:26+02	files/Users/photo/42/avatar	http://www.sicoval.fr/fr/index.html	2		2016-07-25 15:31:32+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org42	User_Logo_sicoval.png	f	\N	\N
52	Conseil Départemental Seine Maritime (76)	2016-06-23 13:58:38+02	\N		2		2016-07-25 15:35:08+02	Admistration	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org52	\N	f	\N	\N
38	Ville Dunkerque (59) 	2016-06-13 16:34:59+02	files/Users/photo/38/avatar	https://www.communaute-urbaine-dunkerque.fr/accueil/	2		2016-07-25 11:57:44+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org38	Software_Logo_dunkerque.png	f	\N	\N
59	Ville Voreppe (38)	2016-07-13 11:02:14+02	files/Users/photo/59/avatar	http://www.voreppe.fr/	2		2016-07-25 11:59:05+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org59	logo_ville_voreppe.png	f	\N	\N
40	Metro Strasbourg - Euro Métropole (67) 	2016-06-13 16:55:55+02	\N	http://www.strasbourg.eu/fr	2		2016-07-25 12:02:49+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org40	\N	f	\N	\N
31	Groupement d'Intérêt Public Aten (34)	2016-06-10 16:54:52+02	files/Users/photo/31/avatar	http://www.espaces-naturels.fr/	6		2016-07-25 16:05:33+02	user\r\n	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org31	User_logo_aten.png	f	\N	\N
81	Mickael Pastor	2016-12-20 18:14:48+01	\N		2		2017-02-10 15:56:37+01	admin	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org81	\N	f	36c21da56aeec766c4e1628710fcfc3b	8e6c9a9f-bba0-4759-b088-3a1d1a2646a8
49	Caisse Primaire d'Assurance Maladie de la Manche (50)	2016-06-14 10:42:27+02	files/Users/photo/49/avatar	http://www.ameli.fr/	2		2016-07-25 15:43:07+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org49	User_Logo_cpammanche.jpg	f	\N	\N
50	Ville de Caluire et Cuire (69) 	2016-06-14 10:46:14+02	files/Users/photo/50/avatar	http://www.ville-caluire.fr/	2		2016-07-25 15:59:51+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org50	User_Logo_caluire.png	f	\N	\N
34	Centre hospitalier des Pyrénées (64)	2016-06-13 11:12:07+02	files/Users/photo/34/avatar	http://www.ch-pyrenees.fr/	2		2016-07-25 15:00:25+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org34	User_Logo_chpyrene.jpg	f	\N	\N
39	Centre De Gestion Mayenne (53) 	2016-06-13 16:41:01+02	files/Users/photo/39/avatar	http://www.cdg53.fr/	2		2016-07-25 15:26:51+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org39	User_logo_cdg53.jpg	f	\N	\N
41	Conseil Départemental Loire Atlantique (44)	2016-06-13 17:06:09+02	files/Users/photo/41/avatar	http://www.loire-atlantique.fr/jcms/services-fr-c_5026	2		2016-07-25 15:50:03+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org41	User_Logo_cd44.png	f	\N	\N
46	Ville d'Albi (81) 	2016-06-14 10:02:47+02	files/Users/photo/46/avatar		2		2016-07-25 15:58:35+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org46	User_Logo_albi.png	f	\N	\N
64	Digital Conseil	2016-07-13 14:00:33+02	files/Users/photo/64/avatar	http://www.digital-conseil.fr/	5	Créée en 2010, DIGITAL CONSEIL est une jeune société de service dans les Nouvelles Techno­logies d'Informations et Communications.	2016-07-20 17:32:59+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org64	logo_digital-conseil.png	f	\N	\N
66	Teclib	2016-07-21 17:25:14+02	files/Users/photo/66/avatar	http://www.teclib.com/	5	TECLIB' est une société de services et conseils informatiques spécialisée dans l'industrialisation des technologies libres. 	2016-07-21 17:27:07+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org66	User_Logo_techlib.png	f	\N	\N
58	Service départemental d'incendie et de secours Isère (38) 	2016-07-13 10:52:38+02	files/Users/photo/58/avatar	http://www.sdis38.fr/	2		2016-07-25 15:21:27+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org58	logo_sdis38.png	f	\N	\N
4	Centre De Gestion Vendée (85) 	2016-04-15 11:19:09+02	files/Users/photo/4/avatar	http://www.cdg85.fr/	2		2016-07-25 15:39:35+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org4	User_Logo_CDG85.png	f	\N	\N
85	Maarch	2017-02-04 18:11:38+01	files/Users/photo/85/avatar	http://www.maarch.com/	5	Maarch édite des logiciels libres au service de votre dématérialisation.\r\n\r\nMaarch Courrier (GED/GEC) : Gestion des correspondances (courrier postal et courriel) \r\nMaarch RM  : Logiciel d'archivage électronique à valeur probante, auditable et certifiable	2017-02-04 18:21:18+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org85	Maarch_logo_350.png	f	a6ce9e2eb03aafbeb1c52df46761f627	\N
47	Ville de Béziers (34) 	2016-06-14 10:10:51+02	files/Users/photo/47/avatar	http://www.ville-beziers.fr/	2		2016-07-25 15:59:28+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org47	Logo_officiel_de_la_ville_de_Béziers.svg	f	\N	\N
71	Matthieu FAURE	2016-11-02 15:09:16+01	files/Users/photo/71/avatar	\N	4	Créateur de Asqatasun http://Asqatasun.org/ logiciel libre de mesure d'accessibilité web et de SEO (anciennement connu sous le nom de Tanaguru). Architecte Logiciel chez Adullact http://www.adullact.org/ association loi 1901 de promotion du logiciel libre auprès des Collectivités et Administrations. Moniteur de voile bénévole à l'école des Glénans http://www.glenans.asso.fr/	2017-02-07 16:55:35+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org71	mfaure_img_4946_recadre_110x110.jpg	f	af55e3de0d0bf2be40ceb880b30a8fef	39ca12bf-5588-407b-8989-45b809cf40ce
88	M.Faure	2017-02-09 13:46:42+01	\N		4		2017-02-09 13:46:43+01	admin	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org88	\N	f	d6f0086ab5990fec1805f45e73a489e5	\N
3	Adullact	2016-04-11 11:44:31+02	files/Users/photo/3/avatar	http://www.adullact.org/	6	ADULLACT a pour objectifs de soutenir et coordonner l'action des Administrations et Collectivités territoriales dans le but de promouvoir, développer et maintenir un patrimoine de logiciels libres utiles aux missions de service public.	2017-02-09 17:20:47+01	admin	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org3	User_logo_adullactasso.png	f	07121b8e1eca5490a23efca9ebb0a3e9	1665334f-65e9-43e5-b6e9-68de1cd2b569
37	Centre De Gestion Nord (59) 	2016-06-13 16:31:32+02	files/Users/photo/37/avatar	http://www.cdg59.fr/	2		2017-02-15 06:46:11+01	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org37	User_Logo_cdg59.jpg	f	d319070fdd8e8768d29a38f1671474f5	5300a15b-913d-42dc-9f22-99dbf6f9c34b
76	OpenSides	2016-11-22 19:31:58+01	files/Users/photo/76/avatar	\N	5	Spécialiste sur les annuaires OpenLDAP, nous éditons FusionDirectory, une solution web de gestion d'annuaire OpenLDAP. FusionDirectory rend l'utilisation des données de l'annuaire d'entreprise accessible aux différents intervenants de l'entreprise.\r\n\r\nNous nous occupons aussi de toute les problematiques d'audit d'annuaires, de SSO et de formations sur ces technologies	2017-03-10 15:58:44+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org76	opensides-logo1.png	f	8f947372316c49b83ac39bfd24de8787	\N
128	Clément OUDOT	2017-03-10 16:46:54+01	files/Users/photo/128/avatar		4		2017-03-10 16:46:54+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org128	clement.jpg	f	bd1dbb9fa72bd4f6b4d93e3092463ed6	\N
51	Conseil Départemental Seine Maritime (76) 	2016-06-14 10:52:49+02	files/Users/photo/51/avatar	http://www.seinemaritime.fr/	2		2016-07-25 15:51:21+02	user	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org51	User_Logo_cd76.png	f	\N	\N
127	Savoir-faire Linux	2017-03-10 16:38:05+01	files/Users/photo/127/avatar	https://www.savoirfairelinux.com/	5	Savoir-faire Linux* est le chef de file de l'informatique à code ouvert sous licence libre au Québec et au Canada. Depuis 1999, l'entreprise développe une expertise exceptionnelle qu'elle met au service des entreprises et des organisations publiques afin de répondre aux défis d'évolution de leurs systèmes d'information en mutation constante.\r\n\r\nDotée en 2016 d'une équipe multidisciplinaire de plus de 140 consultants, Savoir-faire Linux sert une clientèle composée de centaines d'organisations publiques régionales, nationales et internationales, géants industriels et PME/PMI.  L'entreprise a son siège social à Montréal et des bureaux à Québec, Toronto, Paris et Lyon. Certifiée ISO 9001 et ISO 14001,  elle est fortement implantée dans les communautés du logiciel libre, membre de la prestigieuse Foundation Linux et apporte des contributions majeures à des nombreux logiciels libres.\r\n\r\n* La marque de commerce Linux® est utilisée conformément à une sous-licence de LMI, licencié exclusif de Linus Torvalds, propriétaire de la marque au niveau mondial.	2017-03-10 17:01:41+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org127	logo-sfl-coul-rgb-72dpi.png	f	e5bc345418056dec0be5e8b57769caee	\N
130	Christophe	2017-03-15 16:49:02+01	files/Users/photo/130/avatar		4	Trésorier La Mouette, 	2017-03-15 16:49:03+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org130	cazin.jpg	f	411aa100d79659b57c16d8d9a34c6f44	\N
131	benasse	2017-03-19 21:01:06+01	\N	https://cicogna.fr	4		2017-03-19 21:01:07+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org131	\N	f	1b477b77869313e340532fd336bc15f6	\N
133	canizares	2017-03-25 07:36:19+01	\N	http://www.ccfg.fr	2		2017-03-25 07:36:20+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org133	\N	f	7544a33c7c627f832f13f6f36e31137c	\N
6	Libriciel SCOP	2016-04-18 11:16:10+02	files/Users/photo/6/avatar	https://www.libriciel.fr/	5	Libriciel SCOP est une Société Coopérative et Participative. Libriciel SCOP a pour raison d'être le financement, le développement, la maintenance d'un patrimoine d'applications libres métiers, à destination des collectivités territoriales et des administrations.	2017-03-14 09:40:00+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org6	Libriciel_TR---Copie.png	f	dd7bfa2920b6a9e7a665b734533301df	7c5debcd-4da9-4283-8085-e6d90a9ad4bb
132	Syloé	2017-03-20 16:59:30+01	files/Users/photo/132/avatar	http://www.syloe.fr	5	Chez Syloé, c’est la sérénité de nos clients qui parle pour nous!\r\nSYLOE est une ENL (Entreprise du Numérique Libre) qui propose une expertise Linux de pointe, en phase avec toutes les solutions innovantes dans le domaine de la sécurité et de l’ingénierie des systèmes d’information.\r\nNous accompagnons nos clients dans le cadre de 4 activités complémentaires :\r\n+ Audit des serveurs existants (sécurité, performance, configuration, Haute Disponibilité)\r\n+ Intégration des outils opensource dans leur Systèmes d'Informations\r\n+ Infogérance des serveurs hébergés ou distants \r\n+ Formation et support distant niveau 3	2017-03-20 22:28:17+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org132	Syloe_logo_French_Tech.png	f	f8e37698a872a922ae89a8c56975bb2c	\N
129	Fabrice	2017-03-14 14:30:34+01	files/Users/photo/129/avatar	https://contrast-finder.org/	4	Contributeur aux logiciels Contrast-Finder et Asqatasun	2017-03-15 16:14:54+01	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org129	Tx8JYq_p_400x400.jpg	f	6cdfcf1bc8aadebc1cd301628813ef46	\N
134	Asqatasun.org	2017-03-28 19:49:29+02	files/Users/photo/134/avatar	http://asqatasu.org	6		2017-03-28 19:52:28+02	User	$2y$10$yF7BtjpI318tHCy3gMlEr.N3Lwf0BZmYN5VzJ6EjkR715rkJ524gu	fake-email@comptoir-du-libre.org134	logo-picto_RVB-215x254.png	f	ad839015377c4a40e1bb8a460bb24cd6	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: comptoir
--

SELECT pg_catalog.setval('public.users_id_seq', 160, true);


--
-- Name: idx_26468_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licence_types
    ADD CONSTRAINT idx_26468_primary PRIMARY KEY (id);


--
-- Name: idx_26477_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licenses
    ADD CONSTRAINT idx_26477_primary PRIMARY KEY (id);


--
-- Name: idx_26486_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT idx_26486_primary PRIMARY KEY (id);


--
-- Name: idx_26495_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT idx_26495_primary PRIMARY KEY (id);


--
-- Name: idx_26501_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT idx_26501_primary PRIMARY KEY (id);


--
-- Name: idx_26507_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT idx_26507_primary PRIMARY KEY (id);


--
-- Name: idx_26513_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationship_types
    ADD CONSTRAINT idx_26513_primary PRIMARY KEY (id);


--
-- Name: idx_26522_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT idx_26522_primary PRIMARY KEY (id);


--
-- Name: idx_26531_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.screenshots
    ADD CONSTRAINT idx_26531_primary PRIMARY KEY (id);


--
-- Name: idx_26540_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares
    ADD CONSTRAINT idx_26540_primary PRIMARY KEY (id);


--
-- Name: idx_26549_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT idx_26549_primary PRIMARY KEY (id);


--
-- Name: idx_26558_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.user_types
    ADD CONSTRAINT idx_26558_primary PRIMARY KEY (id);


--
-- Name: panels_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.panels
    ADD CONSTRAINT panels_pkey PRIMARY KEY (id);


--
-- Name: phinxlog_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.phinxlog
    ADD CONSTRAINT phinxlog_pkey PRIMARY KEY (version);


--
-- Name: raw_metrics_softwares_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.raw_metrics_softwares
    ADD CONSTRAINT raw_metrics_softwares_pkey PRIMARY KEY (id);


--
-- Name: requests_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.requests
    ADD CONSTRAINT requests_pkey PRIMARY KEY (id);


--
-- Name: softwares_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ADD CONSTRAINT softwares_tags_pkey PRIMARY KEY (id);


--
-- Name: statistics_averages_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.statistics_averages
    ADD CONSTRAINT statistics_averages_pkey PRIMARY KEY (id);


--
-- Name: statistics_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_statistics
    ADD CONSTRAINT statistics_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: unique_panel; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.panels
    ADD CONSTRAINT unique_panel UNIQUE (request_id, panel);


--
-- Name: idx_26477_fk_license_licence_type_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26477_fk_license_licence_type_idx ON public.licenses USING btree (license_type_id);


--
-- Name: idx_26486_fk_relationships_relationship_types_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26486_fk_relationships_relationship_types_idx ON public.relationships USING btree (relationship_type_id);


--
-- Name: idx_26495_fk_software_relationship_relation_type_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26495_fk_software_relationship_relation_type_idx ON public.relationships_softwares USING btree (relationship_id);


--
-- Name: idx_26495_fk_software_relationship_software_from; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26495_fk_software_relationship_software_from ON public.relationships_softwares USING btree (software_id);


--
-- Name: idx_26495_fk_software_relationship_to_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26495_fk_software_relationship_to_idx ON public.relationships_softwares USING btree (recipient_id);


--
-- Name: idx_26501_fk_entity_software_relationship_entity_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26501_fk_entity_software_relationship_entity_idx ON public.relationships_softwares_users USING btree (user_id);


--
-- Name: idx_26501_fk_entity_software_relationship_relationship_type_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26501_fk_entity_software_relationship_relationship_type_idx ON public.relationships_softwares_users USING btree (relationship_id);


--
-- Name: idx_26501_fk_user_software_relationship_software; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26501_fk_user_software_relationship_software ON public.relationships_softwares_users USING btree (software_id);


--
-- Name: idx_26507_fk_entities_relationships_entities_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26507_fk_entities_relationships_entities_idx ON public.relationships_users USING btree (user_id);


--
-- Name: idx_26507_fk_entities_relationships_relationtion_types_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26507_fk_entities_relationships_relationtion_types_idx ON public.relationships_users USING btree (relationship_id);


--
-- Name: idx_26507_fk_entities_relationships_users_recipient_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26507_fk_entities_relationships_users_recipient_idx ON public.relationships_users USING btree (recipient_id);


--
-- Name: idx_26513_cd_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26513_cd_unique ON public.relationship_types USING btree (cd);


--
-- Name: idx_26513_name_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26513_name_unique ON public.relationship_types USING btree (name);


--
-- Name: idx_26522_fk_review_software_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26522_fk_review_software_idx ON public.reviews USING btree (software_id);


--
-- Name: idx_26522_fk_review_user_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26522_fk_review_user_idx ON public.reviews USING btree (user_id);


--
-- Name: idx_26531_fk_screens_software_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26531_fk_screens_software_idx ON public.screenshots USING btree (software_id);


--
-- Name: idx_26540_fk_software_licence_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26540_fk_software_licence_idx ON public.softwares USING btree (licence_id);


--
-- Name: idx_26540_software_url_repository_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26540_software_url_repository_unique ON public.softwares USING btree (url_repository);


--
-- Name: idx_26540_softwarename_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26540_softwarename_unique ON public.softwares USING btree (softwarename);


--
-- Name: idx_26549_fk_users_user_types_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26549_fk_users_user_types_idx ON public.users USING btree (user_type_id);


--
-- Name: unique_web_site_url; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX unique_web_site_url ON public.softwares USING btree (web_site_url);


--
-- Name: fk_entities_relationships_relationtion_types; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT fk_entities_relationships_relationtion_types FOREIGN KEY (relationship_id) REFERENCES public.relationships(id);


--
-- Name: fk_entities_relationships_users; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT fk_entities_relationships_users FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: fk_entities_relationships_users_recipient; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT fk_entities_relationships_users_recipient FOREIGN KEY (recipient_id) REFERENCES public.users(id);


--
-- Name: fk_entity_software_relationship_entity; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT fk_entity_software_relationship_entity FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- Name: fk_license_licence_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licenses
    ADD CONSTRAINT fk_license_licence_type FOREIGN KEY (license_type_id) REFERENCES public.licence_types(id);


--
-- Name: fk_relationships_relationship_types; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT fk_relationships_relationship_types FOREIGN KEY (relationship_type_id) REFERENCES public.relationship_types(id);


--
-- Name: fk_review_software; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT fk_review_software FOREIGN KEY (software_id) REFERENCES public.softwares(id);


--
-- Name: fk_review_user; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT fk_review_user FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: fk_screens_software; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.screenshots
    ADD CONSTRAINT fk_screens_software FOREIGN KEY (software_id) REFERENCES public.softwares(id);


--
-- Name: fk_software_licence; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares
    ADD CONSTRAINT fk_software_licence FOREIGN KEY (licence_id) REFERENCES public.licenses(id);


--
-- Name: fk_software_relationship_relation_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT fk_software_relationship_relation_type FOREIGN KEY (relationship_id) REFERENCES public.relationships(id);


--
-- Name: fk_software_relationship_software_from; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT fk_software_relationship_software_from FOREIGN KEY (software_id) REFERENCES public.softwares(id);


--
-- Name: fk_software_relationship_to; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT fk_software_relationship_to FOREIGN KEY (recipient_id) REFERENCES public.softwares(id);


--
-- Name: fk_softwares; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_statistics
    ADD CONSTRAINT fk_softwares FOREIGN KEY (software_id) REFERENCES public.softwares(id);


--
-- Name: fk_user_software_relationship_relationship_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT fk_user_software_relationship_relationship_type FOREIGN KEY (relationship_id) REFERENCES public.relationships(id);


--
-- Name: fk_user_software_relationship_software; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT fk_user_software_relationship_software FOREIGN KEY (software_id) REFERENCES public.softwares(id);


--
-- Name: fk_user_user_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_user_user_type FOREIGN KEY (user_type_id) REFERENCES public.user_types(id);


--
-- Name: raw_metrics_softwares_software_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.raw_metrics_softwares
    ADD CONSTRAINT raw_metrics_softwares_software_id_fkey FOREIGN KEY (software_id) REFERENCES public.softwares(id);


--
-- Name: softwares_tags_software_id; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ADD CONSTRAINT softwares_tags_software_id FOREIGN KEY (software_id) REFERENCES public.softwares(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: softwares_tags_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ADD CONSTRAINT softwares_tags_tag_id FOREIGN KEY (tag_id) REFERENCES public.tags(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

