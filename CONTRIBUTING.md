# Contributing

First off, thanks for taking the time to contribute!

## How Can I Contribute?

- Reporting bugs
- Suggesting enhancements
- Code contribution

## Styleguides

- [Git Commit message](Documentation/GIT_CONVENTION.md)

## Sources of inspiration for CONTRIBUTING.md

* For contributors doc [Gitlab Workflow](https://about.gitlab.com/handbook/#gitlab-workflow)
* [Atom's Contributing file](https://github.com/atom/atom/blob/master/CONTRIBUTING.md) that is really good and brightly written.
