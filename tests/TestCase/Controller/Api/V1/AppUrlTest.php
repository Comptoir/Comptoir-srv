<?php

namespace App\Test\TestCase\Controller\Api\V1;

/**
 * URls webapp Test Case
 */
class AppUrlTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxonomys',
        'app.taxonomys_softwares',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
   //     'app.usedsoftwares',
   //     'app.backedsoftwares',
   //     'app.createdsoftwares',
   //     'app.contributionssoftwares',
   //     'app.providerforsoftwares',
        'app.screenshots',
        'app.softwares_statistics',
        'app.tags',
        'app.softwares_tags',
    //    'app.userssoftwares',
    //    'app.backerssoftwares',
    //    'app.creatorssoftwares',
    //    'app.contributorssoftwares',
    //    'app.providerssoftwares',
    //    'app.workswellsoftwares',
    //    'app.alternativeto'
    ];


// Mapping method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * Providing the list of URLs to be tested
     * with the expected redirection URLs (with english prefix at begining of URL)
     *
     * @param bool $extractOnly
     * @return string[]      example: Array('/<tested_URL>' => '/en/<expected_URL>')
     */
    private function getEnglishUrls($extractOnly = false)
    {
        $urlsExtract = [
            '/' => '/en/',
            '/pages/contact' => '/en/pages/contact',
            '/users/2' => '/en/users/2',
        ]; // Format: ''/<tested_URL>' => '/en/<expected_URL>'

        $urls = [
            '/' => '/en/',
            '/pages/index' => '/en/',
            '/pages/contact' => '/en/pages/contact',
            '/pages/accessibility' => '/en/pages/accessibility',
            '/users/2' => '/en/users/2',
            '/users/usedSoftwares/2' => '/en/users/usedSoftwares/2',
            '/users/providerforSoftwares/2' => '/en/users/providerforSoftwares/2',
            '/users/2/reviews' => '/en/users/2/reviews',

            '/softwares/3' => '/en/softwares/3',
            '/softwares/usersSoftware/3' => '/en/softwares/usersSoftware/3',
            '/softwares/servicesProviders/3' => '/en/softwares/servicesProviders/3',
            '/softwares/usersSoftware/3' => '/en/softwares/usersSoftware/3',
            '/softwares/1/reviews' => '/en/softwares/1/reviews',
            '/softwares/1/reviews/2' => '/en/softwares/1/reviews/2',
        ]; // Format: '/<tested_URL>' => '/en/<expected_URL>'

        if ($extractOnly === true) {
            return $urlsExtract;
        }
        return $urls;
    }

    /**
     * Providing the list of URLs to be tested
     * with the expected redirection URLs (with french prefix at begining of URL)
     *
     * @param bool $extractOnly
     * @return string[]      example: Array('/<tested_URL>' => '/fr/<expected_URL>')
     */
    private function getFrenchUrls($extractOnly = false)
    {
        $urls = [];
        foreach ($this->getEnglishUrls($extractOnly) as $testedUrl => $expectedUrl) {
            $urls[$testedUrl] = str_replace('/en/', '/fr/', $expectedUrl);
        }
        return $urls;
    }


    /**
     * Check that the redirections are working
     * when the language prefix is not present in the URL.
     *
     * @group public
     * @group url
     * @return void
     */
    public function testRedirectWhenLangPrefixIsMissing()
    {
        // Load data
        $englishUrls = $this->getEnglishUrls(); // Format: '/<tested_URL>' => '/en/<expected_URL>'
        $frenchUrls = $this->getFrenchUrls();   // Format: '/<tested_URL>' => '/fr/<expected_URL>'

        // Checki if the URLs redirects correctly
        $acceptLanguages = ['en']; // force user language to english using HTTP header "Accept-Language"
        $this->commonCheck($englishUrls, $acceptLanguages);
        $acceptLanguages = ['fr']; // force user language to french using HTTP header "Accept-Language"
        $this->commonCheck($frenchUrls, $acceptLanguages);
    }



    /**
     * Check if the URLs redirects correctly for English variations of HTTP 'Accept-Language' header
     * when the language prefix is not present in the URL.
     *
     * @group public
     * @group url
     * @return void
     */
    public function testRedirectForEnglishVariationsOfHttpAcceptLanguage()
    {
        $urls = $this->getEnglishUrls(true); // Format: '/<tested_URL>' => '/en/<expected_URL>'
        $acceptLanguages = [
            'en-US',
            'en-gb',
            'en-GB,en;q=0.5',
        ];
        $this->commonCheck($urls, $acceptLanguages);
    }

    /**
     * Check if the URLs redirects correctly for French variations of HTTP 'Accept-Language' header
     * when the language prefix is not present in the URL.
     *
     * @group public
     * @group url
     * @return void
     */
    public function testRedirectForFrenchVariationsOfHttpAcceptLanguage()
    {
        $urls = $this->getFrenchUrls(true);  // Format: '/<tested_URL>' => '/fr/<expected_URL>'
        $acceptLanguages = [
            'fr-CA',
            'fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5',
        ];
        $this->commonCheck($urls, $acceptLanguages);
    }


    /**
     * Check if the URLs redirects correctly for not allowed HTTP 'Accept-Language' header
     * when the language prefix is not present in the URL.
     *
     * @group public
     * @group url
     * @return void
     */
    public function testRedirectWhenHttpAcceptLanguageIsNotAllowed()
    {
        $urls = $this->getEnglishUrls(true); // Format: '/<tested_URL>' => '/en/<expected_URL>'
        $acceptLanguages = [
            'de',
            'de-CH',
            'da, en-GB;q=0.8, en;q=0.7',
            'zh, en-us; q=0.8, en; q=0.6',
        ];
        $this->commonCheck($urls, $acceptLanguages);
    }


    /**
     * Check that redirections are working
     * for old URLs
     *
     * @group public
     * @group url
     * @return void
     */
    public function testRedirectWhenOldURL()
    {
        // Format: 'Tested URL' => 'Expected URL'
        $urls = [
            '/softwares/services-providers/3' => '/en/softwares/servicesProviders/3',
            '/softwares/users-software/3' => '/en/softwares/usersSoftware/3',
        ];
        $englishUrls = [];
        foreach ($urls as $testedUrl => $expectedUrl) {
            $englishUrls['/en'. $testedUrl] = $expectedUrl;
        }

        // Checki if the URLs redirects correctly
        $acceptLanguages = ['en']; // force user language to english using HTTP header "Accept-Language"
        $this->commonCheck($urls, $acceptLanguages);
        $this->commonCheck($englishUrls, $acceptLanguages);
    }

    /**
     * Checking if a list of URLs redirects correctly
     * and force user language using HTTP header "Accept-Language"
     *
     * @param array $urls
     * @param array|string[] $acceptLanguages  default: ['en']
     */
    private function commonCheck(array $urls, array $acceptLanguages = ['en'])
    {
        foreach ($acceptLanguages as $acceptLanguage) {
            $this->_request['headers']['Accept-Language'] = "$acceptLanguage";
            foreach ($urls as $testedUrl => $expectedUrl) {
                $this->checkUrlRedirectToAnotherUrl($testedUrl, $expectedUrl, ['html'], 301);
            }
        }
    }
}
