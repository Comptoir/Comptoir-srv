<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\TagsController;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\TagsController Test Case
 */
class TagsControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tags',
        'app.softwares',
        'app.softwares_tags',
        'app.licenses',
        'app.licence_types',
        'app.raw_metrics_softwares',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',

    ];

    /**
     * Test index method of Tags controller.
     * List all Tags with at least 1 relation with a software. If no one is missing test is Ok.
     *
     * @group public
     * @group tag
     * @group tag_public
     * @group tag_method_index
     *
     * @return void
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/api/v1/tags/');

        $expected = [
            "tags" =>
                [
                    [
                        "used_tag_number" => 3,
                        "id" => 1,
                        "name" => "myNewTagCreated",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "tag_string" => "myNewTagCreated myTagDeleted",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Description Lorem Ipsum",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                            [
                                "id" => 2,
                                "softwarename" => "Lutèce",
                                "url_repository" => "http://www.fake-repo-lutece.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-lutece.com",
                                "tag_string" => "myNewTagCreated myTagAdded1",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                        ],
                        "usedTagNumber" => 3
                    ],
                    [
                        "used_tag_number" => 2,
                        "id" => 2,
                        "name" => "myTagAdded1",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 2,
                                "softwarename" => "Lutèce",
                                "url_repository" => "http://www.fake-repo-lutece.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-lutece.com",
                                "tag_string" => "myNewTagCreated myTagAdded1",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Description Lorem Ipsum",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ]
                        ],
                        "usedTagNumber" => 2
                    ],
                    [
                        "used_tag_number" => 1,
                        "id" => 3,
                        "name" => "myTagAdded2",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Description Lorem Ipsum",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ]
                        ],
                        "usedTagNumber" => 1
                    ],
                    [
                        "used_tag_number" => 1,
                        "id" => 5,
                        "name" => "myTagDeleted",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "tag_string" => "myNewTagCreated myTagDeleted",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ]
                        ],
                        "usedTagNumber" => 1
                    ],
                ]
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test view method of Tags controller for HTML format
     * must redirect to new tag URL
     *
     *      URL :   Webapp  /$lang/tags/$idTag   ---> redirect 301 to /$lang/tags/$idTag/software
     *                      /$lang/tags/$idTag/  ---> redirect 301 to /$lang/tags/$idTag/software
     *
     * @group public
     * @group tag
     * @group tag_public
     * @group tag_method_view
     *
     * @return void
     */
    public function testViewMethodForHtmlFormat()
    {
        $this->get('/tags/4');
        $this->currentResponseIsRedirectionToAnotherUrl('/en/tags/4/software', 301);

        $this->get('/fr/tags/4');
        $this->currentResponseIsRedirectionToAnotherUrl('/fr/tags/4/software', 301);

        $this->get('/fr/tags/4/');
        $this->currentResponseIsRedirectionToAnotherUrl('/fr/tags/4/software', 301);
    }

    /**
     * Test API view method of Tags controller for JSON format.
     * Show us all data of one tag, here is tested tag id=4.
     *
     *      URL :   API     /$lang/tags/$idTag.json
     *
     * @group public
     * @group tag
     * @group tag_public
     * @group tag_method_view
     *
     * @return void
     */
    public function testViewMethodForJsonFormat()
    {
        $this->get('/api/v1/tags/4.json');

        $expected = [
            "tag" => [
                "id" => 4,
                "name" => "myTagAdded3",
                "created" => null,
                "modified" => null,
                "softwares" => [],
                "usedTagNumber" => 0
            ],
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add method of Tags controller for connected user,
     * with new tag named "christian" but without Software relationship.
     * If 'christian' is found in result, test is OK.
     *
     *      URL: /api/v1/taxonomys/add
     *           /api/v1/taxonomys/add.json
     *           ----> allowed only for connected user
     *
     * @group user
     * @group tag
     * @group tag_user
     * @group tag_method_add
     *
     * @return void
     */
    public function testAddTagWithoutSoftware()
    {
        $this->setConnectedUserSession();
        $data = [
            'name' => "christian",
            'created' => null,
            'modified' => null,
        ];
        $this->post('/api/v1/tags', $data);

        $this->assertResponseSuccess();
        $tag = TableRegistry::get('Tags');
        $query = $tag->find()->where(['name' => $data['name']]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test add method of Tags controller for anonymous user,
     * with new tag named "christian" but without Software relationship.
     * - Try to add a new tag  ---> redirect to /users/login
     * - Check if the new tag is not be added
     *
     *      URL: /api/v1/taxonomys/add
     *           /api/v1/taxonomys/add.json
     *           ----> allowed only for connected user
     *
     * @group anonymous
     * @group tag
     * @group tag_user
     * @group tag_method_add
     * @return void
     */
    public function testFailAddForAnonymousUser()
    {
        // Try to add a new tag
        $this->setAnonymousUserSession();
        $data = [
            'name' => "christian",
            'created' => null,
            'modified' => null,
        ];
        $this->post('/api/v1/tags', $data);
        $this->assertResponseCode(302);
        $headers = $this->_response->header();
        $this->assertEquals('/users/login', $headers['Location']);

        // Check if the new tag is NOT added
        $this->setAdminSessionOfPersonType();
        $this->assertResponseSuccess();
        $tag = TableRegistry::get('Tags');
        $query = $tag->find()->where(['name' => $data['name']]);
        $this->assertEquals(0, $query->count());
    }



    /**
     * Common for edit method
     * - Update a tag
     */
    private function commonEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $change = [
            'name' => 'myTagChangedName',
            'modified' => "2017-01-23T14:32:40+00:00",
        ];
        $this->put('/api/v1/tags/4.json', $change);
    }

    /**
     * Test edit method of Tags controller, By changing the name of a Tag (TagId =4) without any software related.
     * Edit Tag's name: "myTagAdded3" to "myTagChangedName"
     * If the new tag is found, test is OK.
     *
     * @group admin
     * @group tag
     * @group tag_admin
     * @group tag_method_edit
     *
     * @return void
     */
    public function testEdit()
    {
        // Edit a tag
        $this->setAdminSessionOfPersonType();
        $this->commonEdit();

        // Check if the tag is updated
        $this->get('/api/v1/tags/4.json');
        $expected =
            [
                "tag" =>
                    [
                        "id" => 4,
                        "name" => "myTagChangedName",
                        "created" => null,
                        "modified" => "2017-01-23T14:32:40+00:00",
                        "softwares" => [],
                        "usedTagNumber" => 0
                    ]
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test edit method of Tags controller for anonymous user,
     * - Try to edit a tag  ---> redirect to /users/login
     * - Check if the tag is not be updated
     *
     *      URL: /api/v1/taxonomys/edit
     *           /api/v1/taxonomys/edit.json
     *           ----> allowed only for admin user
     *
     * @group anonymous
     * @group tag
     * @group tag_user
     * @group tag_method_add
     * @return void
     */
    public function testFailEditForAnonymousUser()
    {
        // Try to edit a tag
        $this->setAnonymousUserSession();
        $this->commonEdit();
        $this->assertResponseCode(302);
        $headers = $this->_response->header();
        $this->assertEquals('/users/login', $headers['Location']);

        // Check that the tag is actually not updated
        $this->commonCheckFailEdit();
    }


    /**
     * Test edit method of Tags controller for connected user ,
     * - Try to edit a tag  ---> HTTP response code = 403 "You are not authorized"
     * - Check if the tag is not be updated
     *
     *      URL: /api/v1/taxonomys/edit
     *           /api/v1/taxonomys/edit.json
     *           ----> allowed only for admin user
     *
     * @group anonymous
     * @group tag
     * @group tag_user
     * @group tag_method_add
     * @return void
     */
    public function testFailEditForConnectedUser()
    {
        // Try to edit a tag
        $this->setConnectedUserSession();
        $this->commonEdit();
        $objJson = json_decode($this->_response->body());
        $this->assertResponseCode(403);
        $this->assertEquals("You are not authorized to access that location.", $objJson->message);
        $this->assertEquals(403, $objJson->code);

        // Check that the tag is actually not updated
        $this->commonCheckFailEdit();
    }



    /**
     * Common checks for edit fail
     * - Connect as admin user
     * - Check if the tag is not be updated
     */
    private function commonCheckFailEdit()
    {
        // Check that the tag is not updated
        $this->setAdminSessionOfPersonType();
        $this->get('/api/v1/tags/4.json');
        $expected =
            [
                "tag" =>
                    [
                        "id" => 4,
                        "name" => "myTagAdded3",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [],
                        "usedTagNumber" => 0
                    ]
            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Common for delete method
     * - delete a tag (id = 4)
     */
    private function commonDelete()
    {
        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);
        $this->delete('/api/v1/tags/4');
    }

    /**
     * Common checks for delete fail
     * - Connect as anonymous user
     * - Check if the tag is not be deleted (id = 4)
     */
    private function commonCheckFailDelete()
    {
        $this->setAnonymousUserSession();
        $this->get('/api/v1/tags/4.json');
        $expected = [
            "tag" => [
                "id" => 4,
                "name" => "myTagAdded3",
                "created" => null,
                "modified" => null,
                "softwares" => [],
                "usedTagNumber" => 0
            ],
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test delete method of Tags controller for anonymous user (allowed only for admin user),
     * - Try to delete a tag (id = 4)  ---> redirect to /users/login
     * - Check if the tag is not be deleted (id = 4)
     *
     * @group anonymous
     * @group tag
     * @group tag_admin
     * @group tag_method_delete
     * @return void
     */
    public function testFailDeleteForAnonymousUser()
    {
        // Try to delete a tag
        $this->setAnonymousUserSession();
        $this->commonDelete();
        $this->assertResponseCode(302);
        $headers = $this->_response->header();
        $this->assertEquals('/users/login', $headers['Location']);

        // Check that the tag is actually not deleted
        $this->commonCheckFailDelete();
    }


    /**
     * Test delete method of Tags controller for connected user (allowed only for admin user),
     * - Try to delete a tag (id = 4) ---> HTTP response code = 403 "You are not authorized"
     * - Check if the tag is not be deleted (id = 4)
     *
     * @group user
     * @group tag
     * @group tag_admin
     * @group tag_method_delete
     *
     * @return void
     */
    public function testFailDeleteForConnectedUser()
    {
        // Try to delete a tag
        $this->setConnectedUserSession();
        $this->commonDelete();
        $objJson = json_decode($this->_response->body());
        $this->assertResponseCode(403);
        $this->assertEquals("You are not authorized to access that location.", $objJson->message);
        $this->assertEquals(403, $objJson->code);

        // Check that the tag is actually not deleted
        $this->commonCheckFailDelete();
    }



    /**
     * Test delete method of Tags controller, for tags id=4 without relationship with any Software.
     * Tag id=4 and his data must be not found as result.
     *
     * @group admin
     * @group tag
     * @group tag_admin
     * @group tag_method_delete
     *
     * @return void
     */
    public function testDeleteIfNoRelationshipBetweenSoftwareAndTag()
    {
        $this->setAdminSessionOfPersonType();
        $this->commonDelete();


        $this->get('/api/v1/tags.json');
        $expected = [
            "tags" =>
                [
                    [
                        "used_tag_number" => 3,
                        "id" => 1,
                        "name" => "myNewTagCreated",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "tag_string" => "myNewTagCreated myTagDeleted",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Description Lorem Ipsum",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                            [
                                "id" => 2,
                                "softwarename" => "Lutèce",
                                "url_repository" => "http://www.fake-repo-lutece.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-lutece.com",
                                "tag_string" => "myNewTagCreated myTagAdded1",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                        ],
                        "usedTagNumber" => 3
                    ],
                    [
                        "used_tag_number" => 2,
                        "id" => 2,
                        "name" => "myTagAdded1",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 2,
                                "softwarename" => "Lutèce",
                                "url_repository" => "http://www.fake-repo-lutece.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-lutece.com",
                                "tag_string" => "myNewTagCreated myTagAdded1",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Description Lorem Ipsum",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ]
                        ],
                        "usedTagNumber" => 2
                    ],
                    [
                        "used_tag_number" => 1,
                        "id" => 3,
                        "name" => "myTagAdded2",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Description Lorem Ipsum",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ]
                        ],
                        "usedTagNumber" => 1
                    ],
                    [
                        "used_tag_number" => 1,
                        "id" => 5,
                        "name" => "myTagDeleted",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "tag_string" => "myNewTagCreated myTagDeleted",
                                "sill" => null,
                                "cnll" => null,
                                "wikidata" => null,
                                "framalibre" => null,
                                "wikipedia_en" => null,
                                "wikipedia_fr" => null,
                                "average_review" => 0,
                            ]
                        ],
                        "usedTagNumber" => 1
                    ],
                ]
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test delete method of Tags controller, for tag id=5 linked to software id="1"
     * Tag id=5 and his data must be NOT found as result.
     *
     * @group admin
     * @group tag
     * @group tag_admin
     * @group tag_method_delete
     *
     * @return void
     */
    public function testDeleteTagLinkedToSoftware()
    {
        $this->setAdminSessionOfPersonType();
        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);

        $this->delete('/api/v1/tags/5');

        $this->get('/api/v1/softwares-tags.json');

        $this->assertNotContains('"tag_id": 5', $this->_response->body());
        $this->assertResponseOk();
    }

    /**
     * Test add method of Tags controller for connected user,
     * if a "really clever" connected user tries to create a Tag longer than 100 characters.
     * "Provided value is too long (max : 100)." is returned, everything is under controll.
     *
     *      URL: /api/v1/taxonomys/add
     *           /api/v1/taxonomys/add.json
     *           ----> allowed only for connected user
     *
     * @group user
     * @group tag
     * @group tag_user
     * @group tag_method_add
     *
     * @return void
     */
    public function testCreateTagTooLong()
    {
        $this->setConnectedUserSession();
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $tag_too_long = "AAAAAAAAAAAAAAaaaaaaaaaaaaaaaaaaaCCCCCCCCcccccccccccccccccccccccccccc";
        $tag_too_long .= "DDDDDDDDDDDDDDDDddddddddddddddCCCCCCCCCCCCCCCcccccccccccccccccccccccccc";
        $data = [
            'name' => $tag_too_long,
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["maxLength" => "Provided value is too long (max : 100)."]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test add method of Tags controller for connected user,
     * if a connected user tries to set any value different from "A~Z a~z 0~9 - _",
     * this error "Name must be an alphaNumeric value. You provided an invalid value." welcome him.
     *
     *      URL: /api/v1/taxonomys/add
     *           /api/v1/taxonomys/add.json
     *           ----> allowed only for connected user
     *
     * @group user
     * @group tag
     * @group tag_user
     * @group tag_method_add
     *
     * @return void
     */
    public function testCreateTagWithForbiddenValue()
    {
        $this->setConnectedUserSession();
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => 'ààààà#çççéééé',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" =>
                            [
                                "alphaNumeric" => "Name must be an alphaNumeric value. You provided an invalid value."
                            ]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add method of Tags controller for connected user,
     * if a connected user tries, God only knows why, to create a Tag without name.
     *
     *      URL: /api/v1/taxonomys/add
     *           /api/v1/taxonomys/add.json
     *           ----> allowed only for connected user
     *
     * @group user
     * @group tag
     * @group tag_user
     * @group tag_method_add
     *
     * @return void
     */
    public function testCreateTagWithoutValue()
    {
        $this->setConnectedUserSession();
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => '',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["_empty" => "This field cannot be left empty"]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test add method of Tags controller for connected user,
     * if someone try to create a new Tag, but the name is already registered.
     *
     *      URL: /api/v1/taxonomys/add
     *           /api/v1/taxonomys/add.json
     *           ----> allowed only for connected user
     *
     * @group user
     * @group tag
     * @group tag_user
     * @group tag_method_add
     *
     * @return void
     */
    public function testCreateTagWhichAlreadyExist()
    {
        $this->setConnectedUserSession();
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => 'myNewTagCreated',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["_isUnique" => "A tag with this name already exists in Comptoir du libre."]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }
}
