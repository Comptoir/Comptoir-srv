<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\UserTypesController;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\UserTypesController Test Case
 */
class UserTypesControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_types',
        'app.users'
    ];

    public function setUp()
    {

        parent::setUp();
        $config = TableRegistry::exists('UserTypes') ? [] : ['className' => 'App\Model\Table\UserTypesTable'];

        $this->UserTypes = TableRegistry::get('UserTypes', $config);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {

        $this->get('api/v1/UserTypes.json');
        $this->assertResponseOk();

        $expected = [
            'userTypes' => [
                [
                    'id' => 1,
                    'name' => 'Association',
                    'created' => "2017-02-14T15:02:50+00:00",
                    'modified' => "2017-02-14T15:02:50+00:00",
                    'cd' => 'Asso'
                ],
                [
                    'id' => 2,
                    'name' => 'Company',
                    'created' => "2017-02-14T15:02:50+00:00",
                    'modified' => "2017-02-14T15:02:50+00:00",
                    'cd' => 'Company'
                ],
                [
                    'id' => 3,
                    'name' => 'Administration',
                    'created' => "2017-02-14T15:02:50+00:00",
                    'modified' => "2017-02-14T15:02:50+00:00",
                    'cd' => 'Administration'
                ],
                [
                    'id' => 4,
                    'name' => 'Person',
                    'created' => "2017-02-14T15:02:50+00:00",
                    'modified' => "2017-02-14T15:02:50+00:00",
                    'cd' => 'Person'
                ],
            ]
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('api/v1/UserTypes/view/2.json');
        $this->assertResponseOk();

        $expected = [
            'userType' =>
                [
                    'id' => 2,
                    'name' => 'Company',
                    'created' => "2017-02-14T15:02:50+00:00",
                    'modified' => "2017-02-14T15:02:50+00:00",
                    'cd' => 'Company'
                ],
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test view method when licenceTypes does not exists
     *
     * @return void
     */
    public function testViewWhenDoesNotExists()
    {

        $this->get('api/v1/userTypes/view/333.json');
        $this->assertEquals($this->_exception->getCode(), 404);
    }


    /**
     * Test add method
     * Should return an 401 HTTP code because should connected and known as a Admin user
     * @return void
     */
    public function testAddWhenNotConnected()
    {

        $data = [
            'name' => 'Administration_agent',
            'cd' => 'AA'
        ];

        $this->post('api/v1/userTypes/add.json', $data);

        $this->assertRedirect("users/login");
    }

    /**
     * Test add method
     * Should return an 403 HTTP code because should connected and known as a Admin user
     * @return void
     */
    public function testAddWhenNotConnectedAsAdmin()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "username" => "Avencall Company",
                    "user_type_id" => 2,
                    "role" => "user",
                ]
            ]
        ]);
        $data = [
            'name' => 'Administration_agent',
            'cd' => 'AA'
        ];
        $this->post('api/v1/userTypes/add.json', $data);
    }


    /**
     * Test add method
     * Should return an 200 HTTP code
     * And a Success for adding a userType
     * @return void
     */
    public function testAddWhenConnectedAsAdmin()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "username" => "Admin User",
                    "user_type_id" => 1,
                    "role" => "admin",
                ]
            ]
        ]);

        $data = [
            'name' => 'Administration_agent',
            'cd' => 'AA'
        ];
        $this->post('api/v1/userTypes/add.json', $data);

        $this->assertResponseOk();

        $this->assertContains("Administration_agent", $this->_response->body());
    }

    /**
     * Test edit method
     * Should return an 200 HTTP code
     * And a Success for editing a userType
     * @return void
     */
    public function testEditConnectedAsAdmin()
    {

        $this->login("fake-email-admin@comptoir-du-libre.org", "good-password");
        $this->assertSession(1, 'Auth.User.id');
        // Définit des données de session
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "username" => "Admin User",
                    "user_type_id" => 1,
                    "role" => "admin",
                ]
            ]
        ]);
        $data = [
            'name' => 'Edit',
            'cd' => 'Edit'
        ];
        $this->post('api/v1/userTypes/edit/2.json', $data);

        $this->assertResponseOk();

        $this->assertContains("Edit", $this->_response->body());
    }

    /**
     * Test edit method
     * Should return an 401 HTTP code because should connected and known as a Admin user
     * @return void
     */
    public function testEditNotConnected()
    {

        $data = [
            'name' => 'Edit',
            'cd' => 'Edit'
        ];

        $this->post('api/v1/userTypes/edit/2.json', $data);

        $this->assertRedirect("users/login");
    }

    /**
     * Test edit method
     * Should return an 403 HTTP code because should connected and known as a Admin user
     * @return void
     */
    public function testEditWhenNotConnectedAsAdmin()
    {


        $data = [
            'name' => 'Administration_agent',
            'cd' => 'AA'
        ];
        $this->post('api/v1/userTypes/edit.json', $data);

        $this->assertRedirect("users/login");
    }

    /**
     * Test delete method
     * Should return an 200 HTTP code
     * And a Success for editing a userType
     * @return void
     */
    public function testDeleteConnectedAsAdmin()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "username" => "Admin User",
                    "user_type_id" => 1,
                    "role" => "admin",
                ]
            ]
        ]);

        $data = [
            'name' => 'NewOne',
            'cd' => 'NewOne'
        ];
        $this->post('api/v1/userTypes/add.json', $data);
        $this->UserTypes->exists(["id" => 5, "name" => "NewOne"]);

        $this->assertResponseOk();

        $this->delete('api/v1/userTypes/delete/5.json');

        $this->assertFalse($this->UserTypes->exists(["id" => 5, "name" => "NewOne"]));
    }

    /**
     * Test delete method
     * Should return an 401 HTTP code because should connected and known as a Admin user
     * @return void
     */
    public function testDeleteNotConnected()
    {

        $this->delete('api/v1/userTypes/delete/1.json');

        $this->assertRedirect("users/login");
    }

    /**
     * Test delete method
     * Should return an 403 HTTP code because should connected and known as a Admin user
     * @return void
     */
    public function testDeleteWhenNotConnectedAsAdmin()
    {


        $this->delete('api/v1/userTypes/delete.json');

        $this->assertRedirect("users/login");
    }
}
