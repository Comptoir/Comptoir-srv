<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SoftwaresTable;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SoftwaresTable Test Case
 */
class SoftwaresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var SoftwaresTable
     */
    public $Softwares;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.raw_metrics_softwares',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
        'app.reviews',
        'app.screenshots',
        'app.softwares_statistics'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Softwares') ? [] : ['className' => 'App\Model\Table\SoftwaresTable'];
        $this->Softwares = TableRegistry::get('Softwares', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Softwares);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }


    public function testFindScreenCaptured()
    {
        $softwaresScreenCapturedQuery = $this->Softwares->find("screenCaptured");
        $this->assertInstanceOf('Cake\ORM\Query', $softwaresScreenCapturedQuery);
        $softwaresScreenCaptured = $softwaresScreenCapturedQuery->hydrate(false)->toArray();

        $expected = [
            [
                'id' => 1,
                'softwarename' => 'Asalae',
                'url_repository' => 'http://www.fake-repo-asalae.git',
                'description' => 'Asalae description',
                'licence_id' => 1,
                'created' => Time::createFromTimestamp(1487084568),
                'modified' => Time::createFromTimestamp(1487084568),
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-asalae.com',
                'web_site_url' => null
            ],
            [
                'id' => 2,
                'softwarename' => 'Lutèce',
                'url_repository' => 'http://www.fake-repo-lutece.git',
                'description' => 'Lutèce description',
                'licence_id' => 1,
                'created' => Time::createFromTimestamp(1487084568),
                'modified' => Time::createFromTimestamp(1487084568),
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-lutece.com',
                'web_site_url' => null
            ],
        ];

        $this->assertEquals($expected, $softwaresScreenCaptured);
    }


    public function testFindReviewed()
    {
        $softwaresReviewedQuery = $this->Softwares->find("reviewed");
        $this->assertInstanceOf('Cake\ORM\Query', $softwaresReviewedQuery);
        $softwaresReviewed = $softwaresReviewedQuery->hydrate(false)->toArray();

        $expected = [
            [
                'id' => 1,
                'softwarename' => 'Asalae',
                'url_repository' => 'http://www.fake-repo-asalae.git',
                'description' => 'Asalae description',
                'licence_id' => 1,
                'created' => Time::createFromTimestamp(1487084568),
                'modified' => Time::createFromTimestamp(1487084568),
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-asalae.com',
                'web_site_url' => null
            ],
            [
                'id' => 2,
                'softwarename' => 'Lutèce',
                'url_repository' => 'http://www.fake-repo-lutece.git',
                'description' => 'Lutèce description',
                'licence_id' => 1,
                'created' => Time::createFromTimestamp(1487084568),
                'modified' => Time::createFromTimestamp(1487084568),
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-lutece.com',
                'web_site_url' => null
            ],
        ];
        $this->assertEquals($expected, $softwaresReviewed);
    }


    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
