<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelationshipsUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelationshipsUsersTable Test Case
 */
class RelationshipsUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var RelationshipsUsersTable
     */
    public $RelationshipsUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships_users',
        'app.users',
        'app.user_types',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares_users',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships_softwares'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RelationshipsUsers')
            ? []
            : ['className' => 'App\Model\Table\RelationshipsUsersTable'];
        $this->RelationshipsUsers = TableRegistry::get('RelationshipsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelationshipsUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
