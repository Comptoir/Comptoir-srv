<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelationshipsSoftwaresUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelationshipsSoftwaresUsersTable Test Case
 */
class RelationshipsSoftwaresUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var RelationshipsSoftwaresUsersTable
     */
    public $RelationshipsSoftwaresUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships_softwares_users',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.users',
        'app.user_types',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RelationshipsSoftwaresUsers')
            ? []
            : ['className' => 'App\Model\Table\RelationshipsSoftwaresUsersTable'];
        $this->RelationshipsSoftwaresUsers = TableRegistry::get('RelationshipsSoftwaresUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelationshipsSoftwaresUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
