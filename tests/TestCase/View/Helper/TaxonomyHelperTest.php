<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\TaxonomyHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\TaxonomyHelper Test Case
 */
class TaxonomyHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\TaxonomyHelper
     */
    public $Taxonomy;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Taxonomy = new TaxonomyHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Taxonomy);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
