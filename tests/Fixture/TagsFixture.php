<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TagsFixture
 *
 */
class TagsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => 100, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    public function init()
    {
        $this->records = [
            [
                'name' => "myNewTagCreated",
                'created' => null,
                'modified' => null,
            ],
            [
                'name' => "myTagAdded1",
                'created' => null,
                'modified' => null,
            ],
            [
                'name' => "myTagAdded2",
                'created' => null,
                'modified' => null,
            ],
            [
                'name' => "myTagAdded3",
                'created' => null,
                'modified' => null,
            ],
            [
                'name' => "myTagDeleted",
                'created' => null,
                'modified' => null,
            ],
        ];
        parent::init();
    }
}
