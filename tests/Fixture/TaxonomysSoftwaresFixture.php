<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TaxonomysSoftwaresFixture
 *
 */
class TaxonomysSoftwaresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'taxonomy_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'software_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'comment' => [
            'type' => 'text',
            'length' => null,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'taxonomys_softwares_software_id' => ['type' => 'foreign', 'columns' => ['software_id'], 'references' => ['softwares', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'taxonomys_softwares_taxonomy_id' => ['type' => 'foreign', 'columns' => ['taxonomy_id'], 'references' => ['taxonomys', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'taxonomys_softwares_user_id' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//       'id' => 1,
            'taxonomy_id' => 3,
            'software_id' => 1,
            'user_id' => null,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 2,
            'taxonomy_id' => 3,
            'software_id' => 1,
            'user_id' => 5,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 3,
            'taxonomy_id' => 3,
            'software_id' => 1,
            'user_id' => 10,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 4,
            'taxonomy_id' => 5,
            'software_id' => 2,
            'user_id' => 4,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 5,
            'taxonomy_id' => 5,
            'software_id' => 2,
            'user_id' => 1,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 6,
            'taxonomy_id' => 5,
            'software_id' => 3,
            'user_id' => 1,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 7,
            'taxonomy_id' => 5,
            'software_id' => 2,
            'user_id' => 3,
            'created' => 1581686289,
            'modified' => 1581686289,
            'comment' => 'blabla bla du comentaire'
        ],
        [
//       'id' => 8,
            'taxonomy_id' => 1,
            'software_id' => 4,
            'user_id' => 3,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 8,
            'taxonomy_id' => 5,
            'software_id' => 4,
            'user_id' => 3,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 9,
            'taxonomy_id' => 6,
            'software_id' => 4,
            'user_id' => null,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 10,
        'taxonomy_id' => 3,
        'software_id' => 6,
        'user_id' => null,
        'created' => 1581686289,
        'modified' => 1581686289
        ],
        [
//       'id' => 11,
            'taxonomy_id' => 3,
            'software_id' => 7,
            'user_id' => null,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
        [
//       'id' => 12,
            'taxonomy_id' => 5,
            'software_id' => 7,
            'user_id' => null,
            'created' => 1581686289,
            'modified' => 1581686289
        ],
    ];
}
