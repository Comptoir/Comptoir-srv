<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LicensesFixture
 *
 */
class LicensesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => [
            'type' => 'biginteger',
            'length' => 20,
            'autoIncrement' => true,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null
        ],
        'name' => [
            'type' => 'text',
            'length' => null,
            'default' => null,
            'null' => false,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        'license_type_id' => [
            'type' => 'biginteger',
            'length' => 20,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'created' => [
            'type' => 'timestamp',
            'length' => null,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null
        ],
        'modified' => [
            'type' => 'timestamp',
            'length' => null,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null
        ],
        '_indexes' => [
            'idx_26477_fk_license_licence_type_idx' => [
                'type' => 'index',
                'columns' => ['license_type_id'],
                'length' => []
            ],
        ],
        '_constraints' => [
            'primary' => [
                'type' => 'primary',
                'columns' => ['id'],
                'length' => []
            ],
            'fk_license_licence_type' => [
                'type' => 'foreign',
                'columns' => ['license_type_id'],
                'references' => ['licence_types', 'id'],
                'update' => 'noAction',
                'delete' => 'noAction',
                'length' => []
            ],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//            'id' => 1,
            'name' => 'CeCill V2',
            'license_type_id' => 1,
            'created' => 1487084563,
            'modified' => 1487084563
        ],
        [
//            'id' => 2,
            'name' => 'MIT',
            'license_type_id' => 1,
            'created' => 1487084563,
            'modified' => 1487084563
        ],
        [
//            'id' => 3,
            'name' => 'BSD',
            'license_type_id' => 1,
            'created' => 1487084563,
            'modified' => 1487084563
        ],
        [
//            'id' => 4,
            'name' => 'LGPLv3',
            'license_type_id' => 1,
            'created' => 1487084563,
            'modified' => 1487084563
        ],
        [
//            'id' => 5,
            'name' => 'GNU GPLv2',
            'license_type_id' => 1,
            'created' => 1487084563,
            'modified' => 1487084563
        ],
    ];
}
