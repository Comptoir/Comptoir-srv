<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RelationshipsSoftwaresFixture
 *
 */
class RelationshipsSoftwaresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => [
            'type' => 'biginteger',
            'length' => 20,
            'autoIncrement' => true,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null
        ],
        'software_id' => [
            'type' => 'integer',
            'length' => 20,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'relationship_id' => [
            'type' => 'biginteger',
            'length' => 20,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'recipient_id' => [
            'type' => 'integer',
            'length' => 20,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'created' => [
            'type' => 'timestamp',
            'length' => null,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null
        ],
        'modified' => [
            'type' => 'timestamp',
            'length' => null,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null
        ],
        '_indexes' => [
            'idx_26495_fk_software_relationship_software_from' => [
                'type' => 'index',
                'columns' => ['software_id'],
                'length' => []
            ],
            'idx_26495_fk_software_relationship_relation_type_idx' => [
                'type' => 'index',
                'columns' => ['relationship_id'],
                'length' => []
            ],
            'idx_26495_fk_software_relationship_to_idx' => ['type' => 'index', 'columns' => ['recipient_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => [
                'type' => 'primary',
                'columns' => ['id'],
                'length' => []
            ],
            'fk_software_relationship_relation_type' => [
                'type' => 'foreign',
                'columns' => ['relationship_id'],
                'references' => ['relationships', 'id'],
                'update' => 'noAction',
                'delete' => 'noAction',
                'length' => []
            ],
            'fk_software_relationship_software_from' => [
                'type' => 'foreign',
                'columns' => ['software_id'],
                'references' => ['softwares', 'id'],
                'update' => 'cascade',
                'delete' => 'cascade',
                'length' => []
            ],
            'fk_software_relationship_to' => [
                'type' => 'foreign',
                'columns' => ['recipient_id'],
                'references' => ['softwares', 'id'],
                'update' => 'cascade',
                'delete' => 'cascade',
                'length' => []
            ],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'software_id' => 1,
            'relationship_id' => 1,
            'recipient_id' => 2,
            'created' => 1460130266,
            'modified' => 1460130266
        ],
        [
            'software_id' => 1,
            'relationship_id' => 2,
            'recipient_id' => 2,
            'created' => 1460130266,
            'modified' => 1460130266
        ],

    ];
}
