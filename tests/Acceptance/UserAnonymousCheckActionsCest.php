<?php
/**
 * This file is to test actions that an anonymous user can do:
 * - can't declare as user of a soft,
 *
 * @package App\Test\Acceptance
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Inherited Methods from App\src\TestSuite.Codeception\AcceptanceTester
 *
 * @package App\Test\Acceptance
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class UserAnonymousCheckActionsCest
{
    private $lang = 'en';

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $lang = $this->lang;
        $I->amOnPage("/$lang/");
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

    /**
     * Function to test that a anonymous user cannot declaring himself as user of the ATOM software
     *
     * @group user_declareUserOfSoftware
     * @group anonymous
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function cantDeclareAsUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click("//a[@href=\"/$lang/softwares/163\"]");              // software Atom
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSeeElement('//*[@id="btnDisabled_Softwares-usersSoftware-163"]');
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-163"]');           // button : 'Se déclarer utilisateur'
        $I->seeInCurrentUrl("/$lang/users/login");
    }


    /**
     * When trying to update JSON export of all software
     * verify that we can't see link to export (only user with "admin" role can see it)
     *
     * @group admin
     * @group  export
     * @group  software
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayExportJsonOfAllSoftware(AcceptanceTester $I)
    {
        $I->amOnPage('/softwares/export'); // Generate export
        $I->dontSeeElement("//h1[@id='export-json_displayed']");
        $I->dontSeeElement("//a[@id='link_export-json_displayed']");
        $I->dontSeeElement("//h1[@id='error_export-json_displayed']");
        $I->seeElement("//div[@id='export-json_not-displayed']");
        $I->dontSeeElement("//div[@id='error_export-json_not-displayed']");
    }

    /**
     * When trying to update export of all users
     * verify that we can't export (only user with "admin" role can do it)
     *
     * @group admin
     * @group  export
     * @group  user
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayExportOfAllUsers(AcceptanceTester $I)
    {
        $lang = 'fr';
        $I->amOnPage("/users/export"); // Generate export
        $I->seeInCurrentUrl("/$lang/users/login");
        $I->seeInTitle('Se connecter');

        $I->dontSeeElement("//h1[@id='users-exportusers-export_displayed']");
        $I->dontSeeElement("//a[@id='link_users-export_displayed']");
        $I->dontSeeElement("//h1[@id='error_users-export_displayed']");
        $I->dontSeeElement("//div[@id='error_users-export_not-displayed']");
        $I->dontSeeElement("//div[@id='users-export_not-displayed']");
    }


    /**
     * When I display a software,
     * I don't see the external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displaySoftwareNotSeeExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->dontSeeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");

        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Consulter la fiche SILL');
        $I->dontSee('Consulter la fiche CNLL');
        $I->dontSee('Consulter la fiche wikidata.org');
        $I->dontSee('Consulter la fiche FramaLibre');
        $I->dontSee('Consulter la page Wikipédia en français');
        $I->dontSee('Consulter la page Wikipédia en anglais');
        $I->dontSee('Q698');
    }
}
