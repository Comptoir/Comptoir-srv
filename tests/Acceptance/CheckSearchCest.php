<?php

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Class CheckSearchCest
 *
 * @todo generate files for each search with the expected software and users (for the current dataset)
 * @todo load from a file the software and users expected for a search
 * @todo check the number of expected users/software by comparing it with the file generated for current dataset
 * @todo check the order of expected users/software by comparing it with the file generated for current dataset
 *
 *
 * @package App\Test\Acceptance
 */
class CheckSearchCest
{
    const SOFTWARE_CSS_ID_PREFIX = 'softwarePreviewCard';
    const USER_CSS_ID_PREFIX = 'userPreviewCard';

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }


    /**
     * Check search with HTML injection (prevent XSS bug)
     *
     * @group search
     * @group security
     * @param AcceptanceTester $I
     */
    public function checkSearchWithHtmlInjection(AcceptanceTester $I)
    {
        $baseSearch = "InjectionHTML_exemple";
        $HtmlInjection = '<hr><taginjection /> <div id="test_injectionHTML"></div><hr>';
        $search = $baseSearch. $HtmlInjection;
        $searchInUrl = str_replace('%2B', '+', urlencode($search)); // replace "+" encoded in UTF-8 by "+"
        $I->submitForm('//*[@id="navbar-search"]', ['search' => $search], 'submit');
        $I->seeInCurrentUrl("pages/search?search=$searchInUrl");
        $I->see('Aucun résultat pour');
        $I->dontSeeElement("//taginjection");
        $I->dontseeElement("//div[@id='test_injectionHTML']");
    }


    /**
     * Check search with word "zzzujdrhouigrfe"
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordZzzujdrhouigrfe(AcceptanceTester $I)
    {
        $I->submitForm('//*[@id="navbar-search"]', ['search' => 'Zzzujdrhouigrfe'], 'submit');
        $I->seeInCurrentUrl('pages/search?search=Zzzujdrhouigrfe');
        $I->See('Aucun résultat pour ');
        $I->seeElement('//h2[1]');
        $I->see('Aucun logiciel pour');
        $I->seeElement('//h2[2]');
        $I->see('Aucun utilisateur pour');
    }


    /**
     * Check search with two word
     * with excess space characters:
     *      - double spaces
     *      - non-breaking space
     *      - carriage return ('\n', \r')
     *      - tabulation ('\t')
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithTwoWordWithSpacesCharacters(AcceptanceTester $I)
    {
        $Software = [
            66 => "Dia Diagram Editor"
        ];
        $Users = [];
        $Tags = [];

        $nbsp = html_entity_decode('&nbsp;', null, 'UTF-8');
        $this->sharedChecksForSearchResult($I, "Diagram Editor", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, "Diagram$nbsp"."Editor$nbsp", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, "Diagram Editor ", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, " Diagram Editor ", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, "Diagram  Editor", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, " Diagram                  Editor  ", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, "\tDiagram\tEditor\t", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, "\nDiagram\nEditor\n", $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, "\rDiagram\rEditor\r", $Software, $Users, $Tags);
    }

    /**
     * Check search with word "sill" without any sort by
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordSillWithoutAnySortBy(AcceptanceTester $I)
    {
        $Software = [];
        $Users = [];
        $Tags = [
            29 => 'SILL-2017',
            27 => 'SILL-2018',
        ];
        $this->sharedChecksForSearchResult($I, 'sill', $Software, $Users, $Tags);
    }

    /**
     * Check search with word "sil" without any sort by
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordSilWithoutAnySortBy(AcceptanceTester $I)
    {
        $Software = [
            56 => 'Greenshot',
        ];
        $Users = [
            155 => 'GIP SILPC',
        ];
        $Tags = [
            29 => 'SILL-2017',
            27 => 'SILL-2018',
        ];
        $this->sharedChecksForSearchResult($I, 'sil', $Software, $Users, $Tags);
    }


    /**
     * Check search with word "mail" without any sort by
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordMailWithoutAnySortBy(AcceptanceTester $I)
    {
        $Software = [
            46 => 'FusionDirectory',
            28 => 'LinShare',
            50 => 'KeePass',
        ];
        $Users = [];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'mail', $Software, $Users, $Tags);
    }

    /**
     * Shared checks for searchResult
     *
     * @param AcceptanceTester $I
     * @param string $search
     * @param array $expectedSoftware
     * @param array $expectedUsers
     */
    private function sharedChecksForSearchResult(
        AcceptanceTester $I,
        string $search,
        array $expectedSoftware = [],
        array $expectedUsers = [],
        array $expectedTags = []
    ) {

        $searchInUrl = str_replace('%2B', '+', urlencode($search)); // replace "+" encoded in UTF-8 by "+"
        $I->submitForm('//*[@id="navbar-search"]', ['search' => $search], 'submit');
        $I->seeInCurrentUrl("pages/search?search=$searchInUrl");
        $I->dontSee('Aucun résultat pour');

        $pre = self::SOFTWARE_CSS_ID_PREFIX;
        foreach ($expectedSoftware as $id => $name) {
            $softwareCard = "//div[@id='$pre-$id']";
            $softwareLink = "//div[@id='$pre-$id']/a[@href='/fr/softwares/$id']";
            $I->see($name);
            $I->seeElement($softwareCard);
            $I->seeElement($softwareLink);
        }
        $pre = self::USER_CSS_ID_PREFIX;
        foreach ($expectedUsers as $id => $name) {
            $userCard = "//div[@id='$pre-$id']";
            $userLink = "//div[@id='$pre-$id']/a[@href='/fr/users/$id']";
            $I->see($name);
            $I->seeElement($userCard);
            $I->seeElement($userLink);
        }
        foreach ($expectedTags as $id => $tag) {
            $tagLink = "//a[@href='/fr/tags/$id/software']/div[@class='tagUnit']";
            $I->see($tag);
            $I->seeElement($tagLink);
        }
    }

    /**
     * Check search with two words "logiciel libre" (or "LOgiciel LIBre")  in upper or lower case
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithTwoWordsLogicielLibreInUpperOrLowerCase(AcceptanceTester $I)
    {
        $Software = [
            212 => 'Apache',
            9 => 'Asqatasun',
            218 => 'Docker',
            38 => 'WordPress',
        ];
        $Users = [
            57 => '2i2L',
            225 => 'Entrouvert',
        ];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'logiciel libre', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'LOgiciel LIBre', $Software, $Users, $Tags);
    }

    /**
     * Check search with two words "open studio" (or "OPen sTUDIo")  in upper or lower case
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithTwoWordsOpenStudioInUpperOrLowerCase(AcceptanceTester $I)
    {
        $Software = [196 => 'Talend Open Studio',];
        $Users = [];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'open studio', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'OPen sTUDIo', $Software, $Users, $Tags);
    }

    /**
     * Check search with three words "visual studio code" (or "Visual Studio CODE")  in upper or lower case
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithThreeWordsVisualStudioCodeInUpperOrLowerCase(AcceptanceTester $I)
    {
        $Software = [171 => 'Visual Studio Code',];
        $Users = [];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'visual studio code', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'Visual Studio CODE', $Software, $Users, $Tags);
    }


    /**
     * Check search with word "Départements"  (or "dÉpartements")
     * with accented characters in upper or lower case
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordDepartementWithAccentedCharactersInUpperOrLowerCase(AcceptanceTester $I)
    {
        $Software = [115 => 'Départements & Notaires',];
        $Users = [
            224 => 'AFI',
            30 => 'COGITIS',
            158 => 'Conseil Départemental du Rhône',
        ];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'Département', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'DÉpartement', $Software, $Users, $Tags);
    }


    /**
     * Check search with word "électronique" (or "Électronique")
     * with accented characters in upper or lower case
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordElectroniqueWithAccentedCharactersInUpperOrLowerCase(AcceptanceTester $I)
    {
        $Software = [
            3 => 'As@lae',
            215 => 'Cyrus',
            86 => 'Nuxeo',
        ];
        $Users = [
            85 => 'Maarch',
            152 => 'BlueXML',
        ];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'électronique', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'Électronique', $Software, $Users, $Tags);
    }


    /**
     * Check search with word "rhône" (or "RHÔNE")
     * with accented characters in upper or lower case
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordRhoneWithAccentedCharactersInUpperOrLowerCase(AcceptanceTester $I)
    {
        $Software = [115 => 'Départements & Notaires',];
        $Users = [
            164 => 'Association Ploss-ra',
            158 => 'Conseil Départemental du Rhône',
        ];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'rhône', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'rhÔne', $Software, $Users, $Tags);
    }

    /**
     * Check search with word "S²LOW" (superscript digits)
     *
     * @group search
     * @param AcceptanceTester $I
     */
    public function checkSearchWithOneWordS2lowWithSuperscriptDigits(AcceptanceTester $I)
    {
        $Software = [5 => 'S²LOW',];
        $Users = [];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'S²LOW', $Software, $Users, $Tags);
    }

    /**
     * Check search with two words "libre" (or "LiBRe") in upper or lower case
     *
     * @param AcceptanceTester $I
     * @param string $search
     */
    private function checkSearchWithOneWordLibreInUpperOrLowerCase(AcceptanceTester $I, string $search)
    {
        $Software = [
            47 => 'Dolibarr ERP/CRM',
            15 => 'i-delibRE',
            78 => 'Ooo.HG',
        ];
        $Users = [
            57 => '2i2L',
            68 => 'Open-DSI',
            66 => 'Teclib',
        ];
        $Tags = [];
        $this->sharedChecksForSearchResult($I, 'libre', $Software, $Users, $Tags);
        $this->sharedChecksForSearchResult($I, 'LiBRe', $Software, $Users, $Tags);
    }




// Note: MFaure 2019-07-15
// All following tests are commented out as we prefer having a working subset of F-tests
// than a larger non working set. Those F-tests will be re-factored, hopefully in a near future
// Nevertheless, we don't want to loose this code, hence we keep it commented.
// If not refactored by 2020-09-01, the commented code can be deleted
    /*


        public function checkSearchWithOneWordLibreWithFilterReview(AcceptanceTester $I)
        {
            $SoftwareWithWordLibre1st = '//ol/li[1]/div/a[@href="/fr/softwares/4"]'; // i parapheur
            $SoftwareWithWordLibre4th = '//ol/li[4]/div/a[@href="/fr/softwares/26"]'; // Publik
            $UserWithWordLibre1st = '//ol/li[1]/div/a[@href="/fr/users/57"]'; // 2i2L
            $UserWithWordLibre8th = '//ol/li[8]/div/a[@href="/fr/users/68"]'; // Opendsi
            $UserWithWordLibre12th = '//ol/li[12]/div/a[@href="/fr/users/66"]'; // teclib

            $I->amOnPage('pages/search?search=LiBRe');
            $I->selectOption('//form/div[2]/select', 'true');
            $I->click('Filtrer');
            $I->dontSee('Aucun résultat pour');
            $I->seeElement($SoftwareWithWordLibre1st);
            $I->seeElement($SoftwareWithWordLibre4th);
            $I->seeElement($UserWithWordLibre1st);
            $I->seeElement($UserWithWordLibre8th);
            $I->seeElement($UserWithWordLibre12th);
        }
    */
}
