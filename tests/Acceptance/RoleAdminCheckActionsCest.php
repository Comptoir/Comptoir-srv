<?php
/**
 * Test actions that a person account,
 * with "admin" role in database, can do:
 * - Add and edit a software,
 * - declare as a user of a software and cancel it,
 * - edit its own user account,
 * - add a review to a software,
 * - by login a user, testing an action and logout.
 *
 * The account used is dev-admin_person from the Dataset02.
 *
 * @package App\Test\Acceptance
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Inherited Methods from App\src\TestSuite.Codeception\AcceptanceTester
 *
 * @method  void loginMe
 * @method  void logoutMe
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class RoleAdminCheckActionsCest
{
    private $lang = 'en';

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/');
        $I->loginMe('dev-admin_person@comptoir-du-libre.org', 'comptoir', 'dev-admin_person');
        $I->seeElement('div.message.success');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->logoutMe('dev-admin_person');
        $I->canSeeElement('#signinform');
    }

    /**
     * Function to test the user dev-admin_person declaring himself as user of the ATOM software
     *
     * @group admin
     * @group security
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function declareAsUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('//*[@id="softwaresPage"]');                      // button 'Logiciels'
        $I->click("//a[@href=\"/$lang/softwares/163\"]");            // software Atom
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSee('dev-admin_person', ['css' => 'ol li']);            // dev-admin_person declared on the user list
        $I->click('//*[@id="btn_Softwares-usersSoftware-163"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeElement('button.btn.btn-default.removeOne');
        $I->see('dev-admin_person', ['css' => 'ol li']);                // dev-admin_person on the list of users
    }

    /**
     * Function to test if dev-admin_person is not declared as a user of Atom software anymore
     *
     * @group admin
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function removeUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('a[id="usersPage"]');                             // button Users
        $I->canSeeInCurrentUrl("/$lang/users");
        $I->click("a[href=\"/$lang/users/91\"]");                          // User dev-admin_person
        $I->seeInCurrentUrl("/$lang/users/91");
        $I->seeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]"); // Atom software declared
        $I->click("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');  // button : 'Ne plus être utilisateur'
        $I->dontSeeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
    }

    /**
     * Check a correct declaration of userOf by the user dev-admin_person
     *
     * @group admin
     * @param AcceptanceTester $I codeception constant
     *
     * @return void
     */
    public function declareUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/91');                                    // dev-admin_person's page
        $I->dontSeeElement('7-zip');
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->click('//*[@id="btn_Softwares-usersSoftware-72"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->seeInCurrentUrl('softwares/usersSoftware/72');
        $I->see('dev-admin_person', ['css' => 'ol li']);                        // dev-admin_person on the list of users
    }

    /**
     * Remove a declared user dev-admin_person from the userList of a software 7-zip with more than 3 users.
     *
     * @group admin
     * @param AcceptanceTester $I codeception constant
     *
     * @return void
     */
    public function removeUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/91');                                    // dev-admin_person's page
        $I->see('7-zip');                                               // declared as userOf 7-zip
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');  // button : 'Ne plus être utilisateur'
        $I->seeElement('div.message.success');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');  // button : see all users of 7-Zip
        $I->dontsee('dev-admin_person', ['css' => 'ol li']);    // dev-admin_person is not in the list of users
    }

    /**
     * Edit a connected user account to change the password 'comptoir' by a new password 'comptoir'.
     * A new password can be the same as an older one.
     * We check that the user can reconnect himself with the new password
     *
     * @group userAccount
     * @group userAccount_changePassword
     * @group admin
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function changePasswordOfConnectedUser(AcceptanceTester $I)
    {
        $newPassword = 'comptoir';
        $userId = 91;
        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/$userId\"]");             // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->click("a[href=\"/api/v1/users/change-password/$userId?language=$lang\"]");       // Change password link
        $I->canSeeInCurrentUrl("users/change-password/$userId?language=$lang");
        $I->submitForm(
            '#editAccountPasswordForm',
            [
                'old_password' => 'comptoir',
                'new_password' => $newPassword,
                'confirm_password' => $newPassword,
            ]
        );
        $I->seeInCurrentUrl("/users/edit/$userId");
        $I->seeElement('div.message.success');
        $I->logoutMe('dev-admin_person');
        $I->loginMe('dev-admin_person@comptoir-du-libre.org', $newPassword, 'dev-admin_person');
        $I->seeElement('div.message.success');
    }

    /**
     * Edit a connected user account to modify information such as name, url, description, email and avatar.
     * Here we add an url and a word as description.
     *
     * @group userAccount
     * @group userAccount_edit
     * @group admin
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function editUserAccount(AcceptanceTester $I)
    {
        $userId = 91;
        $lang = $this->lang;

        $I->click("a[href=\"/$lang/users/edit/$userId\"]"); // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->submitForm(
            '#editInformationAccountForm',
            [
                'url' => 'http://example.com:8080/users/91',
                'description' => 'person',
            ]
        );
        $I->canSeeInCurrentUrl("/users/$userId");
        $I->seeElement('div.message.success');
    }

    /**
     * A user with a "admin" role cannot modify it.
     *
     * Edit a connected user account to try to change its role (the "admin" role
     * should become "newRole") by adding a new parameter in the POST request.
     *
     * After, display edit form for another user account
     * allowed only for user with "admin" role.
     *
     * @group admin
     * @group  security
     * @group userAccount
     * @group userAccount_edit
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function failToEditUserAccountForUpdatingRole(AcceptanceTester $I)
    {
        // current user with role = "admin"
        $userId = 91;
        $lang = $this->lang;

        $I->click("a[href=\"/$lang/users/edit/$userId\"]"); // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->submitForm(
            '#editInformationAccountForm',
            [
                'url' => 'http://example.com:8080/users/91',
                'description' => 'person',
                'role'        => 'newRole'
            ]
        );
        $I->canSeeInCurrentUrl("/users/$userId");
        $I->seeElement('div.message.success');

        // Display edit form for another user account
        // allowed only for user with "admin" role
        $I->amOnPage('/users/edit/3'); // "Adullact" acount (id:3) ----> not the current user
        $I->seeElement("//form[@id='editInformationAccountForm']");
    }

    /**
     * When trying to modify another user's data (by changing the id in the url),
     * verify that we can even see the form (because we have "admin" role)
     *
     * @group admin
     * @group  security
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayFormToEditAnotherUserAccount(AcceptanceTester $I)
    {
        $I->amOnPage('/users/edit/3'); // "Adullact" acount (id:3) ----> not the current user
        $I->dontSeeInTitle('Error-400');
        $I->dontSeeElement('div.message.error');
        $I->dontSee('You are not allowed to do that');
        $I->seeElement("//form[@id='editInformationAccountForm']");
        $I->seeElement("//input[@id='username']");
        $I->seeElement("//input[@id='url']");
        $I->seeElement("//input[@id='email']");
        $I->seeElement("//input[@id='photo']");
        $I->seeElement("//textarea[@id='description']");
    }


    /**
     * When trying to update JSON export of all software
     * verify that we can see link to export (because we have "admin" role)
     *
     * @group admin
     * @group  security
     * @group  export
     * @group  software
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayExportJsonOfAllSoftware(AcceptanceTester $I)
    {
        $I->amOnPage('/softwares/export'); // Generate export
        $I->seeElement("//h1[@id='export-json_displayed']");
        $I->seeElement("//a[@id='link_export-json_displayed']");
        $I->dontSeeElement("//h1[@id='error_export-json_displayed']");
        $I->dontSeeElement("//div[@id='export-json_not-displayed']");
        $I->dontSeeElement("//div[@id='error_export-json_not-displayed']");
    }

    /**
     * When trying to update export of all users
     * verify that we can export data (because we have "admin" role)
     *
     * @group admin
     * @group  security
     * @group  export
     * @group  user
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayExportOfAllUsers(AcceptanceTester $I)
    {
        $I->amOnPage('/users/export'); // Generate export
        $I->seeElement("//h1[@id='users-export_displayed']");
        $I->seeElement("//a[@id='link_users-export_displayed']");
        $I->dontSeeElement("//h1[@id='error_users-export_displayed']");
        $I->dontSeeElement("//div[@id='users-export_not-displayed']");
        $I->dontSeeElement("//div[@id='error_users-export_not-displayed']");
    }


    /**
     * When I display a software,
     * I see external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displaySoftwareSeeExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';

        // Firefox software
        $sofwareId = 82;
        $I->amOnPage("/$lang/softwares/$sofwareId");
        $this->commonDisplayFirefoxSoftwareSeeExternalRessources($I);

        // Asqatasun software
        $sofwareId = 9;
        $I->amOnPage("/$lang/softwares/$sofwareId");
        $I->seeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->seeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->seeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");
        $I->see('Ressources externes visible uniquement par les admins');
        $I->see('Consulter la fiche SILL');
        $I->see('Consulter la fiche wikidata.org');
        $I->dontSeeElement('Consulter la fiche CNLL');
        $I->dontSeeElement('Consulter la fiche FramaLibre');
        $I->dontSeeElement('Consulter la page Wikipédia en français');
        $I->dontSeeElement('Consulter la page Wikipédia en anglais');
        $I->see('Q24026504');

        // LibreOffice software
        $sofwareId = 33;
        $I->amOnPage("/$lang/softwares/$sofwareId");
        $I->seeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->dontSeeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->seeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");
        $I->see('Ressources externes visible uniquement par les admins');
        $I->dontSee('Consulter la fiche SILL');
        $I->see('Consulter la fiche wikidata.org');
        $I->dontSeeElement('Consulter la fiche CNLL');
        $I->dontSeeElement('Consulter la fiche FramaLibre');
        $I->dontSeeElement('Consulter la page Wikipédia en français');
        $I->dontSeeElement('Consulter la page Wikipédia en anglais');
        $I->see('Q10135');

        //  7-zip  software
        $sofwareId = 72;
        $I->amOnPage("/$lang/softwares/$sofwareId");
        $I->seeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->seeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");
        $I->see('Ressources externes visible uniquement par les admins');
        $I->see('Consulter la fiche SILL');
        $I->dontSee('Consulter la fiche wikidata.org');
        $I->dontSeeElement('Consulter la fiche CNLL');
        $I->dontSeeElement('Consulter la fiche FramaLibre');
        $I->dontSeeElement('Consulter la page Wikipédia en français');
        $I->dontSeeElement('Consulter la page Wikipédia en anglais');
    }

    /**
     * When I edit a software,
     * I see form inputs for external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function editSoftwareSeeFormInputsForExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/edit/$sofwareId");

        $I->seeElement("//div[@id='only-for-admin_software-external-ressources_edit']");
        $I->see('Ressources externes visible uniquement par les admins');
        $I->see('Identifiant SILL');
        $I->see('Identifiant CNLL');
        $I->see('Identifiant Wikidata.org');
        $I->see('Identifiant FramaLibre');
        $I->see('Identifiant Wikipedia en anglais');
        $I->see('Identifiant Wikipedia en français');

        $I->seeElement("//input[@id='sill']");
        $I->seeElement("//input[@id='cnll']");
        $I->seeElement("//input[@id='wikidata']");
        $I->seeElement("//input[@id='framalibre']");
        $I->seeElement("//input[@id='wikipedia-en']");
        $I->seeElement("//input[@id='wikipedia-fr']");

        $I->seeElement("//input[@id='sill'][@value='89']");
        $I->seeElement("//input[@id='cnll'][@value='98']");
        $I->seeElement("//input[@id='wikidata'][@value='Q698']");
        $I->seeElement("//input[@id='framalibre'][@value='firefox']");
        $I->seeElement("//input[@id='wikipedia-en'][@value='Firefox']");
        $I->seeElement("//input[@id='wikipedia-fr'][@value='Mozilla_Firefox']");
    }

    /**
     * When I edit a software,
     * I can update external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function editSoftwareUpdateExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82; // Firefox software
        $I->amOnPage("/$lang/softwares/$sofwareId");
        $this->commonDisplayFirefoxSoftwareSeeExternalRessources($I);

        // Remove external ressources (wikidata and SILL Ids)
        $I->click("a[href=\"/$lang/softwares/edit/$sofwareId\"]");    // Edit sofware's button
        $I->canSeeInCurrentUrl("/$lang/softwares/edit/$sofwareId");
        $I->seeElement("//input[@id='sill'][@value='89']");
        $I->seeElement("//input[@id='cnll'][@value='98']");
        $I->seeElement("//input[@id='wikidata'][@value='Q698']");
        $I->seeElement("//input[@id='framalibre'][@value='firefox']");
        $I->seeElement("//input[@id='wikipedia-en'][@value='Firefox']");
        $I->seeElement("//input[@id='wikipedia-fr'][@value='Mozilla_Firefox']");
        $updateData = [
            'sill' => '',
            'cnll' => '',
            'wikidata' => '',
            'framalibre' => '',
            'wikipedia_en' => '',
            'wikipedia_fr' => '',
        ];
        $I->submitForm('#editSoftwareForm', $updateData);
        $I->canSeeInCurrentUrl("/$lang/softwares/$sofwareId");
        $I->seeElement('div.message.success');
        $I->seeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->dontSeeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");
        $I->see('Ressources externes visible uniquement par les admins');
        $I->dontSee('Consulter la fiche SILL');
        $I->dontSee('Consulter la fiche CNLL');
        $I->dontSee('Consulter la fiche wikidata.org');
        $I->dontSee('Consulter la fiche FramaLibre');
        $I->dontSee('Consulter la page Wikipédia en français');
        $I->dontSee('Consulter la page Wikipédia en anglais');
        $I->dontSee('Q698');

        // add external ressources (wikidata and SILL Ids)
        $I->click("a[href=\"/$lang/softwares/edit/$sofwareId\"]");    // Edit sofware's button
        $I->canSeeInCurrentUrl("/$lang/softwares/edit/$sofwareId");
        $I->seeElement("//input[@id='sill']");
        $I->seeElement("//input[@id='cnll']");
        $I->seeElement("//input[@id='wikidata']");
        $I->dontSeeElement("//input[@value='Q24026504']");
        $updateData = [
            'sill' => '89',
            'cnll' => '98',
            'wikidata' => 'Q698',
            'framalibre' => 'firefox',
            'wikipedia_en' => 'Firefox',
            'wikipedia_fr' => 'Mozilla_Firefox',
        ];
        $I->submitForm('#editSoftwareForm', $updateData);
        $I->canSeeInCurrentUrl("/$lang/softwares/$sofwareId");
        $I->seeElement('div.message.success');
        $this->commonDisplayFirefoxSoftwareSeeExternalRessources($I);
    }

    /**
     *
     * @param $I
     * @return void
     */
    private function commonDisplayFirefoxSoftwareSeeExternalRessources($I)
    {
        $sofwareId = 82; // Firefox software
        $I->seeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->see('Ressources externes visible uniquement par les admins');
        $I->see('Consulter la fiche SILL');
        $I->see('Consulter la fiche CNLL');
        $I->see('Consulter la fiche wikidata.org');
        $I->see('Consulter la fiche FramaLibre');
        $I->see('Consulter la page Wikipédia en français');
        $I->see('Consulter la page Wikipédia en anglais');
        $I->see('Q698');

        $I->seeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->seeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->seeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->seeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->seeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->seeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");
        $I->seeElement(
            "//a[@id='external-link_sill_$sofwareId'][@href='https://code.gouv.fr/sill/fr/software?id=89']"
        );
        $I->seeElement(
            "//a[@id='external-link_cnll_$sofwareId'][@href='https://annuaire.cnll.fr/solutions/98']"
        );
        $I->seeElement(
            "//a[@id='external-link_wikidata_$sofwareId'][@href='https://www.wikidata.org/wiki/Q698']"
        );
        $I->seeElement(
            "//a[@id='external-link_framalibre_$sofwareId'][@href='https://framalibre.org/content/firefox']"
        );
        $I->seeElement(
            "//a[@id='external-link_wikipedia-en_$sofwareId'][@href='https://en.wikipedia.org/wiki/Firefox']"
        );
        $I->seeElement(
            "//a[@id='external-link_wikipedia-fr_$sofwareId'][@href='https://fr.wikipedia.org/wiki/Mozilla_Firefox']"
        );
    }
}
