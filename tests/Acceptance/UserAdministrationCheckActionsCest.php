<?php
/**
 * This file is to test actions that an administration account can do:
 *      Add and edit a software,
 *      declare and undeclare as user of a soft,
 *      edit its own user account,
 *      add a review to a software,
 *      by login a user, testing an action and logout.
 * The account used is dev-Administration from the Dataset02.
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Inherited Methods from App\src\TestSuite.Codeception\AcceptanceTester
 *
 * @method  void loginMe
 * @method  void logoutMe
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class UserAdministrationCheckActionsCest
{
    private $lang = 'en';
    private $userId = 284;

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/');
        $I->loginMe(
            'dev-collectivite@comptoir-du-libre.org',
            'comptoir',
            'dev-collectivite',
            $this->userId
        );
        $I->seeElement('div.message.success');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->logoutMe('dev-collectivite');
        $I->canSeeElement('#signinform');
    }

// Note: MFaure 2019-07-15
// All following tests are commented out as we prefer having a working subset of F-tests
// than a larger non working set. Those F-tests will be re-factored, hopefully in a near future
// Nevertheless, we don't want to loose this code, hence we keep it commented.
// If not refactored by 2020-09-01, the commented code can be deleted

/*
    public function AddNewSoftware(AcceptanceTester $I)
    {
        $I->loginMe('fake-email@comptoir-du-libre.org133', 'pwdComptoirTest', 'canizares');
        $I->seeElement('div.message.success');
        $I->click('//*[@id="softwaresPage"]'); // button 'Logiciels'
        $I->click('//body/div[3]/main/section/div[1]/form/button'); // button 'Ajouter un logiciel'
        $I->fillField('//*[@id="softwarename"]', 'testSoftware1');
        $I->fillField('//*[@id="url-website"]', 'http://testSoftware1.fr');
        $I->fillField('//*[@id="url-repository"]', 'http://testSoftware1.git');
        $I->attachFile(
            '//*[@id="photo"]',
            'correctSoftwareLogo.jpg'
        ); // can't fill field with a file out of Fixture file??
        $I->selectOption('//*[@id="licence-id"]', '13');
        $I->fillField('//*[@id="tags"]', 'tag');
        $I->click('//body/div[3]/main/div[3]/div/div/form/button');
        $I->seeElement('div.message.success');
        $I->logoutMe('canizares');
        $I->canSeeElement('#signinform');
    }

    public function AddSoftwareWithNameAlreadyExisting(AcceptanceTester $I)
    {
        $I->loginMe('fake-email@comptoir-du-libre.org133', 'pwdComptoirTest', 'canizares');
        $I->seeElement('div.message.success');
        $I->click('//*[@id="softwaresPage"]'); // button 'Logiciels'
        $I->click('//body/div[3]/main/section/div[1]/form/button'); // button 'Ajouter un logiciel'
        $I->fillField('//*[@id="softwarename"]', 'Alfresco');
        $I->fillField('//*[@id="url-website"]', 'http://testSoftware2.fr');
        $I->fillField('//*[@id="url-repository"]', 'http://testSoftware2.git');
        $I->attachFile(
            '//*[@id="photo"]',
            'correctSoftwareLogo.jpg'
        ); // can't fill field with a file out of Fixture file ??
        $I->selectOption('//*[@id="licence-id"]', '14');
        $I->fillField('//*[@id="tags"]', 'tag');
        $I->click('//body/div[3]/main/div[3]/div/div/form/button'); // submit button
        $I->seeElement('div.error.error');
        $I->logoutMe('canizares');
        $I->canSeeElement('#signinform');
    }

    public function editSoftware(AcceptanceTester $I)
    {
        $I->loginMe('fake-email@comptoir-du-libre.org133', 'pwdComptoirTest', 'canizares');
        $I->seeElement('div.message.success');
        $I->click('//*[@id="softwaresPage"]'); // button 'Logiciels'
        $I->click('Alfresco');
        $I->seeInCurrentUrl('softwares/29');
        $I->click('//body/div[3]/main/section[1]/div[2]/ul/li[6]/a'); // button 'Modifier'
        $I->seeInCurrentUrl('softwares/edit/29');
        $I->fillField('//*[@id="softwarename"]', 'Alfresco');
        $I->fillField('//*[@id="url-website"]', 'http://testSoftware3.fr');
        $I->fillField('//*[@id="url-repository"]', 'http://testSoftware3.git');
        $I->fillField('//*[@id="description"]', 'Test modification');
        $I->attachFile('//*[@id="photo"]', 'correctSoftwareLogo.jpg');
        $I->selectOption('//*[@id="licence-id"]', '14');
        $I->fillField('//*[@id="tags"]', 'tag');
        $I->click('(//button[@type=\'submit\'])[2]'); // submit button
        $I->seeElement('div.message.success');
        $I->logoutMe('canizares');
        $I->canSeeElement('#signinform');
    }
*/


    /**
     * Function to test the user dev-collectivite declaring himself as user of the ATOM software,
     * using the buttons in the mapping section
     *
     * @group user_declareUserOfSoftware
     * @group user_administration
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function declareUserOfSoftwareUsingButtonsInMappingSection(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $softwareToAddAsUser = "//a[@href=\"/$lang/softwares/163\"]";               // software Atom

        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSee('dev-collectivite', ['css' => 'ol li']);            // user dev-collectivite
        $I->SeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->SeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btnMapping_Softwares-usersSoftware-163"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeInCurrentUrl("/$lang/mappingForm/163");

        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->seeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->seeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->seeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->seeElement('button.btn.btn-default.removeOne');
        $I->see('dev-collectivite', ['css' => 'ol li']);                // dev-collectivite on the list of users

        $I->click('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');  // button : 'Ne plus être utilisateur'
        $I->dontSeeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->SeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
    }



    /**
     * Function to test the user dev-collectivite declaring himself as user of the ATOM software
     *
     * @group user_declareUserOfSoftware
     * @group user_administration
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function declareUserOfSoftwareWithLessThan3Users(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $softwareToAddAsUser = "//a[@href=\"/$lang/softwares/163\"]";               // software Atom

        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSee('dev-collectivite', ['css' => 'ol li']);            // user dev-collectivite
        $I->SeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->SeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-163"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeInCurrentUrl("/$lang/mappingForm/163");

        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->seeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->seeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->seeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->seeElement('button.btn.btn-default.removeOne');
        $I->see('dev-collectivite', ['css' => 'ol li']);                // dev-collectivite on the list of users
    }

    /**
     * Function to test if dev-collectivite is not declared anymore as a user of Atom software anymore
     *
     * @group user_declareUserOfSoftware
     * @group user_administration
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function removeUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('a[id="usersPage"]');                                 // button Users
        $I->canSeeInCurrentUrl("/$lang/users");
        $I->click("a[href=\"/$lang/users/284\"]");                              // User dev-collectivite
        $I->seeInCurrentUrl("/$lang/users/284");
        $I->seeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");     // Atom software declared
        $I->click("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->seeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->seeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');  // button : 'Ne plus être utilisateur'
        $I->dontSeeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->SeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
    }

    /**
     * Check a correct declaration of userOf by the user dev-collectivite
     *
     * @group user_declareUserOfSoftware
     * @group user_administration
     *
     * @param AcceptanceTester $I codeception constant
     * @return void
     */
    public function declareUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->amOnPage("/$lang/users/284");                                    // dev-collectivite's page
        $I->dontSeeElement('7-zip');
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click('7-zip');
        $I->seeInCurrentUrl("/$lang/softwares/72");
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-72"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeInCurrentUrl("/$lang/mappingForm/72");

        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click('7-zip');
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->seeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->seeInCurrentUrl("/$lang/softwares/usersSoftware/72");
        $I->see('dev-collectivite', ['css' => 'ol li']);                // dev-collectivite on the list of users
    }

    /**
     * Remove a declared user dev-collectivite from the userList of a software 7-zip with more than 3 users.
     *
     * @group user_declareUserOfSoftware
     * @group user_administration
     *
     * @param AcceptanceTester $I codeception constant
     * @return void
     */
    public function removeUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/284');                                    // dev-collectivite's page
        $I->see('7-zip');                                               // declared as userOf 7-zip
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->seeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');  // button : 'Ne plus être utilisateur'
        $I->seeElement('div.message.success');
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->dontsee('dev-collectivite', ['css' => 'ol li']);             // dev-collectivite is not in the list of users
    }

    /**
     * Edit a connected user account to change the password 'comptoir' by a new password 'comptoir'.k
     * A new password can be the same as an older one.
     * We check that the user can reconnect himself with the new password
     *
     * @group userAccount
     * @group userAccount_changePassword
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function changePasswordOfConnectedUser(AcceptanceTester $I)
    {
        $newPassword = 'comptoir';

        $userId = 284;
        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/$userId\"]");             // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->click("a[href=\"/api/v1/users/change-password/$userId?language=$lang\"]");       // Change password link
        $I->canSeeInCurrentUrl("users/change-password/$userId?language=$lang");
        $I->submitForm(
            '#editAccountPasswordForm',
            [
                'old_password' => 'comptoir',
                'new_password' => $newPassword,
                'confirm_password' => $newPassword,
            ]
        );
        $I->seeInCurrentUrl("/users/edit/$userId");
        $I->seeElement('div.message.success');
        $I->logoutMe('dev-collectivite');
        $I->loginMe('dev-collectivite@comptoir-du-libre.org', $newPassword, 'dev-collectivite');
        $I->seeElement('div.message.success');
    }


    /**
     * Commun to edit a connected user account
     * to modify information such as name, url, description, email and avatar.
     *
     * Here we add an url and a word as description.
     * If the username is provided, it is also updated.
     *
     * @param AcceptanceTester $I
     * @param string $username     default: ''
     */
    private function commonEditeUserAccount(AcceptanceTester $I, string $username = '')
    {
        $updateData = [
            'url' => 'http://example.com:8080/users/284',
            'description' => 'administration',
        ];
        $checkNewName = false;
        if (!empty($username)) {
            $updateData['username'] = $username;
            $checkNewName = true;
        }

        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/284\"]");    // Edit user's button
        $I->canSeeInCurrentUrl('/users/edit/284');
        $I->submitForm('#editInformationAccountForm', $updateData);
        $I->canSeeInCurrentUrl('/users/284');
        $I->seeElement('div.message.success');
        if ($checkNewName === true) {
            $I->see($username, ['css' => '#userPageFirstTitle']);
            $I->see($username, ['css' => 'li a']);
        }
    }

    /**
     * Edit a connected user account to modify information such as name, url, description, email and avatar.
     * Here we add an url and a word as description.
     *
     * @group userAccount
     * @group userAccount_edit
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function editeUserAccount(AcceptanceTester $I)
    {
        $this->commonEditeUserAccount($I);
    }


    /**
     * Edit a connected user account to modify name, url and description
     * and then declare as user of software
     *
     * @group userAccount
     * @group userAccount_edit
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function editAccountAndDeclareUserOf(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $oldName = 'dev-collectivite';
        $newName = 'dev-coll4ectivite updated';
        $softwareId = 163;
        $softwareToAddAsUser = "//a[@href=\"/$lang/softwares/$softwareId\"]"; // software Atom

        // rename user
        $this->commonEditeUserAccount($I, $newName);

        // declare user of
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->seeInCurrentUrl("/$lang/softwares/$softwareId");
        $I->dontSee($oldName, ['css' => 'ol li']);
        $I->dontSee($newName, ['css' => 'ol li']);
        $I->click("//*[@id=\"btn_Softwares-usersSoftware-$softwareId\"]");  // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeInCurrentUrl("/$lang/mappingForm/$softwareId");

        //  Checks on software page
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->seeInCurrentUrl("/$lang/softwares/$softwareId");
        $I->seeElement("//*[@id=\"btn_Softwares-deleteUsersSoftware-$softwareId\"]");
        $I->dontSee($oldName, ['css' => 'ol li']);
        $I->see($newName, ['css' => 'ol li']);

        // rename the user and no longer be a user of the software
        $this->commonEditeUserAccount($I, $oldName);
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click($softwareToAddAsUser);
        $I->seeInCurrentUrl("/$lang/softwares/$softwareId");
        $I->see($oldName, ['css' => 'ol li']);
        $I->dontSee($newName, ['css' => 'ol li']);
        $I->click("//*[@id=\"btn_Softwares-deleteUsersSoftware-$softwareId\"]"); // button : 'ne plus être utilisateur'
        $I->seeElement('div.message.success');
        $I->dontSee($oldName, ['css' => 'ol li']);
        $I->dontSee($newName, ['css' => 'ol li']);
    }



    /**
     * When trying to modify another user's data (by changing the id in the url),
     * verify that we can't even see the form
     *
     * @group  security
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function tryToEditAnotherUserAccount(AcceptanceTester $I)
    {
        $I->amOnPage('/users/edit/3'); // "Adullact" acount (id:3)

        $I->dontSeeElement('form#editInformationAccountForm');
        $I->dontSeeInTitle('Edit your account');
        $I->dontSee('Edit your account');

        $I->seeInTitle('Error-400');
        $I->seeElement('div.message.error');
        $I->see('You are not allowed to do that');
    }


    /**
     * When trying to update JSON export of all software
     * verify that we can't see link to export (only user with "admin" role can see it)
     *
     * @group  security
     * @group  export
     * @group  software
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayExportJsonOfAllSoftware(AcceptanceTester $I)
    {
        $I->amOnPage('/softwares/export'); // Generate export
        $I->dontSeeElement("//h1[@id='export-json_displayed']");
        $I->dontSeeElement("//a[@id='link_export-json_displayed']");
        $I->dontSeeElement("//h1[@id='error_export-json_displayed']");
        $I->seeElement("//div[@id='export-json_not-displayed']");
        $I->dontSeeElement("//div[@id='error_export-json_not-displayed']");
    }

    /**
     * When trying to update export of all users
     * verify that we can't export (only user with "admin" role can do it)
     *
     * @group  security
     * @group  export
     * @group  user
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displayExportOfAllUsers(AcceptanceTester $I)
    {
        $I->amOnPage('/users/export'); // Generate export
        $I->seeInTitle('Error-400');
        $I->seeElement('div.message.error');
        $I->see('You are not allowed to do that');

        $I->dontSeeElement("//h1[@id='users-export_displayed']");
        $I->dontSeeElement("//a[@id='link_users-export_displayed']");
        $I->dontSeeElement("//h1[@id='error_users-export_displayed']");
        $I->dontseeElement("//div[@id='users-export_not-displayed']");
        $I->dontSeeElement("//div[@id='error_users-export_not-displayed']");
    }


    /**
     * When I display a software,
     * I don't see the external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @group  user_administration
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displaySoftwareNotSeeExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->dontSeeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");

        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Consulter la fiche SILL');
        $I->dontSee('Consulter la fiche CNLL');
        $I->dontSee('Consulter la fiche wikidata.org');
        $I->dontSee('Consulter la fiche FramaLibre');
        $I->dontSee('Consulter la page Wikipédia en français');
        $I->dontSee('Consulter la page Wikipédia en anglais');
        $I->dontSee('Q698');
    }

    /**
     * When I edit a software,
     * I don't see form inputs for external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @group  user_administration
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function editSoftwareNotSeeFormInputsForExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/edit/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources_edit']");
        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Identifiant SILL');
        $I->dontSee('Identifiant CNLL');
        $I->dontSee('Identifiant Wikidata.org');
        $I->dontSee('Identifiant FramaLibre');
        $I->dontSee('Identifiant Wikipedia en anglais');
        $I->dontSee('Identifiant Wikipedia en français');
        $I->dontSeeElement("//input[@id='sill']");
        $I->dontSeeElement("//input[@id='cnll']");
        $I->dontSeeElement("//input[@id='wikidata']");
        $I->dontSeeElement("//input[@id='framalibre']");
        $I->dontSeeElement("//input[@id='wikipedia-en']");
        $I->dontSeeElement("//input[@id='wikipedia-fr']");
    }
}
