<?php
/**
 * This action should be tested manually.
 *      Here is only tested the mail request to reset the password.
 *      It need to verify if the mail is sended and if the
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * This action should be tested manually.
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class CheckResetPasswordActionCest
{
    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

    /**
     * Check the mail request to reset the password.
     *
     * @param AcceptanceTester $I codeception constant
     *
     * @return void
     */
    public function askForResetPasswordEmailingLink(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
        $I->click('//nav/ul[2]/li[3]/a');                       // Login link
        $I->seeInCurrentUrl('/users/login');
        $I->click('a.forgotPasswordLink');           // Forgot password link
        $I->seeInCurrentUrl('/users/forgot-password');
        $I->submitForm(
            '#forgotPasswordForm',
            [
                'email' => 'dev-collectivite@comptoir-du-libre.org',
            ]
        );
        $I->seeElement('div.message.success');
    }
}
