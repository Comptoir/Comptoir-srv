<?php
/**
 * This file is to test actions that a person account can do:
 * Add and edit a software,
 * declare and undeclare as user of a soft,
 * edit its own user account,
 * add a review to a software,
 * by login a user, testing an action and logout.
 * The account used is dev-individu from the Dataset02.
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Inherited Methods from App\src\TestSuite.Codeception\AcceptanceTester
 *
 * @method  void loginMe
 * @method  void logoutMe
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class UserPersonCheckActionsCest
{
    private $lang = 'en';
    private $userId = 285;

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/');
        $I->loginMe(
            'dev-individu@comptoir-du-libre.org',
            'comptoir',
            'dev-individu',
            $this->userId
        );
        $I->seeElement('div.message.success');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->logoutMe('dev-individu');
        $I->canSeeElement('#signinform');
    }

    /**
     * Function to test the user dev-individu declaring himself as user of the ATOM software
     *
     * @group user_declareUserOfSoftware
     * @group user_person
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function declareAsUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('//*[@id="softwaresPage"]');                      // button 'Logiciels'
        $I->click("//a[@href=\"/$lang/softwares/163\"]");           // software Atom
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSee('dev-individu', ['css' => 'ol li']);            // dev-individu declared on the user list
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-163"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeElement('button.btn.btn-default.removeOne');
        $I->see('dev-individu', ['css' => 'ol li']);                // dev-collectivite on the list of users
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
    }

    /**
     * Function to test if dev-individu is not declared as a user of Atom software anymore
     *
     * @group user_declareUserOfSoftware
     * @group user_person
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function removeUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('a[id="usersPage"]');                             // button Users
        $I->canSeeInCurrentUrl("/$lang/users");
        $I->click("a[href=\"/$lang/users/285\"]");                          // User dev-collectivite
        $I->seeInCurrentUrl("/$lang/users/285");
        $I->seeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]"); // Atom software declared
        $I->click("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');  // button : 'Ne plus être utilisateur'
        $I->dontSeeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
    }

    /**
     * Check a correct declaration of userOf by the user dev-individu
     *
     * @group user_declareUserOfSoftware
     * @group user_person
     *
     * @param AcceptanceTester $I codeception constant
     *
     * @return void
     */
    public function declareUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/285');                                    // dev-individu's page
        $I->dontSeeElement('7-zip');
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-72"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->seeInCurrentUrl('softwares/usersSoftware/72');
        $I->see('dev-individu', ['css' => 'ol li']);                        // dev-individu on the list of users
    }

    /**
     * Remove a declared user dev-individu from the userList of a software 7-zip with more than 3 users.
     *
     * @group user_declareUserOfSoftware
     * @group user_person
     *
     * @param AcceptanceTester $I codeception constant
     *
     * @return void
     */
    public function removeUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/285');                                    // dev-individu's page
        $I->see('7-zip');                                               // declared as userOf 7-zip
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');  // button : 'Ne plus être utilisateur'
        $I->seeElement('div.message.success');
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->dontsee('dev-individu', ['css' => 'ol li']);                 // dev-individu is not in the list of users
    }

    /**
     * Edit a connected user account to change the password 'comptoir' by a new password 'comptoir'.
     * A new password can be the same as an older one.
     * We check that the user can reconnect himself with the new password
     *
     * @group userAccount
     * @group userAccount_changePassword
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function changePasswordOfConnectedUser(AcceptanceTester $I)
    {
        $newPassword = 'comptoir';

        $userId = 285;
        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/$userId\"]");             // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->click("a[href=\"/api/v1/users/change-password/$userId?language=$lang\"]");       // Change password link
        $I->canSeeInCurrentUrl("users/change-password/$userId?language=$lang");
        $I->submitForm(
            '#editAccountPasswordForm',
            [
                'old_password' => 'comptoir',
                'new_password' => $newPassword,
                'confirm_password' => $newPassword,
            ]
        );
        $I->seeInCurrentUrl("/users/edit/$userId");
        $I->seeElement('div.message.success');
        $I->logoutMe('dev-individu');
        $I->loginMe('dev-individu@comptoir-du-libre.org', $newPassword, 'dev-individu');
        $I->seeElement('div.message.success');
    }

    /**
     * Edit a connected user account to modify information such as name, url, description, email and avatar.
     * Here we add an url and a word as description.
     *
     * @group userAccount
     * @group userAccount_edit
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function editeUserAccount(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/285\"]");    // Edit user's button
        $I->canSeeInCurrentUrl('/users/edit/285');
        $I->submitForm(
            '#editInformationAccountForm',
            [
                'url' => 'http://example.com:8080/users/285',
                'description' => 'person',
            ]
        );
        $I->canSeeInCurrentUrl('/users/285');
        $I->seeElement('div.message.success');
    }


    /**
     * A user with a "User" role cannot modify it.
     *
     * Edit a connected user account to try to change its role (the "user" role
     * should become "admin") by adding a new parameter in the POST request.
     *
     * After, try to display edit form for another user account
     * allowed only for user with "admin" role.
     *
     * @group userAccount
     * @group userAccount_edit
     * @group  security
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function failToEditUserAccountForUpdatingRole(AcceptanceTester $I)
    {
        // current user with role = "User"
        $userId = 285;
        $lang = $this->lang;

        $I->click("a[href=\"/$lang/users/edit/$userId\"]"); // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->submitForm(
            '#editInformationAccountForm',
            [
                'url' => 'http://example.com:8080/users/285',
                'description' => 'person',
                'role'        => 'admin'
            ]
        );
        $I->canSeeInCurrentUrl("/users/$userId");
        $I->seeElement('div.message.success');

        // Display edit form for another user account
        // allowed only for user with "admin" role
        $I->amOnPage('/users/edit/3'); // "Adullact" acount (id:3) ----> not the current user
        $I->dontSeeElement("//form[@id='editInformationAccountForm']");
    }

    /**
     * When trying to modify another user's data (by changing the id in the url),
     * verify that we can't even see the form
     *
     * @group  security
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function tryToEditAnotherUserAccount(AcceptanceTester $I)
    {
        $I->amOnPage('/users/edit/3'); // "Adullact" acount (id:3)

        $I->dontSeeElement('form#editInformationAccountForm');
        $I->dontSeeInTitle('Edit your account');
        $I->dontSee('Edit your account');

        $I->seeInTitle('Error-400');
        $I->seeElement('div.message.error');
        $I->see('You are not allowed to do that');
    }


    /**
     * When I display a software,
     * I don't see the external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @group  user_person
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displaySoftwareNotSeeExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->dontSeeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");

        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Consulter la fiche SILL');
        $I->dontSee('Consulter la fiche CNLL');
        $I->dontSee('Consulter la fiche wikidata.org');
        $I->dontSee('Consulter la fiche FramaLibre');
        $I->dontSee('Consulter la page Wikipédia en français');
        $I->dontSee('Consulter la page Wikipédia en anglais');
        $I->dontSee('Q698');
    }

    /**
     * When I edit a software,
     * I don't see form inputs for external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @group  user_person
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function editSoftwareNotSeeFormInputsForExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/edit/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources_edit']");
        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Identifiant SILL');
        $I->dontSee('Identifiant CNLL');
        $I->dontSee('Identifiant Wikidata.org');
        $I->dontSee('Identifiant FramaLibre');
        $I->dontSee('Identifiant Wikipedia en anglais');
        $I->dontSee('Identifiant Wikipedia en français');
        $I->dontSeeElement("//input[@id='sill']");
        $I->dontSeeElement("//input[@id='cnll']");
        $I->dontSeeElement("//input[@id='wikidata']");
        $I->dontSeeElement("//input[@id='framalibre']");
        $I->dontSeeElement("//input[@id='wikipedia-en']");
        $I->dontSeeElement("//input[@id='wikipedia-fr']");
    }
}
