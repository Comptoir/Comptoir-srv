<?php

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

class CheckUsersSortByCest
{
    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/users');
        $I->seeInTitle('Listes des utilisateurs');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

// Note: MFaure 2019-07-15
// All following tests are commented out as we prefer having a working subset of F-tests
// than a larger non working set. Those F-tests will be re-factored, hopefully in a near future
// Nevertheless, we don't want to loose this code, hence we keep it commented.
// If not refactored by 2020-09-01, the commented code can be deleted

/*
    public function checkUsersSortByAscWithSoftwareDeclare(AcceptanceTester $I)
    {
        $UserSortByAscWithSoftware1st = ('//ol/li[1]/div/a["Administration1"]');
        $UserSortByAscWithSoftware5th = ('//ol/li[5]/div/a["Association la Mouette"]');
        $UserSortByAscWithSoftware22th = (
        '//ol/li[22]/div/a[@title="Nom de l\'utilisateur : Conseil Départemental Côtes d\'Armor (22)"]'
        );
        $I->selectOption('//form/div[1]/select', 'true');
        $I->click('Filtrer');
        $I->seeOptionIsSelected('//form/div[1]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[5]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($UserSortByAscWithSoftware1st);
        $I->seeElement($UserSortByAscWithSoftware5th);
        $I->seeElement($UserSortByAscWithSoftware22th);
    }

    public function checkUsersSortByAscWithReviewPosted(AcceptanceTester $I)
    {
        $UserSortByAscWithReview1st = ('//ol/li[1]/div/a[@href="/fr/users/45"]'); // Centre Régional Réunion
        $UserSortByAscWithReview3th = ('//ol/li[3]/div/a[@href="/fr/users/42"]'); // Communauté d'Agglomération Sud...
        $UserSortByAscWithReview6th = ('//ol/li[6]/div/a[@href="/fr/users/15"]'); // Ville de Paris

        $I->selectOption('//form/div[2]/select', 'true');
        $I->click('Filtrer');
        $I->seeOptionIsSelected('//form/div[2]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[5]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($UserSortByAscWithReview1st);
        $I->seeElement($UserSortByAscWithReview3th);
        $I->seeElement($UserSortByAscWithReview6th);
    }

    public function checkUsersSortByAscWithServiceProviderDeclareOnSoftware(AcceptanceTester $I)
    {
        $UserSortByAscWithServProvider1st = ('//ol/li[1]/div/a[@href="/fr/users/75"]'); // AF83
        $UserSortByAscWithServProvider4th = ('//ol/li[4]/div/a[@href="/fr/users/6"]'); // Libriciel SCOP
        $UserSortByAscWithServProvider10th = ('//ol/li[10]/div/a[@href="/fr/users/132"]'); // Syloé

        $I->selectOption('//form/div[3]/select', 'true');
        $I->click('Filtrer');
        $I->seeOptionIsSelected('//form/div[3]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[5]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($UserSortByAscWithServProvider1st);
        $I->seeElement($UserSortByAscWithServProvider4th);
        $I->seeElement($UserSortByAscWithServProvider10th);
    }

    public function checkUsersSortByAscAsAdministration(AcceptanceTester $I)
    {
        $UserSortByAscAsAdministration1st = ('//ol/li[1]/div/a[@href="/fr/users/204"]'); // Administration1
        $UserSortByAscAsAdministration12th = (
        '//ol/li[12]/div/a[@href="/fr/users/42"]'
        ); // Communauté d'Aglomeration Sud...
        $UserSortByAscAsAdministration33th = ('//ol/li[33]/div/a[@href="/fr/users/47"]'); // Ville de Bezier

        $I->selectOption('//form/div[4]/select', 'Administration');
        $I->click('Filtrer');
        // $I->seeOptionIsSelected('//form/div[4]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[5]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($UserSortByAscAsAdministration1st);
        $I->seeElement($UserSortByAscAsAdministration12th);
        $I->seeElement($UserSortByAscAsAdministration33th);
    }

    public function checkUsersSortByAscAsPerson(AcceptanceTester $I)
    {
        $UserSortByAscAsPerson1st = ('//ol/li[1]/div/a[@href="/fr/users/131"]'); // benasse
        $UserSortByAscAsPerson5th = ('//ol/li[5]/div/a[@href="/fr/users/84"]'); // Francois ELIE
        $UserSortByAscAsPerson8th = ('//ol/li[8]/div/a[@href="/fr/users/71"]'); // Matthieu FAURE

        $I->selectOption('//form/div[4]/select', 'Person');
        $I->click('Filtrer');
        // $I->seeOptionIsSelected('//form/div[4]/select', 'Individu');
        $I->seeOptionIsSelected('//form/div[5]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($UserSortByAscAsPerson1st);
        $I->seeElement($UserSortByAscAsPerson5th);
        $I->seeElement($UserSortByAscAsPerson8th);
    }
*/
}
