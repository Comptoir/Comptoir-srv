<?php

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

class BasicTestsCest
{
    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }


    /**
     * Check if we are on the Homepage
     *
     * @param $I
     */
    public function seeContactOnHomepage($I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
        $I->see('Contact');
    }

    public function clickOnLegalLinkFromHomepage($I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
        $I->see('Mentions légales');
        $I->click('Mentions légales');
        $I->seeInCurrentUrl('/pages/legal');
        $I->seeInTitle('Mentions légales');
    }

    public function clickOnLegalAccessibilityFromHomepage($I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
        $I->see('Accessibilité : non conforme');
        $I->click('Accessibilité : non conforme');
        $I->seeInCurrentUrl('/pages/accessibility');
        $I->seeInTitle("Déclaration d'accessibilité");
    }

    public function clickOnContactLinkFromHomepage($I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
        $I->see('Contact');
        $I->click('Contact');
        $I->seeInCurrentUrl('/pages/contact');
        $I->seeInTitle('Information pour nous contacter');
    }

    public function clickOnOpendataLinkFromHomepage($I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
        $I->see('Open Data');
        $I->click('Open Data');
        $I->seeInCurrentUrl('/pages/opendata');
        $I->seeInTitle('Open data - Données ouvertes');
    }

    //Todo check pages softwares, users and services providers
}
