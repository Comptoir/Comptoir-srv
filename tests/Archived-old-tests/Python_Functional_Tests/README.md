# DOCUMENTATION for Functional tests

##  Resources:

* [Selenium with Python](http://selenium-python.readthedocs.io/)
* [Python Selenium package](https://pypi.python.org/pypi/selenium)

## To be installed on developer's computer

* Install PIP:
    * `apt-get install python3.5 python3.5-dev python-pip`
    * `sudo -H pip install --upgrade pip`
* In IntelliJ, configure SDK for project:
    * File > Project Structure > platform SDK > SDK: add Python as SDK
    * File > Project Structure > platform SDK > SDK > Classpath: add directory `tests/Function_Test`

##  Developer notes:

Code should be enhanced by using `setUpModule` and `tearDownModule`. See:

* [How can I share one webdriver instance in my test classes in the suite? I use Selenium2 and Python](https://stackoverflow.com/questions/8352929/how-can-i-share-one-webdriver-instance-in-my-test-classes-in-the-suite-i-use-se)
* [Python doc: setUpModule and tearDownModule](https://docs.python.org/3/library/unittest.html#setupmodule-and-teardownmodule)
* As [explained in the 4th comment, author Paul Becotte](https://stackoverflow.com/questions/49260833/python-unit-test-whole-module-setup), put the global `setUpModule` and `tearDownModule` in the init.py file in your test directory and get it run once per package as well
