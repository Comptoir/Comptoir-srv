# -*- coding: utf-8 -*-
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class TestSignIn(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_sign_in_as_administration_wrong_password(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("8ie9nxz5")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.error"))

    def test_sign_in_as_administration_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        # driver.find_element_by_link_text("Deconnexion").click()

    def test_sign_in_as_company_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-presta@comptoir-du-libre.org")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-MyCompany")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        # driver.find_element_by_link_text("Deconnexion").click()

    def test_sign_in_as_non_profit_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-nonProfit")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-asso@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        # driver.find_element_by_link_text("Deconnexion").click()

    def test_sign_in_as_person_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-individu")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-individu@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        # driver.find_element_by_link_text("Deconnexion").click()


if __name__ == "__main__":
    unittest.main()
