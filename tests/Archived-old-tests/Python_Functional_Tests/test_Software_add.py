# -*- coding: utf-8 -*-
import time
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class AddSoftwareAsAdministrationUser(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_add_software_as_administration_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        driver.find_element_by_link_text("Logiciels").click()
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/')]//button").click()
        driver.find_element_by_id("softwarename").clear()
        driver.find_element_by_id("softwarename").send_keys("Nouveau Logiciel")
        driver.find_element_by_id("url-website").clear()
        driver.find_element_by_id("url-website").send_keys("http://nouveaulogiciel.fr")
        driver.find_element_by_id("url-repository").clear()
        driver.find_element_by_id("url-repository").send_keys("http://nouveaulogiciel.git")
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("Description du nouveau logiciel")
        driver.find_element_by_id("tags").clear()
        driver.find_element_by_id("tags").send_keys("tag")
        driver.find_element_by_id("photo").clear()
        driver.find_element_by_id("photo").click()
        driver.find_element_by_id("photo").send_keys(
            "/var/www/html/tests/TestFiles/Softwares/photo/avatar/correctSoftwareLogo.jpg")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_add_software_exists_in_database(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_link_text("Logiciels").click()
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/')]//button").click()

        driver.find_element_by_id("softwarename").clear()
        driver.find_element_by_id("softwarename").send_keys("Asqatasun")

        driver.find_element_by_id("url-website").clear()
        driver.find_element_by_id("url-website").send_keys("http://nouveaulogiciel.fr")
        driver.find_element_by_id("url-repository").clear()
        driver.find_element_by_id("url-repository").send_keys("http://nouveaulogiciel.git")

        driver.find_element_by_id("photo").clear()
        driver.find_element_by_id("photo").click()
        driver.find_element_by_id("photo").send_keys(
            "/var/www/html/tests/TestFiles/Softwares/photo/avatar/correctSoftwareLogo.jpg")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.error.error"))

    def test_add_software_with_tags_as_administration_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        driver.find_element_by_link_text("Logiciels").click()
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/')]//button").click()
        driver.find_element_by_id("softwarename").clear()
        driver.find_element_by_id("softwarename").send_keys("Logiciel avec Tag")
        driver.find_element_by_id("url-website").clear()
        driver.find_element_by_id("url-website").send_keys("http://nouveaulogicielavectag.fr")
        driver.find_element_by_id("url-repository").clear()
        driver.find_element_by_id("url-repository").send_keys("http://nouveaulogicielavectag.git")
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("Description du nouveau logiciela avec tag")
        driver.find_element_by_id("tags").clear()
        driver.find_element_by_id("tags").send_keys("new tag")
        driver.find_element_by_id("photo").clear()
        driver.find_element_by_id("photo").click()
        driver.find_element_by_id("photo").send_keys(
            "/var/www/html/tests/TestFiles/Softwares/photo/avatar/correctSoftwareLogo.jpg")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(5)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))


if __name__ == "__main__":
    unittest.main()
