# -*- coding: utf-8 -*-
import time
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class TestSoftwaresAssertFilters(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_softwares_assert_ordered_a_z(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("order")).select_by_visible_text("Ordre alphabétique : A-Z")
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/72')
        self.assertEquals(driver.find_element_by_xpath("//li[95]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/110')

    def test_softwares_assert_ordered_creation_date_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("order")).select_by_index(2)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/2')  # Pastell
        self.assertEquals(driver.find_element_by_xpath("//li[2]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/3')  #  Asalae
        self.assertEquals(driver.find_element_by_xpath("//li[5]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/9')  # Asqatasun
        self.assertEquals(driver.find_element_by_xpath("//li[18]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/22')  # Xivo
        self.assertEquals(driver.find_element_by_xpath("//li[25]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/29')  #  Alfresco
        self.assertEquals(driver.find_element_by_xpath("//li[103]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/110')  #  Zimbra

    def test_softwares_assert_ordered_creation_date_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        time.sleep(1)
        Select(driver.find_element_by_name("order")).select_by_index(3)
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        time.sleep(1)
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/110')  #  Zimbra
        self.assertEquals(driver.find_element_by_xpath("//li[5]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/106')  # WebCil
        self.assertEquals(driver.find_element_by_xpath("//li[25]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/86')  # WebCil
        self.assertEquals(driver.find_element_by_xpath("//li[32]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/79')  # WebCil
        self.assertEquals(driver.find_element_by_xpath("//li[103]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/2')  #  Zimbra

    def test_softwares_assert_ordered_modification_date_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("order")).select_by_index(4)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/11')  #  Asqatasun
        self.assertEquals(driver.find_element_by_xpath("//li[63]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/79')  #  GRAMALECTE
        self.assertEquals(driver.find_element_by_xpath("//li[103]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/9')  #  Asqatasun

    def test_softwares_assert_ordered_modification_date_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("order")).select_by_index(5)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/9')  #  Asqatasun
        self.assertEquals(driver.find_element_by_xpath("//li[14]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/32')  #  WebGFC
        self.assertEquals(driver.find_element_by_xpath("//li[41]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/79')  #  GRAMALECTE
        self.assertEquals(driver.find_element_by_xpath("//li[103]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/11')  # XWIKI

    def test_softwares_assert_ordered_z_a(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("order")).select_by_visible_text(u"Ordre alphabétique : Z-A")
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertEquals(driver.find_element_by_xpath("//li[9]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/110')
        self.assertEquals(driver.find_element_by_xpath("//li[103]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/72')

    def test_softwares_assert_rating_non_ordered(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        time.sleep(1)
        Select(driver.find_element_by_name("reviewed")).select_by_index(1)
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li[2]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li[3]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li[4]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li[5]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li[6]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//div[@id='super-main']/main/section/ol/li[7]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))

    def test_softwares_at_least_one_user_non_ordered(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        time.sleep(1)
        Select(driver.find_element_by_name("hasUser")).select_by_index(1)
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Authentik\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-Parapheur\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"LibreOffice\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"SPIP\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-delibRE\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"openScrutin\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"eGroupware\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Pastell\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"As@lae\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                u"//*[contains(text(),\"GRR - Gestion Réservation de...\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Jorani\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"LimeSurvey\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Maarch Courrier\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenBiblio\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Nuxeo\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenADS\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"openCimetière\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"openMairie\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"BlueMind\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenRecensement\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenReglement\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"OpenRésultat\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"OpenRésultat\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Typo3\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"S²LOW\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Xemelios\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"web-delib\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WebGFC\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Publik\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WordPress\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WebGFC\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WordPress\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"XiVO\")]/ancestor::li"))

    def test_softwares_reviewed_assert_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        time.sleep(1)
        Select(driver.find_element_by_name("reviewed")).select_by_index(1)
        time.sleep(1)
        Select(driver.find_element_by_name("order")).select_by_visible_text("Score : croissant")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        time.sleep(1)
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/29')  #  WebGFC
        self.assertEquals(driver.find_element_by_xpath("//li[3]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/32')  #  WebGFC
        self.assertEquals(driver.find_element_by_xpath("//li[6  ]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/26')  #  Publik

    def test_softwares_reviewed_assert_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("reviewed")).select_by_index(1)  # oui
        Select(driver.find_element_by_name("order")).select_by_visible_text(u"Score : décroissant")
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertEquals(driver.find_element_by_xpath("//li[1]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/26')  #  Publik
        self.assertEquals(driver.find_element_by_xpath("//li[3]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/32')  #  WebGFC
        self.assertEquals(driver.find_element_by_xpath("//li[7]/div/a").get_attribute('href'),
                          self.base_url + '/softwares/4')  #  WebGFC

    def test_softwares_screenshoted_non_ordered(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        Select(driver.find_element_by_name("screenCaptured")).select_by_visible_text("Oui")
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"Lutèce\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-Parapheur\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"LibreOffice\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"SPIP\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-delibRE\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"openScrutin\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WebRsa\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Pastell\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"As@lae\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Asqatasun\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"BlueMind\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Exoplatform\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"GLPI\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OBM\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"S²LOW\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Xemelios\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"XiVO\")]/ancestor::li"))


if __name__ == "__main__":
    unittest.main()
