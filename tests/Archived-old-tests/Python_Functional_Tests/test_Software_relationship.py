# -*- coding: utf-8 -*-
import time
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class TestSoftwareRelationship(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_declare_as_service_provider_for_and_remove_from_service_provider_list(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-presta@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-MyCompany")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        driver.find_element_by_css_selector("div.message.success").click()
        time.sleep(1)
        driver.find_element_by_link_text("Logiciels").click()
        time.sleep(1)
        driver.get(self.base_url + "/softwares/29")
        time.sleep(2)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/services-providers/29')]//button").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        time.sleep(1)
        driver.find_element_by_xpath(
            "//form[contains(@action,'/softwares/delete-services-providers/29')]//button").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_declare_as_user_of_and_remove_from_user_of_list(self):
        driver = self.driver
        driver.get(self.base_url + "/users/login")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        driver.get(self.base_url + "/softwares/9")
        time.sleep(1)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/users-software/9')]//button").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        driver.get(self.base_url + "/softwares/9")
        time.sleep(1)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/delete-users-software/9')]//button").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_post_review_as_administration_user(self):
        driver = self.driver
        driver.get(self.base_url + "/users/login")
        time.sleep(1)
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        driver.get(self.base_url + "/softwares/25")
        time.sleep(1)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/add-review/25')]//button").click()
        time.sleep(1)
        driver.find_element_by_id("title").send_keys(u"Témoignage")
        time.sleep(1)
        time.sleep(1)
        driver.find_element_by_id("comment").send_keys(u"Témoignage Test")
        Select(driver.find_element_by_name("evaluation")).select_by_visible_text("3")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_post_review_as_company_user_comment_link_is_missing(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_xpath("//a[contains(@href, '/softwares/9')]").click()
        time.sleep(1)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/add-review/9')]//button").click()
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-presta@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-MyCompany")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        driver.get(self.base_url + "/softwares/9")
        self.assertFalse(
            self.is_element_present(By.XPATH, "//form[contains(@action,'/softwares/add-review/9')]//button"))

    def test_post_review_as_unknown_redirect_to_login_page(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.get(self.base_url + "/softwares/9")
        time.sleep(1)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/add-review/9')]//button").click()
        time.sleep(1)
        self.assertEquals(driver.current_url, self.base_url + '/users/login')


if __name__ == "__main__":
    unittest.main()
