[![License : AGPL v3](https://img.shields.io/badge/license-AGPL3-blue.svg)](LICENSE.txt)
[![Contributing welcome](https://img.shields.io/badge/contributing-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)
[![Code of Conduct](https://img.shields.io/badge/code%20of-conduct-ff69b4.svg?style=flat-square)](CODE_OF_CONDUCT.md)
[![CI](https://gitlab.adullact.net/Comptoir/Comptoir-srv/badges/develop/pipeline.svg)](https://gitlab.adullact.net/Comptoir/Comptoir-srv/pipelines?scope=branches)

# Comptoir du Libre

Source code of [comptoir-du-libre.org](https://comptoir-du-libre.org/fr/) website.

## About

Free software list useful to the public services as well as their users and providers.

## Documentation

- [Contributing](CONTRIBUTING.md)
- [Install documentation](Documentation/For_ops/)
- [Developer documentation](Documentation/For_developers/)
- [QA : URLs + online tools](Documentation/QA-tools.md)
- [Mementos](Documentation/Mementos/)

## FEDER

Project funded by the "Fonds Européen de Développement Régional"

![](webroot/img/logos/Logo_FEDER.png)

## License

[AGPL v3 - GNU Affero General Public License](LICENSE.txt)

