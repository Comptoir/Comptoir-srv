<?php
$this->layout("base");
$this->assign('title', "Error-400");
?>

<h2><?= __d('Error', 'This page does not exist.') ?></h2>
