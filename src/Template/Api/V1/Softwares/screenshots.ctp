<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */


$this->assign('title', __d("Softwares", "Screenshots of {0}", $software->softwarename));

?>

<?php
$this->layout('base');
?>



<section>

    <?php
    if (!empty($software->screenshots)){
        echo $this->Lists->block(

            $software->screenshots,
            [
                "type" => "screenshot",
                "title" => "<h1>" . __d("Softwares", "Screenshots of {0}", $software->softwarename) . "</h1>",
                "link" => [
                    "id" => $software->id,
                    "action" => "screenshots",
                    "participate" => __d("Softwares", "Add a screenshot for {0}.", $software->softwarename),
                ],
                "titleAddMore" => __d("Softwares", "Add a screenshot for {0}.", $software->softwarename),
            ],
            false
        );
    }
    ?>
</section>
