<?php
$this->layout('base');

$this->assign('title',  __d("Softwares", "Alternative to {0}", $software->softwarename));
?>

<section>



    <?php echo $this->Lists->block(
        $software->alternativeto,
        [
            "type" => "software",
            "title" => "<h1>" . __d("Softwares", "Alternative to {0}", $software->softwarename) . "</h1>",
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "",
                "text" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "softwaresAlternativeToListId",
                "indicatorMessage" => __d("Softwares", "softwares.softwaresAlternativeToList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
            "emptyMsg" => __d("Softwares", "No alternative to {0}.", $software->softwarename)
        ]
    ); ?>
</section>
