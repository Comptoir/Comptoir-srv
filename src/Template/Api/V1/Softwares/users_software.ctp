<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */

$this->assign('title', __d("Softwares", "Users of {0}", $software->softwarename));
$this->layout('base');

?>


<section>
    <?php echo $this->Lists->block(
        $software->userssoftwares,
        [
            "type" => "user",
            "title" => "<h1>" . __d("Softwares", "Users of {0}", $software->softwarename) . "</h1>",
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "usersSoftware",
                "text" => __d("Softwares", "Softwares.Users.DeclareAs.user.addMessage", $software->softwarename),
            ],
            "linkFallBack" => [
                "id" => $software->id,
                "action" => "fallBackusersSoftware",
                "text" => __d("Softwares", "Softwares.Users.DeclareAs.user.removeMessage", $software->softwarename),
            ],
            "titleAddMore" => "",
            "titleFallBack" => __d("Softwares", "Roll back from users' list of {0}.", $software->softwarename),
        ],
        false
    ); ?>

</section>
