<?php
$this->layout('base');
$this->assign('title', __d("Softwares", "Service providers for {0}", $software->softwarename));
?>

<section>
    <?php
    echo $this->Lists->block(
        $software->providerssoftwares,
        [
            "type" => "user",
            "title" => "<h2>" . __d("Softwares", "Service providers for {0}", $software->softwarename) . "</h2>",
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "servicesProviders",
                "text" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.addMessage", $software->softwarename),
            ],
            "linkFallBack" => [
                "id" => $software->id,
                "action" => "fallBackServicesProvider",
                "text" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.removeMessage", $software->softwarename),
            ],
            "indicator" => [
                "idTooltip" => "serviceProvidersOfListId",
                "indicatorMessage" => __d("Softwares", "softwares.serviceProvidersOfList", $software->softwarename)
            ],
            "tooManyMsg" => __d("Layout", "See all ({0} services providers)", count($software->providerssoftwares)),
            "titleSeeAll" => __d("Softwares", "See all service providers of {0}", $software->softwarename),
            "titleAddMore" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.addMessage", $software->softwarename),
            "titleFallBack" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.removeMessage", $software->softwarename),
            "emptyMsg" => __d("Softwares", "No service provider for {0}.", $software->softwarename)
        ],
        false
    ); ?>
</section>
