<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Add a review{0}', isset($message) ? " - ".$message : ""));

?>



<div class="row">
    <div class = "warning-form bg-warning  col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="reviews form col-xs-offset-3 col-xs-6 columns content">
            <?= $this->Form->create($review,["enctype"=>"multipart/form-data"]) ?>
        <fieldset>
            <legend><?= __d("Forms",'Add Review') ?></legend>

                <?= $this->Form->input(
                    'title',
                    [
                        'label'=>["class"=>"control-label","text"=>__d("Forms"," {0} Title: ", '<span class = "asterisk">*</span>')],
                        "class"=>"form-control",
                        "required"=>"required",
                        "type"=>"text",
                        "escape"=>false,
                    ]); ?>

                <?= $this->Form->input("comment",
                    [
                        'label'=>["class"=>"control-label","text"=>__d("Forms"," {0} Comment: ", '<span class = "asterisk">*</span>')],
                        "class"=>"form-control",
                        "required"=>"required",
                        "type"=>"textarea",
                        "escape"=>false,
                    ]); ?>

                <label  class = "control-label" for="evaluation"><?= __d("Forms"," {0} Evaluation: ", '<span class = "asterisk">*</span>')?></label>

                <select name = "evaluation" class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                </select>
            <div class='form-group'>
                <?=  $this->Form->input('software_id',
                    [
                        'value' => isset($softwareId) ? $softwareId : null,
                        'empty' => true,
                        "type"=>"hidden",
                        "required"=>"required",
                        "escape"=>false,
                    ]);?>
            </div>
        </fieldset>
        <?= $this->Form->button(__d("Forms",'Submit'),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
