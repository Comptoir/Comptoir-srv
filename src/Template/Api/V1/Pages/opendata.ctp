<?php
$this->layout = 'base';
$this->assign('title', __d("Pages", "page.opendata.title"));
?>
<h1><?= __d("Pages", "page.opendata.h1") ?></h1>

<p>
    <?= __d("Pages", "page.opendata.content1") ?>
</p>
<p>
    <?= __d("Pages", "page.opendata.content2", [
        'https://www.data.gouv.fr/fr/datasets/logiciels-libres-sur-le-comptoir-du-libre-org/' ,
        'data.gouv.fr',
    ]) ?>
</p>
