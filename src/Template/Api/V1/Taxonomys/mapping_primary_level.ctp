<?php
/**
 *      URL: /api/v1/taxonomys/mappingPrimaryLevel/<id>    ---> redirect to '/en/mapping/<slugPrimaryLevel>/ '
 *           /fr/cartographie/<slugPrimaryLevel>/
 *           /en/mapping/<slugPrimaryLevel>/
 */
$this->layout = 'base';
$html_h1 = __d("Taxonomy", "Taxonomy.primaryLevelPage.H1",  h($title));
$html_title = __d("Taxonomy", "Taxonomy.primaryLevelPage.Title",  h($title));
$this->assign('title', $html_title);

$attId = "mappingPrimaryLevel$primaryId";
?>

<section id="<?php echo $attId; ?>" class="taxonomyPages taxonomyPrimaryLevelPage clearfix">
    <h1>
        <?php    echo $html_h1;   ?>
    </h1>

    <?php
        $subLlist = [];
        $htmlSubLlist = "";
        if(isset($mappingTaxons[$primaryId]['children'])){
            foreach ($mappingTaxons[$primaryId]['children'] as $subId => $subName) {
                $subSlug = $mappingTaxons[$subId]['slug'];
                $url = "$mappingBaseUrl/$primarySlug/$subSlug/$subId";
                $subLlist[] = $this->Html->link($subName, $url);
            }
            echo  $this->Html->nestedList($subLlist);
        }
    ?>
</section>

