<?php
/**
 *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
 *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
 *           /api/v1/taxonomys/mappingTaxon/<id>       ---> disable ---> redirect to /en/mapping/
 */
$this->layout = 'base';
$html_h1 = __d("Taxonomy", "Taxonomy.taxonPage.H1",  h($title));
$html_title = __d("Taxonomy", "Taxonomy.taxonPage.Title",  h($title));
$this->assign('title', $html_title);

$attId = "mappingTaxon$taxonId";
?>
<section id="<?php echo $attId; ?>" class="taxonomyPages taxonomyTaxonPage clearfix">
    <h1>
        <?php  echo $html_h1; ?>
    </h1>

<?php
        if(!is_array($data) || count($data) === 0) {
            echo __d("Taxonomy", "Taxonomy.taxonPage.noSoftwareMsg");
        } else {
            $html = '';
            foreach ($data AS $softwareId => $softwareTaxon) {
                $software = $softwareTaxon['software'];
                $softwareName = $software->softwarename;
                $softwareSlug = $software->slug_name;
                $url = "$baseUrl/$softwareSlug/$taxonId.$softwareId";
                $attId = "software$softwareId"."-forTaxon$taxonId";
                $usersHtml = __d("Taxonomy", "Taxonomy.taxonPage.preselectionMsg");
                if (isset($softwareTaxon['users'])) {
                    $numberOfUsers = count($softwareTaxon['users']);
                    $userNames = [];
                    foreach($softwareTaxon['users'] as $userId => $user) {
                        $userNames[] = $user->username;
                    }
                    $linkTitle = __d("Taxonomy", "Taxonomy.taxonPage.Link.seeAllUser.title", $softwareName);
                    $linkSeeMoreUsers = $this->Html->link(
                        __d("Taxonomy", "Taxonomy.taxonPage.Link.seeAllUser"),
                         $url,
                         [
                             'class' => 'btn btn-default btn-info addmore',
                             'id' => "link-$attId",
                             'title' => $linkTitle,
                         ]
                    );
                    $txtUserNb = __d("Taxonomy", "Taxonomy.taxonPage.nbOfRegisteredUsers", $numberOfUsers);
                    $usersHtml = "$txtUserNb<br> - "
                                  . implode('<br> - ', $userNames)
                                  . "$linkSeeMoreUsers";
                }

                $htmlSoftwareImg = $this->Software->displayLogoLink($software);
                $htmlSoftware = "<div>
                                        <strong>$softwareName</strong>
                                  </div>
                                  <div class=\"size-logo\">
                                        $htmlSoftwareImg
                                  </div>";
                $html .= "<tr id=\"item-$attId\" class=\"item-softwareForTaxon$taxonId\">
                                <td class=\".col-lg-4\">$htmlSoftware</td>
                                <td>$usersHtml</td>
                           </tr>";
            }
////////////////////////////////////////////////////////////////
 ?>
    <table id="sofwareListForTaxon<?php echo $taxonId;?>" class="table table-striped" style="margin-top: 2.5em; max-width: 800px">
        <thead>
            <tr>
                <th scope="col">
                    <?= __d("Taxonomy", "Taxonomy.taxonPage.tableCol.software"); ?>
                </th>
                <th scope="col">
                    <?= __d("Taxonomy", "Taxonomy.taxonPage.tableCol.registeredUsers"); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php  echo $html; ?>
        </tbody>
    </table>
<?php
////////////////////////////////////////////////////////////////
}
?>
</section>




