<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $softwaresTag->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $softwaresTag->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Softwares Tags'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="softwaresTags form large-9 medium-8 columns content">
    <?= $this->Form->create($softwaresTag) ?>
    <fieldset>
        <legend><?= __('Edit Softwares Tag') ?></legend>
        <?php
            echo $this->Form->input('software_id', ['options' => $softwares]);
            echo $this->Form->input('tag_id', ['options' => $tags]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
