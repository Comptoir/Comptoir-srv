<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Screenshot'), ['action' => 'edit', $screenshot->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Screenshot'), ['action' => 'delete', $screenshot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $screenshot->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Screenshots'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Screenshot'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="screenshots view large-9 medium-8 columns content">
    <h3><?= h($screenshot->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Software') ?></th>
            <td><?= $screenshot->has('software') ? $this->Html->link($screenshot->software->id, ['controller' => 'Softwares', 'action' => 'view', $screenshot->software->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Photo') ?></th>
            <td><?= h($screenshot->photo) ?></td>
        </tr>
        <tr>
            <th><?= __('Url Directory') ?></th>
            <td><?= h($screenshot->url_directory) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($screenshot->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($screenshot->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($screenshot->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Name') ?></h4>
        <?= $this->Text->autoParagraph(h($screenshot->name)); ?>
    </div>
</div>
