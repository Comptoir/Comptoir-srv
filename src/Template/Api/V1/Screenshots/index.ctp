<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Screenshot'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="screenshots index large-9 medium-8 columns content">
    <h3><?= __('Screenshots') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('software_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('photo') ?></th>
                <th><?= $this->Paginator->sort('url_directory') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($screenshots as $screenshot): ?>
            <tr>
                <td><?= $this->Number->format($screenshot->id) ?></td>
                <td><?= $screenshot->has('software') ? $this->Html->link($screenshot->software->id, ['controller' => 'Softwares', 'action' => 'view', $screenshot->software->id]) : '' ?></td>
                <td><?= h($screenshot->created) ?></td>
                <td><?= h($screenshot->modified) ?></td>
                <td><?= h($screenshot->photo) ?></td>
                <td><?= h($screenshot->url_directory) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $screenshot->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $screenshot->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $screenshot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $screenshot->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
