<?php
    $this->layout = 'base';
    $this->assign('title', __d("Users",'Users.index.title'));

    // View debug json only for connected user with role = admin
    if ($this->request->session()->read('Auth.User.role') === 'admin' ) {
        if (isset($exportJson) && isset($exportCsv)) {
            echo "<h1 id=\"users-export_displayed\">Export</h1>";
            echo "export JSON : <a id=\"link_users-export_displayed\" href=\"/$exportJsonFilePath\">$exportJsonFileName</a><br>";
            echo "export CSV : <a id=\"link_users-export_displayed\" href=\"/$exportCsvFilePath\">$exportCsvFileName</a><hr>";
            echo "<h1 id=\"users-export-json_displayed\">Export CSV</h1>";
            echo "<pre>$exportCsv</pre><hr>";
            echo "<h1 id=\"users-export-csv_displayed\">Export JSON</h1>";
            echo "<pre>$exportJson</pre><hr>";
        }
        else {
            echo "<h1 id=\"error_users-export_displayed\">Error: export not available</h1>";
        }
    }
    else {
        if (isset($exportJson) || isset($exportCsv)) {
            echo '<div id="users-export_not-displayed"></div>';
        }
        else {
            echo '<div id="error_users-export_not-displayed"></div>';
        }
    }
?>

