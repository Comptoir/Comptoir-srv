<?php

$this->layout("base");

$myTotalUsers = !empty($users) ? count($users) : '0';
$this->assign('title', __d("Users", "providersPage.TITLE", $myTotalUsers));

?>

<?php if (!empty($users)): ?>


    <section>

        <?php

        echo $this->Lists->block($users,
            [
                "type" => "user",
                "title" => "<h1>"
                    . __d("Users", "providersPage.H1", $myTotalUsers)
                    . "</h1>"
                    . "<div class=\"subtitle\">"
                        . "<p>"
                            . __d("Users", "providersPage.subtitle", $myTotalUsers)
                        ."</p>"
                    ."</div>",
                "linkParticipate" => isset($linkParticipate["linkParticipate"]) ? $linkParticipate["linkParticipate"] : null,
                "titleAddMore" => "" ,
            ],
            false
        ); ?>
    </section>

<?php endif; ?>
