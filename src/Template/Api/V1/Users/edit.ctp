<?php
$this->layout("base");
$this->assign('title', __d("Users","Edit your account : ", $user->username));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->Form->create($user, ['type' => 'file', 'id' => "editInformationAccountForm", 'url' => ['prefix' => false, 'controller' => 'Users', 'action' => 'edit', isset($user->id) ? $user->id : null]]) ?>
        <fieldset>
            <legend><?= "<h1>" .  __d("Forms",'Edit your account') . "</h1>"  ?></legend>
            <div  class = "form-group">
            <?= $this->Form->input('username',
                [
                    "class"=>"form-control",
                    'label'=>["class"=>"control-label","text"=>__d("Forms", "{0} Name: ",'<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "type"=>"text",
                    "escape" => false
                ]);?>

            <?= $this->Form->input('url',
                [
                    "class"=>"form-control",
                    "label" => ["class"=>"control-label","text"=>__d("Forms", "Website URL: ")],
                    'type'=>'url',
                    "escape" => false
                ]);?>

            <?= $this->Form->input('email',
                [
                    "class"=>"form-control",
                    "label" => ["class"=>"control-label","text"=>__d("Forms","{0} Email: ", '<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "escape"=>false,
                ]) ?>

            <div class='form-group'>
                <?= $this->Form->input('description',["type"=>"textarea","class"=>"form-control","label"=>__d("Forms","Description : ")]); ?>
            </div>

            <?= $this->Form->input(
                'photo',
                [
                    'type' => 'file',
                    "label" => ["class"=>"control-label","text"=>__d("Forms", " {0} Avatar: ", '') ],
                    "escape"=>false,

                ]) ?>

            <?php $help = '<div class="help-block">
                        <ul>';
            !isset($user->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
            !isset($user->photo->fileBelowMaxWidth) || !isset($user->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
            !isset($user->photo->fileBelowMaxSize) ? $help .=  '<li>'.__d("Forms","Maximum weight: 1{0}.",__d('Forms',"<abbr title='Megabit'>MB</abbr>")).'</li>' : "";
            $help .='</ul>
                    </div>';
            echo $help;
            ?>

        <?= $this->Form->button(__d("Forms","Save"),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->Form->end() ?>

        <?=
            $this->Html->link(__d("Users","Change password"),
            ["controller"=> "Users", "action" => "changePassword",$user->id],
            ['escape' => false])
        ?>
        </fieldset>
    </div>
</div>
