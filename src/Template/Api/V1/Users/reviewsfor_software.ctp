<?php

$this->layout('base');

$titleAndH1 = $user->username .' -  '.  __d("Users", "Users.View.Reviews.Title", count($user->reviews));

$this->assign('title', $titleAndH1);


?>
<section>
    <?php

    echo $this->Lists->block(
        $user->reviews,
        [
            "type" => "review",
            "title" => "<h1>" .  $titleAndH1  ."</h1>",
        ],
        false
    ); ?>
</section>
