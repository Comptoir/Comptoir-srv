<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Taxonomys Software'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxonomys'), ['controller' => 'Taxonomys', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxonomy'), ['controller' => 'Taxonomys', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="taxonomysSoftwares index large-9 medium-8 columns content">
    <h3><?= __('Taxonomys Softwares') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('taxonomy_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('software_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
                <th scope="col">Comment</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($taxonomysSoftwares as $taxonomysSoftware): ?>
            <tr>
                <td><?= $this->Number->format($taxonomysSoftware->id) ?></td>
                <td>
                    <?= $taxonomysSoftware->has('taxonomy') ? $this->Html->link(
                            $taxonomysSoftware->taxonomy->id .' -  '. $this->Text->truncate(
                                $taxonomysSoftware->taxonomy->title_i18n_en,
                                35,
                                ['ellipsis' => '...',
                                'exact' => false]
                            ),
                            ['controller' => 'Taxonomys', 'action' => 'view', $taxonomysSoftware->taxonomy->id]
                    ) : '' ?>
                </td>
                <td><?= $taxonomysSoftware->has('software') ? $this->Html->link($taxonomysSoftware->software->softwarename, ['controller' => 'Softwares', 'action' => 'view', $taxonomysSoftware->software->id]) : '' ?></td>
                <td><?= $taxonomysSoftware->has('user') ? $this->Html->link($taxonomysSoftware->user->username, ['controller' => 'Users', 'action' => 'view', $taxonomysSoftware->user->id]) : '' ?></td>
                <td><?= h($taxonomysSoftware->created) ?></td>
                <td><?= h($taxonomysSoftware->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $taxonomysSoftware->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $taxonomysSoftware->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $taxonomysSoftware->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxonomysSoftware->id)]) ?>
                </td>
                <td><?= $this->Text->truncate(h($taxonomysSoftware->comment), 55, ['ellipsis' => '…', 'exact' => false]);  ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
