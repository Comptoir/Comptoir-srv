<?php
    // This template is used all the time (even on error pages).
    // But, on error pages, the $selectedLanguage variable is not available.
    // We force its creation by using a translation string that is always available.
    if (!isset($selectedLanguage)) {
        $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
    }

    // This template is used all the time (even on error pages).
    // But, on error pages, the $appVersion variable is not available.
    // We force its creation that is always available.
    if (!isset($appVersion)) {
        $appVersion = Cake\Core\Configure::read("VERSION.footer");
    }

    // This template is used all the time (even on error pages).
    // But, on error pages, the $mappingBaseUrl variable is not available.
    // We force its creation that is always available.
    if (!isset($mappingBaseUrl)) {
        $mappingBaseUrl= '/en/mapping/';
        if($selectedLanguage === 'fr') {
            $mappingBaseUrl = '/fr/cartographie/';
        }
    }
?>

<header class="navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <?php if ($this->isHere("pages", "index")): ?>
                    <span class="navbar-brand cdl-navbar-logo ">
                    <?= "<h1 class = 'cdl-navbar-logo'> ". $this->Html->image(
                        "logos/Logo-CDL_kranken.io-lossless.png?$appVersion",
                        ["alt" => __d("ElementNavigation", "Comptoir du libre")]) . "</h1>"?>
                    </span>
                <?php elseif ($this->request->here != $this->request->base): ?>
                    <span class="navbar-brand cdl-navbar-logo-link">
                    <?=  $this->Html->link(
                        $this->Html->image("logos/Logo-CDL_kranken.io-lossless.png?$appVersion",
                            ["alt" => __d("ElementNavigation", "Comptoir du libre - back home")]),
                        "/$selectedLanguage/",
                        ['escape' => false, "class" => "cdl-navbar-logo-link"]) ?>
                    </span>

                <?php endif; ?>
            </div>

            <!-- /!\ without id = no button working -->
            <div id="navbar" class="navbar-collapse collapse">
                <nav role = "navigation" class="row">

                    <?php // ===== main navigation bar ============================================================== ?>

                    <ul class="nav navbar-nav navbar-left nav-pull-down" id="navbar-mainMenu">

                        <?php if ($this->isHere("Softwares")): ?>
                    <li class="current">
                    <span class=" text-center"><?= __d("ElementNavigation", "All softwares") ?></span>
                    <?php elseif (!$this->isHere("Softwares")): ?>
                        <li class="">
                            <?= $this->Html->link(__d("ElementNavigation", "All softwares"),
                                [
                                    'language' => $selectedLanguage,
                                    'prefix'=>false,
                                    'controller'=>'softwares'
                                ],
                                ['escape' => false, "id" => "softwaresPage"]) ?>
                            <?php endif; ?>
                        </li>

                        <?php if ($this->isHere("Users")): ?>
                    <li class="current">
                    <span class=" text-center"><?= __d("ElementNavigation", "All users") ?></span>
                    <?php elseif (!$this->isHere("Users")): ?>
                        <li class="">
                            <?= $this->Html->link(__d("ElementNavigation", "All users"),
                                [
                                    'language' => $selectedLanguage,
                                    'prefix' => false,
                                    'controller' => 'users',
                                    'action' => 'index'
                                ],
                                ['escape' => false, "id" => "usersPage"]) ?>
                            <?php endif; ?>
                        </li>

                        <?php if ($this->isHere("Users", "providers")): ?>
                        <li class="current">
                            <span class=" text-center"><?= __d("ElementNavigation", "Services providers") ?></span>
                            <?php elseif (!$this->isHere("Users", "providers")): ?>
                        <li class="">
                            <?= $this->Html->link(__d("ElementNavigation", "Services providers"),
                                [
                                    'language' => $selectedLanguage,
                                    'prefix' => false,
                                    'controller' => 'users',
                                    'action' => 'providers'
                                ],
                                ['escape' => false, "id" => "ServicesProvidersPage"]) ?>
                            <?php endif; ?>
                        </li>

                        <?php
                        $txtMapping = __d("ElementNavigation", "navbar.mapping") ;
                        ?>
                        <?php if ($this->isHere("Taxonomys", 'mapping')): ?>
                            <li class="current">
                                <span class=" text-center"><?= __d("ElementNavigation", "navbar.mapping") ?></span>
                            </li>
                        <?php else: ?>
                            <li> <?= $this->Html->link($txtMapping,  $mappingBaseUrl, ["id" => "mappingPage"]) ?></li>
                        <?php endif; ?>

                        <?php if ($this->isHere("tags")): ?>
                        <li class="current">
                            <span class=" text-center"><?= __d("ElementNavigation", "Tags") ?></span>
                            <?php elseif (!$this->isHere("tags")): ?>
                        <li class="">
                            <?= $this->Html->link(__d("ElementNavigation", "Tags"),
                                [
                                    'language' => $selectedLanguage,
                                    'prefix' => false,
                                    'controller' => 'tags',
                                    'action' => 'index'
                                ],
                                ['escape' => false, "id" => "tagsPage"]) ?>
                            <?php endif; ?>
                        </li>

                    </ul>

                    <?php // ===== auth + search ================================================================= ?>
                    <?php if (!$this->request->session()->read('Auth.User.username')) : ?>
                        <ul class = " nav navbar-nav navbar-right nav-pull-down-right">
                            <li class="mainSearchElement">
                                <?= $this->element("Pages/SearchForm") ?>
                            </li>
                            <li>
                                <?= $this->Html->link(__d("Layout", "Sign up"),
                                    [
                                        'language' => $selectedLanguage,
                                        "prefix"=>false,
                                        'controller' => 'Users',
                                        'action' => 'add'
                                    ],
                                    [
    //                                'class'=>"btn btn-default",
                                        'escape' => false]) ?>
                            </li>
                            <li class="clean-nav">
                                <?= $this->Html->link(__d("Layout", "Sign in"),
                                    [
                                        'language' => $selectedLanguage,
                                        "prefix"=>false,
                                        'controller' => 'Users',
                                        'action' => 'login'],
                                    [
    //                                'class'=>"btn btn-default",
                                        'escape' => false]) ?>
                            </li>
                        </ul>
                    <?php elseif ($this->request->session()->read('Auth.User.username')) : ?>
                        <ul class = "nav navbar-nav navbar-right nav-pull-down-right">
                            <li class="mainSearchElement">
                                <?= $this->element("Pages/SearchForm") ?>
                            </li>
                            <li class="clean-nav" id="userProfil-nav">
                                <?= $this->Html->link($this->request->session()->read('Auth.User.username').'<span class ="caret"></span>',
                                    ["prefix"=>false,'controller' => 'Users', 'action' => $this->request->session()->read('Auth.User.id')],
                                    [
//                                    'class'=>"btn btn-default",
                                        'escape' => false]) ?>
                                <ul class = "">
                                    <li class="">
                                        <?=  $this->Html->link(
                                                __d("Layout","nav.profile"),
                                                ["prefix"=>false,'controller' => 'Users', 'action' => $this->request->session()->read('Auth.User.id')],
                                                ['class'=>"",'escape' => false]
                                        );
                                        ?>
                                    </li>
                                    <li class="">
                                        <?= $this->Html->link(
                                            __d("Layout","Profile setting"),
                                            ["prefix"=>false,'controller' => 'Users', 'action' => 'edit',$this->request->session()->read('Auth.User.id')],
                                            ['class'=>"",'escape' => false]) ?>
                                    </li>
                                    <li class="__item" id="nav_logout_item">
                                        <?= $this->Html->link(__d("Layout", "Logout"),
                                            ["prefix"=>false,'controller' => 'Users', 'action' => 'logout'],
                                            ['class'=>"",'escape' => false]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    <?php endif; ?>
                </nav>
            </div>
        </div>
        </div>
</header>
