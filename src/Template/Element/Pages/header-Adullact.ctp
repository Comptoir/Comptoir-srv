<?php
    // This template is used all the time (even on error pages).
    // But, on error pages, the $appVersion variable is not available.
    // We force its creation that is always available.
    if (!isset($appVersion)) {
        $appVersion = Cake\Core\Configure::read("VERSION.footer");
    }
?>
<div class="container-fluid" id="parentship">
    <div class="row">
        <div class="col-sm-12">
            <span>
                <?= __d("Pages","Paternity.message")?>
            </span>
            <?= $this->Html->link(
                $this->Html->image("logos/Logo_adullact_trunked_kraken.io-lossless.png?$appVersion",
                    ["alt" => "Adullact","id" => "parentship-logo"]),
                "https://adullact.org",
                ['escape' => false ,"target"=>"_blank", 'rel' => 'noopener']
            ) ?>
        </div>
    </div>
</div>

