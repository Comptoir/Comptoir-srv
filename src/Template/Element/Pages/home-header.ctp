<div class="jumbotron home-header">
    <div class="container">
        <h1>
            <?= $this->Html->image(
                "logos/logo_ADULLACT_bgTransp_w100_h33_modif.png",
                ["alt" => __d("Home", "Comptoir du Libre")])?>

            <?= __d(
                "Home",
                "Comptoir du Libre")?>
        </h1>
            <?php if (!$this->request->session()->read('Auth.User.username')) : ?>

                <?= $this->Html->link(__d("Layout", "Sign up"),
                    ['controller' => 'Users', 'action' => 'add'],
                    [
                        'class'=>"btn btn-primary",
                        'escape' => false]) ?>

                <?= $this->Html->link(__d("Layout", "Sign in"),
                    ['controller' => 'Users', 'action' => 'login'],
                    [
                        'class'=>"btn btn-primary",
                        'escape' => false]) ?>

            <?php endif ; ?>
    </div>
</div>
