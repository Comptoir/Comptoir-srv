<?php

/**
 * This template contains all information for the footer
 */

    // This template is used all the time (even on error pages).
    // But, on error pages, the $selectedLanguage variable is not available.
    // We force its creation by using a translation string that is always available.
    if (!isset($selectedLanguage)) {
        $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
    }

    // This template is used all the time (even on error pages).
    // But, on error pages, the $appVersion variable is not available.
    // We force its creation that is always available.
    if (!isset($appVersion)) {
        $appVersion = Cake\Core\Configure::read("VERSION.footer");
    }


    // Set options to footer links
    $optionLegalLink = [];
    $optionA11yLink = [];
    $optionContactLink = [];
    $optionOpendataLink = [];
    $optionOpendataLink['title']  = __d("ElementNavigation", "footer.opendata.link.title");
    $optionLink = ['aria-current' => 'page'];
    if(isset($currentPage)){
        if($currentPage === 'page_legal'){
            $optionLegalLink = $optionLink;
        } elseif ($currentPage === 'page_accessibility'){
            $optionA11yLink  = $optionLink;
        } elseif ($currentPage === 'page_contact'){
            $optionContactLink = $optionLink;
        } elseif ($currentPage === 'page_opendata'){
            $optionOpendataLink =  array_merge($optionOpendataLink, $optionLink);
        }
    }
?>
<footer class="row">
    <div>
        <?= $this->Html->link(__d("ElementNavigation", "Contact"),
            "/$selectedLanguage/pages/contact",
            $optionContactLink) ?> -

        <?= $this->Html->link(__d("Home", "Legal"),
            "/$selectedLanguage/pages/legal",
            $optionLegalLink) ?> -

        <?= $this->Html->link(__d("Home", "footer-accessibility-link"),
            "/$selectedLanguage/pages/accessibility",
            $optionA11yLink) ?> -

        <?= $this->Html->link(__d("ElementNavigation", "footer.opendata.link"),
            "/$selectedLanguage/pages/opendata",
            $optionOpendataLink) ?>

        <?php if (isset($availableLanguages)): ?>
            -
            <?php foreach($availableLanguages as $lang => $alias): ?>
                <?php if ($alias === $selectedLanguage) continue; ?>

                <span lang="<?php echo $lang; ?>">
                    <?php
                        // Lien vers la page d'accueil du site web en anglais
                        // si la page courante est en français (et inversement)
                        ////////////////////////////////////////////////////////
                        echo $this->Html->link(
                            ucfirst($explicitLanguages[$lang]),
                            "/$lang/",
                            [
                                "hreflang" => $lang,
                                "title" => __d("Pages","menu.switch")
                            ]
                        );

                        // Ancienne version problèmatique
                        // ---> lien vers la page équivalente en anglais (ou en français)
                        // ---> problème : beaucoup d'URL en erreur ou URL non canonique
                        ////////////////////////////////////////////////////////////////
                        //    $pass = $this->request->data('search');
                        //    echo $this->Html->link(ucfirst($explicitLanguages[$lang]), [
                        //        'language' => $lang,
                        //        'controller' => $this->request->param('controller'),
                        //        'action' => $this->request->param('action'),
                        //        $this->request->param('id'),
                        //        "prefix"=>false,
                        //        "?"=> isset($pass) ? ['search'=> $pass ]: null
                        //    ],["lang"=>$lang, "title"=> __d("Pages","menu.switch")]);
                     ?>
                </span>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <div>
        <?= $this->Html->link(
                $this->Html->image("logos/Logo_FEDER_kraken.io-lossy.png?$appVersion",[
                    "alt" => "Site web du 'Fonds Européen de Développement Régional' de la région Occitanie",
                    "id" => "Feder-logo",
                    "loading" => "lazy"
                ]),
                "https://www.europe-en-occitanie.eu/Fonds-et-programmes-europeens-en-bref",
                [
                    'escape' => false,
                    "target" => "_blank",
                    'rel' => 'nofollow noopener noreferrer'
                ]) ?>
    </div>
    <div class="footer-version">
        <?=  __d("Pages", "Comptoir du Libre version ") ?>
        <?= $this->Html->link( $appVersion,
            'https://gitlab.adullact.net/Comptoir/Comptoir-srv/blob/main/CHANGELOG.md',
            [
                'escape' => false,
                "target" => "_blank",
                'rel' => 'nofollow noopener noreferrer'
            ])
        ?>
    </div>
</footer>

