<?php
// This template is used all the time (even on error pages).
// But, on error pages, the $selectedLanguage variable is not available.
// We force its creation by using a translation string that is always available.
if (!isset($selectedLanguage)) {
    $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
}
?>
<?= $this->Form->create(null, ['url' =>
    [
        'language' => $selectedLanguage,
        'prefix' => false,
        'controller' => "Pages",
        'action' => "search"
    ],
    'class' => "form-inline", 'id' => 'navbar-search']) ?>

        <div class="input-group">
            <?= $this->Form->input('search', [
                'title' => 'search',
                'label' => false,
                "type" => "text",
                "maxLength" => 64,
                "required"  => "required",
                "class" => "form-control"]) ?>
            <div class="input-group-btn">
                <?= $this->Form->button(__d("ElementNavigation", "Search"), ['class' => 'btn btn-default submit-form search']) ?>
            </div>
        </div>

<?= $this->Form->end() ?>
