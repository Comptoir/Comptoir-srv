<?= $this->Form->create(null, ['class' => "form-inline", 'type' => 'get']) ?>

<?php

echo '<div class="input-group">';
echo '<label class = "control-label" for="userFilter_isUserOf">' . __d('Forms', "Search.Label.Users.isUserOf") . '</label>';
echo $this->Form->select('isUserOf', $isUserOf, ["id" => "userFilter_isUserOf", "name" => "isUserOf", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="userFilter_hadReview">' . __d('Forms', "Search.Label.Users.hadReview") . '</label>';
echo $this->Form->select('hadReview', $hadReview, ["id" => "userFilter_hadReview", 'name'=>'hadReview',"class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="userFilter_IsSerivicesProvider">' . __d('Forms', "Search.Label.Users.IsSerivicesProvider") . '</label>';
echo $this->Form->select('IsSerivicesProvider', $isServiceProvider, ["id" => "userFilter_IsSerivicesProvider", "name" => "IsSerivicesProvider", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="userFilter_user_type">' . __d('Forms', "Search.Label.Users.userType") . '</label>';
echo $this->Form->select('userType', $userType, ["id" => "userFilter_user_type", "name"=>'userType',"class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="userFilter_order">' . __d('Forms', "Search.Label.Sort") . '</label>';
echo $this->Form->select('order', $order, ["id" => "userFilter_order", "class" => 'form-control']);
echo '</div>';

?>

<div class="input-group">
    <?= $this->Form->input('search', [
        'id'    => 'userFilter',
        'title' => 'search',
        'label' => false,
        "type" => "text",
        "class" => "hidden"]) ?>
</div>

<div class="form-group">
    <?= $this->Form->button(__d("ElementNavigation", "Filter"), ['class' => 'btn btn-default submit-form filter']) ?>
</div>

<?= $this->Form->end() ?>

