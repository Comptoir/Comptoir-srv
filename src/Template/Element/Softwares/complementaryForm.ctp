<?php
    ///////////////////////////////////////////////////////////////
    // Additional form to encourage users
    // to declare themselves as a provider or user of the software.
    //
    // Used in edit software form and add software form.
    ///////////////////////////////////////////////////////////////

    // User of ?
    // Additional form to encourage users to declare themselves user of the software.
    if (isset($displayUserOfForm) && $displayUserOfForm === true) {
        // Tip: if needed we can personalize the i18n string
        // according to the type of user: $userTypeId is available here
        $legend   = __d("Forms", "software_fieldset.legend_are.you.a.user");
        $yesLabel = __d("Forms", "software_radio.label_yes.already.user");
        $noLabel  = __d("Forms", "software_radio.label_no.not.a.user");
        $formUserOf =  $this->Form->input('userOf', [
            'options' => [
                ['value' => 'yes', 'text' => $yesLabel],
                ['value' => 'no',  'text' => $noLabel],
            ],
            'type' => 'radio',
            'required' => 'required',
            'label' => false,
            'hiddenField' => false,
        ]);
        echo "<fieldset id=\"softwareForm_askUserOf\" class=\"form_fieldsetRadio\">
                    <legend>
                        <span class=\"asterisk\">*</span>
                        $legend
                    </legend>
                    $formUserOf
              </fieldset>";
    }

    // Provider ?
    // Additional form to encourage users to declare themselves as a provider.
    if (isset($displayProviderForm) && $displayProviderForm === true) {
        // Tip: if needed we can personalize the i18n string
        // according to the type of user: $userTypeId is available here
        $legend   = __d("Forms", "software_fieldset.legend_are.you.a.provider");
        $yesLabel = __d("Forms", "software_radio.label_yes.a.provider");
        $noLabel  = __d("Forms", "software_radio.label_no.not.a.provider");
        $formProvider =  $this->Form->input('providerFor', [
            'options' => [
                ['value' => 'yes', 'text' => $yesLabel],
                ['value' => 'no',  'text' => $noLabel],
            ],
            'type' => 'radio',
            'required' => 'required',
            'label' => false,
            'hiddenField' => false,
        ]);
        echo "<fieldset id=\"softwareForm_askProviderFor\" class=\"form_fieldsetRadio\">
                    <legend>
                        <span class=\"asterisk\">*</span>
                        $legend
                    </legend>
                    $formProvider
              </fieldset>";
    }
?>
