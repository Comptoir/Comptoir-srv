
<?php

$this->layout = 'json';
$this->assign('templateName', 'missing_controller');


$error = [
            "success"=> false,
            "error"=> 404,
            "message"=> [
                "Bad Request" => "Validation failed : Url not found. Try to put a correct action (request;forgeList). Please refer to the manual for a correct use.",
                "help"=> "Please check the manual at api.adullact.org/manivelle/manual"
            ]
        ];

echo json_encode($error,JSON_PRETTY_PRINT);
