<?php
/*
 * This is the base of all pages
 */

    // This template is used all the time (even on error pages).
    // But, on error pages, the $selectedLanguage variable is not available.
    // We force its creation by using a translation string that is always available.
    if (!isset($selectedLanguage)) {
        $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
    }

    // This template is used all the time (even on error pages).
    // But, on error pages, the $appVersion variable is not available.
    // We force its creation that is always available.
    if (!isset($appVersion)) {
        $appVersion = Cake\Core\Configure::read("VERSION.footer");
    }
?>

<!DOCTYPE html>
<html lang="<?= $selectedLanguage ?>">
  <?= $this->element("Pages/header") ?>
    <body class="backgroundbody">

        <?= $this->element("Pages/header-Adullact") ?>

        <?= $this->element("Navigation/navbarFixedTop") ?>
        <main>
        <div class="container-fluid" id="super-main">
            <?= $this->element("Navigation/navBreadcrumbs") ?>

                <div class="messagesContainer">
                    <?= $this->Flash->render() ?>
                </div>
                <?= $this->fetch("content")?>
            </main>
                <?= $this->element("Pages/footer") ?>
        </div>
        <?= \Cake\Core\Configure::read("Piwik") ?>
    </body>
</html>
