<?php
// Dans src/Error/AppError.php
namespace App\Error;

use Cake\Error\BaseErrorHandler;
use Cake\Error\Debugger;
use Cake\Routing\Exception\MissingControllerException;

/**
 * Try to maange errors occurred in the core
 */
class AppError extends BaseErrorHandler
{
    public function _displayError($error, $debug)
    {
        return $error;
    }

    public function _displayException($exception)
    {
        if ($exception instanceof MissingControllerException) {
            debug($exception);
            Debugger::log($exception);
            $message = ['400' => 'bar'];
            //parent::_displayException($exception);
        } elseif ($exception instanceof InternalErrorException) {
            $error = ['500' => 'bar'];
            debug($exception);
        } else {
            //debug($exception->getCode());die;
            parent::_displayException($exception);
        }
    }
}
