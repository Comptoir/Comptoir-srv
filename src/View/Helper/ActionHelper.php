<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

/**
 * Class ActionHelper
 * Build forms needed to participate on Comptoir du libre.
 *
 * @package App\View\Helper
 */
class ActionHelper extends Helper
{

    use StringTemplateTrait;

    public $helpers = ['Html', 'Text', 'Time', 'Form'];


    private function hasAccess($type, $controller, $action)
    {
        return Configure::read("ACL.$controller.$action.$type");
    }

    /**
     * Create form with one button
     *
     * @param $formMethod
     * @param $controller
     * @param $action
     * @param $id
     * @param $btnTxt
     * @param $btnClass
     * @param $formUrl
     * @param string $btnExtraClass  default = ''
     * @param bool $bypassCheckId    default = false
     * @return string|null
     */
    private function communBtn(
        $formMethod,
        $controller,
        $action,
        $id,
        $btnTxt,
        $btnClass,
        $formUrl,
        $btnExtraClass = '',
        $bypassCheckId = false
    ) {
        $userType = $this->request->session()->read('Auth.User.user_type');
        if ($userType === null) {
            $userType = "Unknown";
        }

        $htmlId = "$controller-$action-$id";
        $hasAccess = $this->hasAccess($userType, $controller, $action);
        if ((!is_null($id) || $bypassCheckId) && $hasAccess) {
            $btnCssClass = $btnClass . ' ' . $btnExtraClass;
            $formOptions =  [
                "url" => $formUrl,
                "class" => "addmore-form",
                "type" => $formMethod,
                "id" => "Form_$htmlId",
            ];
            $html = '';
            $html .= $this->Form->create(null, $formOptions);
            $html .= $this->Form->button($btnTxt, ["class" => $btnCssClass, "id" => "btn_$htmlId",]);
            $html .= $this->Form->end();
            return $html;
        } else {
            return null;
        }
    }


    /**
     * Edit button:
     * - edit taxonomySoftware
     *
     * @param array $options
     * @return string|null
     */
    public function edit(array $options)
    {
        $btnTxt = isset($options["text"]) ? $options["text"] : "Edit";
        $btnClass = isset($options["class"]) ? $options['class'] : "btn btn-default addmore btn-edit";
        $formMethod = isset($options["method"]) ? $options["method"] : "post"; // default post method
        $controller = isset($options["controller"]) ? $options["controller"] : $this->request->controller;
        $action = isset($options["action"]) ? $options["action"] : 'edit';
        $id = isset($options["id"]) ? $options["id"] : null;
        if (isset($options["url"])) {
            $formUrl = $options["url"];
        } else {
            $formUrl = ['prefix' => false, 'controller' => $controller, 'action' => $action, $id];
        }
        return $this->communBtn($formMethod, $controller, $action, $id, $btnTxt, $btnClass, $formUrl);
    }

    /**
     * Undo button:
     * - no longer being a user
     * - no longer being a provider
     *
     * @param array $options
     */
    public function fallBack(array $options)
    {
        $btnTxt = isset($options["text"]) ? $options["text"] : "Delete";
        $btnClass = isset($options["class"]) ? $options['class'] : "btn btn-default removeOne";
        $formMethod = isset($options["method"]) ? $options["method"] : "delete"; // default post method
        $controller = isset($options["controller"]) ? $options["controller"] : $this->request->controller;
        $action = isset($options["action"]) ? $options["action"] : null;
        $id = isset($options["id"]) ? $options["id"] : null;
        if (isset($options["url"])) {
            $formUrl = $options["url"];
        } else {
            $formUrl = ['prefix' => false, 'controller' => $controller, 'action' => $action, $id];
        }
        return $this->communBtn($formMethod, $controller, $action, $id, $btnTxt, $btnClass, $formUrl);
    }

    /**
     * Participate button:
     * - declare as user
     * - declare as provider
     * - Testify
     *
     * @param array $options
     * @return mixed
     */
    public function participate(array $options)
    {
        $btnTxt = isset($options["text"]) ? $options["text"] : null;
        $btnClass = isset($options["class"]) ? $options['class'] : "btn btn-default addmore";
        $btnExtraClass = isset($options["extraCssClass"]) ? $options["extraCssClass"] : "";
        $formMethod = isset($options["method"]) ? $options["method"] : "post"; // default post method
        $ctrl = isset($options["controller"]) ? $options["controller"] : $this->request->controller;
        $action = isset($options["action"]) ? $options["action"] : "add";
        $id = isset($options["id"]) ? $options["id"] : null;
        if (isset($options["url"])) {
            $formUrl = $options["url"];
        } else {
            $formUrl = ['prefix' => false, 'controller' => $ctrl, 'action' => $action, $id];
        }

        $bypassTestId = false;
        if ($ctrl === 'Softwares' && $action === 'add') {
            // prevent test on ID (null equals) performed into communBtn() for this case
            $bypassTestId = true;
        }
        $html = $this->communBtn($formMethod, $ctrl, $action, $id, $btnTxt, $btnClass, $formUrl, '', $bypassTestId);
        if (is_null($html) && !is_null($btnTxt)) {
            $htmlId = "$ctrl-$action-$id";
            $html = $this->Form->button(
                $btnTxt,
                [
                    "class" => "btn btn-default inactive-button " . $btnExtraClass,
                    "disabled" => "disabled",
                    "id" => "btnDisabled_$htmlId",
                ]
            );
        }
        return $html;
    }
}
