<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\StringTemplateTrait;

/**
 * Class TagHelper
 * Build the Html structure for the Tag item
 *
 * @package App\View\Helper
 */
class TagHelper extends ListItemHelper
{

    use StringTemplateTrait;

    public $helpers = ['Url'];
    protected $_defaultConfig = [
        'templates' => [
            'tag' => '
                <li {{attribsListItem}}>
                    <a {{attribsHref}}>
                       <div {{attribtItem}}>
                           {{tagName}}
                       </div>
                    </a>

                    <span>
                      {{tagNumber}}
                   </span>
               </li>
           ',
        ]
    ];

    /**
     * List of Software's tag in SoftwareBlock
     * display "..." when number of tags is more than the limit
     * display "tagNumber" only when the option is true.
     *
     * @param  $tag
     * @param bool $limit
     * @return null|string
     */
    public function display($tags, $options = null)
    {
        // This helper is used all the time.
        // But, the $selectedLanguage variable is not available.
        // We force its creation by using a translation string that is always available.
        if (!isset($selectedLanguage)) {
            $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
        }

        $result = null;
        if (!empty($tags)) {
            $displayTags = isset($options["limit"]) ? array_slice(
                $tags,
                0,
                Configure::read("Softwares.tags.limit")
            ) : $tags;
            foreach ($displayTags as $tag) {
                $result .= $this->formatTemplate(
                    'tag',
                    [
                        "attribsHref" => $this->templater()->formatAttributes(
                            [
                                'href' => $this->Url->build(
                                    [
                                        'language' => $selectedLanguage,
                                        'prefix' => false,
                                        'controller' => 'Tags',
                                        'action' => $tag->id,
                                        'software'
                                    ]
                                )
                            ]
                        ),
                        "tagName" => $tag->name,
                        "tagNumber" => (isset($options["displayNumber"]) && $options["displayNumber"] == true)
                            ? ("x ") . $tag->usedTagNumber
                            : null,
                        "attribsListItem" => $this
                            ->templater()
                            ->formatAttributes(
                                [
                                    'class' => isset($options["class"])
                                        ? $options["class"]
                                        : ['class' => 'tagsContainer']
                                ]
                            ),
                        'attribtItem' => $this->templater()->formatAttributes(['class' => 'tagUnit']),
                    ]
                );
            }
        }
        if (count($tags) > Configure::read("Softwares.tags.limit") && isset($options["limit"])) {
            $result .= $this->Html->tag('li', "...", ['class' => 'tagUnit']);
        }
        return null !== $result ? $result : null;
    }

    /**
     * Return a link with containing the user'logo if it get one, placeholder otherwise
     *
     * @param  $user
     * @return mixed
     */
    public function displayLogoLink($user)
    {

        return !$this->hasLogo($user) ?

            $this->Html->link(
                $this->Html->image(
                    "logos/User_placeholder.jpg",
                    ["alt" => __d("Users", "Go to the {0}'s page", $user->username), "class" => "img-responsive"]
                ),
                ['prefix' => false, 'controller' => 'Users', 'action' => $user->id],
                ['escape' => false]
            )
            :
            $this->Html->link(
                $this->Html->image(
                    $user->logo_directory . DS . $user->photo,
                    ["alt" => __d("Users", "Go to the {0}'s page", $user->username), "class" => "img-responsive"]
                ),
                ['prefix' => false, 'controller' => 'Users', 'action' => $user->id],
                ['escape' => false]
            );
    }
}
