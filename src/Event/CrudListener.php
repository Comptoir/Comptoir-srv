<?php
namespace App\Event;

use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;

class CrudListener implements EventListenerInterface
{
    use MailerAwareTrait;


    public function implementedEvents()
    {
        return
            [
                'Model.Software.created' => 'updateSoftwareLog',
                'Model.Software.modified' => 'updateSoftwareLog',
                'Model.User.created' => 'updateUserLog',
                'Model.User.modified' => 'updateUserLog',
                'Model.Review.created' => 'updateReviewLog',
                'Model.Screenshot.created' => 'updateScreenshotLog',
                'Model.RelationshipSoftwareUser.created' => 'updateRelationshipSoftwareUserLog',
                'Model.RelationshipSoftwareUser.deleted' => 'updateRelationshipSoftwareUserLog',
                'Model.TaxonomySoftware.created' => 'updateTaxonomySoftwareLog',
                'Model.TaxonomySoftware.modified' => 'updateTaxonomySoftwareLog',
                'Model.TaxonomySoftware.deleted' => 'updateTaxonomySoftwareLog',
            ];
    }

    public function updateSoftwareLog(Event $event, $entity, $options)
    {

        Log::write(
            'info',
            '
            Class Name : Software ' . '
            Event Name : ' . $event->name() . ' ' .
            '
            Software name : ' . ' ' . $event->data['softwarename'] .
            '
            Software id : ' . $event->data['id'] . ' ',
            ['scope' => ['softwares']]
        );

        $software = TableRegistry::get("Softwares")
            ->get(
                $event->data['id'],
                [
                    "contain" => [
                        "Licenses" => [
                            'fields' => ["name"]
                        ]
                    ]]
            );

        if (isset($event->data['user'])) {
            $software->user = TableRegistry::get("Users")
                ->get(
                    $event->data['user']->id,
                    [
                        "fields" => ["id", "username", "email", "role"],
                        "contain" => [
                            "UserTypes" => [
                                'fields' => ["name"]
                            ]
                        ]]
                );
        }

        $event->data = $software;


        $event->name() === "Model.Software.created" ?
            $this->getMailer('Crud')->send('created', [$event, "Software"]) :
            $this->getMailer('Crud')->send('modifiedSoftware', [$event, $software->user->username, "Software"]);
    }

    public function updateUserLog(Event $event, $entity, $options)
    {

        Log::write(
            'info',
            '
            Class Name : USER ' . '
            Event Name : ' . $event->name() . ' ' .
            '
            User name : ' . ' ' . $event->data['username'] .
            '
            User email ' . $event->data['email'],
            ['scope' => ['users']]
        );

        $user = TableRegistry::get("Users")
            ->get(
                $event->data['id'],
                [
                    "contain" => [
                        "UserTypes" => [
                            'fields' => ["name"]
                        ]
                    ]]
            );

        $event->data = $user;

        $event->name() === "Model.User.created" ?
            $this->getMailer('Crud')->send('created', [$event, "User"]) :
            $this->getMailer('Crud')->send('modifiedUser', [$event, $user->username, "User"]);
    }


    /**
     * @param Event $event
     * @param $entity
     * @param $options
     */
    public function updateTaxonomySoftwareLog(Event $event, $entity, $options)
    {
        $mailMsg = "TaxonomysSoftwares ";
        if ($event->name() === "Model.TaxonomySoftware.deleted") {
            $action = 'DELETED';
            $this->getMailer('Crud')->send('deleted', [$event, $mailMsg]);
        } else {
            $RSU = TableRegistry::get("TaxonomysSoftwares")
                ->get(
                    $event->data['id'],
                    [
                        "contain" => [
                            "Users" => [
                                'fields' => ["username"]
                            ],
                            "Softwares" => [
                                'fields' => ["softwarename"]
                            ],
                            "Taxonomys" => [
                                'fields' => ["title_i18n_en"]
                            ]
                        ]]
                );
            $event->data = $RSU;

            if ($event->name() === "Model.TaxonomySoftware.created") {
                $action = 'CREATED';
                $this->getMailer('Crud')->send('created', [$event, $mailMsg]);
            } else {
                $action = 'UPDATED';
                $this->getMailer('Crud')->send('modifiedTaxonomySoftware', [$event, $mailMsg]);
            }
        }

        Log::write(
            'info',
            '
            Class Name : TaxonomysSoftwares ' . '
            Event Name : ' . $event->name() . '
            TaxonomysSoftwares : ' . ' ' . $event->data['id'] . ' ---> '. $action .'
            - Taxonomy id : ' . $event->data['taxonomy_id'] . '
            - Software id : ' . $event->data['software_id'] . '
            - User id : ' . $event->data['user_id'],
            ['scope' => ['taxonomys_softwares']]
        );
    }

    /**
     * @param Event $event
     * @param $entity
     * @param $options
     */
    public function updateReviewLog(Event $event, $entity, $options)
    {

        Log::write(
            'info',
            '
            Class Name : Review ' . '
            Event Name : ' . $event->name() . ' ' .
            '
            Review title : ' . ' ' . $event->data['title'] .
            '
            Review  comment : ' . $event->data['comment'] . '
            Review  Evaluation : ' . $event->data['evaluation'],
            ['scope' => ['reviews']]
        );

        $review = TableRegistry::get("Reviews")
            ->get(
                $event->data['id'],
                [
                    "fields" => ["title", "comment", "evaluation"],
                    "contain" => [
                        "Users" => [
                            'fields' => ["username"]
                        ],
                        "Softwares" => [
                            'fields' => ["softwarename"]
                        ]
                    ]]
            );

        $event->data = $review;

        $this->getMailer('Crud')->send('created', [$event, "Review"]);
    }

    /**
     * @param Event $event
     * @param $entity
     * @param $options
     */
    public function updateScreenshotLog(Event $event, $entity, $options)
    {

        Log::write(
            'info',
            '
            Class Name : Screenshot ' . '
            Event Name : ' . $event->name() . ' ' .
            '
            Screenshot name : ' . ' ' . $event->data['photo'] .
            '
            Review  software : ' . $event->data['software_id'] . '
            Review  url : ' . $event->data['url_directory'] . DS . $event->data['photo'],
            ['scope' => ['screenshots']]
        );

        $screenshot = TableRegistry::get("Screenshots")
            ->get(
                $event->data['id'],
                [
                    "fields" => ["photo", "url_directory"],
                    "contain" => [
                        "Softwares" => [
                            'fields' => ["softwarename"]
                        ]
                    ]]
            );

        $event->data = $screenshot;

        $this->getMailer('Crud')->send('created', [$event, "Screenshot"]);
    }

    /**
     * @param Event $event
     * @param $entity
     * @param $options
     */
    public function updateRelationshipSoftwareUserLog(Event $event, $entity, $options)
    {

        if ($event->name() === "Model.RelationshipSoftwareUser.created") {
            Log::write(
                'info',
                '
            Class Name : RelationshipsSoftwaresUsers ' . '
            Event Name : ' . $event->name() . ' ' .
                '
            RelationshipsSoftwaresUsers id : ' . ' ' . $event->data['id'] .
                '
            Relationship  User id : ' . $event->data['user_id'] . '
            Relationship  Software id : ' . $event->data['software_id'] . '
            Relationship   id : ' . $event->data['relationship_id'],
                ['scope' => ['relationships_softwares_users']]
            );

            $RSU = TableRegistry::get("RelationshipsSoftwaresUsers")
                ->get(
                    $event->data['id'],
                    [
                        "contain" => [
                            "Users" => [
                                'fields' => ["username"]
                            ],
                            "Softwares" => [
                                'fields' => ["softwarename"]
                            ],
                            "Relationships" => [
                                'fields' => ["name"]
                            ]
                        ]]
                );

            $event->data = $RSU;

            $this->getMailer('Crud')->send('created', [$event, "Relationship between a software and a user "]);
        } else {
            Log::write(
                'info',
                '
            Class Name : RelationshipsSoftwaresUsers ' . '
            Event Name : ' . $event->name() . ' ' .
                '
            RelationshipsSoftwaresUsers id : ' . ' ' . $event->data['id'] .
                '
            Relationship  User id : ' . $event->data['user_id'] . '
            Relationship  Software id : ' . $event->data['software_id'] . '
            Relationship   id : ' .  $event->data['relationship_id'],
                ['scope' => ['relationships_softwares_users']]
            );


            $this->getMailer('Crud')->send('deleted', [$event,"Relationship between a software and a user "]);
        }
    }
}
