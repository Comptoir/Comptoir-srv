<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Entity\Software;
use App\Model\Entity\User;
use App\Model\Table\SoftwaresTable;
use App\Network\Exception\RelationshipNotFoundException;
use App\Network\Exception\SoftwareNotFoundException;
use App\Network\Exception\UserNotFoundException;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Softwares Controller
 *
 * @property SoftwaresTable $Softwares
 */
class SoftwaresController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->paginate = [
            'limit' => Configure::read('LIMIT'),
            'maxLimit' => Configure::read('LIMIT'),
            'order' => [
                'Softwares.softwarename' => Configure::read('ORDER')
            ],
            'contain' => ['Licenses', 'Reviews', 'Screenshots', 'Relationships']
        ];
        $this->loadModel("Users");
    }

    /**
     * Manage all rights for the controllers' actions.
     * Returns true if the user can use the currrent action, FALSE otherwise.
     * Returns true for add a project if the user is connected
     * Returns true for edit and delete action if the user is owner.
     *
     * notice: $forceDeny parameter is not used here,
     *                     but is mandatory to be compatible with parent::isAuthorized()
     *
     * @param Array $user User informations
     * @param boolean $forceDeny by default FALSE, set TRUE to force the deny on parent::isAuthorized()
     * @return boolean
     */
    public function isAuthorized($user, $forceDeny = false)
    {
        //If the user is connected
        if ($this->Auth->user('id')) {
            if (in_array($this->request->action, ["deleteUsersSoftware", "deleteServicesProviders"])) {
                return true;
            }
        }
        if ($this->request->action === 'add' && $this->Auth->user('id')) {
            return true;
        }

        if ($this->request->action === 'edit' && $this->Auth->user('id')) {
            return true;
        }

        if ($this->Auth->user('id')) {
            return true;
        }

        if ($this->request->is('post')) {
            switch ($this->request->action) {
                case 'usersSoftware':
                    return $this->Users->isCompany($this->Auth->user('id')) ? false : true;
            }
        }
        return parent::isAuthorized($user, $forceDeny);
    }

    public function beforeFilter(Event $event)
    {
        if ($this->request->is('get')) {
            $this->Auth->allow(
                [
                    'export',
                    'index',
                    'add',
                    'view',
                    'lastAdded',
                    "getProjectsById",
                    'usersSoftware',
                    'reviewsSoftware',
                    "servicesProviders",
                    "workswellSoftwares",
                    "alternativeTo",
                    "screenshots",
                    "tagged",
                ]
            );
        }
        parent::beforeFilter($event);
    }


    /**
     * $links = [ 0 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'],
     *            1 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'], ]
     *
     * @param array $links
     */
    protected function setBreadcrumbs(array $links = [])
    {
        $firstLink = [
            'name' => __d('Breadcrumbs', 'Software.ListOfSoftware'),
            'url' => 'softwares'
        ];
        array_unshift($links, $firstLink);
        parent::setBreadcrumbs($links);
    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->request->allowMethod(['post', 'get']);

        // For sorting
        if (!empty($this->request->data) && isset($this->request->data["order"])) {
            $sort = explode(".", $this->request->data["order"]);
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }

        // For filtering
        $reviewed = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnReview"),
            "true" => __d("Forms", "Search.Filter.WithReview"),
        ];

        $screenCaptured = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnScreenshot"),
            "true" => __d("Forms", "Search.Filter.WithScreenshot"),
        ];

        $used = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnUser"),
            "true" => __d("Forms", "Search.Filter.WithUser"),
        ];

        $hasServiceProvider = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnServiceProvider"),
            "true" => __d("Forms", "Search.Filter.ServiceProvider"),
        ];

        $order = [
            "softwarename.asc" => __d("Forms", "Search.Sort.softwarenameAsc"),
            "softwarename.desc" => __d("Forms", "Search.Sort.softwarenameDesc"),
            "created.asc" => __d("Forms", "Search.Sort.createdAsc"),
            "created.desc" => __d("Forms", "Search.Sort.createdDesc"),
            "modified.asc" => __d("Forms", "Search.Sort.modifiedAsc"),
            "modified.desc" => __d("Forms", "Search.Sort.modifiedDesc"),
            "average_review.asc" => __d("Forms", "Search.Sort.averageReviewsAsc"),
            "average_review.desc" => __d("Forms", "Search.Sort.averageReviewsDesc"),
        ];


        // Redefine paginate for the use case
        $this->paginate = [
            "Softwares" => [
                'sortWhitelist' => [
                    'softwarename',
                    'average_review',
                    'created',
                    'modified',
                ],

                'limit' => Configure::read('LIMIT'),
                'maxLimit' => Configure::read('LIMIT'),
                'order' => ['softwarename' => Configure::read('ORDER')],
                'contain' => ['Reviews'],
            ],
            'contain' => ['Tags']
        ];

        $softwares = $this->Softwares
            ->find(
                'search',
                [
                    'search' => $this->request->data,
                    "contain" => ["Reviews"],
                ]
            )
            ->select(
                [
                    'average_review' => $this->Softwares
                        ->query()
                        ->func()
                        ->coalesce(
                            [
                                $this->Softwares->query()
                                    ->func()
                                    ->avg('Reviews.evaluation'),
                                0
                            ]
                        )
                ]
            )
            // allow to sort by the virtual property "Average"
            ->leftJoinWith('Reviews')
            ->group(['Softwares.id'])
            ->autoFields(true);


        $this->set('softwares', $this->paginate($softwares));
        $this->set('reviewed', $reviewed);
        $this->set('used', $used);
        $this->set('hasServiceProvider', $hasServiceProvider);
        $this->set('screenCaptured', $screenCaptured);
        $this->set('order', $order);
        $this->set('_serialize', ['softwares']);

        // Breadcrumbs
        $this->setBreadcrumbs();
    }


    /**
     * Export method
     *
     * @return void
     */
    public function export()
    {
        $this->request->allowMethod(['get']);

        ////////////////////////////////////////////////////////////
        // Retrieve data from database
        ////////////////////////////////////////////////////////////
        $this->paginate = [
            "Softwares" => [
                'sortWhitelist' => [
                    'softwarename',
                    'average_review',
                    'created',
                    'modified',
                ],

                'limit' => Configure::read('LIMIT'),
                'maxLimit' => Configure::read('LIMIT'),
                'order' => ['softwarename' => Configure::read('ORDER')],
                'contain' => [
                    'Reviews',
                    'Licenses',
                    'Tags',
                    'Userssoftwares' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Users.username' => 'ASC']);
                        },
                        'Users' => [
                            'fields' => [
                                'id',
                                'username',
                                'url'
                            ]
                        ]
                    ],
                    'Providerssoftwares' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Users.username' => 'ASC']);
                        },
                        'Users' => [
                            'fields' => [
                                'id',
                                'username',
                                'url'
                            ]
                        ]
                    ],

                ],
            ],
            'contain' => ['Tags']
        ];

        $softwares = $this->Softwares
            ->find(
                'search',
                [
                    'search' => $this->request->data,
                    "contain" => [
                        "Reviews",
                        "Licenses"],
                ]
            )
            ->select(
                [
                    'average_review' => $this->Softwares
                        ->query()
                        ->func()
                        ->coalesce(
                            [
                                $this->Softwares->query()
                                    ->func()
                                    ->avg('Reviews.evaluation'),
                                0
                            ]
                        )
                ]
            )
            // allow to sort by the virtual property "Average"
            ->leftJoinWith('Reviews')
            ->leftJoinWith('Licenses')
            ->group(['Softwares.id'])
            ->autoFields(true);

        ////////////////////////////////////////////////////////////
        // Generate JSON export
        ////////////////////////////////////////////////////////////
        $changelog = 'https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tree/main/Documentation/API/CHANGELOG.md';
        $docUrl = 'https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tree/main/Documentation/API/';
        $data = [
            'api_documentation'  => [
                'version' => '1.2.1',
                'changelog' => "$changelog",
                'documentation' => "$docUrl",
                'deprecated' => [
                    'date_of_export' => 'replaced by [ data_date ] in next major version',
                    'number_of_software' => 'replaced by [ data_statistics -> software ] in next major version',
                    'software -> licence' => 'replaced by [ software -> license ] in next major version'
                ],
            ],
            'data_documentation'  => [
                'license'  => [
                    'spdx_id' => 'CC0-1.0',
                    'spdx_name' => 'Creative Commons Zero v1.0 Universal',
                    'spdx_url' => 'https://spdx.org/licenses/CC0-1.0.html',
                    'i18n_url' => [
                        'fr' =>  'https://creativecommons.org/publicdomain/zero/1.0/deed.fr',
                        'en' =>  'https://creativecommons.org/publicdomain/zero/1.0/deed.en',
                    ],
                ],
            ],
            'data_statistics'  => [],
            'data_date' => date(DATE_ATOM),
            'date_of_export' => date(DATE_ATOM),
            'number_of_software' => $softwares->count(),
            'softwares' => []
        ];
        $globalUsers = [];
        $globalProviders = [];
        $globalFramalibreIDs = [];
        $globalSillIDs = [];
        $globalCnllIDs = [];
        $globalWikidataIDs = [];
        $globalWikipediaIDs = [];
        foreach ($this->paginate($softwares) as $software) {
            $providers = [];
            # @TODO bug $provider->created
            foreach ($software->providerssoftwares as $provider) {
                $providers[] = [
                    'id' => $provider->user_id,
//                  'created' => $provider->created,
//                  'modified' => $provider->modified,
                    'url' => 'https://comptoir-du-libre.org/fr/users/'. $provider->user_id,
                    'name' => $provider->user->username,
                    'type' => $provider->user->user_type->name,
                    'external_resources' => [
                        'website' =>  $provider->user->url,
                    ],
                ];
                $globalProviders[$provider->user_id] =  $provider->user->username;
            }
            $users = [];
            # @TODO bug $user->createdd
            foreach ($software->userssoftwares as $user) {
                if ($user->user->user_type->name === 'Administration') {
                    $users[] = [
                        'id' => $user->user_id,
//                      'created' => $user->created,
//                      'modified' => $user->modified,
                        'url' => 'https://comptoir-du-libre.org/fr/users/'. $user->user_id,
                        'name' => $user->user->username,
                        'type' => $user->user->user_type->name,
                        'external_resources' => [
                            'website' =>  $user->user->url,
                        ],
                    ];
                }
                $globalUsers[$user->user_id] =  $user->user->username;
            }

            $wikidata = [];
            if (!empty($software->wikidata)) {
                $wikidata = [
                    'id' => $software->wikidata,
                    'url' => "https://www.wikidata.org/wiki/". $software->wikidata,
                    'data' => "https://www.wikidata.org/wiki/Special:EntityData/". $software->wikidata.".json",
                ];
                $globalWikidataIDs[] = $wikidata;
            }

            $sill = [];
            if (!empty($software->sill)) {
                $sill = [
                    'id' => (int) $software->sill,
                    'url' => 'https://code.gouv.fr/sill/fr/software?id='. $software->sill,
                    'i18n_url' => [
                        'fr' =>  'https://code.gouv.fr/sill/fr/software?id='. $software->sill,
                        'en' =>  'https://code.gouv.fr/sill/en/software?id='. $software->sill,
                    ],
                ];
                $globalSillIDs[] = $sill;
            }

            $cnll = [];
            if (!empty($software->cnll)) {
                $cnll = [
                    'id' => (int) $software->cnll,
                    'url' => 'https://annuaire.cnll.fr/solutions/'. $software->cnll,
                ];
                $globalCnllIDs[] = $cnll;
            }

            $framalibre = [];
            if (!empty($software->framalibre)) {
                $framalibre = [
                    'slug' => $software->framalibre,
                    'url' => 'https://framalibre.org/content/'. $software->framalibre,
                ];
                $globalFramalibreIDs[] = $framalibre;
            }

            $wikipedia = [];
            if (!empty($software->wikipedia_en)) {
                $wikipedia['url'] = 'https://en.wikipedia.org/wiki/'. $software->wikipedia_en;
                $wikipedia['i18n_url']['en'] = 'https://en.wikipedia.org/wiki/'. $software->wikipedia_en;
            }
            if (!empty($software->wikipedia_fr)) {
                if (!isset($wikipedia['url'])) {
                    $wikipedia['url'] = 'https://fr.wikipedia.org/wiki/'. $software->wikipedia_fr;
                }
                $wikipedia['i18n_url']['fr'] = 'https://en.wikipedia.org/wiki/'. $software->wikipedia_fr;
            }
            if (count($wikipedia) > 0) {
                $globalWikipediaIDs[] = $wikipedia;
            }

            $licenceName = '';
            if (is_object($software->license)) {
                $licenceName = $software->license->name;
            }
            $item = [
                'id' => $software->id,
                'name' => $software->softwarename,
                'url' => 'https://comptoir-du-libre.org/fr/softwares/'. $software->id,
                'i18n_url' => [
                    'fr' =>  'https://comptoir-du-libre.org/fr/softwares/'. $software->id,
                    'en' =>  'https://comptoir-du-libre.org/en/softwares/'. $software->id,
                ],
                'licence' => $licenceName,
                'license' => $licenceName,
                'created' => $software->created,
                'modified' => $software->modified,
                'external_resources' => [
                    'website' =>  $software->url_website,
                    'repository' =>  $software->url_repository,
                    'framalibre' =>  $framalibre,
                    'cnll' =>  $cnll,
                    'sill' =>  $sill,
                    'wikidata' =>  $wikidata,
                    'wikipedia' =>  $wikipedia,
                ],
                'providers' => $providers,
                'users' => $users,
            ];
            $data['softwares'][] = $item;
        }

        // Add statistics
        $data['data_statistics']['users'] = count($globalUsers);
        $data['data_statistics']['providers'] = count($globalProviders);
        $data['data_statistics']['software'] = $softwares->count();
        $data['data_statistics']['software_external_resources'] = [
            'framalibre' =>  count($globalFramalibreIDs),
            'cnll' =>  count($globalCnllIDs),
            'sill' =>  count($globalSillIDs),
            'wikidata' =>  count($globalWikidataIDs),
            'wikipedia' =>  count($globalWikipediaIDs),
        ];

        unset($globalProviders);
        $json = \json_encode($data);
        // $json = \json_encode($data, JSON_PRETTY_PRINT);
        $exportPath = 'public/export/';
        $exportFile = $exportPath . "comptoir-du-libre_export_v1.json";
        if (is_dir(WWW_ROOT . $exportPath)) {
            file_put_contents(WWW_ROOT . $exportFile, "$json");
            $this->set('exportFile', "$exportFile");
            $this->set('export', file_get_contents(WWW_ROOT . $exportFile));
        }
    }


    /**
     * View method
     *
     * @param string|null $id Software id.
     * @return void
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/softwares/". (int) $id;
        if ($allowedUrl !== $this->request->here(false) && !$this->request->is('json')) {
            return $this->redirect("$allowedUrl", 301);
        }

        // an id is specified => get details of the software
        if ($this->request->is('get') && $id != null) {
            $software = $this->Softwares->get(
                $id,
                [
                    'finder' => 'average',
                    'contain' => [
                        'Licenses',
                        'Reviews' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Reviews.created' => 'DESC']);
                            },
                            'Users' => [
                                'UserTypes',
                                'fields' => [
                                    'id',
                                    'username',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ],
                        'Screenshots',
                        'Userssoftwares' => [],
                        'Creatorssoftwares' => [],
                        'Providerssoftwares' => [],
                        'workswellsoftwares' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'Reviews',
                                'Tags',
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ],
                        'alternativeto' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'Reviews',
                                'Tags',
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description',
                                ]
                            ]
                        ],
                        'Tags' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Tags.name' => 'ASC']);
                            },
                        ],
                    ],
                ]
            );

            // Load mapping data if available for current software and populate view data
            $this->commonMappingForCurrentSoftware($software->id);

            $this->set(compact(['software']));
            $this->set('_serialize', ['software']);

            // Breadcrumbs
            $links = array();
            $links[] = [
                'name' => $software->softwarename,
                'url' => "softwares/$id"
            ];
            $this->setBreadcrumbs($links);
        }

        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title" => $software->softwarename,
            "description" => $software->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $software->logo_directory . DS . $software->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title" => $software->softwarename,
            "description" => $software->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $software->logo_directory . DS . $software->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);

        // no id specified => ALL software listed
        if ($id == null && $this->request->is('get') && $this->request->is('json')) {
            $softwares = $this->Softwares
                ->find('search', ['search' => $this->request->query]);

            $this->set('softwares', $this->paginate($softwares));
            $this->set('_serialize', ['softwares']);
        }
    }

    /**
     * Load mapping data if available for given software ID and populate view data
     * @param int $softwareId
     */
    protected function commonMappingForCurrentSoftware(int $softwareId)
    {
        // Get taxonomy records grouped by taxon ID for this software
        $this->loadModel("TaxonomysSoftwares");
        $taxonomiesSoftware = $this->TaxonomysSoftwares->getListBySofwareId($softwareId);

        // Load mapping data if necessary
        $mappingFirstLevels = [];
        $mappingTaxons = [];
        $taxonomiesPreSelection = [];
        if (count($taxonomiesSoftware) > 0) {
            $taxonIds = array_keys($taxonomiesSoftware);
            $mappingFirstLevels = $this->getMappingFirstLevels($this->selectedLanguage);
            $mappingTaxons = $this->getMappingTaxonsWithTaxonIdsFilter($taxonIds, $this->selectedLanguage);
        } else {
            $taxonomiesPreSelection = $this->TaxonomysSoftwares->getPreSelectionBySofwareId($softwareId);
            if (count($taxonomiesPreSelection) > 0) {
                $taxonIds = array_keys($taxonomiesPreSelection);
                $mappingFirstLevels = $this->getMappingFirstLevels($this->selectedLanguage);
                $mappingTaxons = $this->getMappingTaxonsWithTaxonIdsFilter($taxonIds, $this->selectedLanguage);
            }
        }

        // Populate view data
        $this->set(compact(['mappingFirstLevels']));
        $this->set(compact(['mappingTaxons']));
        $this->set(compact(['taxonomiesSoftware']));
        $this->set(compact(['taxonomiesPreSelection']));
        $this->set('_serialize', [
            'taxonomiesSoftware',
            'taxonomiesPreSelection',
            'mappingTaxons',
            'mappingFirstLevels'
        ]);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if (isset($this->request->data)
            && !empty($this->request->data)
        ) {
            // only "admin" role can edit external ressources
            if ($this->request->session()->read('Auth.User.role') !== 'admin') {
                if (isset($this->request->data['sill'])) {
                    unset($this->request->data['sill']);
                }
                if (isset($this->request->data['cnll'])) {
                    unset($this->request->data['cnll']);
                }
                if (isset($this->request->data['wikidata'])) {
                    unset($this->request->data['wikidata']);
                }
                if (isset($this->request->data['framalibre'])) {
                    unset($this->request->data['framalibre']);
                }
                if (isset($this->request->data['wikipedia_en'])) {
                    unset($this->request->data['wikipedia_en']);
                }
                if (isset($this->request->data['wikipedia_fr'])) {
                    unset($this->request->data['wikipedia_fr']);
                }
            }
            $software = $this->Softwares->newEntity(
                $this->request->data,
                ['associated' => "Screenshots", "Tags"]
            );
        } else {
            $software = $this->Softwares->newEntity();
        }

        $user = TableRegistry::get("Users")->get($this->Auth->user('id'));
        $software->user = $user;

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.Add'),
            'url' => "softwares/add"
        ];
        $this->setBreadcrumbs($links);


        // (*) additional form to declare "provider for" or "user of"
        $displayUserOfForm = false;
        $displayProviderForm = false;
        if ($user->isCompanyType()) {
            $displayProviderForm = true;
        } else { // other user type
            $displayUserOfForm = true;
        }
        $message = "";
        $lang = $this->selectedLanguage;
        if ($this->Auth->user('id')) {
            if (!empty($this->request->data)) {
                if ($this->Softwares->save($software)) {
                    $message = "Success";
                    // $this->request->is('json')
                    // ? null
                    // : $this->Flash->success(
                    // __d("Forms", "Congratulations ! Thanks to you a new software is on the Comptoir du Libre !"));
                    if (!$this->request->is('json')) {
                        $this->Flash->success(__d(
                            "Forms",
                            "Congratulations ! Thanks to you a new software is on the Comptoir du Libre !"
                        ));

                        // (*) Processing additional form to declare "provider for".
                        $this->processExtraProviderForm(
                            $displayProviderForm,
                            $user->id,
                            $software->get('id'),
                            $software->softwarename
                        );

                        // (*) Processing additional form to declare "user of".
                        if ($this->processExtraUserOfForm($displayUserOfForm, $user->id, $software->get('id'))) {
                            // if the user is an administration,
                            // redirect to the additional form for mapping.
                            if ($user->isAdministrationType()) {
                                return $this->redirect("/$lang/mappingForm/" . $software->get('id'));
                            }
                        }

                        // REDIRECTS TO ---> /<lang>/softwares/<idSoftware>
                        return $this->redirect("/$lang/softwares/" . $software->get('id'));
                        // REDIRECTS TO ---> /api/v1/softwares/view/<idSoftware>
                        // return $this->redirect(['action' => 'view', $software->get('id')]);
                    }
                } else {
                    $message = "Error";
                    $this->request->is('json')
                        ? $software = $software->errors()
                        : null;
                    $this->request->is('json')
                        ? null
                        : $this->Flash->error(__d(
                            "Forms",
                            "Your request to add a software failed, please follow rules in red."
                        ));
                }
            }
        }

        $this->ValidationRules->config('tableRegistry', "Softwares");
        $rules = $this->ValidationRules->get();
        $licenses = $this->Softwares->Licenses->find('list', ['limit' => 200]);
        $this->set(compact('software', 'licenses', 'rules', 'message'));
        $this->set('_serialize', ['software', 'licenses', 'rules', 'message']);
        if (!$this->request->is('json')) {
            $this->set('displayProviderForm', $displayProviderForm);
            $this->set('displayUserOfForm', $displayUserOfForm);
            $this->set('userTypeId', $software->user->user_type_id);
        }
    }


    /**
     * Processing additional form (used in add and edit forms)
     * to declare current user "UserOf" current software
     *
     * @param bool $displayUserOfForm
     * @param int $userId
     * @param int $softwareId
     * @return bool
     */
    private function processExtraUserOfForm(bool $displayUserOfForm, int $userId, int $softwareId)
    {
        if ($displayUserOfForm === true
            && isset($this->request->data['userOf'])
            && $this->request->data['userOf'] === 'yes') {
            // Relationship ---> Unit Test = 2
            //              ---> Database  = 7
            $relationship_id = $this->getRelationshipIdByName('UserOf');
            $conditions = [
                'user_id' => $userId,
                'software_id' => $softwareId,
                'relationship_id' => $relationship_id,
            ];
            $relSoftUser = $this->Softwares->RelationshipsSoftwaresUsers->newEntity();
            $this->Softwares->RelationshipsSoftwaresUsers->patchEntity($relSoftUser, $conditions);
            if ($this->Softwares->RelationshipsSoftwaresUsers->save($relSoftUser)) {
                $this->Flash->success(__d("Forms", "Your are now known as user of."));
                return true;
            }
        }
        return false;
    }


    /**
     * Processing additional form (used in add and edit forms)
     * to declare current user "ProviderFor" current software
     *
     * @param bool $displayProviderForm
     * @param int $userId
     * @param int $softwareId
     * @param string $softwareName
     * @return bool
     */
    private function processExtraProviderForm(
        bool $displayProviderForm,
        int $userId,
        int $softwareId,
        string $softwareName
    ) {
        if ($displayProviderForm === true
            && isset($this->request->data['providerFor'])
            && $this->request->data['providerFor'] === 'yes') {
            // Relationship ---> Unit Test = 3
            //              ---> Database  = 10
            $relationship_id = $this->getRelationshipIdByName('ProviderFor');
            $conditions = [
                'user_id' => $userId,
                'software_id' => $softwareId,
                'relationship_id' => $relationship_id,
            ];
            $relSoftUser = $this->Softwares->RelationshipsSoftwaresUsers->newEntity();
            $this->Softwares->RelationshipsSoftwaresUsers->patchEntity($relSoftUser, $conditions);
            if ($this->Softwares->RelationshipsSoftwaresUsers->save($relSoftUser)) {
                $this->Flash->success(__d(
                    "Forms",
                    "Your are now known as service providers for {0}.",
                    $softwareName
                ));
                return true;
            }
        }
        return false;
    }

    /**
     * Edit method
     *
     * @param string|null $id Software id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $software = $this->Softwares->get(
            $id,
            [
                'contain' => [
                    'Screenshots',
                    'Tags' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Tags.name' => 'ASC']);
                        },
                    ],
                ]
            ]
        );

        try {
            $user = $this->Users->get($this->Auth->user('id'));
        } catch (Exception $e) {
            throw new UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
        }
        $software->user = $user;


        // (*) additional form to declare "provider for" or "user of"
        $displayUserOfForm = false;
        $displayProviderForm = false;
        $extraFormConditions = [
            'user_id' => $user->id,
            'software_id' => $software->get('id'),
        ];
        $registry = TableRegistry::get("RelationshipsSoftwaresUsers");
        if ($user->isCompanyType()) {
            $extraFormConditions['relationship_id'] = $this->getRelationshipIdByName('ProviderFor');
            $displayProviderForm = !$registry->exists($extraFormConditions); // true if not already "provider for"
        } else { // other user type
            $extraFormConditions['relationship_id'] =  $this->getRelationshipIdByName('UserOf');
            $displayUserOfForm = !$registry->exists($extraFormConditions);  // true if not already "user of"
        }


        //TODO A refaire lors de la fusion (Comptoir-srv / Comptoir-web)
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (isset($this->request->data['screenshots'][0]['photo']['name'])) {
                $this->request->data['screenshots'][0]['photo']['name'] =
                    time() . "_" . $this->request->data['screenshots'][0]['photo']['name'];
            }

            // only "admin" role can edit external ressources
            if ($this->request->session()->read('Auth.User.role') !== 'admin') {
                if (isset($this->request->data['sill'])) {
                    unset($this->request->data['sill']);
                }
                if (isset($this->request->data['cnll'])) {
                    unset($this->request->data['cnll']);
                }
                if (isset($this->request->data['wikidata'])) {
                    unset($this->request->data['wikidata']);
                }
                if (isset($this->request->data['framalibre'])) {
                    unset($this->request->data['framalibre']);
                }
                if (isset($this->request->data['wikipedia_en'])) {
                    unset($this->request->data['wikipedia_en']);
                }
                if (isset($this->request->data['wikipedia_fr'])) {
                    unset($this->request->data['wikipedia_fr']);
                }
            }
            $software = $this->Softwares->patchEntity(
                $software,
                $this->request->data,
                ['associated' => "Screenshots", "Tags"]
            );
            if ($this->Softwares->save($software)) {
                $message = "Success";
                if (!$this->request->is('json')) {
                    $lang = $this->selectedLanguage;
                    $this->Flash->success(__d("Forms", "Edit.Congratulations.software.success"));

                    // (*) Processing additional form to declare "provider for".
                    $this->processExtraProviderForm(
                        $displayProviderForm,
                        $user->id,
                        $software->get('id'),
                        $software->softwarename
                    );

                    // (*) Processing additional form to declare "user of".
                    if ($this->processExtraUserOfForm($displayUserOfForm, $user->id, $software->get('id'))) {
                        // if the user is an administration,
                        // redirect to the additional form for mapping.
                        if ($user->isAdministrationType()) {
                            return $this->redirect("/$lang/mappingForm/" . $software->get('id'));
                        }
                    }

                    // REDIRECTS TO ---> /<lang>/softwares/<idSoftware>
                    return $this->redirect("/$lang/softwares/" . $software->get('id'));
                    // REDIRECTS TO ---> /api/v1/softwares/view/<idSoftware>
                    // return $this->redirect(['action' => 'view', $software->get('id')]);
                }
            } else {
                $message = "Error";
                $this->Flash->error(__d("Forms", "Edit.Congratulations.software.error"));
                $this->set('errors', $software);
            }
        }

        $this->ValidationRules->config('tableRegistry', "Softwares");
        $rules = $this->ValidationRules->get();
        $licenses = $this->Softwares->Licenses->find('list', ['limit' => 200]);

        $this->set('displayProviderForm', $displayProviderForm);
        $this->set('displayUserOfForm', $displayUserOfForm);
        $this->set('userTypeId', $software->user->user_type_id);
        $this->set(compact('software', 'licenses', 'rules', 'message'));
        $this->set('_serialize', ['software', 'licenses', 'rules', 'message', 'errors']);


        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$id"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.Edit'),
            'url' => "softwares/edit/$id/"
        ];
        $this->setBreadcrumbs($links);
    }

    /**
     * Delete method
     *
     * @param string|null $id Software id.
     * @return Response|null Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $software = $this->Softwares->get($id);
        if ($this->Softwares->delete($software)) {
            $this->Flash->success(__('The software has been deleted.'));
        } else {
            $this->Flash->error(__('The software could not be deleted. Please, try again.'));
        }
        return $this->redirect(["prefix" => false, 'action' => 'index']);
    }




    /**
     * Returns list of users of a given software (GET method)
     * Adds current user in the list of users declared for the current software (POST method)
     *
     * @param null $id Id of a given software
     */
    public function usersSoftware($id = null)
    {

        $this->request->allowMethod(['post', 'get']);
        try {
            $software = $this->Softwares->get(
                $id,
                [
                    "contain" =>
                        [
                            'Userssoftwares' =>
                                [
                                    'strategy' => 'select',
                                    'queryBuilder' => function ($q) {
                                        return $q->order(['users.username' => 'ASC']);
                                    },
                                ],
                        ]
                ]
            );
        } catch (Exception $e) {
            throw new SoftwareNotFoundException("The software with the id " . $id . " does not exist");
        }

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/softwares/usersSoftware/". (int) $id;
        if ($allowedUrl !== $this->request->here(false) && $this->request->is('get')) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$id"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.Users'),
            'url' => "softwares/usersSoftware/$id"
        ];
        $this->setBreadcrumbs($links);

        if ($this->request->is('post')) {
            $datas = [];
            $datas["software_id"] = $software->id;

            // Case user doesn't exist
            try {
                $user = $this->Users->get($this->Auth->user('id'));
                $datas["user_id"] = $user->id;
            } catch (Exception $e) {
                throw new UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
            }

            try {
                $this->loadModel("Relationships");
                $relationship = $this->Relationships->find("all")->where(["cd" => "UserOf"])->first();
                $datas["relationship_id"] = $relationship->id;
            } catch (Exception $e) {
                throw new RelationshipNotFoundException("The relationship with the cd " . "UserOf" . " does not exist");
            }

            $relSoftUser = $this->Softwares->RelationshipsSoftwaresUsers->newEntity();
            $this->Softwares->RelationshipsSoftwaresUsers->patchEntity($relSoftUser, $datas);

            if ($this->Softwares->RelationshipsSoftwaresUsers->save($relSoftUser)) {
                $software = $this->Softwares->get($id, ['contain' => 'Userssoftwares']);

                $this->Flash->success(__d("Forms", "Your are now known as user of."));
                $message = "Success";

                // if the user is an administration,
                // redirect to the additional form for mapping.
                if ($user->isAdministrationType() && !$this->request->is('json')) {
                    $lang = $this->selectedLanguage;
                    return $this->redirect("/$lang/mappingForm/" . $software->id);
                } else {
                    $this->request->is('json')
                        ? null
                        : $this->redirect(
                            [
                            "prefix" => false,
                            "controller" => "Softwares",
                            "action" => $id,
                            "language" => $this->request->param("language"),
                            ]
                        );
                }
            } else {
                ($relSoftUser->errors("software_id")['_isUnique'] != null)
                    ? $this->Flash->error(__d(
                        "Forms",
                        "You can not be declared twice as user of {0}.",
                        $software->softwarename
                    ))
                    : $this->Flash->error(__d("Forms", "We can not respond to your request"));
                $message = "Error";
                $this->request->is('json') ? $software = $relSoftUser->errors() : $this->redirect([
                    "prefix" => false,
                    "controller" => "Softwares",
                    "action" => $id,
                    "language" => $this->request->param("language"),
                ]);
            }
        }

        $this->set(compact('software', 'message', 'softwareName'));
        $this->set('_serialize', ['software', 'message', 'softwareName']);
    }

    /**
     * @param null $id
     */
    public function deleteUsersSoftware($id = null)
    {
        $this->request->allowMethod(['delete']);
        $this->viewBuilder()->template("view");

        try {
            $this->loadModel("Relationships");
            $relationship = $this->Relationships->find("all")->where(["cd" => "UserOf"])->first();
        } catch (Exception $e) {
            throw new RelationshipNotFoundException(__d(
                "Forms",
                "The relationship with the cd " . "UserOf" . " does not exist"
            ));
        }

        $conditionsToDelete = [
            "software_id" => $id,
            "user_id" => $this->Auth->user('id'),
            "relationship_id" => $relationship->id
        ];
        if ($this->Softwares->RelationshipsSoftwaresUsers->exists($conditionsToDelete)) {
            $entity = $this->Softwares->RelationshipsSoftwaresUsers->find()->where($conditionsToDelete)->first();
            $this->Softwares->RelationshipsSoftwaresUsers->delete($entity, $conditionsToDelete);

            // If exist, delete associated TaxonomySoftware rows after deleting "User Of"
            $this->deleteAssociatedTaxonomysSoftwares($this->Auth->user('id'), $id);

            $message = "Success";
            $this->Flash->success(__d("Forms", "Your are not know as user of anymore."));
        } else {
            $message = "No relationship in our data base.";
            $this->Flash->error(__d("Forms", "We can not respond to your request"));
        }

        $this->set(compact('message'));
        $this->set('_serialize', ['message']);
        $this->request->is('json')
            ? null
            : $this->redirect(
                [
                "prefix" => false,
                "controller" => "Softwares",
                "action" => $id,
                "language" => $this->request->param("language"),
                ]
            );
    }

    /**
     * If exist, delete associated TaxonomysSoftwares rows
     *
     * @param $userId
     * @param $softwareId
     * @return bool
     */
    private function deleteAssociatedTaxonomysSoftwares($userId = null, $softwareId = null)
    {
        if (is_null($userId) || is_null($softwareId)) {
            return false;
        }
        $table = TableRegistry::get("TaxonomysSoftwares");
        $conditionsToDelete = [
            "user_id" => (int) $userId,
            "software_id" => (int) $softwareId,
        ];
        if ($table->exists($conditionsToDelete)) {
            $nbRows = $table->deleteAll($conditionsToDelete);
            if ($nbRows > 0) {
                return true;
            }
        }
        return false;
    }


    /**
     * @param null $id
     */
    public function reviewsSoftware($id = null)
    {
        $software = $this->Softwares->get(
            $id,
            [
                "contain" =>
                    [
                        'Reviews' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Reviews.created' => 'DESC']);
                            },
                            'Users' => [
                                'UserTypes',
                                'fields' => [
                                    'id',
                                    'username',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ]
                    ]
            ]
        );


        $this->set(compact('software'));
        $this->set('_serialize', ['software']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$id"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.Reviews'),
            'url' => "softwares/usersSoftware/$id"
        ];
        $this->setBreadcrumbs($links);
    }

    /**
     * Get all alternative for a software
     *
     * @param integer $id
     */
    public function alternativeTo($id = null)
    {
        $software = $this->Softwares->get(
            $id,
            [
                'finder' => 'average',
                "contain" =>
                    [
                        'Licenses',
                        'alternativeto' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ]
                    ]
            ]
        );

        $this->set(compact('software'));
        $this->set('_serialize', ['software']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$id"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.AlternativeTo'),
            'url' => "softwares/servicesProviders/$id"
            /// BUG URL à vérifier /|\
        ];
        $this->setBreadcrumbs($links);
    }

    /**
     * Get all services provider for a software
     *
     * @param integer $id
     */
    public function servicesProviders($id = null)
    {
        try {
            $software = $this->Softwares->get(
                $id,
                [
                    "contain" =>
                        [
                            'Providerssoftwares' =>
                                [
                                    'strategy' => 'select',
                                    'queryBuilder' => function ($q) {
                                        return $q->order(['users.username' => 'ASC']);
                                    },
                                ],
                        ]
                ]
            );
        } catch (Exception $e) {
            throw new SoftwareNotFoundException("The software with the id " . $id . " does not exist");
        }

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/softwares/servicesProviders/". (int) $id;
        if ($allowedUrl !== $this->request->here(false) && $this->request->is('get')) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$id"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.ServicesProviders'),
            'url' => "softwares/servicesProviders/$id"
        ];
        $this->setBreadcrumbs($links);

        if ($this->request->is('post')) {
            $datas = [];
            $this->loadModel("Users");

            $datas["software_id"] = $software->id;

            try {
                $user = $this->Users->get($this->Auth->user('id'));
                $datas["user_id"] = $user->id;
            } catch (Exception $e) {
                throw new UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
            }

            try {
                $this->loadModel("Relationships");
                $relationship = $this->Relationships->find("all")->where(["cd" => "ProviderFor"])->first();
                $datas["relationship_id"] = $relationship->id;
            } catch (Exception $e) {
                throw new RelationshipNotFoundException(
                    "The relationship with the cd " . "ProviderFor" . " does not exist"
                );
            }

            $relSoftUser = $this->Softwares->RelationshipsSoftwaresUsers->newEntity();
            $this->Softwares->RelationshipsSoftwaresUsers->patchEntity($relSoftUser, $datas);

            if ($this->Softwares->RelationshipsSoftwaresUsers->save($relSoftUser)) {
                $software = $this->Softwares->get($id, ['contain' => 'Providerssoftwares']);
                $message = "Success";
                $this->Flash->success(__d(
                    "Forms",
                    "Your are now known as service providers for {0}.",
                    $software->softwarename
                ));
                $this->request->is('json')
                    ? null
                    : $this->redirect(
                        [
                        "prefix" => false,
                        "controller" => "Softwares",
                        "action" => $id,
                        "language" => $this->request->param("language"),
                        ]
                    );
            } else {
                ($relSoftUser->errors("software_id")['_isUnique'] != null)
                    ? $this->Flash->error(__d(
                        "Forms",
                        "You can not be declared twice as service provider for {0}.",
                        $software->softwarename
                    ))
                    : $this->Flash->error(__d("Forms", "We can not respond to your request."));
                $message = "Error";
                $this->request->is('json')
                    ? $software = $relSoftUser->errors()
                    : $this->redirect(
                        [
                        "prefix" => false,
                        "controller" => "Softwares",
                        "action" => $id,
                        "language" => $this->request->param("language"),
                        ]
                    );
            }
        }

        $this->set(compact('software', 'message'));
        $this->set('_serialize', ['software', 'message']);
    }

    /**
     * @param null $id
     */
    public function deleteServicesProviders($id = null)
    {
        $this->request->allowMethod(['delete']);
        $this->viewBuilder()->template("view");

        try {
            $this->loadModel("Relationships");
            $relationship = $this->Relationships->find("all")->where(["cd" => "ProviderFor"])->first();
        } catch (Exception $e) {
            throw new RelationshipNotFoundException(__d(
                "Forms",
                "The relationship with the cd " . "ProviderFor" . " does not exist"
            ));
        }

        $conditionsToDelete = [
            "software_id" => $id,
            "user_id" => $this->Auth->user('id'),
            "relationship_id" => $relationship->id
        ];

        if ($this->Softwares->RelationshipsSoftwaresUsers->exists($conditionsToDelete)) {
            $entity = $this->Softwares->RelationshipsSoftwaresUsers->find()->where($conditionsToDelete)->first();
            $this->Softwares->RelationshipsSoftwaresUsers->delete($entity, $conditionsToDelete);

            $message = "Success";
            $this->Flash->success(__d("Forms", "Your are not know as service provider of anymore."));
        } else {
            $message = "No relationship in our data base.";
            $this->Flash->error(__d("Forms", "No relationship in our data base."));
        }
        $this->set(compact('message'));
        $this->set('_serialize', ['message']);

        $this->request->is('json')
            ? null
            : $this->redirect(
                [
                "prefix" => false,
                "controller" => "Softwares",
                "action" => $id,
                "language" => $this->request->param("language"),
                ]
            );
    }

    /**
     * Get all softwares working well with the current software
     *
     * @param integer $id
     */
    public function workswellSoftwares($id = null)
    {
        $software = $this->Softwares->get(
            $id,
            [
                'contain' => [
                    'Licenses',
                    'workswellsoftwares' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Softwares.softwarename' => 'ASC']);
                        },
                        'Softwares' => [
                            'fields' => [
                                'id',
                                'softwarename',
                                'logo_directory',
                                'photo',
                                'description'
                            ]
                        ]
                    ],
                ],
            ]
        );


        $this->set(compact('software'));
        $this->set('_serialize', ['software']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$id"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.WorksWellSoftwares'),
            'url' => "softwares/worksWellSoftwares/$id"
        ];
        $this->setBreadcrumbs($links);
    }

    /**
     * Get all screenshots for the current software
     *
     * @param integer $id
     */
    public function screenshots($id = null)
    {
        $software = $this->Softwares->get(
            $id,
            [
                "contain" =>
                    [
                        'Screenshots'
                    ]
            ]
        );

        $this->set(compact('software'));
        $this->set('_serialize', ['software']);
    }

    /**
     * list of softwares with tag='pass' associate
     * send the list to the view
     *
     * @param string 'pass' are tags we looking for
     */
    public function tagged()
    {
        $tags = $this->request->params['pass'];
        $softwares = $this->Softwares->find(
            'tagged',
            [
                'tags' => $tags
            ]
        );

        $this->set(
            [
                'softwares' => $softwares,
                'tags' => $tags
            ]
        );
    }
}
