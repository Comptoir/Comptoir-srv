<?php
namespace App\Controller\Api\V1;

use App\Controller\Api\V1\Taxonomy\CommonTaxonomySoftwareController;
use App\Model\Entity\Software;
use App\Model\Entity\User;
use App\Model\Table\TaxonomysSoftwaresTable;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;

/**
 * TaxonomysSoftwares Controller
 * - define actions allowed to editor
 * - define actions allowed to connected user
 * - define actions allowed to anonymous user
 * - implemente actions used by editor, connected user and anonymous user
 *
 * notice:
 * - actions dedicated to admin must be implemented in the parent class.
 * - beforeFilter() is defined in the parent class and should not be overwritten.
 * - isAuthorized() is defined in the parent class and should not be overwritten.
 *
 * @property TaxonomysSoftwaresTable $TaxonomysSoftwares
 */
class TaxonomysSoftwaresController extends CommonTaxonomySoftwareController
{

    /**
     * Actions allowed only to editor (connected user + business rules)
     * @var array
     */
    protected $actionsAllowedOnlyToEditor = [
        'mappingForm',
    ];

    /**
     * Actions allowed only for connected user
     * @var array
     */
    protected $actionsAllowedOnlyToConnectedUser = [];

    /**
     * Actions allowed to everyone
     * @var array
     */
    protected $actionsAllowedToEveryone = [];


    /**
     * Mapping form used by connected user (type = "Administration")
     * after registering as user of a sofware.
     *
     *   URL: /fr/mappingForm/<softwareId>
     *        /en/mappingForm/<softwareId>
     *        /api/v1/taxonomys-softwares/mappingForm                   ---> redirect to '/en/mapping/'
     *        /api/v1/taxonomys-softwares/mappingForm/<softwareId>      ---> redirect to '/en/mappingForm/<softwareId>'
     *        /api/v1/taxonomys-softwares/mappingForm.json              ---> disable via parent::beforeFilter()
     *        /api/v1/taxonomys-softwares/mappingForm/<softwareId>.json ---> disable via parent::beforeFilter()
     *
     * @return Response|null
     */
    public function mappingForm($softwareId = null)
    {
        // Get taxonomy data
        $mappingTaxons = $this->getMappingTaxons($this->selectedLanguage);
        $mappingHead = $this->getmappingFirstLevels($this->selectedLanguage);

        // Get user
        $user = TableRegistry::get("Users")->get($this->Auth->user('id'));
        $userId = (int) $user->id;

        // $softwareId is not null only with /api/v1/ prefix
        if (is_null($softwareId)) { // Get the ID from the URL
            if (isset($this->request->params['softwareId'])) {
                $softwareId = $this->request->params['softwareId'];
            } else {
                return $this->redirect($this->getBaseUrl('mapping'), 301);
            }
        }

        // Get software
        $softwaresTable = TableRegistry::get("Softwares");
        $softwareId = (int) $softwareId;
        try {
            $software = $softwaresTable->get($softwareId);
        } catch (RecordNotFoundException $e) { // When there is no $softwareId record in database
            return $this->redirect($this->getBaseUrl('mapping'), 301);
        }

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/mappingForm/$softwareId";
        if ($allowedUrl !== $this->request->here(false)) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Check if current user is already a "user of"
        $registry = TableRegistry::get("RelationshipsSoftwaresUsers");
        $userOf = $registry->exists([
            'user_id' => $userId,
            'software_id' => $softwareId,
            'relationship_id' =>  $this->getRelationshipIdByName('UserOf'),
        ]);
        if ($userOf === false) { // not already "user of"
            return $this->redirect("/$lang/softwares/$softwareId", 302);
        }

        // Get already existing associations between the user, the software and the taxonomies
        $existingEntries = $this->TaxonomysSoftwares->getListByUserIdBySofwareId($userId, $softwareId);
                // example: Array( <taxonId> => taxonomySoftware, ...)

        // Form processing
        if (isset($this->request->data['id'])) { // form sent
            $processing = $this->processingMappingForm($software, $user, $existingEntries);
            if ($processing === true) {
                return $this->redirect("/$lang/softwares/" . $software->get('id'));
            } else {
                return $this->redirect("/$lang/mappingForm/" . $software->get('id'));
            }
        }

        // Parameters for the view
        $this->set(compact('user'));
        $this->set(compact('software'));
        $this->set(compact('existingEntries'));
        $this->set(compact('mappingTaxons'));
        $this->set(compact('mappingHead'));
        $this->set('_serialize', ['existingEntries']);
        $this->set('_serialize', ['software']);
        $this->set('_serialize', ['mappingTaxons']);
        $this->set('_serialize', ['mappingHead']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.ListOfSoftware'),
            'url' => 'softwares'
        ];
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/". $software->id
        ];
        $links[] = [
        //    'name' => __d('Breadcrumbs', __d("Taxonomy", "TaxonomySoftware.Form.Add.Breadcrumbs")),
            'name' => __d('Breadcrumbs', __d("Softwares", "Softwares.Users.DeclareAs.user.addMessage")),
            'url' => "mappingForm/". $software->id
        ];
        $this->setBreadcrumbs($links);
    }


    /**
     * Processing mapping form
     *
     * Two cases:
     * - Add/Edit form sent empty ---> sub-processing by $this->processingMappingEmptyForm()
     * - Add/Edit form sent with data --> sub-processing by $this->processingMappingFormWithData()
     *
     * @param Software $software
     * @param User $user
     * @param array $existingEntries   example: [ <taxonId> => taxonomySoftware, ...]
     * @return bool
     */
    final private function processingMappingForm(Software $software, User $user, array $existingEntries): bool
    {
        $processing = true;

        // Form sent empty
        if (!isset($this->request->data['taxonCheckboxes']) || count($this->request->data['taxonCheckboxes']) === 0) {
            $processing = $this->processingMappingEmptyForm($existingEntries); // calls flash messages directly
        } else { // Form sent with data
            $processing = $this->processingMappingFormWithData($software, $user, $existingEntries);
            if ($processing === true) {
                $this->Flash->success(__d("Taxonomy", "TaxonomySoftware.FormProcessing.sucessMsg"));
            } else {
                $this->Flash->error(__d("Taxonomy", "TaxonomySoftware.FormProcessing.errorMsg"));
            }
        }
        return $processing;
    }


    /**
     * Processing add/edit form sent with data
     *
     * @param Software $software
     * @param User $user
     * @param array $existingEntries   example: [ <taxonId> => taxonomySoftware, ...]
     * @return bool
     */
    final private function processingMappingFormWithData(Software $software, User $user, array $existingEntries): bool
    {
        $processing = true;
        $softwareId = $software->id;
        $userId = $user->id;
        $selectedTaxon = $this->request->data['taxonCheckboxes'];
        foreach ($selectedTaxon as $idTaxon) {
            $idTaxon = (int) $idTaxon;
            // Preparing to delete old entries that have not been selected
            if (isset($existingEntries[$idTaxon])) {
                unset($existingEntries[$idTaxon]);
            } else {
                // Add new entries
                $taxonomySoftware = $this->TaxonomysSoftwares->newEntity();
                $taxonomySoftware->set('taxonomy_id', $idTaxon);
                $taxonomySoftware->set('software_id', $softwareId);
                $taxonomySoftware->set('user_id', $userId);
                // Additional form ----> to be reviewed (refs. z881)
                /////////////////////////////////////////////////////////
                //    if (isset($this->request->data['comment'])) {
                //        $comment = trim(strip_tags($this->request->data['comment']));
                //        if (!empty($comment)) {
                //            $taxonomySoftware->set('comment', "$comment");
                //        }
                //    }
                if (false === $this->TaxonomysSoftwares->save($taxonomySoftware)) {
                    $processing = false;
                }
            }
        }
        // Delete old entries that have not been selected.
        if (false === $this->TaxonomysSoftwares->deleteRecords($existingEntries)) {
            $processing = false;
        }
        return $processing;
    }

    /**
     * Processing empty mapping form
     *
     * Two cases:
     * - Add form sent empty
     * - Edit form sent empty --> delete all database entries
     *
     * @param array $existingEntries   example: [ taxonomySoftware, ...]
     * @return bool
     */
    final private function processingMappingEmptyForm(array $existingEntries): bool
    {
        $processing = true;
        if (count($existingEntries) === 0) { // Add form sent empty
            $this->Flash->error(__d("Taxonomy", "TaxonomySoftware.FormProcessing.formEmptyErrorMsg"));
            $processing = false;
        } else { // Edit form sent empty --> delete all database entries
            if (false === $this->TaxonomysSoftwares->deleteRecords($existingEntries)) {
                $processing = false;
            }

            if ($processing === true) {
                $this->Flash->success(__d("Taxonomy", "TaxonomySoftware.FormProcessing.formEmptySucessMsg"));
            } else {
                $this->Flash->error(__d("Taxonomy", "TaxonomySoftware.FormProcessing.errorMsg"));
            }
        }
        return $processing;
    }
}
