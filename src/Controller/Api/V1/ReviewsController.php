<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\ReviewsTable;
use App\Network\Exception\SoftwareNotFoundException;
use App\Network\Exception\UserNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;
use Exception;

/**
 * Reviews Controller
 *
 * @property ReviewsTable $Reviews
 */
class ReviewsController extends AppController
{

    /**
     * Manage all rights for the controllers' actions.
     * notice: $forceDeny parameter is not used here,
     *                     but is mandatory to be compatible with parent::isAuthorized()
     *
     * @param Array $user User informations
     * @param  boolean $forceDeny by default FALSE, set TRUE to force the deny on parent::isAuthorized()
     * @return boolean
     */
    public function isAuthorized($user, $forceDeny = false)
    {
        if ($this->Auth->user()) {
            if ($this->request->action === 'add') {
                $this->loadModel("Users");
                return $this->Users->isAdministration($this->Auth->user('id'));
            }
        }
    }

    public function beforeFilter(Event $event)
    {
        //        $this->Auth->allow('add');
        $this->Auth->deny('delete');
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if (isset($this->request->params['software_id'])) {
            $this->viewBuilder()->template("reviews_software");
            $software = $this->Reviews->Softwares->find("all")->select([
                "id",
                "softwarename"
            ])->where(["id" => $this->request->params['software_id']])->firstOrFail();
            $this->set('software', $software);
            $this->set('_serialize', ['software']);
        }

        $this->paginate = [
            'conditions' => isset($this->request->params['software_id']) ? [
                'Softwares.id ' => $this->request->params['software_id']
            ] : [],
            'contain' => [
                'Users' => [
                    'UserTypes',
                    "fields" => [
                        "id",
                        "username",
                        'logo_directory',
                        'photo',
                        ]
                ],
                'Softwares' => [
                    "fields" => [
                        "id",
                        "softwarename"
                    ]
                ]
            ]
        ];

        // Check that the current URL is correct
        $softwareId = $software->id;
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/softwares/$softwareId/reviews";
        if ($allowedUrl !== $this->request->here(false)) {
            return $this->redirect("$allowedUrl", 301);
        }

        $this->set('reviews', $this->paginate($this->Reviews));
        $this->set('_serialize', ['reviews']);


        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$softwareId"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.Reviews'),
            'url' => "softwares/$softwareId/reviews"
        ];
        $this->setBreadcrumbsSoftware($links);
    }

    /**
     * View method
     *
     * @param string|null $id Review id.
     * @return void
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {

        $review = $this->Reviews->get(
            $id,
            [
                'contain' => [
                    'Users' => [
                        'UserTypes',
                        'fields' => [
                            'id',
                            'username',
                            'logo_directory',
                            'photo',
                            'description'
                        ]
                    ],
                    'Softwares' => [
                        "Reviews",
                        'fields' => [
                            'id',
                            'softwarename',
                            'url_website',
                            'url_repository',
                            'logo_directory',
                            'photo',
                            'description',
                        ]
                    ]
                ]
            ]
        );
        $software   = $review->software;
        $softwareId = $software->id;

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/softwares/$softwareId/reviews/". (int) $id;
        if ($allowedUrl !== $this->request->here(false)) {
            return $this->redirect("$allowedUrl", 301);
        }

        $this->set('review', $review);
        $this->set('_serialize', ['review']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$softwareId"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.Reviews'),
            'url' => "softwares/$softwareId/reviews"
        ];
        $this->setBreadcrumbsSoftware($links);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post', 'get']);

        try {
            $software = $this->Reviews->Softwares->get($this->request->params['software_id']);
            $this->request->data['software_id'] = $software->id;
        } catch (Exception $e) {
            throw new SoftwareNotFoundException(
                "The software with the id " . $this->request->params['software_id'] . " does not exist"
            );
        }

        try {
            $user = $this->Reviews->Users->get($this->Auth->user('id'));
            $this->request->data['user_id'] = $user->id;
        } catch (Exception $e) {
            throw new UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
        }

        $review = $this->Reviews->newEntity();
        $message = "";
        if ($this->request->is('post')) {
            $review = $this->Reviews->patchEntity($review, $this->request->data);

            if ($this->Reviews->save($review)) {
                $message = "Success";
                if (!$this->request->is('json')) {
                    $this->Flash->success(__d("Forms", "Your review has been posted"));
                    $this->redirect("softwares/" . $this->request->data['software_id']);
                }
            } else {
                if (!$this->request->is('json')) {
                    $this->Flash->error(__d("Forms", "You can not post more than one review for a software."));
                    $this->redirect("softwares/" . $this->request->data['software_id']);
                } else {
                    $message = "Error";
                    $review = $review->errors();
                }
            }
        }
        $this->set(
            [
                'message' => $message,
                'review' => $review,
                '_serialize' => ['message', 'review']
            ]
        );

        // Breadcrumbs
        $softwareId = $software->id;
        $links = array();
        $links[] = [
            'name' => $software->softwarename,
            'url' => "softwares/$softwareId"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Software.AddReview'),
            'url' => "softwares/add-review/$softwareId"
        ];
        $this->setBreadcrumbsSoftware($links);
    }

    /**
     * $links = [ 0 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'],
     *            1 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'], ]
     *
     * @param array $links
     */
    protected function setBreadcrumbsSoftware(array $links = [])
    {
        $firstLink = [
            'name' => __d('Breadcrumbs', 'Software.ListOfSoftware'),
            'url' => 'softwares'
        ];
        array_unshift($links, $firstLink);
        parent::setBreadcrumbs($links);
    }

    /**
     * Edit method
     *
     * @param string|null $id Review id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $review = $this->Reviews->get(
            $id,
            [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $review = $this->Reviews->patchEntity($review, $this->request->data);
            if ($this->Reviews->save($review)) {
                $this->Flash->success(__('The review has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The review could not be saved. Please, try again.'));
            }
        }
        $users = $this->Reviews->Users->find('list', ['limit' => 200]);
        $softwares = $this->Reviews->Softwares->find('list', ['limit' => 200]);
        $this->set(compact('review', 'users', 'softwares'));
        $this->set('_serialize', ['review']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Review id.
     * @return Response|null Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $review = $this->Reviews->get($id);
        if ($this->Reviews->delete($review)) {
            $this->Flash->success(__('The review has been deleted.'));
        } else {
            $this->Flash->error(__('The review could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
