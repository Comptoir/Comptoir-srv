<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\RawMetricsSoftwaresTable;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * RawMetricsSoftwares Controller
 *
 * @property RawMetricsSoftwaresTable $RawMetricsSoftwares
 */
class RawMetricsSoftwaresController extends AppController
{

    /**
     * Index method
     *
     * @return Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Softwares']
        ];
        $rawMetricsSoftwares = $this->paginate($this->RawMetricsSoftwares);

        $this->set(compact('rawMetricsSoftwares'));
        $this->set('_serialize', ['rawMetricsSoftwares']);
    }

    /**
     * View method
     *
     * @param string|null $id Raw Metrics Software id.
     * @return Response|null
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rawMetricsSoftware = $this->RawMetricsSoftwares->get(
            $id,
            [
                'contain' => ['Softwares']
            ]
        );

        $this->set('rawMetricsSoftware', $rawMetricsSoftware);
        $this->set('_serialize', ['rawMetricsSoftware']);
    }

    /**
     * Add method
     *
     * @return Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rawMetricsSoftware = $this->RawMetricsSoftwares->newEntity();
        if ($this->request->is('post')) {
            $rawMetricsSoftware = $this->RawMetricsSoftwares->patchEntity($rawMetricsSoftware, $this->request->data);
            if ($this->RawMetricsSoftwares->save($rawMetricsSoftware)) {
                $this->Flash->success(__('The raw metrics software has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The raw metrics software could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->RawMetricsSoftwares->Softwares->find('list', ['limit' => 200]);
        $this->set(compact('rawMetricsSoftware', 'softwares'));
        $this->set('_serialize', ['rawMetricsSoftware']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Raw Metrics Software id.
     * @return Response|void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rawMetricsSoftware = $this->RawMetricsSoftwares->get(
            $id,
            [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rawMetricsSoftware = $this->RawMetricsSoftwares->patchEntity($rawMetricsSoftware, $this->request->data);
            if ($this->RawMetricsSoftwares->save($rawMetricsSoftware)) {
                $this->Flash->success(__('The raw metrics software has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The raw metrics software could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->RawMetricsSoftwares->Softwares->find('list', ['limit' => 200]);
        $this->set(compact('rawMetricsSoftware', 'softwares'));
        $this->set('_serialize', ['rawMetricsSoftware']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Raw Metrics Software id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rawMetricsSoftware = $this->RawMetricsSoftwares->get($id);
        if ($this->RawMetricsSoftwares->delete($rawMetricsSoftware)) {
            $this->Flash->success(__('The raw metrics software has been deleted.'));
        } else {
            $this->Flash->error(__('The raw metrics software could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Returns raw metrics contain in the database for a software ID.
     */
    public function getRawMetricsBySoftwareId($id = null)
    {

        if ($id != null && $this->request->is('get') && $this->response->type('json')) {
            $rawMetrics = $this->RawMetricsSoftwares->find(
                'all',
                [
                    'conditions' => ["software_id = " => $id]
                ]
            );
            $this->set(compact('rawMetrics'));

            $this->set('_serialize', ['rawMetrics']);
        }
    }
}
