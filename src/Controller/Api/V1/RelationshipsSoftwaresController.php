<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\RelationshipsSoftwaresTable;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * RelationshipsSoftwares Controller
 *
 * @property RelationshipsSoftwaresTable $RelationshipsSoftwares
 */
class RelationshipsSoftwaresController extends AppController
{


    /**
     * Manage all rights for the controllers' actions.
     * notice: $forceDeny parameter is not used here,
     *                     but is mandatory to be compatible with parent::isAuthorized()
     *
     * @param Array $user User informations
     * @param  boolean $forceDeny by default FALSE, set TRUE to force the deny on parent::isAuthorized()
     * @return boolean
     */
    public function isAuthorized($user, $forceDeny = false)
    {

        //If the user is connected
        if ($this->Auth->user('id')) {
            if (in_array($this->request->action, ["add", "edit", "delete"])) {
                return true;
            }
        }
        return parent::isAuthorized($user, $forceDeny);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('relationshipsSoftwares', $this->paginate($this->RelationshipsSoftwares));
        $this->set('_serialize', ['relationshipsSoftwares']);
    }

    /**
     * View method
     *
     * @param string|null $id Relationships Software id.
     * @return void
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $relationshipsSoftware = $this->RelationshipsSoftwares->get(
            $id,
            [
                'contain' => []
            ]
        );
        $this->set('relationshipsSoftware', $relationshipsSoftware);
        $this->set('_serialize', ['relationshipsSoftware']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $relationshipsSoftware = $this->RelationshipsSoftwares->newEntity();
        if ($this->request->is('post')) {
            $relationshipsSoftware = $this->RelationshipsSoftwares->patchEntity(
                $relationshipsSoftware,
                $this->request->data
            );
            if ($this->RelationshipsSoftwares->save($relationshipsSoftware)) {
                $this->Flash->success(__('The relationships software has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationships software could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->RelationshipsSoftwares->Softwares->find('list', ['limit' => 200]);
        $relationships = $this->RelationshipsSoftwares->Relationships->find('list', ['limit' => 200]);
        $this->set(compact('relationships', 'softwares'));
        $this->set('_serialize', ['relationshipsSoftware']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Relationships Software id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $relationshipsSoftware = $this->RelationshipsSoftwares->get(
            $id,
            [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $relationshipsSoftware = $this->RelationshipsSoftwares->patchEntity(
                $relationshipsSoftware,
                $this->request->data
            );
            if ($this->RelationshipsSoftwares->save($relationshipsSoftware)) {
                $this->Flash->success(__('The relationships software has been saved.'));
            } else {
                $this->Flash->error(__('The relationships software could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('relationshipsSoftware'));
        $this->set('_serialize', ['relationshipsSoftware']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Relationships Software id.
     * @return Response|null Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $relationshipsSoftware = $this->RelationshipsSoftwares->get($id);
        if ($this->RelationshipsSoftwares->delete($relationshipsSoftware)) {
            $this->Flash->success(__('The relationships software has been deleted.'));
        } else {
            $this->Flash->error(__('The relationships software could not be deleted. Please, try again.'));
        }
    }
}
