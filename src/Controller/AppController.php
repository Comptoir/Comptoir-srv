<?php //

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Cache\AppCacheTrait as AppCacheTrait;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Crud\Controller\ControllerTrait;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    use AppCacheTrait;
    use ControllerTrait;

    private $breadcrumbs = [];

    public $components = [
        'RequestHandler',
    ];
    protected $availableLanguages = [
        'en' => 'en',
        'fr' => 'fr',
    ];
    protected $explicitLanguages = [
        'en' => 'english',
        'fr' => 'français',
    ];
    protected $selectedLanguage = 'en';

    protected $appVersion;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');

        $this->loadComponent('Flash');

        $this->loadComponent(
            'Auth',
            [
                'authorize' => ['Controller'],
                'loginAction' => [
                    'controller' => 'Users',
                    'action' => 'login',
                    'prefix' => false,
                ],
                'authenticate' => [
                    'Form' => [
                        'fields' => ['username' => 'email']
                    ]
                ],
                'unauthorizedRedirect' => false,

                'storage' => 'Session'
            ]
        );

        $this->loadComponent(
            'Search.Prg',
            [
                // This is default config. You can modify "actions" as needed to make
                // the PRG component work only for specified methods.
                'actions' => ['index', 'search']
            ]
        );

        $this->loadComponent('ValidationRules');

        $this->appVersion = Configure::read("VERSION.footer");
    }

    /**
     * User is authorized
     * by default, all GET requests are allowed
     * unless the $forceDeny parameter is true (default is false)
     *
     * @param  array   $user
     * @param  boolean $forceDeny  Allow descendant to refuse GET requests on current object
     * @return bool
     */
    public function isAuthorized($user, $forceDeny = false)
    {
        // Allow all get action, except when the optional parameter $forceDeny is true
        if ($this->request->is('get') && $forceDeny === false) {
            return true;
        }

        // Admin peuvent accéder à chaque action
        if (isset($user['role']) && $user['role'] === 'admin') {
            return true;
        }
        $this->Flash->error(__('You are not allowed to do that.'));
        // Par défaut refuser
        return false;
    }


    /**
     * Before render callback.
     *
     * @param Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars)
            && in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->RequestHandler->renderAs($this, 'json');
            $this->set('_serialize', true);
        }
        $this->set('appVersion', $this->appVersion);
        $this->set('appFullBaseUrl', Configure::read('App.fullBaseUrl'));
        $this->set('mappingBaseUrl', $this->getBaseUrl('mapping'));
        $this->set('breadcrumbs', $this->getBreadcrumbs());
    }

    /**
     * @return array
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }


    /**
     * @param array $links
     */
    protected function setBreadcrumbs(array $links = [])
    {
        $firstLink = [
            'name' => __d('Breadcrumbs', 'Home'),
            'url' => ''
        ];
        array_unshift($links, $firstLink);
        foreach ($links as $link) {
            $this->addToBreadcrumbs($link);
        }
    }

    /**
     * @param array $linkData ex: [ 'name' => '…', 'url' => '/dir/file', 'title' => '…']
     */
    private function addToBreadcrumbs(array $linkData)
    {
        if (isset($linkData['name'])) {
            $data = [];
            $data['name'] = strip_tags(trim($linkData['name']));
            $data['name'] = substr($data['name'], 0, 60);
            if (isset($linkData['title']) && !empty($linkData['title'])) {
                $data['title'] = trim(filter_var($linkData['title'], FILTER_SANITIZE_STRING));
            }
            if (isset($linkData['url'])) {
                $lang = $this->selectedLanguage;
                $urlPart = $linkData['url'];
                $data['url'] = "/$lang/$urlPart";
            }
            $this->breadcrumbs[] = $data;
        }
    }


    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['index', 'logout', 'view', 'request', 'search']);
        $this->setLocale();
        $this->setOpenGraph();
        parent::beforeFilter($event);
    }

    /**
     * Sets the current locale based on url param and available languages
     *
     * @return void
     */
    protected function setLocale()
    {
        if ($this->request->param('language')) {
            $lang = $this->request->param('language');
        } else {
            $lang = preg_replace('/_\w*/', "", I18n::locale());
        }

        if (isset($this->availableLanguages[$lang])) {
            I18n::locale($lang);
            $selectedLanguage = $this->availableLanguages[$lang];
        } else {
            I18n::locale('en');
            $selectedLanguage = I18n::locale();
        }

        $this->selectedLanguage = $selectedLanguage;
        $this->set('selectedLanguage', $selectedLanguage);
        $this->set('availableLanguages', $this->availableLanguages);
        $this->set('explicitLanguages', $this->explicitLanguages);
    }

    /**
     * Returns the base URL based on the user's language
     * Usage:
     * - $this->getBaseUrl()          = /fr/              or /en/
     * - $this->getBaseUrl('mapping') = /fr/cartographie/ or /en/mapping/
     *
     * @param string $type             (optional) allowed values : '' or 'mapping', by default it's ''
     * @param bool $withLanguagePrefix (optional) add the language prefix in the URL, by default it's true
     * @return string base URL based on the user's language.
     */
    final protected function getBaseUrl(string $type = '', $withLanguagePrefix = true)
    {
        $url = '';
        if ($withLanguagePrefix === true) {
            $url .= '/'.$this->selectedLanguage .'/';
        }

        if ($type === 'mapping') {
            if ($this->selectedLanguage === 'fr') {
                $url .= 'cartographie/';
            } else {
                $url .= 'mapping/';
            }
        }
        return $url ;
    }

    private function setOpenGraph()
    {
        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title" => Configure::read("OpenGraph.title"),
            "description" => __d("Layout", "opengraph.description"),
            "image" => Configure::read("OpenGraph.image"),
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title" => Configure::read("OpenGraph.title"),
            "description" => __d("Layout", "opengraph.description"),
            "image" => Configure::read("OpenGraph.image"),
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);
    }
}
