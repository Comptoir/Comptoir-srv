<?php

namespace App\Network\Exception;

use Cake\Network\Exception\NotFoundException;

class UserTypeNotFoundException extends NotFoundException
{

}
