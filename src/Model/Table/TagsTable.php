<?php

namespace App\Model\Table;

use App\Model\Entity\Tag;
use App\Network\Exception\TagNotFoundException;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;

/**
 * Tags Model
 *
 * @property BelongsToMany $Softwares
 *
 * @method Tag newEntity($data = null, array $options = [])
 * @method Tag[] newEntities(array $data, array $options = [])
 * @method Tag|bool save(EntityInterface $entity, $options = [])
 * @method Tag patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Tag[] patchEntities($entities, array $data, array $options = [])
 * @method Tag findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tags');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany(
            'Softwares',
            [
                'foreignKey' => 'tag_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'softwares_tags'
            ]
        );



        //  #######################
        //  ##### For search ######
        //  #######################

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('id')
            ->add(
                'search',
                'Search.Like',
                [
                    'before' => true,
                    'after' => true,
                    'mode' => 'OR',
                    'comparison' => 'ILIKE',
                    'wildcardAny' => '*',
                    'wildcardOne' => '?',
                    'field' => [
                        $this->aliasField('name')
                    ]
                ]
            )
            //  ########################## End For search ##########################
        ;
    }

    /**
     * Default validation rules.
     * Is allow : alphanumeric character a->z and A->Z and "-" "_" only.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->maxLength('name', 100, "Provided value is too long (max : 100).");

        $validator
            ->allowEmpty('description_i18n_fr');

        $validator
            ->allowEmpty('description_i18n_en');

        $validator->add(
            'name',
            'alphaNumeric',
            [
                'rule' => [
                    'custom',
                    '/^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\-]+$/'
                ],
                'message' => __d("Tags", "Name must be an alphaNumeric value. You provided an invalid value.")

            ]
        );

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(
            ['name'],
            __d("Tags", "A tag with this name already exists in Comptoir du libre.")
        ));
        return $rules;
    }


    /**
     * getTag search tag's data with an id.
     * If this tag (id) does not exist error message will be : The Tag with the id $id does not exist.
     *
     * @override
     *
     * @param int $id
     * @param array $options
     *
     * @return EntityInterface|mixed
     */
    public function get($id, $options = [])
    {
        if (!is_array($options) || count($options) === 0) {
            $options = [
                'contain' => [
                    'Softwares' => [
                        'Tags'
                    ]
                ]
            ];
        }
        try {
            $tag = parent::get(
                $id,
                $options
            );
        } catch (Exception $e) {
            throw new TagNotFoundException(__d("Tags", "The Tag with id {0} does not exist", $id));
        }
        return $tag;
    }
}
