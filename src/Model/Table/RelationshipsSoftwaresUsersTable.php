<?php

namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RelationshipsSoftwaresUsers Model
 *
 * @property BelongsTo $Softwares
 * @property BelongsTo $Users
 * @property BelongsTo $Relationships
 */
class RelationshipsSoftwaresUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('relationships_softwares_users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Softwares',
            [
                'foreignKey' => 'software_id',
                'joinType' => 'INNER'
            ]
        );
        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER'
            ]
        );
        $this->belongsTo(
            'Relationships',
            [
                'foreignKey' => 'relationship_id',
                'joinType' => 'INNER'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
        return $validator;
    }


    public function afterSave(Event $created, $options = array())
    {

        if ($options->isNew()) {
            $event = new Event('Model.RelationshipSoftwareUser.created', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    public function afterDelete(Event $deleted, $options = array())
    {

        if ($deleted) {
            $event = new Event('Model.RelationshipSoftwareUser.deleted', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    public function findusersForSoftwares(Query $query, array $options)
    {
        $query->contain(
            ['Users' => [
                'UserTypes',
                'fields' => [
                    'id',
                    'username',
                    'logo_directory',
                    'photo',
                    'description'
                ]
            ]]
        );
        return $query;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['relationship_id'], 'Relationships'));

        $rules->add(
            $rules->isUnique(
                ['software_id', 'user_id', 'relationship_id'],
                __d("Forms", 'You can not be declare twice for the same relationships.')
            )
        );
        return $rules;
    }
}
