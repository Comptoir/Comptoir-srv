<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Josegonzalez\Upload\Validation\DefaultValidation;

// User auth


/**
 * Users Model
 *
 * @property BelongsTo $UserTypes
 * @property HasMany $Reviews
 * @property BelongsToMany $RelationshipsSoftwares
 * @property BelongsToMany $Relationships
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'Josegonzalez/Upload.Upload',
            [
                'photo' => [
                    'fields' => [
                        'dir' => 'logo_directory',
                    ],
                    'filesystem' => [
                        'root' => WWW_ROOT . 'img' . DS,
                    ],
                    'path' => 'files{DS}{model}{DS}{field}{DS}{primaryKey}{DS}avatar',
                ],
            ]
        );
        //----------------------------------------------------------------------
        //-------------------------Logique BDD----------------------------------
        //----------------------------------------------------------------------
        $this->belongsTo(
            'UserTypes',
            [
                'foreignKey' => 'user_type_id',
                'joinType' => 'INNER'
            ]
        );
        $this->hasMany(
            'Reviews',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->hasMany(
            'RelationshipsSoftwaresUsers',
            [
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'relationships_softwares_users'
            ]
        );
        $this->belongsToMany(
            'Relationships',
            [
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'relationship_id',
                'joinTable' => 'relationships_users'
            ]
        );

        //----------------------------------------------------------------------
        //-------------------------Logique applicative--------------------------
        //----------------------------------------------------------------------

        /**
         * Used softwares
         */
        $this->hasMany(
            'Usedsoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => [
                    'relationship_id' => $this->query()
                        ->from('Relationships')
                        ->select('Relationships.id')->where(['Relationships.cd' => 'UserOf'])
                        ->hydrate(true)
                ]
            ]
        );

        /**
         * Backed softwares
         */
        $this->hasMany(
            'Backedsoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => [
                    'relationship_id' => $this->query()
                        ->from('Relationships')
                        ->select('Relationships.id')->where(['Relationships.cd' => 'BackerOf'])
                        ->hydrate(true)
                ]
            ]
        );
        $this->hasMany(
            'Createdsoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => [
                    'relationship_id' => $this->query()
                        ->from('Relationships')
                        ->select('Relationships.id')->where(['Relationships.cd' => 'CreatorOf'])
                        ->hydrate(true)
                ]
            ]
        );
        /**
         * Contributior to
         */
        $this->hasMany(
            'Contributionssoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => [
                    'relationship_id' => $this->query()
                        ->from('Relationships')
                        ->select('Relationships.id')->where(['Relationships.cd' => 'ContributorOf'])
                        ->hydrate(true)
                ]
            ]
        );
        //TODO revoir la base de données
        /**
         * Provider For
         */
        $this->hasMany(
            'Providerforsoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' =>
                    [
                        "OR" => [
                            [
                                "relationship_id" => $this->query()
                                    ->from('Relationships')
                                    ->select('Relationships.id')->where(['Relationships.cd' => 'ServicesProvider'])
                                    ->hydrate(true)
                            ]
                            ,
                            [
                                "relationship_id" => $this->query()
                                    ->from('Relationships')
                                    ->select('Relationships.id')->where(['Relationships.cd' => 'ProviderFor'])
                                    ->hydrate(true)
                            ]
                        ]
                    ]

            ]
        );

        //  #######################
        //  ##### For search ######
        //  #######################

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('id')
            ->add(
                'search',
                'Search.Like',
                [
                    'before' => true,
                    'after' => true,
                    'mode' => 'OR',
                    'comparison' => 'ILIKE',
                    'wildcardAny' => '*',
                    'wildcardOne' => '?',
                    'field' => [
                        $this->aliasField('username'),
                        $this->aliasField('description')
                    ]
                ]
            )
            ->add(
                'userType',
                'Search.Callback',
                [
                    'callback' => function (Query $query, $args, $manager) {
                        if (!empty($args["userType"]) && $args["userType"] !== 'all') {
                            return $query->andWhere(
                                [
                                    "Users.user_type_id IN " => $this
                                        ->find("byType", $args)
                                ]
                            )
                                ->select('UserTypes.id');
                        }
                    }
                ]
            )
            ->add(
                'hadReview',
                'Search.Callback',
                [
                    'callback' => function (Query $query, $args, $manager) {
                        if ($args["hadReview"] == "true") {
                            return $query->andWhere(
                                [
                                    "Users.id IN " => $this
                                        ->find("hadReview")
                                        ->select("Reviews.user_id")
                                ]
                            );
                        }
                    }
                ]
            )
            ->add(
                'isUserOf',
                'Search.Callback',
                [
                    'callback' => function (Query $query, $args, $manager) {
                        if ($args["isUserOf"] == "true") {
                            return $query->andWhere(
                                [
                                    "Users.id IN " => $this
                                        ->find("isUserOf")
                                        ->select("RelationshipsSoftwaresUsers.user_id")
                                ]
                            );
                        }
                    }
                ]
            )
            ->add(
                'IsSerivicesProvider',
                'Search.Callback',
                [
                    'callback' => function (Query $query, $args, $manager) {
                        if ($args["IsSerivicesProvider"] == "true") {
                            return $query->andWhere(
                                [
                                    "Users.id IN " => $this
                                        ->find("IsSerivicesProvider")
                                        ->select("RelationshipsSoftwaresUsers.user_id")
                                ]
                            );
                        }
                    }
                ]
            )//  ########################## End For search ##########################
        ;
    }


    /**
     * Check before the creation of the entity if the 'username' and 'description' field are containing html tags.
     * If there are, they are deleted
     *
     * @param Event $event
     * @param $data
     * @param $options
     * @link  https://book.cakephp.org/3.0/en/orm/table-objects.html#lifecycle-callbacks
     *          to see more on life cycle of objects
     */
    public function beforeMarshal(Event $event, $data, $options)
    {
        if (isset($data['username'])) {
            $data['username'] = strip_tags($data['username']);
        }
        if (isset($data['description'])) {
            $data['description'] = strip_tags($data['description']);
        }
    }

    /**
     * Create an event after save an user
     *
     * @param $created
     * @param array $options
     *
     * @return True if created False otherwise
     */
    public function afterSave(Event $created, $options = array())
    {

        if ($options->isNew()) {
            $event = new Event('Model.User.created', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        } else {
            $event = new Event('Model.User.modified', $this, $options);
            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    /**
     * Returns TRUE if the user as know as Administration FALSE otherwise
     *
     * @param $userId user id Auth
     *
     * @return bool
     */
    public function isAdministration($userId)
    {

        $administration = $this->UserTypes->find("all")
            ->select('id')
            ->where(["cd = " => "Administration"])->first();
        return $this->exists(["user_type_id" => $administration->id, "id" => $userId]);
    }

    /**
     * Returns TRUE if the user as know as Administration FALSE otherwise
     *
     * @param $userId user id Auth
     *
     * @return bool
     */
    public function isCompany($userId)
    {

        $company = $this->UserTypes->find("all")
            ->select('id')
            ->where(["cd = " => "Company"])->first();
        return $this->exists(["user_type_id" => $company->id, "id" => $userId]);
    }

    public function findByType(Query $query, array $options)
    {
        return $this->find("all")
            ->select('UserTypes.id')
            ->contain(['UserTypes'])
            ->where(['UserTypes.cd LIKE' => $options['userType']]);
    }

    /**
     * Return a list of users published
     *
     * @param Query $query
     * @param array $options
     *
     * @return $this
     */
    public function findHadReview(Query $query, array $options)
    {
        $users = $this->find()
            ->innerJoinWith("Reviews")
            ->where(
                [
                    "OR" =>
                        [
                            "Reviews.title IS NOT " => null,
                            "Reviews.evaluation IS NOT " => null,
                            "Reviews.comment IS NOT " => null
                        ]
                ]
            );
        return $users->group(['Reviews.user_id', "Users.id"]);
    }

    public function findIsUserOf(Query $query, array $options)
    {

        $users = $this->find()
            ->innerJoinWith("RelationshipsSoftwaresUsers")
            ->where(
                [
                    "relationship_id" => $this->query()
                        ->from('Relationships')
                        ->select('Relationships.id')->where(['Relationships.cd' => 'UserOf'])
                        ->hydrate(true)
                ]
            ) // userOF
        ;
        return $users->group(['RelationshipsSoftwaresUsers.user_id', 'Users.id']);
    }

    /**
     * Return all users who is services provider
     *
     * @param Query $query
     * @param array $options
     *
     * @return $this
     */
    public function findIsSerivicesProvider(Query $query, array $options)
    {
        $users = $this->find()
            ->innerJoinWith("RelationshipsSoftwaresUsers")
            ->where(
                [
                    "relationship_id" =>
                        $this->query()
                            ->from('Relationships')
                            ->select('Relationships.id')
                            ->where(['Relationships.cd' => 'ProviderFor'])
                            ->hydrate(true)
                ]
            );
        return $users->group(['RelationshipsSoftwaresUsers.user_id', 'Users.id']);
    }


    public function findByEmail(Query $query, array $options)
    {
        $user = $options['user'];
        return $query->where(['email' => $user]);
    }

    public function validationPassword(Validator $validator)
    {
        $validator
            ->add(
                'old_password',
                'check',
                [
                    'rule' => function ($value, $context) {
                        $user = $this->get($context['data']['id']);
                        if ($user) {
                            if ((new DefaultPasswordHasher())->check($value, $user->password)) {
                                return true;
                            }
                        }
                        return false;
                    },
                    'message' => __d("Forms", 'Your old password does not match the entered password!'),
                ]
            )
            ->notEmpty('old_password');
        $validator
            ->add(
                'password',
                [
                    'compare' => [
                        'rule' => ['compareWith', 'confirm_password'],
                        'message' => __d("Forms", 'Password field and confirm-password field should be equals.')
                    ]
                ]
            )
            ->notEmpty('password');

        $validator
            ->add(
                'confirm_password',
                [
                    'compare' => [
                        'rule' => ['compareWith', 'password'],
                        'message' => __d("Forms", 'Password field and confirm-password field should be equals.')
                    ]
                ]
            )
            ->notEmpty('confirm_password');

        return $validator;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create');

        $validator
            ->allowEmpty('logo_directory');

        // URL field: allow only a valid URL with HTTP scheme ('https://' or 'http://')
        $validator
            ->allowEmpty('url')
            ->add('url', [
                'validUrlFormat' => [
                      'rule' => array('url'),
                      'last' => true,
                      'message' => 'Please enter a valid URL'
                ],
                'isHttpProtocol' => [
                      'rule' => array('custom', '/^(http|https)(:\/\/)/i'),
                      'message' => 'Please enter a valid URL'
                ]
            ]);

        $validator
            ->allowEmpty('description');

        $validator
            ->requirePresence('role', 'create')
            ->notEquals("role", 'admin')
            ->notEquals('role', "Admin")
            ->notEmpty('role');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator->add(
            'password',
            [
                'compare' => [
                    'rule' => ['compareWith', 'confirm_password'],
                    'message' => __d('Forms', 'Password field and confirm-password field should be equals.'),
                    'on' => "create",
                ]
            ]
        );


        $validator
            ->requirePresence('email', 'create')
            ->email('email', false, __d("Forms", "The email is invalid"));

        $validator
            ->allowEmpty('photo');

        $validator->provider('upload', new DefaultValidation());

        $validator->add(
            'photo',
            "UnderPhpSizeLimit",
            [
                "rule" => "isUnderPhpSizeLimit",
                "message" => __d("Forms", "The file can not be uploaded"),
                'provider' => 'upload',

            ]
        );

        $validator->add(
            'photo',
            'fileFileUpload',
            [
                'rule' => 'isFileUpload',
                //            'message' => __d('Forms','There was no file found to upload'),
                'provider' => 'upload'
            ]
        );

        $validator->add(
            'photo',
            'fileCompletedUpload',
            [
                'rule' => 'isCompletedUpload',
                //            'message' => __d('Forms','This file could not be uploaded completely'),
                'provider' => 'upload'
            ]
        );

        $validator->add(
            'photo',
            'file',
            [
                'rule' => ['mimeType', ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml']],
                'message' => __d(
                    'Forms',
                    "This format is not allowed please use one in this list: JPEG, PNG, GIF, SVG."
                )
            ]
        );

        $validator->add(
            'photo',
            'fileBelowMaxSize',
            [
                'rule' => ['isBelowMaxSize', 1048576],
                'message' => __d('Forms', 'The maximum weight allowed is 1MB, please use a file less than 1MB.'),
                'provider' => 'upload',
            ]
        );


        //Check that the file is below the maximum height requirement (checked in pixels)
        $validator->add(
            'photo',
            'fileBelowMaxHeight',
            [
                'rule' => ['isBelowMaxHeight', 350],
                'message' => __d(
                    'Forms',
                    'The size of your image is too big, please use an image fitting a 350x350px size.'
                ),
                'provider' => 'upload',
                'on' => function ($context) {
                    return in_array($context['data']['photo']['type'], ['image/jpeg', 'image/png', 'image/gif']);
                },
                'last' => true,
            ]
        );

        //Check that the file is below the maximum width requirement (checked in pixels)
        $validator->add(
            'photo',
            'fileBelowMaxWidth',
            [
                'rule' => ['isBelowMaxWidth', 350],
                'message' => __d(
                    'Forms',
                    'The size of your image is too big, please use an image fitting a 350x350px size.'
                ),
                'provider' => 'upload',
                'on' => function ($context) {
                    return in_array($context['data']['photo']['type'], ['image/jpeg', 'image/png', 'image/gif']);
                }
            ]
        );

        return $validator;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username'], __d('Forms', "A user with this name already exists.")));
        $rules->add($rules->isUnique(['email'], __d('Forms', "A user with this email already exists.")));
        $rules->add($rules->existsIn(['user_type_id'], 'UserTypes'));
        return $rules;
    }
}
