<?php

namespace App\Model\Table;

use App\Model\Entity\StatisticsAverage;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StatisticsAverages Model
 *
 * @method StatisticsAverage get($primaryKey, $options = [])
 * @method StatisticsAverage newEntity($data = null, array $options = [])
 * @method StatisticsAverage[] newEntities(array $data, array $options = [])
 * @method StatisticsAverage|bool save(EntityInterface $entity, $options = [])
 * @method StatisticsAverage patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method StatisticsAverage[] patchEntities($entities, array $data, array $options = [])
 * @method StatisticsAverage findOrCreate($search, callable $callback = null)
 *
 * @mixin TimestampBehavior
 */
class StatisticsAveragesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('statistics_averages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('delta_commit_one_month')
            ->requirePresence('delta_commit_one_month', 'create')
            ->notEmpty('delta_commit_one_month');

        $validator
            ->numeric('delta_commit_twelve_month')
            ->requirePresence('delta_commit_twelve_month', 'create')
            ->notEmpty('delta_commit_twelve_month');

        $validator
            ->numeric('number_of_contributors')
            ->requirePresence('number_of_contributors', 'create')
            ->notEmpty('number_of_contributors');

        return $validator;
    }
}
