<?php

namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reviews Model
 *
 * @property BelongsTo $Users
 * @property BelongsTo $Softwares
 */
class ReviewsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reviews');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->belongsTo(
            'Softwares',
            [
                'foreignKey' => 'software_id'
            ]
        );
    }

    /**
     * Check before the creation of the entity if the 'title' and 'comment' field are containing html tags.
     * If there are, they are deleted
     *
     * @param Event $event
     * @param $data
     * @param $options
     * @link  https://book.cakephp.org/3.0/en/orm/table-objects.html#lifecycle-callbacks
     *          to see more on life cycle of objects
     */
    public function beforeMarshal(Event $event, $data, $options)
    {
        if (isset($data['title'])) {
            $data['title'] = strip_tags($data['title']);
        }
        if (isset($data['comment'])) {
            $data['comment'] = strip_tags($data['comment']);
        }
    }


    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->range('evaluation', [1, 4]);

        return $validator;
    }

    /**
     * @param Event $created
     * @param array $options
     *
     * @return bool
     */
    public function afterSave(Event $created, $options = array())
    {

        if ($options->isNew()) {
            $event = new Event('Model.Review.created', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        $rules->add($rules->isUnique(
            ['software_id', 'user_id'],
            __d("Forms", "You can not post more than one review for a software.")
        ));

        return $rules;
    }
}
