<?php
namespace App\Model\Table;

use App\Model\Entity\TaxonomysSoftware;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TaxonomysSoftwares Model
 *
 * @property BelongsTo $Taxonomys
 * @property BelongsTo $Softwares
 * @property BelongsTo $Users
 *
 * @method TaxonomysSoftware get($primaryKey, $options = [])
 * @method TaxonomysSoftware newEntity($data = null, array $options = [])
 * @method TaxonomysSoftware[] newEntities(array $data, array $options = [])
 * @method TaxonomysSoftware|bool save(EntityInterface $entity, $options = [])
 * @method TaxonomysSoftware patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method TaxonomysSoftware[] patchEntities($entities, array $data, array $options = [])
 * @method TaxonomysSoftware findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TaxonomysSoftwaresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('taxonomys_softwares');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Taxonomys', [
            'foreignKey' => 'taxonomy_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Softwares', [
            'foreignKey' => 'software_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Check before the creation of the entity if the 'comment' field is containing html tags.
     * If there is, they are deleted
     *
     * @param Event $event
     * @param $data
     * @param $options
     * @link  https://book.cakephp.org/3.0/en/orm/table-objects.html#lifecycle-callbacks
     *          to see more on life cycle of objects
     */
    public function beforeMarshal(Event $event, $data, $options)
    {
        if (isset($data['comment'])) {
            $data['comment'] = trim(strip_tags($data['comment']));
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Create an event after save an entity
     *
     * @param $created
     * @param array $options
     *
     * @return True if created False otherwise
     */
    public function afterSave(Event $created, $options = array())
    {
        if ($options->isNew()) {
            $event = new Event('Model.TaxonomySoftware.created', $this, $options);
            $this->eventManager()->dispatch($event);
            return true;
        } else {
            $event = new Event('Model.TaxonomySoftware.modified', $this, $options);
            $this->eventManager()->dispatch($event);
            return true;
        }
        return false;
    }

    /**
     * Create an event after delete an entity
     *
     * @param Event $deleted
     * @param array $options
     * @return bool
     */
    public function afterDelete(Event $deleted, $options = array())
    {
        if ($deleted) {
            $event = new Event('Model.TaxonomySoftware.deleted', $this, $options);
            $this->eventManager()->dispatch($event);
            return true;
        }
        return false;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['taxonomy_id'], 'Taxonomys'));
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }


    /**
     * Get the list of software (sorted by software name)
     * with their associated users (sorted by user name)
     * from a taxonomy ID
     *
     *      return example:  [  <sofwareId1> => [  'software' => <softwareObj1> ],
     *                          <sofwareId2> => [  'software' => <softwareObj2> ],
     *                          <sofwareId3> => [
     *                                             'software' => <softwareObj3>,
     *                                             'users'    => [ <userId> => <userObj>, ... ]
     *                                          ],
     *                      ]
     *
     * @todo 1. refactor getAggregateSoftwareListWithUsersFromSqlResult() method
     * @todo 2. update documentation
     *
     * @param int $taxonomyId  taxonomy ID
     * @return array
     */
    public function getSoftwareListWithUsersFromTaxonomyId(int $taxonomyId)
    {
        $result =  $this->find('all', ['contain' => ['Softwares', 'Users']])
            ->where(['taxonomy_id' => $taxonomyId])
            ->order(['Softwares.softwarename' => 'ASC', 'Users.username' => 'ASC'])
            ->toList();
        return $this->getAggregateSoftwareListWithUsersFromSqlResult($result);
    }


    /**
     * Get a software with their associated users (sorted by user name)
     * from a taxonomy ID and a software ID
     *
     *      return example:  [  <sofwareId> => [
     *                                             'software' => <softwareObj>,
     *                                             'users'    => [ <userId> => <userObj>, ... ]
     *                                          ],
     *                      ]
     *
     * @todo 1. refactor getAggregateSoftwareListWithUsersFromSqlResult() method
     * @todo 2. update documentation
     *
     * @param int $taxonomyId  taxonomy ID
     * @param int $softwareId  software ID
     * @return array
     */
    public function getSoftwareTaxonomyWithUsers(int $taxonomyId, int $softwareId)
    {
        $result =  $this->find('all', ['contain' =>
                [
                        'Softwares',
                        'Users' => [
                            'UserTypes',
                            'fields' => ['id', 'username', 'logo_directory', 'photo',]
                        ],
                ]
            ])
            ->where([
                'taxonomy_id' => $taxonomyId,
                'software_id' => $softwareId,
                'user_id IS NOT'  => null,
            ])
            ->order(['Users.username' => 'ASC'])
            ->toList();
        return $this->getAggregateSoftwareListWithUsersFromSqlResult($result);
    }

    /**
     * Return aggregate software list with users
     * from an SQL query result
     *
     *      return example:  [  <sofwareId1> => [  'software' => <softwareObj1> ],
     *                          <sofwareId2> => [  'software' => <softwareObj2> ],
     *                          <sofwareId3> => [
     *                                             'software' => <softwareObj3>,
     *                                             'users'    => [ <userId> => <userObj>, ... ]
     *                                          ],
     *                      ]
     *
     * @todo 1. refactor
     * @todo 2. update documentation
     *
     * @param array $result
     * @return array
     */
    final private function getAggregateSoftwareListWithUsersFromSqlResult(array $result)
    {
        $data = [];
        foreach ($result as $taxonomysSoftware) {
            $comment = $taxonomysSoftware->comment;
            $softwareId = $taxonomysSoftware->software_id;
            $userId = $taxonomysSoftware->user_id;
            if (!isset($data[$softwareId])) {
                $data[$softwareId]['software'] = $taxonomysSoftware->software;
                $data[$softwareId]['lastUpdate'] = $taxonomysSoftware->modified;
            } else {
                if ($data[$softwareId]['lastUpdate'] < $taxonomysSoftware->modified) {
                    $data[$softwareId]['lastUpdate'] = $taxonomysSoftware->modified;
                }
            }
            if (!is_null($userId)) {
                $data[$softwareId]['users'][$userId] = $taxonomysSoftware->user;
                $data[$softwareId]['userUpdateDates'][$userId] = $taxonomysSoftware->modified;
                if (!is_null($comment)) {
                    $data[$softwareId]['comments'][$userId] = $comment;
                }
            }
        }
        // Compute some values and sort dates
        foreach ($data as $softwareId => $softwareData) {
            $data[$softwareId]['numberOfUsers'] = 0;
            if (isset($softwareData['users'])) {
                $data[$softwareId]['numberOfUsers'] = count($softwareData['users']);
            }
            $data[$softwareId]['numberOfComments'] = 0;
            if (isset($softwareData['comments'])) {
                $data[$softwareId]['numberOfComments'] = count($softwareData['comments']);
            }
            // Sort update dates
            if (isset($softwareData['userUpdateDates'])) {
                arsort($softwareData['userUpdateDates']);  // Newest first
//              asort($data[$softwareId]['updateByUser']); // The older ones first
                $data[$softwareId]['userUpdateDates'] = $softwareData['userUpdateDates'];
            }
        }
        return $data;
    }


    /**
     * Returns a records list (with user ID NULL)
     * grouped by Taxon for given software ID
     *
     * @param int $softwareId
     * @return array  example: [ <taxonId> => [taxonomySoftware, ...], ]
     */
    public function getPreSelectionBySofwareId(int $softwareId)
    {
        $existingEntries = $this->find()->where(['software_id' => $softwareId, 'user_id IS NULL'])->toArray();
        return $this->getRecordsGroupByTaxonId($existingEntries);
    }


    /**
     * Returns a records list (with user ID not NULL)
     * grouped by Taxon for given software ID
     *
     * @param int $softwareId
     * @return array  example: [ <taxonId> => [taxonomySoftware, ...], ]
     */
    public function getListBySofwareId(int $softwareId)
    {
        $existingEntries = $this->find()->where(['software_id' => $softwareId, 'user_id IS NOT NULL'])->toArray();
        return $this->getRecordsGroupByTaxonId($existingEntries);
    }

    /**
     * Returns a records list grouped by Taxon
     * for given user ID
     *
     * @param int $userId
     * @return array  example: [ <taxonId> => [taxonomySoftware, ...], ]
     */
    public function getListByUserId(int $userId)
    {
        $existingEntries = $this->find()->where(['user_id' => $userId,])->toArray();
        return $this->getRecordsGroupByTaxonId($existingEntries);
    }

    /**
     * Returns a records list grouped by Taxon
     * for given user ID and given software ID
     *
     * @param int $userId
     * @param int $softwareId
     * @return array  example: [ <taxonId> => [taxonomySoftware, ...], ]
     */
    public function getListByUserIdBySofwareId(int $userId, int $softwareId)
    {
        $existingEntries = $this->find()->where(['software_id' => $softwareId, 'user_id' => $userId])->toList();
        return $this->getRecordsGroupByTaxonId($existingEntries);
    }

    /**
     * Returns a records list grouped by Taxon
     * from records list
     *
     * @param $records  example: [ taxonomySoftware, ...]
     * @return array    example: [ <taxonId> => [taxonomySoftware, ...], ]
     */
    final private function getRecordsGroupByTaxonId($records)
    {
        $recordsGroupByTaxonId = [];
        if (is_array($records) && count($records) > 0) {
            foreach ($records as $record) {
                $recordsGroupByTaxonId[$record->taxonomy_id][] = $record;
            }
        }
        return $recordsGroupByTaxonId;
    }

    /**
     * Delete a records list
     *
     * @param array $recordsList  example: [ <taxonId> => [taxonomySoftware, ...], ]
     * @return bool  returns false if one of records could not be deleted.
     */
    public function deleteRecords(array $recordsList)
    {
        $processing = true;
        foreach ($recordsList as $recordsListForOneTaxon) {
            foreach ($recordsListForOneTaxon as $obj) {
                if (false === $this->delete($obj)) {
                    $processing = false;
                }
            }
        }
        return $processing;
    }
}
