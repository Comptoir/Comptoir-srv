<?php
namespace App\Model\Table;

use App\Model\Entity\Taxonomy;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Taxonomys Model
 *
 * @property BelongsTo $ParentTaxonomys
 * @property HasMany $TaxonomysSoftwares
 * @property HasMany $ChildTaxonomys
 *
 * @method Taxonomy get($primaryKey, $options = [])
 * @method Taxonomy newEntity($data = null, array $options = [])
 * @method Taxonomy[] newEntities(array $data, array $options = [])
 * @method Taxonomy|bool save(EntityInterface $entity, $options = [])
 * @method Taxonomy patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Taxonomy[] patchEntities($entities, array $data, array $options = [])
 * @method Taxonomy findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TaxonomysTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('taxonomys');
    //  $this->displayField('id');            // Display only taxon ID in form (add/edit)
        $this->displayField('title_i18n_en'); // Display taxon text in form (add/edit) instead of only ID
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ParentTaxonomys', [
            'className' => 'Taxonomys',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('TaxonomysSoftwares', [
            'foreignKey' => 'taxonomy_id'
        ]);
        $this->hasMany('ChildTaxonomys', [
            'className' => 'Taxonomys',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsToMany(
            'Softwares',
            [
                'foreignKey' => 'taxonomy_id',
                'targetForeignKey' => 'software_id',
                'joinTable' => 'TaxonomysSoftwares'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title_i18n_en', 'create')
            ->notEmpty('title_i18n_en');

        $validator
            ->requirePresence('title_i18n_fr', 'create')
            ->notEmpty('title_i18n_fr');

        $validator
            ->allowEmpty('description_i18n_en');

        $validator
            ->allowEmpty('description_i18n_fr');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentTaxonomys'));

        return $rules;
    }
}
