<?php

namespace App\Model\Table;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RelationshipsUsers Model
 *
 * @property BelongsTo $Users
 * @property BelongsTo $Relationships
 */
class RelationshipsUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('relationships_users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER'
            ]
        );
        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'recipient_id',
                'joinType' => 'INNER'
            ]
        );
        $this->belongsTo(
            'Relationships',
            [
                'foreignKey' => 'relationship_id',
                'joinType' => 'INNER'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['recipient_id'], 'Users'));
        $rules->add($rules->existsIn(['relationship_id'], 'Relationships'));
        $rules->add(
            $rules->isUnique(
                ['user_id', 'relationship_id'],
                __d("Forms", 'You can not be declare twice for the same relationships.')
            )
        );
        return $rules;
    }
}
