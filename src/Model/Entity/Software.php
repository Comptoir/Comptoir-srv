<?php
namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\I18n\Time;
use Cake\ORM\Entity;
use Cake\Utility\Text;

/**
 * Software Entity.
 *
 * @property int $id
 * @property string $softwarename
 * @property string $url_repository
 * @property string $description
 * @property int $licence_id
 * @property License $license
 * @property Time $created
 * @property Time $modified
 * @property string $logo_directory
 * @property $photo
 * @property int $sill
 * @property int $cnll
 * @property string $wikidata
 * @property string $framalibre
 * @property string $wikipedia_en
 * @property string $wikipedia_fr
 * @property Review[] $reviews
 * @property Screenshot[] $screenshots
 * @property Relationship[] $relationships
 */
class Software extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'tag_string' => true,
    ];

    protected $_hidden = [
        '_joinData',
        'web_site_url'
    ];

    protected $_virtual = ['average_review','tag_string'];

    protected function _getTagString()
    {
        if (isset($this->_properties['tag_string'])) {
            return $this->_properties['tag_string'];
        }
        if (empty($this->tags)) {
            return '';
        }
        $tags = new Collection($this->tags);
        $str = $tags->reduce(
            function ($string, $tag) {
                return $string . $tag->name . ' ';
            },
            ''
        );

        return trim($str, ' ');
    }

    protected function _getAverageReview()
    {
        $total = 0 ;
        $average = 0;
        if (isset($this->_properties['reviews']) && !empty($this->_properties['reviews'])) {
            foreach ($this->_properties['reviews'] as $review) {
                $total += $review->evaluation;
            };
            $average = round($total / count($this->_properties['reviews']));
        }

        return $average ;
    }

    /**
     * Get software slug for using it in URL
     * @return string
     */
    protected function _getSlugName()
    {
        $name = $this->_properties['softwarename'];
        $name = str_replace('@', 'a', $name);
        return strtolower(Text::slug($name));
    }
}
