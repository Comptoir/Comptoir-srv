<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\ORM\Entity;

/**
 * Taxonomy Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title_i18n_en
 * @property string $title_i18n_fr
 * @property string $description_i18n_en
 * @property string $description_i18n_fr
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\ParentTaxonomy $parent_taxonomy
 * @property \App\Model\Entity\TaxonomysSoftware[] $taxonomys_softwares
 * @property \App\Model\Entity\ChildTaxonomy[] $child_taxonomys
 */
class Taxonomy extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
