<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LicnseType Entity.
 *
 * @property int $LICENSE_TYPE_id
 * @property \App\Model\Entity\LICENSETYPE $l_i_c_e_n_s_e_t_y_p_e
 * @property string $LICENSE_TYPE_value
 */
class LicenceType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'LICENSE_TYPE_id' => false,
    ];
}
