<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SoftwaresStatistic Entity.
 *
 * @property int $id
 * @property int $software_id
 * @property \App\Model\Entity\Software $software
 * @property int $last_commit_age
 * @property int $project_age
 * @property int $delta_commit_one_month
 * @property int $delta_commit_one_year
 * @property int $nb_contributors
 * @property float $contributors_code_percent
 * @property int $declared_users
 * @property float $average_review_score
 * @property int $screenshots
 * @property int $codegouv_label
 * @property float $score
 */
class SoftwaresStatistic extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
