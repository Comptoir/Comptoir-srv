<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\ORM\Entity;

/**
 * RelationshipsSoftwaresUser Entity.
 *
 * @property int $id
 * @property int $software_id
 * @property Software $software
 * @property int $user_id
 * @property User $user
 * @property int $relationship_id
 * @property Relationship $relationship
 * @property Time $created
 * @property Time $modified
 */
class RelationshipsSoftwaresUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
